appH5.controller('MessageCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {

    function PostJson(url, data) { var deferred = $q.defer(); $http.post(url, data).success(function (d) { deferred.resolve(d); }); return deferred.promise; }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });

    $scope.message = "Hello";
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "TeacherPrivateMsg.htm";
});

appH5.controller('allMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
    .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            }
            else {
                return;
            }

    }).then(function ($rs) {
        $scope.profile_data = $rs;
        return TmpValue.post(APIForder + "?service=Message.readMessageList", data);
    }).then(function ($rs) {
        console.log("list:");
        alert(1234);
        console.log($rs);
        $scope.messageList = $rs.data.message_list;
    });

    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "allMsg.htm";

});

appH5.controller('newMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) { var deferred = $q.defer(); $http.post(url, data).success(function (d) { deferred.resolve(d); }); return deferred.promise; }

    formdata = {};
    formdata.teach_id = $routeParams.group_id;
    data = formdata;

    $scope.message = {};

    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            }
            else {
                return;
            }

        }).then(function ($rs) {
        $scope.profile_data = $rs;
        return TmpValue.post(APIForder + "?service=Message.readMessageByGroupId&group_id="+$routeParams.group_id, data);
    }).then(function ($rs) {
        $scope.messageList = $rs.data.messageList;
        $scope.members = $rs.data.members;
        $scope.nowID = $rs.data.nowID;
        $scope.nowGroup = $rs.data.nowGroup;


    });

    $scope.pushMessageList = function(data){
        console.log(data);
        $scope.messageList.push(data);
    }



    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "newMsg.htm";





});

appH5.controller('msgProcController', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) { var deferred = $q.defer(); $http.post(url, data).success(function (d) { deferred.resolve(d); }); return deferred.promise; }

    $scope.messageList = [];
    $scope.privateMessageList = [];
    $scope.privateMessageName = "";

    var data = {};

    $scope.getMessageListData = function(){
        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                console.log($scope.seedata);
                if ($rs.data[0].f_backend != null) {
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                }
                else {
                    return;
                }

            }).then(function ($rs) {
            $scope.profile_data = $rs;
            return TmpValue.post(APIForder + "?service=Message.readMessageList", data);
        }).then(function ($rs) {
            console.log('list:');
            console.log($rs);
            $scope.messageList = $rs.data.message_list;
            console.log($scope.messageList);
        });
    }

    $scope.getMessageListData();

    $scope.openPrivateMsgBox = function(group_data){
        $scope.getMessageListData();
        $scope.showPrivateBox();
        $scope.showPrivateMsg(group_data);
    };




    $scope.showPrivateMsg = function (group_data) {
        //alert(1);
        console.log('1234');
        console.log(group_data);
        $(".name_box").removeClass("nowSchoolMsg");
        //$(".private_msg_pup").removeClass("nowSchoolMsgShow");
        if(typeof group_data.group_id != 'undefined'){
            $(".msg_head_"+group_data.group_id).addClass("nowSchoolMsg");
        }else{
            $(".msg_head_"+group_data.id).addClass("nowSchoolMsg");
        }

        formdata = {};
        if(typeof group_data.group_id != 'undefined'){
            formdata.teach_id = group_data.group_id;
        }else{
            formdata.teach_id = group_data.id;
        }

        data = formdata;
        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                if ($rs.data[0].f_backend != null) {
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                }
                else {
                    return;
                }

            }).then(function ($rs) {
            $scope.profile_data = $rs;
            return TmpValue.post(APIForder + "?service=Message.readMessageByGroupId&group_id="+group_data.group_id, data);
        }).then(function ($rs) {
            console.log($rs);
            $scope.privateMessageList = $rs.data.messageList;
            if(typeof group_data.group_name != 'undefined'){
                $scope.privateMessageName = group_data.group_name;
            }else{
                $scope.privateMessageName = group_data.name;
            }
            if(typeof group_data.group_id != 'undefined'){
                changeRoom(group_data.group_id);
            }else{
                changeRoom(group_data.id);
            }
            $(".msg_box").animate({ scrollTop: $(".msg_box").height()+1000+'px' }, "slow");

        });
    };

    $scope.pushMessageList = function(data){
        console.log(data);
        $scope.privateMessageList.push(data);
        $(".msg_box").animate({ scrollTop: $(".msg_box").height()+1000+'px' }, "slow");
    }

    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0);
        $(".privateBar").toggleClass("shadowB");
        //if($('.nowSchoolMsg').length ==0)
        //$scope.showPrivateMsg($scope.messageList[0]);

    };
});



function sendMessage() {
    console.log(window.location.origin.split(":")[0]+":"+window.location.origin.split(":")[1]);
    if( ( $('#message_file').val() == null || $('#message_file').val() == "" ) && ( $('#messageText').val() == null || $('#messageText').val() == "") ){
         return;
    }

    var messageText = $('#messageText').val();

    if($('#message_file').val() != null && $('#message_file').val() != ""){
        $.ajaxFileUpload
        (
            {
                url: APIForder + "?service=Message.fileUpload",
                secureuri: false,
                fileElementId: 'message_file',
                dataType: 'json',
                success: function (data, status)
                {
                    console.log(data.data.file_id);
                    if(data.data.file_id > 0){
                        socket.emit('private message', messageText, data.data.file_id);
                    }else{
                        socket.emit('private message', messageText, null);
                    }
                },
                error: function (data, status, e)
                {
                    console.log(e);
                }
            }
        )
    }else{
        socket.emit('private message', messageText, null);
    }
    $('#messageText').val('');

}
//
// function sendMessage(){
//
//     if( ( $('#message_file').val() == null || $('#message_file').val() == "" ) && ( $('#messageText').val() == null || $('#messageText').val() == "") ){
//         return;
//     }
//
//     if($('#message_file').val() != null && $('#message_file').val() != ""){
//         var data = new FormData($('#message_file')[0]);
//         $.ajax({
//             url: APIForder + '?service=Message.fileUpload',
//             type: 'POST',
//             cache: false,
//             data: data,
//             processData: false,
//             contentType: false
//         }).done(function (res) {
//             //var msg = alert('Upload Success');
//             console.log(res);
//         }).fail(function (res) {
//             console.log(res);
//         });
//         //socket.emit('private message', $('#messageText').val(), null);
//     }
//
//
//
//
//
//
//
// }


appH5.controller('poMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    var data = {};
    function PostJson(url, data) { var deferred = $q.defer(); $http.post(url, data).success(function (d) { deferred.resolve(d); }); return deferred.promise; }
    //$scope.data = TmpValue;
    formdata = {};

    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });

    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "poMsg.htm";

});

