appH5.controller('BasicFromCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue,Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.data = {};
    $scope.data.text = $routeParams.text;

    $scope.BasicSearch = function (formdata) {
        if (formdata === undefined) {
            formdata = {};
        }

        if ($('#inputTarget').val() == '' || $('#inputTarget').val() == '找學校，找系所，找朋友，找社團') {
            Alertify.alert('請輸入文字');
            return
        }
        if ($('.text').val() !== undefined) {
            formdata.text = $('.text').val();
        }
        if ($('#text').val() !== undefined) {
            formdata.text = $('#text').val();
        }

        data = formdata;
        location.href = 'User.ViewList/' + formdata.text;
    };


    $scope.BasicSearch2 = function () {

        $("#inputTarget").unbind().keyup(function (e) {
            code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                $("#inputTarget").click()
                $(".seaechForSBtn").click()
            } else {
                if (code == 8) {
                    $("#inputTarget").click()
                }
            }
        });
    }


});

appH5.controller('BasicSetting', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }



    $scope.FuncBasicSetting = function () {
        //index
        // 幫 input 加上提示，用預設的 className(blur)
        $("input[title!='']").hint();
        // 幫 textarea 加上提示，並用自訂的 className
        $("textarea[title!='']").hint("myBlur");
        //end index        

        //teacher edit
        $(".close-btn , .yesBtn").click(function () {
            $('body').css('overflow', 'auto')
            $(".pop-up").hide()
            $('.member-box').hide()
            $(".recommend-box").hide()
            $('.pop-up video').hide().attr('src', '')
            $('.mid').css('height', 'auto')
            $('.mid').css('overflow', 'auto')
        })

        $('.identity-box .btn').unbind().click(function () {
            $(this).parent().find('ul').slideToggle(150)
        })
        //end teacher edit

        //teachermyprofile
        $('.show_all_class').unbind().click(function () {
            $('.c_wrap').toggleClass('showAll')
            var $this = $(this)
            if ($this.text() == 'Show All') {
                $this.text('Show One')
            } else {
                $this.text('Show All')
            }
        })
        //end teachermyprofile

        //user viewlist
        $('.more-btn').click(function () {
            $(".all-teacher-box").css('height', 'auto')
        })

        $(".close-btn").click(function () {
            $('body').css('overflow', 'auto')
            $(".pop-up").hide()
            $('.member-box').hide()
            $(".recommend-box").hide()
            $('.pop-up video').hide().attr('src', '')
        })
        //end user viewlist

        //teacherText
        $('label img').click(function () {
            var $this = $(this)
            $this.parent().toggleClass('selectThis')
        })


        $('.friend label').each(function () {
            var $this = $(this),
                $this_for = $this.attr('for')
            $this.find('input').attr('id', $this_for)
        })

        $('.left .videos div').click(function () {
            $('body').css('overflow', 'hidden')
            var $this = $(this),
                $video = $this.find('img').attr('alt'),
                $src = 'assets/video/' + $video + '.mp4'
            $('.member-box').hide()
            $('.pop-up').show()
            $('.pop-up').css('position', 'fixed')
            $('.pop-up video').show().attr('src', $src)
            $('.loading').hide()
        })

        $('.msg-btns .btn-1').unbind().click(function () {
            $('.evaluate').slideToggle(50)
        })
        $('.msg-btns .btn-2').unbind().click(function () {
            $('.user-po').slideToggle(50)
        })
        $('.msg-btns .btn-3').unbind().click(function () {
            $('.share').slideToggle(50)
        })

        $(".booking .time-box ul li").click(function () {
            var $this = $(this)
            $(".booking .time-box ul li").removeClass('thisTime')
            $this.addClass('thisTime')
        })

        $('.class-books div').click(function () {
            var $this = $(this)
            $('.class-books div').removeClass('thisBook')
            $this.addClass('thisBook')

        })
        $(".recommend").click(function () {
            $('body').css('overflow', 'hidden')
            $('.member-box').hide()
            $(".pop-up").show()
            $(".recommend-box").show()
        })
        $('.booking .booking-btn').click(function () {
            if ($('.day-box input') != '' && $('.ng-scope li').hasClass('thisTime')) {
                //$('#CourseBookingCtrl').hide()
                $("html,body").animate({
                    scrollTop: 0
                }, 0);
                //$('.loading').show()
            }
        })
        $('.share-at').click(function () {
            $('.share-pop-up').show(150)
            $('body').css('overflow', 'hidden')
        })
        $('.share-pop-up .close , .share-btns .send').click(function () {
            $('.share-pop-up').hide()
            $('body').css('overflow', 'auto')
        })
        //end TeacherText

        //fanMage
        /*
        var hammertime = new Hammer(document.getElementById("upload"));
        hammertime.get('press').set({
            time: 1,
            pointers: 1,
            threshold: 10
        });

        hammertime.on("press", function () {
            var _top = parseInt($('.cutBannerImgBox img').css("top")),
                _imgH = parseInt($('.cutBannerImgBox img').height()),
                _bannerH = parseInt($('.cutBannerImgBox').height()),
                _maxMove = _imgH - _bannerH

            var obj = $('.cutBannerImgBox img')
            var _nowY = parseInt($("#target_bg_image").css("top"))
            hammertime.on("pan", function (e) {
                var _y = e.deltaY,
                    _newY = _y + _nowY

                if (_top <= _maxMove * -1) {
                    obj.css({
                        'top': (_maxMove * -1) + 2
                    });
                } else {
                    $("#target_bg_image").css("top", _newY)
                    if (_top > 0) {
                        obj.css({
                            'top': 0
                        });
                    } else {
                        $("#target_bg_image").css("top", _newY)

                    }
                }
                var imgTop = $('.cutBannerImgBox img').css("top");
                $(".saveCutBanner").attr("value", imgTop)
            });

            if (_top <= _maxMove * -1) {
                obj.css({
                    'top': (_maxMove * -1) + 2
                });
            } else {

                if (_top > 0) {
                    obj.css({
                        'top': 0
                    });
                }
            }

        });
        */

        //fanMangeMember
        $(".member").click(function () {
            $('body').css('overflow', 'hidden')
            $(".pop-up").show()
            $('.login-box').show()
        })

        $(".close-btn , .yesBtn").click(function () {
            $('body').css('overflow', 'auto')
            $(".pop-up").hide()
            $('.member-box').hide()
            $(".recommend-box").hide()
            $('.pop-up video').hide().attr('src', '')
            $('.mid').css('height', 'auto')
            $('.mid').css('overflow', 'auto')
            $('.booking').hide()
            $(".calendar").hide()
        })



        $('.left .videos div div .image-box').click(function () {
            $('body').css('overflow', 'hidden')
            var $this = $(this),
                $video = $this.find('img').attr('alt'),
                $src = 'upload/teach/' + $video
            $('.member-box').hide()
            $('.pop-up').show()
            $('.pop-up video').show().attr('src', $src)
        })


        $('.show_all_class').unbind().click(function () {
            $('.c_wrap').toggleClass('showAll')
            var $this = $(this)
            if ($this.text() == 'Show All') {
                $this.text('Show One')
            } else {
                $this.text('Show All')
            }
        })

        // 幫 input 加上提示，用預設的 className(blur)
        $("input[title!='']").hint();
        $("input[ng-attr-title!='']").hint();
        // 幫 textarea 加上提示，並用自訂的 className
        $("textarea[title!='']").hint("myBlur");
        //end fanMangeMember

        //fanText
        $(".close-btn , .yesBtn").click(function () {
            $('body').css('overflow', 'auto')
            $(".pop-up").hide()
            $('.member-box').hide()
            $(".recommend-box").hide()
            $('.pop-up video').hide().attr('src', '')
            $('.mid').css('height', 'auto')
            $('.mid').css('overflow', 'auto')
            $('.booking').hide()
        })



        $('.left .videos div div .image-box').click(function () {
            $('body').css('overflow', 'hidden')
            var $this = $(this),
                $video = $this.find('img').attr('alt'),
                $src = 'upload/fan/' + $video
            $('.member-box').hide()
            $('.pop-up').show()
            $('.pop-up video').show().attr('src', $src)
        })


        $('.show_all_class').unbind().click(function () {
            $('.c_wrap').toggleClass('showAll')
            var $this = $(this)
            if ($this.text() == 'Show All') {
                $this.text('Show One')
            } else {
                $this.text('Show All')
            }
        })
        //end FanText

        //myfanlist
        $(".member").click(function () {
            $('body').css('overflow', 'hidden')
            $(".pop-up").show()
            $('.login-box').show()
        })

        $(".close-btn , .yesBtn").click(function () {
            $('body').css('overflow', 'auto')
            $(".pop-up").hide()
            $('.member-box').hide()
            $(".recommend-box").hide()
            $('.pop-up video').hide().attr('src', '')
            $('.mid').css('height', 'auto')
            $('.mid').css('overflow', 'auto')
            $('.booking').hide()
        })



        $('.left .videos div div .image-box').click(function () {
            $('body').css('overflow', 'hidden')
            var $this = $(this),
                $video = $this.find('img').attr('alt'),
                $src = 'upload/teach/' + $video
            $('.member-box').hide()
            $('.pop-up').show()
            $('.pop-up video').show().attr('src', $src)
        })


        $('.show_all_class').unbind().click(function () {
            $('.c_wrap').toggleClass('showAll')
            var $this = $(this)
            if ($this.text() == 'Show All') {
                $this.text('Show One')
            } else {
                $this.text('Show All')
            }
        })
        //end myfanlist



    };



    $scope.$watch(function () { return TmpObj.BasicSetting; }, function (newVal) {
        if (newVal != null && newVal != undefined) {
            $scope.FuncBasicSetting();
        }
        TmpObj.BasicSetting = null;
    });


});

appH5.controller('BasicLive', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, $sce) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.$watch(function () { return TmpObj.tmp; }, function (newVal) {
        if (newVal != null && newVal != undefined && newVal != false) {
            //呼叫聊天室 對應 live id
            console.log(newVal);
            $scope.tmp_rs = newVal;
            $scope.tmp_rs.embed_url = $scope.tmp_rs.embed_url + '?autoplay=1&muted=1';
            //$scope.tmp_rs.embed_url = 'https://app.straas.net/showhi.club/lives/ahbjquh9' + '?autoplay=1&muted=1';
            //angular.element(document.getElementById('CommBoard')).scope().StartChatRoom(TmpObj.tmp.live_id);
        }
        //TmpObj.tmp = null;
    });


});


appH5.controller('BasicSettingStart', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    setTimeout(function () {
        TmpObj.BasicSetting = 'on';
    }, 1);

    angular.element(document).on('click', StartEndDate);
    angular.element(document).on('click', Funchammertime);
});


appH5.controller('CommLive', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

});

StartEndDate = function () {
    //起始時間與結束時間
    $("#start_date").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        minDate: 'today',
        maxDate: "+2m"
    });
    $("#end_date").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        minDate: 'today',
        maxDate: "+2m"
    });

    //起始時間與結束時間

    if ($(window).width() < 1024) {
        if ($(".right .banner").css("background-image") == "url(http://localhost/showhi/phalApi_html/upload/fan/bammer.jpg)") {
            $(".right .banner").css("background-position-y", "-5vw")
        }
    }
}



Funchammertime = function () {

    var tmp_hammertime = $('#upload');

    var hammertime = new Hammer(document.getElementById("upload"));

    hammertime.get('press').set({
        time: 1,
        pointers: 1,
        threshold: 10
    });



    hammertime.on("press", function () {
        var _top = parseInt($('.cutBannerImgBox img').css("top")),
            _imgH = parseInt($('.cutBannerImgBox img').height()),
            _bannerH = parseInt($('.cutBannerImgBox').height()),
            _topBarH = parseInt($(".topBar").height()),
            _maxMove = _imgH - _bannerH + _topBarH

        var obj = $('.cutBannerImgBox img')
        var _nowY = parseInt($("#target_bg_image").css("top"))
        hammertime.on("pan", function (e) {
            var _y = e.deltaY,
                _newY = _y + _nowY

            if (_top <= _maxMove * -1) {
                obj.css({
                    'top': (_maxMove * -1) + 2
                });
            } else {
                $("#target_bg_image").css("top", _newY)
                if (_top > _topBarH) {
                    obj.css({
                        'top': _topBarH
                    });
                } else {
                    $("#target_bg_image").css("top", _newY)

                }
            }
            var imgTop = parseInt($('.cutBannerImgBox img').css("top")),
                imgH = $('.cutBannerImgBox img').height(),
                _topBarH = parseInt($(".topBar").height()),
                bannerH = $('.cutBannerImgBox').height() - _topBarH * 2,
                onePer = (imgH - bannerH) / 100,
                imgTopPer = ((_topBarH - imgTop) / onePer)

            $(".saveCutBanner").attr("value", imgTopPer)
        });

        if (_top <= _maxMove * -1) {
            obj.css({
                'top': (_maxMove * -1) + 2
            });
            $(".saveCutBanner").attr("value", "100")
        } else {

            if (_top > _topBarH) {
                obj.css({
                    'top': _topBarH
                });
                $(".saveCutBanner").attr("value", "0")
            }
        }

    });


    // calendar
    /*
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if($(this).find("div").hasClass("event")){
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            }else{
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }
            
        })
        $(".eventBox .subTitle").click(function(){
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }

    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }
    */
    // calendar END


}