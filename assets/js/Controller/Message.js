appH5.controller('MessageCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });

    $scope.message = "Hello";
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "TeacherPrivateMsg.htm";
});

appH5.controller('allMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            } else {
                return;
            }

        }).then(function ($rs) {
            $scope.profile_data = $rs;
            return TmpValue.post(APIForder + "?service=Message.readMessageList", data);
        }).then(function ($rs) {
            console.log($rs);
            $scope.Noreadtotal = $rs.data.Noreadtotal;
            $scope.messageList = $rs.data.message_list;
        });

    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "allMsg.htm";

});

appH5.controller('newMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    formdata = {};
    formdata.teach_id = $routeParams.group_id;
    data = formdata;

    $scope.message = {};

    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            } else {
                return;
            }

        }).then(function ($rs) {
            $scope.profile_data = $rs;
            return TmpValue.post(APIForder + "?service=Message.readMessageByGroupId&group_id=" + $routeParams.group_id, data);
        }).then(function ($rs) {
            $scope.messageList = $rs.data.messageList;
            $scope.members = $rs.data.members;
            $scope.nowID = $rs.data.nowID;
            $scope.nowGroup = $rs.data.nowGroup;


        });

    $scope.pushMessageList = function (data) {
        console.log(data);
        $scope.messageList.push(data);
    }



    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "newMsg.htm";





});

appH5.controller('msgProcController', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.messageList = [];
    $scope.privateMessageList = [];
    $scope.privateMessageName = "";

    var data = {};

    $scope.getMessageListData = function () {
        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                if ($rs.data[0] && $rs.data[0].f_backend != null) {
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                } else {
                    return;
                }

            }).then(function ($rs) {
                $scope.profile_data = $rs;
                return TmpValue.post(APIForder + "?service=Message.readMessageList", data);
            }).then(function ($rs) {
                $scope.Noreadtotal = $rs.data.Noreadtotal;
                $scope.messageList = $rs.data.message_list;
                // console.log($scope.messageList);
            });
    }

    $scope.getMessageListData();

    $scope.openPrivateMsgBox = function (group_data) {
        $scope.getMessageListData();
        $scope.showPrivateBox(group_data);
        $scope.showPrivateMsg(group_data, 0);
    };

    $scope.isFile = function () {

        $(".say .pic_box a").unbind().click(function () {
            var $this = $(this)
            var _link = "http://showhi.co/showhi/phalApi_html/" + $this.attr("href"),
                _title = $this.text(),
                _file = "<div class='fileDiv'>" + "<div class='fileTitle'>" + _title + "</div>" + "<a target='_blank' href='" + _link + "'>" + _link + "</a>" + "</div>"
            $(".msgPopUp").show()
            $(".msgPopUp .msgpopupbox").prepend(_file)
        })

        $(".msgPopUp .close-btn").click(function () {
            $(".msgPopUp").hide()
            $(".msgPopUp .msgpopupbox").html("")
        })
    }

    $scope.sendMsgBtnClick = function () {
        $("#message_file").val("")
        $(".fileBtn").removeClass("hasD")
        $(".fileBtn .fa").eq(0).animate({
            "opacity": "1"
        }, 250).animate({
            "left": "30%"
        }, 350)
        $(".fileBtn .fa").eq(1).animate({
            "opacity": "0"
        }, 250)
    }

    $scope.fileChange = function () {
        $(document).delegate('#message_file:file', 'change', function () {
            $(".fileBtn").addClass("hasD")
            $(".fileBtn .fa").eq(0).animate({
                "left": "10px"
            }, 350).animate({
                "opacity": "0"
            }, 250)
            $(".fileBtn .fa").eq(1).animate({
                "left": "10px"
            }, 350).animate({
                "opacity": "1"
            }, 250)
        });
    }

    $scope.isVideo = function () {
        $(".say .pic_box video").unbind().click(function () {
            var _src = $(this).attr("src"),
                _video = "<video controls><source src='" + _src + "' type='video/mp4'></video>"
            $(".msgPopUp").show()
            $(".msgPopUp .msgpopupbox").prepend(_video)
        })
        $(".msgPopUp .close-btn").click(function () {
            $(".msgPopUp").hide()
            $(".msgPopUp .msgpopupbox").html("")
        })

    }
    $scope.isImg = function () {
        $(".say .pic_box img").unbind().click(function () {
            var _src = $(this).attr("src"),
                _img = "<img src='" + _src + "'>";
            $(".msgPopUp").show()
            $(".msgPopUp .msgpopupbox").prepend(_img)
        })
        $(".msgPopUp .close-btn").click(function () {
            $(".msgPopUp").hide()
            $(".msgPopUp .msgpopupbox").html("")
        })
    }

    $scope.showPrivateMsg = function (group_data, key) {
        if (typeof $scope.messageList[key]['room_Noread_number'] == 'undefined')
            $scope.messageList[key]['room_Noread_number'] = 0;
        $scope.Noreadtotal = $scope.Noreadtotal - $scope.messageList[key]['room_Noread_number'];
        $scope.messageList[key]['room_Noread_number'] = 0;
        console.log(group_data);
        $(".name_box").removeClass("nowSchoolMsg");
        //$(".private_msg_pup").removeClass("nowSchoolMsgShow");
        if (typeof group_data.group_id != 'undefined') {
            $(".msg_head_" + group_data.group_id).addClass("nowSchoolMsg");
        } else {
            $(".msg_head_" + group_data.id).addClass("nowSchoolMsg");
        }

        $(".talkName").find(".msgTitle").show();
        $(".talkName").find(".changeTitle").show();
        $(".talkName").find(".cTitle").hide();
        $(".pulsMembr").hide();
        $(".msg_box").show();
        $(".keyInPlusMemberBox").html("");
        $(".friendPB").removeClass("plusThis");
        $(".removeMembr").hide();

        function playHome()　 {　　
            document.all.sound.src = "bleep.wav";
        }
        formdata = {};
        if (typeof group_data.group_id != 'undefined') {
            formdata.teach_id = group_data.group_id;
        } else {
            formdata.teach_id = group_data.id;
        }

        data = formdata;
        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                if ($rs.data[0].f_backend != null) {
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                } else {
                    return;
                }

            }).then(function ($rs) {
                $scope.profile_data = $rs;
                return TmpValue.post(APIForder + "?service=Message.readMessageByGroupId&group_id=" + formdata.teach_id, data);
            }).then(function ($rs) {

                console.log(formdata.teach_id);
                console.log('~~~');
                console.log($rs);
                //$(".private_msg_pup").removeClass("nowSchoolMsgShow");
                if (typeof group_data.group_id != 'undefined') {
                    $(".msg_head_" + group_data.group_id).addClass("nowSchoolMsg");
                } else {
                    $(".msg_head_" + group_data.id).addClass("nowSchoolMsg");
                }


                $scope.privateMessageList = $rs.data.messageList;
                console.log($scope.privateMessageList);
                if (typeof group_data.group_name != 'undefined') {
                    $scope.privateMessageName = group_data.group_name;
                } else {
                    $scope.privateMessageName = group_data.name;
                }
                if (typeof group_data.group_id != 'undefined') {
                    changeRoom(group_data.group_id);
                } else {
                    changeRoom(group_data.id);
                }
                $(".msg_box").animate({
                    scrollTop: $(".msg_box").height() + 999999 + 'px'
                }, "slow");

            });

        var _imgLenght = $(".name_box:eq(" + this.$index + ")").find(".msgMemberGrounp").length,
            _groupBoxW = $(".msgGroup").width(),
            _ImgBoxW = _groupBoxW * _imgLenght
        $(".msgMemberGrounp").width($(".msgGroup").width())
        $(".name_box:eq(" + this.$index + ")").find(".how").width(_ImgBoxW)

        if (_ImgBoxW > _groupBoxW) {
            $(".name_box:eq(" + this.$index + ")").addClass("moreGUser")
        } else {
            $(".name_box:eq(" + this.$index + ")").removeClass("moreGUser")
        }

    };

    $scope.pnBtnForMsg = function ($event) {

        var _this = $($event.target)
        if (_this.hasClass("nextBtnForMsg")) {
            var _left = parseInt(_this.parents(".msgGroup").find(".how").css("margin-left")),
                _mgb = parseInt($(".msgGroup").width()) * -1,
                _maxL = (_this.parents(".msgGroup").find(".how").width() - $(".msgGroup").width()) * -1
            if (_left > _maxL) {
                var _this = $($event.target)
                _this.parents(".msgGroup").find(".nowMG").next().addClass("nowMG")
                _this.parents(".msgGroup").find(".nowMG").prev().removeClass("nowMG")
                var _N = _this.parents(".msgGroup").find(".nowMG").index(),
                    _newML = 100*_N*-1+"%"
                
                _this.parents(".msgGroup").find(".how").animate({
                    "margin-left": _newML
                }, 250);
            }

        } else {
            var _left = parseInt(_this.parents(".msgGroup").find(".how").css("margin-left"))
            if (_left < 0) {
                var _this = $($event.target)
                _this.parents(".msgGroup").find(".nowMG").prev().addClass("nowMG")
                _this.parents(".msgGroup").find(".nowMG").next().removeClass("nowMG")
                var _N = _this.parents(".msgGroup").find(".nowMG").index(),
                    _newMR = 100*_N*-1+"%"
                
                _this.parents(".msgGroup").find(".how").animate({
                    "margin-left": _newMR
                }, 250);
            }

        }

    };

    $scope.changePlusMember = function () {
        $(".pulsMembr").show();
        $(".msg_box").hide();
        $(".removeMembr").hide();
    }


    $scope.keyinPlus = function ($event) {
        var _this = $($event.target),
            _plusName = _this.parents(".keyinPlus").find("input").val(),
            _UserImgSrc = "../../upload/teach/tp1.jpg",
            _showPlusMemberImg = '<div class="kimib"><i class="fa fa-times" aria-hidden="true"></i><img src="' + _UserImgSrc + '"></div>'
        _this.parents(".pulsMembr").find(".keyInPlusMemberBox").append(_showPlusMemberImg);
        _this.parents(".keyinPlus").find("input").val("");

        $(".kimib .fa").click(function () {
            $(this).parent().remove();
        });

    };
    
    $scope.plusThisF = function ($event) {
        var _this = $($event.target),
            _userImg = _this.parents(".searchPlusUser").find("img").attr("src"),
            _showPlusMemberImg2 = '<div class="kimib"><i class="fa fa-times" aria-hidden="true"></i><img src="' + _userImg + '"></div>'
        _this.parents(".pulsMembr").find(".keyInPlusMemberBox").append(_showPlusMemberImg2);
        $(".keyinPlusInput").val("")
        $(".searchPlusUser").html("").hide();
        
        $(".kimib .fa").click(function () {
            $(this).parent().remove();
        });
    }
    
    $scope.inputCon = function () {
        if($(".keyinPlusInput").val() != ""){
           $(".searchPlusUser").show()
           }else{
               $(".searchPlusUser").hide()
           }
    }

    $scope.colseKeyInPlus = function () {
        $(".pulsMembr").hide();
        $(".msg_box").show();
        $(".keyInPlusMemberBox").html("");
        $(".friendPB").removeClass("plusThis");
    }

    $scope.plusFriendInTalk = function ($event) {
        $($event.target).parents(".friendPB").toggleClass("plusThis");
    };

    $scope.changeTitle = function ($event) {
        $($event.target).parents(".talkName").find(".changeTitle").hide();
        $($event.target).parents(".talkName").find(".msgTitle").hide();
        $($event.target).parents(".talkName").find(".cTitle").show();
    };

    $scope.noChengMsgTitle = function ($event) {
        $($event.target).parents(".talkName").find(".msgTitle").show();
        $($event.target).parents(".talkName").find(".changeTitle").show();
        $($event.target).parents(".talkName").find(".cTitle").hide();
    }

    $scope.saveChengMsgTitle = function ($event) {
        $($event.target).parents(".talkName").find(".msgTitle").show();
        $($event.target).parents(".talkName").find(".changeTitle").show();
        $($event.target).parents(".talkName").find(".cTitle").hide();
    }

    $scope.yesPlus = function () {
        $(".pulsMembr").hide();
        $(".msg_box").show();
        $(".keyInPlusMemberBox").html("");
        $(".friendPB").removeClass("plusThis");
    }

    $scope.noPlus = function () {
        $(".pulsMembr").hide();
        $(".msg_box").show();
        $(".keyInPlusMemberBox").html("");
        $(".friendPB").removeClass("plusThis");
    }

    $scope.removeGrounpMember = function ($event) {
        $($event.target).parents(".friendPB").toggleClass("plusThis");
    };

    $scope.yesRemoveM = function () {

        if ($(".removeMembr .friendPB").hasClass("plusThis")) {

            Alertify.confirm('你确定要移除这些成员？')
                .then(function () {
                    $(".removeMembr").hide();
                    $(".msg_box").show();
                    $(".friendPB").removeClass("plusThis");
                }, function () {
                    return;
                });
        }
    }

    $scope.noRemoveM = function () {
        $(".removeMembr").hide();
        $(".msg_box").show();
        $(".friendPB").removeClass("plusThis");
    }

    $scope.changeRemoveMember = function () {
        $(".pulsMembr").hide();
        $(".removeMembr").show();
        $(".msg_box").hide();
    }

    $scope.pushMessageList = function (data) {
        console.log(data);
        $scope.privateMessageList.push(data);
        $(".msg_box").animate({
            scrollTop: $(".msg_box").height() + 999999 + 'px'
        }, "slow");
    }

    $scope.pushNoReadMessageList = function (data) {
        console.log('=======');
        console.log($scope.messageList);
        $scope.Noreadtotal = 0;
        for (var i = 0; i < $scope.messageList.length; i++) {
            if ($scope.messageList[i]['group_id'] == data['Noread_room']) {
                $scope.messageList[i]['last_message_time'] = data['last_message_time'];
                $scope.messageList[i]['room_Noread_number'] = data['room_Noread_number'];
                $scope.Noreadtotal = $scope.Noreadtotal + data['room_Noread_number'];
            }
        }
        //$scope.$apply();

    }

    // showPrivateMsg end

    $scope.showPrivateBox = function (group_data) {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0);
        $(".privateBar").toggleClass("shadowB");
        if ($('.nowSchoolMsg').length == 0 && typeof group_data == 'undefined')
            $scope.showPrivateMsg($scope.messageList[0], 0);
    };
});



function sendMessage() {
    console.log(window.location.origin.split(":")[0] + ":" + window.location.origin.split(":")[1]);
    if (($('#message_file').val() == null || $('#message_file').val() == "") && ($('#messageText').val() == null || $('#messageText').val() == "")) {
        return;
    }

    var messageText = $('#messageText').val();

    if ($('#message_file').val() != null && $('#message_file').val() != "") {
        $.ajaxFileUpload({
            url: APIForder + "?service=Message.fileUpload",
            secureuri: false,
            fileElementId: 'message_file',
            dataType: 'json',
            success: function (data, status) {
                console.log(data.data.file_id);
                if (data.data.file_id > 0) {
                    socket.emit('private message', messageText, data.data.file_id);
                } else {
                    socket.emit('private message', messageText, null);
                }
                $('#message_file').val('');
            },
            error: function (data, status, e) {
                console.log(e);
            }
        })
    } else {
        socket.emit('private message', messageText, null);
    }
    $('#messageText').val('');

}
//
// function sendMessage(){
//
//     if( ( $('#message_file').val() == null || $('#message_file').val() == "" ) && ( $('#messageText').val() == null || $('#messageText').val() == "") ){
//         return;
//     }
//
//     if($('#message_file').val() != null && $('#message_file').val() != ""){
//         var data = new FormData($('#message_file')[0]);
//         $.ajax({
//             url: APIForder + '?service=Message.fileUpload',
//             type: 'POST',
//             cache: false,
//             data: data,
//             processData: false,
//             contentType: false
//         }).done(function (res) {
//             //var msg = alert('Upload Success');
//             console.log(res);
//         }).fail(function (res) {
//             console.log(res);
//         });
//         //socket.emit('private message', $('#messageText').val(), null);
//     }
//
//
//
//
//
//
//
// }


appH5.controller('poMsgCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    var data = {};

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    //$scope.data = TmpValue;
    formdata = {};

    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });

    $scope.ContentForder = "templates/Message/";
    $scope.ContentHtml = "poMsg.htm";

});