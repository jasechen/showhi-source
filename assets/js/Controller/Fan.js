/*
appH5.factory('TmpValue', function ($http,$q) {
    var data = {
        post: function (url,_data) {
            var deferred = $q.defer();
            $http.post(url, _data,{'Content-Type':'application/x-www-form-urlencoded'}).success(function (d) {
                deferred.resolve(d);
            });
            return deferred.promise;
            //console.log("POST API!!!!!: " + _val)
            
        }
    };
    return data;
});
*/
appH5.controller('FanTextCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, $sce, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()

    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.fan_id = $routeParams.id;
    $scope.fan_id = $routeParams.id;
    data = formdata;
    //if()

    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });
    TmpValue.post(APIForder + "?service=Fan.FanText", data)
        .then(function ($rs) {
            if ($rs.data[0].msg_state == 'Y') {
                $scope.fan_tmp_list = $rs.data["0"].fan_tmp_list;
                $scope.fan_tmp_list_count = $rs.data["0"].fan_tmp_list_count;
                $scope.fan_boardmanage_list = $rs.data["0"].fan_boardmanage_list;
                $scope.fan_boardmanage_list_count = $rs.data["0"].fan_boardmanage_list_count;
                if ($scope.fan_tmp_list[0].approval != 'Y' && $scope.fan_boardmanage_list_count == 0) {
                    location.href = "Index";
                }


                $scope.fan_staff_list = $rs.data["0"].fan_staff_list;
                $scope.fan_man_list = $rs.data["0"].fan_man_list;
                $scope.fan_man_list_count = $rs.data["0"].fan_man_list_count;
                $scope.fan_subscript_list = $rs.data["0"].fan_subscript_list;
                $scope.fan_subscript_list_count = $rs.data["0"].fan_subscript_list_count;

                $scope.fan_Iin_list_count = $rs.data["0"].fan_Iin_list_count;
                $scope.fan_Iin_list = $rs.data["0"].fan_Iin_list;

                TmpObj.board_user_id = $scope.fan_tmp_list['0']['id'];
                TmpObj.friends_user_id = $scope.fan_tmp_list['0']['id'];

            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    //end main 


    //func fan

    $scope.AddFanMember = function (web_data) {
        if ($scope.seedata.data[0].f_backend == null) {
            Alertify.alert('Please login');
            retrun;
        }
        var data = {};
        data.fan_id = web_data;
        TmpValue.post(APIForder + "?service=Fan.AddFanMember", data)
            .then(function ($rs) {
                console.log($rs);
                $scope.fan_Iin_list_count = 1;
                //location.reload();
            });
    };
    //end func fan

    //click func
    $scope.openMsgToggle = function (data) {
        $(".msg_warp:eq(" + this.$index + ")").toggleClass('pack-up');
    }

    $scope.sendPrivateMessage = function () {

        data = {};
        data.users_id = $scope.teach_id;
        TmpValue.post(APIForder + "?service=Message.createGroup", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    if (typeof $rs.msg != 'undefined' && $rs.msg == '') {
                        window.location = WebForder + 'Message.newMsg/' + $scope.teach_id + '/' + $rs.data[0].datas.id
                    } else {
                        Alertify.alert("Please sign in");
                    }
                } else {
                    Alertify.alert("Please sign in");
                    //alert($rs.msg);
                }
            });
    };

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150)
    };

    //openCalendar

    $scope.openCalendar = function () {
        $(".pop-up , .calendar").show()
    };


    // calendar
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if ($(this).find("div").hasClass("event")) {
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            } else {
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }

        })
        $(".eventBox .subTitle").click(function () {
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }

    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }

    // calendar END

    //openCalendar END

    // 註冊 密碼
    $scope.pwdfun = function () {
        $(document).delegate('.pwdinput', 'change', function () {
            if ($(this).val().length > 0) {
                $(this).attr("type", "password")
            } else {
                $(this).attr("type", "")
            }
        });
    }


    //func
    $scope.BookingPage = function () {
        //bookingpage init
        //起始時間與結束時間
        $("#this_day").datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: '+1d',
            maxDate: "+2m"
        });
        $(".this_day").datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: '+1d',
            maxDate: "+2m"
        });
        //起始時間與結束時間


        $(".btn-b").click(function () {
            $('.pop-up').show();
            $('body').css('overflow', 'auto');
            $('.pop-up').css('position', 'absolute');
            $('.pop-up').css('overflow', 'auto');
            $('.booking').show();
            var _midH = $('.pop-up').innerHeight() - $('footer').innerHeight() - $('header').innerHeight() - 20;
            $('.mid').height(_midH);
            $('.mid').css('overflow', 'hidden');
            //$('.day-box input')=''
            $('.ng-scope li').removeClass('thisTime');
        });

        $(".booking .time-box ul li").click(function () {
            var $this = $(this);
            $(".booking .time-box ul li").removeClass('thisTime');
            $this.addClass('thisTime');
        });
        $('.class-books div').click(function () {
            var $this = $(this);
            $('.class-books div').removeClass('thisBook');
            $this.addClass('thisBook');

        });
        $(".recommend").click(function () {
            $('body').css('overflow', 'hidden');
            $('.member-box').hide();
            $(".pop-up").show()
            $(".recommend-box").show();
        });
        $('.booking .booking-btn').click(function () {
            if ($('.day-box input') != '' && $('.ng-scope li').hasClass('thisTime')) {
                //$('#CourseBookingCtrl').hide()
                $("html,body").animate({
                    scrollTop: 0
                }, 0);
                //$('.loading').show()
            }
        })

    };
    //end bookingpage init

    $scope.blackBgShow = function () {
        $('.blackBg').show();
        $('.po-msg-box .title .fa').show();
        $(".po-msg-box").css("z-index", "15")
    };
    // blackBgShow end

    $scope.popUpMenuShow = function () {
        var _eq = this.$index;
        $(".popupMenu").eq(_eq).addClass("showthis");
        $(".post_box").eq(_eq).addClass("zIndexUp");
        $('.blackBg').css('z-index', '13').show();
    };
    // popUpMenuShow end

    $scope.packUp = function () {
        $(".msg_box, .key_in_box").slideToggle(0);
    };
    // packUp end

    $scope.showThisImg = function () {
        $('.left .videos .video-box').click(function () {
            $('body').css('overflow', 'hidden');
            var $this = $(this),
                $src = $this.find('img').attr('src');

            $('.member-box').hide();
            $('.pop-up').show();
            $('.pop-up video').attr('src', $src);
            $('.pop-up img').show().attr('src', $src);
        })
    };
    // showThisImg end

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video , .pop-up .v_iBox img').attr('src', '').hide();
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
        $('.nowUpImage , .blackBg').hide();
    };
    // colsePopUp end

    //colseThisMsg
    $scope.colseThisMsg = function () {
            $(".upVidepFinish").fadeOut()
        }
        //colseThisMsg END

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    $scope.showPrivateMsg = function () {
        $(".name_box").click(function () {
            var index = $(this).index() - 1
            $(".name_box").removeClass("nowSchoolMsg")
            $(this).addClass("nowSchoolMsg")
            $(".private_msg_pup").removeClass("nowSchoolMsgShow")
            $(".private_msg_pup").eq(index).addClass("nowSchoolMsgShow")
        })
    };
    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };
    // showPrivateBox end

    // left position fixed
    $scope.leftBoxFixed = function () {

        var position = $('.leftMenuFixd').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".left").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftMenuFixd").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftMenuFixd").addClass("boxFixed")
                    } else {
                        if ($(".leftMenuFixd").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftMenuFixd").height() + $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        }
                        if ($("body").scrollTop() <= 0) {
                            $(".leftMenuFixd").removeClass("boxFixed")
                            $(".leftMenuFixd").css("top", 0)
                        }
                    }

                } else {

                    if ($(".leftMenuFixd").height() < $(window).height()) {
                        if ($("body").scrollTop() > newY) {
                            $(".leftMenuFixd").addClass("boxFixed")
                            var _top = $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        } else {
                            if ($("body").scrollTop() > y) {
                                var _top2 = $("header").innerHeight()
                                $(".leftMenuFixd").css("top", _top2)
                            }
                            if ($("body").scrollTop() <= 0) {
                                $(".leftMenuFixd").removeClass("boxFixed")
                                $(".leftMenuFixd").css("top", 0)
                            }
                        }
                    }


                    if ($(".leftMenuFixd").height() > $(window).height() - $("footer").height() - $("header").height() - 30) {
                        if ($("body").scrollTop() > $(".right").height() - $(".leftMenuFixd").height()) {
                            var _newTop = $(".leftMenuFixd").height() - ($(window).height() - $("footer").height() - 50)
                            $(".leftMenuFixd").css("top", _newTop)
                        }
                    }
                }


                if ($("body").scrollTop() > $(".leftMenuFixd").height() / 2) {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("footer").innerHeight() + 40)
                    $(".leftMenuFixd").css("top", _top)
                } else {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("header").innerHeight())
                    $(".leftMenuFixd").css("top", _top)
                    if ($("body").scrollTop() <= 0) {
                        $(".leftMenuFixd").removeClass("boxFixed")
                        $(".leftMenuFixd").css("top", 0)
                    }
                }

                if ($("body").scrollTop() < 50) {
                    $(".leftMenuFixd").removeClass("boxFixed")
                }
            })
        }
    };
    // left position fixed END



    // live video
    $scope.liveVideo = function () {
        $(".livePopup").show();
        $("body").css("overflow", "hidden");

        // 判斷 瀏覽器
        var userAgent = navigator.userAgent;
        var isChrome = userAgent.indexOf("Chrome") > -1;
        //var video = document.createElement('video');

        if (isChrome) {
            //var videoObj = { "video": true };
            if (1) {
                //if (navigator.getUserMedia) {
                // Put video listeners into place
                // Standard
                //$(".camera").css("display", "flex")// show 提示視窗顯示
                //$(".camera .camera_ready").show()// show 偵測到鏡頭提示
                /*
                navigator.getUserMedia(videoObj, function () {
                    //關閉鏡頭
                    //$(".goLive").hide();
                    //$("#highest").hide();

                    navigator.getUserMedia(videoObj, function () {
                        //alert(1);
                        $(".camera").css("display", "flex")// show 提示視窗顯示
                        $(".camera .camera_ready").show()// show 偵測到鏡頭提示
                        //$(".goLive").show()// hide 解析度 直撥按鈕
                        //$("#highest").show();
                        //$(".bottomBox select , .bottomBox button").show()// hide 解析度 直撥按鈕
                    }, function () {
                    });

                }, function () {
                    $(".camera").css("display", "flex")// show 提示視窗顯示
                    $(".camera .no_camera").show()// show 沒有鏡頭提示
                    $(".bottomBox select , .bottomBox button").hide()// hide 解析度 直撥按鈕
                });
                */

            } else {
                $(".camera").css("display", "flex") // show 提示視窗顯示
                $(".camera .no_camera").show() // show 沒有鏡頭提示
                $(".bottomBox select , .bottomBox button").hide() // hide 解析度 直撥按鈕 
            }
        } else {
            $(".bottomBox select , .bottomBox button").hide()
            $(".notchrome").css("display", "flex")
        }


    }

    // browser

    $scope.$watch(function () { return TmpObj.page_num; }, function (newVal) {
        if (newVal != null && newVal != undefined) {
            if (TmpObj.old_page_num != null && TmpObj.old_page_num != undefined && TmpObj.old_page_num != newVal) {
                TmpObj.old_page_num = newVal;
                $scope.loadMsg();
            }
        }
    });

    $scope.loadMsg = function () {
        $(document).scroll(function (scrollBarA) {
            if ($(document).height() > $(window).height() * 2) {
                var scrollVal = $(this).scrollTop(),
                    docHeight = $(document).height() - 800;

                if (scrollVal > docHeight) {
                    console.log(TmpObj.page_num);
                    $(".right button.more").click();
                    $(document).unbind(scrollBarA)
                }
            }
        })


    };

    // joinLive
    $scope.joinLive = function (live_id) {
            console.log(this.$index);
            $('.joinlivebtn:eq(' + this.$index + ')').parents(".user-msg-box").addClass("joinlivePopup")
            $(".joinlivePopup , .closeJoinLive").show()
            if (live_id != null) {
                $scope.havechatroom = live_id;
                angular.element(document.getElementById('CommBoard')).scope().StartUserChatRoom('userchatroom', live_id);
            }
            $("body").css("overflow", "hidden")
        }
        // closeJoinLive
    $scope.closeJoinLive = function () {
        $('#userchatroom').html('');
        $(".user-msg-box").removeClass("joinlivePopup")
        $(".joinlivePopup , .closeJoinLive").hide()
        $("body").css("overflow", "auto")
    }


    $scope.goLive = function () {
        $(".livePopup .live .video ,  .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").removeClass("set");
        angular.element(document.getElementById('CommBoard')).scope().CreateLive($scope.fan_tmp_list[0].id);
        //前端load live 
        $(".camera").hide();
        $(".liveIframeBox").show();


        $(window).bind("beforeunload", function (event) {
            angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id);
            return '';
        });


    }; //開始直播

    $scope.hideLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").hide();
        $(".showIframe").show();
    }
    $scope.showLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").show();
        $(".showIframe").hide();
    }

    $scope.closeLive = function () {
        $(".livePopup , .joinlivePopup").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    };

    $scope.finishLive = function () {

        Alertify.confirm('End live video ?')
            .then(function () {
                angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id, $scope.fan_tmp_list[0].id);
                //Alertify.success('You are sure!');
            }, function () {
                return;
            });

        $(".finishText .spet1").css("display", "flex")
            //$(".finishText .spet2").css("display", "flex")
            //$(".finishLive").addClass("set")
    }; // 結束直撥流程 1.先顯示 spet1 loading 流程 loading 完後請 隱藏 spet1 顯示 spet2

    $scope.doneBtn = function () {
        $(".livePopup").hide()
        $(".finishText .spet1 , .finishText .spet2").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    }; //  spet2 按鈕 儲存影片
    //end live


    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    //laout
    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "FanText.htm";

});

appH5.controller('FanReportCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, $sce, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()

    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.fan_id = $routeParams.id;
    $scope.fan_id = $routeParams.id;
    data = formdata;
    //if()

    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            if ($rs.data[0].f_backend == null) {
                location.href = 'Index';
            }
            $scope.seedata = $rs;
            console.log($scope.seedata);
        });
    TmpValue.post(APIForder + "?service=Fan.FanReport", data)
        .then(function ($rs) {
            // if ($rs.ret == 400)
            // location.href = "Index";


            console.log($rs);
            $scope.pagename = 'FanReport';
            if ($rs.data[0].msg_state == 'Y') {
                $scope.fan_view = {};
                $scope.fan_view['article'] = {};
                $scope.fan_view['image'] = {};
                $scope.fan_view['video'] = {};
                $scope.fan_view['live'] = {};
                $scope.fan_view['booking'] = {};
                $scope.fan_manage_list = $rs.data["0"].fan_manage_list;
                $scope.fan_user_list = $rs.data["0"].fan_user_list;
                console.log('report');
                $scope.fan_view['article']['day30'] = $rs.data["0"].fan_view['article']['day30'];
                $scope.fan_view['article']['day7'] = $rs.data["0"].fan_view['article']['day7'];
                $scope.fan_view['article']['day3'] = $rs.data["0"].fan_view['article']['day3'];
                $scope.fan_view_article_df = $rs.data["0"].fan_view['article']['day3'];

                $scope.fan_view['image']['day30'] = $rs.data["0"].fan_view['image']['day30'];
                $scope.fan_view['image']['day7'] = $rs.data["0"].fan_view['image']['day7'];
                $scope.fan_view['image']['day3'] = $rs.data["0"].fan_view['image']['day3'];
                $scope.fan_view_image_df = $rs.data["0"].fan_view['image']['day3'];

                $scope.fan_view['video']['day30'] = $rs.data["0"].fan_view['video']['day30'];
                $scope.fan_view['video']['day7'] = $rs.data["0"].fan_view['video']['day7'];
                $scope.fan_view['video']['day3'] = $rs.data["0"].fan_view['video']['day3'];
                $scope.fan_view_video_df = $rs.data["0"].fan_view['video']['day3'];

                $scope.fan_view['live']['day30'] = $rs.data["0"].fan_view['live']['day30'];
                $scope.fan_view['live']['day7'] = $rs.data["0"].fan_view['live']['day7'];
                $scope.fan_view['live']['day3'] = $rs.data["0"].fan_view['live']['day3'];
                $scope.fan_view_live_df = $rs.data["0"].fan_view['live']['day3'];

                $scope.fan_view['live']['allday'] = $rs.data["0"].fan_view['live']['allday'];
                $scope.fan_view['video']['allday'] = $rs.data["0"].fan_view['video']['allday'];
                $scope.fan_view['image']['allday'] = $rs.data["0"].fan_view['image']['allday'];
                $scope.fan_view['article']['allday'] = $rs.data["0"].fan_view['article']['allday'];
                $scope.fan_view['booking']['allday'] = $rs.data["0"].fan_view['booking']['allday'];

                console.log($scope.fan_view['booking']['allday']);





                //leftmenu 
                $scope.fan_tmp_list = $rs.data["0"].fan_tmp_list;
                $scope.fan_tmp_list_count = $rs.data["0"].fan_tmp_list_count;

                if ($scope.fan_tmp_list[0].approval != 'Y' && $scope.fan_manage_list.length == 0) {
                    //   location.href = "Index";
                }
                if ($scope.fan_manage_list.length == 0) {
                    //   location.href = "Index";
                }

                $scope.fan_staff_list = $rs.data["0"].fan_staff_list;
                $scope.fan_man_list = $rs.data["0"].fan_man_list;
                $scope.fan_man_list_count = $rs.data["0"].fan_man_list_count;

                $scope.fan_subscript_list = $rs.data["0"].fan_subscript_list;
                $scope.fan_subscript_list_count = $rs.data["0"].fan_subscript_list_count;

                $scope.fan_Iin_list_count = $rs.data["0"].fan_Iin_list_count;
                $scope.fan_Iin_list = $rs.data["0"].fan_Iin_list;

                TmpObj.board_user_id = $scope.fan_tmp_list['0']['id'];
                TmpObj.friends_user_id = $scope.fan_tmp_list['0']['id'];
                //end leftmenu

                if ($scope.fan_Iin_list_count == 0) {
                    //  location.href = "Fan.FanText/" + $scope.fan_id;
                }

            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    //end main 


    // fanReport page js
    $scope.openTop50Member = function () {
        $(".dbDT").hide();
        $(".iconDB").removeClass("nowThis");
        $(".forTop50Member").toggle();
        $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

        //抓不到 $index 暫時用
        $(".iconDB").click(function () {
                $(".iconDB").removeClass("nowThis");
                $(this).addClass("nowThis");
            })
            //抓不到 $index 暫時用 抓到後要刪除

    };
    //online time top 50

    $scope.openViweImg = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".viewImg").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //images look number

    // left position fixed
    $scope.leftBoxFixed = function () {

        var position = $('.leftMenuFixd').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".left").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftMenuFixd").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftMenuFixd").addClass("boxFixed")
                    } else {
                        if ($(".leftMenuFixd").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftMenuFixd").height() + $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        }
                        if ($("body").scrollTop() <= 0) {
                            $(".leftMenuFixd").removeClass("boxFixed")
                            $(".leftMenuFixd").css("top", 0)
                        }
                    }

                } else {

                    if ($(".leftMenuFixd").height() < $(window).height()) {
                        if ($("body").scrollTop() > newY) {
                            $(".leftMenuFixd").addClass("boxFixed")
                            var _top = $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        } else {
                            if ($("body").scrollTop() > y) {
                                var _top2 = $("header").innerHeight()
                                $(".leftMenuFixd").css("top", _top2)
                            }
                            if ($("body").scrollTop() <= 0) {
                                $(".leftMenuFixd").removeClass("boxFixed")
                                $(".leftMenuFixd").css("top", 0)
                            }
                        }
                    }


                    if ($(".leftMenuFixd").height() > $(window).height() - $("footer").height() - $("header").height() - 30) {
                        if ($("body").scrollTop() > $(".right").height() - $(".leftMenuFixd").height()) {
                            var _newTop = $(".leftMenuFixd").height() - ($(window).height() - $("footer").height() - 50)
                            $(".leftMenuFixd").css("top", _newTop)
                        }
                    }
                }


                if ($("body").scrollTop() > $(".leftMenuFixd").height() / 2) {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("footer").innerHeight() + 40)
                    $(".leftMenuFixd").css("top", _top)
                } else {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("header").innerHeight())
                    $(".leftMenuFixd").css("top", _top)
                    if ($("body").scrollTop() <= 0) {
                        $(".leftMenuFixd").removeClass("boxFixed")
                        $(".leftMenuFixd").css("top", 0)
                    }
                }

                if ($("body").scrollTop() < 50) {
                    $(".leftMenuFixd").removeClass("boxFixed")
                }
            })
        }
    };
    // left position fixed END

    $scope.openTop50Img = function () {
        $(".dbDT").hide();
        $(".iconDB").removeClass("nowThis");
        $(".forTop50Img").toggle();
        $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

        //抓不到 $index 暫時用
        $(".iconDB").click(function () {
                $(".iconDB").removeClass("nowThis");
                $(this).addClass("nowThis");
            })
            //抓不到 $index 暫時用 抓到後要刪除
    };
    //images look number top 50

    $scope.openViweText = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".viewText").toggle();
            $(".dbDiconDBT:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".dbiconDBDT").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //Text look number

    $scope.openTop50Text = function () {
            $(".dbDT").hide();
            $(".dbDiconDBT").removeClass("nowThis");
            $(".forTop50Text").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //Text look number top 50

    $scope.openViweVideo = function () {
            $(".dbDT").hide();
            $(".dbDT").removeClass("nowThis");
            $(".viewVideo").toggle();
            $(".dbDT:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".dbDT").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //video look number

    $scope.openTop50Video = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".top50Video").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //video look number top 50

    $scope.openViweLive = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".viewLive").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //Live look number

    $scope.openTop50Live = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".top50Live").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //Live look number top 50


    $scope.top50BtnClick = function ($event, own, day) {

        $($event.target).parent().find("li").removeClass("nowThis");
        $($event.target).addClass("nowThis");
        if (own === 'article')
            $scope.fan_view_article_df = $scope.fan_view[own]['day' + day];

        if (own == 'image')
            $scope.fan_view_image_df = $scope.fan_view[own]['day' + day];
        if (own === 'vdieo')
            $scope.fan_view_vdieo_df = $scope.fan_view[own]['day' + day];
        if (own === 'live')
            $scope.fan_view_live_df = $scope.fan_view[own]['day' + day];

        /*
        if(own == 'image'){
            $scope.fan_view_image_df = $scope.fan_view[own]['day'+day];
        }
        */


        //抓不到 $index 暫時用 抓到後要刪除

    };
    // fanReport page js END

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.openBookingList = function () {
            $(".dbDT").hide();
            $(".iconDB").removeClass("nowThis");
            $(".allBookingList").toggle();
            $(".iconDB:eq(" + this.$index + ")").addClass("nowThis")

            //抓不到 $index 暫時用
            $(".iconDB").click(function () {
                    $(".iconDB").removeClass("nowThis");
                    $(this).addClass("nowThis");
                })
                //抓不到 $index 暫時用 抓到後要刪除
        }
        //Booking List ＥＮＤ

    //laout
    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "FanReport.htm";

});



appH5.controller('FanListCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    //var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
    //TmpValue.post("http://api.alfa.showhi.co/Public/showhi/?service=User.GetSession", data)
    .then(function ($rs) {
        console.log('2222');
        console.log($rs);
        $scope.seedata = $rs;
    });
    var data = {};
    TmpValue.post(APIForder + "?service=Fan.ViewList", data)
    //TmpValue.post("http://api.alfa.showhi.co/Public/showhi/?service=Fan.ViewList", data)
    .then(function ($rs) {
        if ($rs.data[0].msg_state == 'Y') {
            $scope.fan_list = $rs.data["0"].tmp_fan_arr;

        } else {
            Alertify.alert($rs.data[0].msg_text);
        }
    });

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo , .nowUpImage').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto')
        $('.pop-up video').hide().attr('src', '')
        $('.mid').css('height', 'auto')
        $('.mid').css('overflow', 'auto')
    };
    // colsePopUp end


    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "ViewList.htm";

});

appH5.controller('FanCalendarCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "calendar.htm";

});

appH5.controller('FanMyFanListCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {


    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()
    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    $scope.NewFanData = {};
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.fan_id = $routeParams.id;
    $scope.fan_id = $routeParams.id;
    data = formdata;


    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;

            if ($rs.data[0].f_backend == null) {
                location.href = 'Index';
            }

            console.log($scope.seedata);
        });
    TmpValue.post(APIForder + "?service=Fan.MyFanList", data)
        .then(function ($rs) {
            console.log($rs);


            if ($rs.data[0].msg_state == 'Y') {

                $scope.fan_manage_list = $rs.data["0"].fan_manage_list;
                console.log($scope.fan_manage_list);
                $scope.fan_myfanlist = $rs.data["0"].fan_myfanlist;
            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    //end main 


    $scope.CreateFan = function () {
        var error_msg = "";
        if ($scope.NewFanData.Name == undefined)
            error_msg += "Fan Name\n";

        if ($scope.NewFanData.type == undefined)
            $scope.NewFanData.type = 'school';

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        var data = {};
        data.nick_name = $scope.NewFanData.Name;
        data.text = $scope.NewFanData.Text;
        data.fuzzy_name = $scope.NewFanData.fuzzyName;
        data.type = $scope.NewFanData.type;

        $('.pop-up').show();
        $('.loading').show();
        TmpValue.post(APIForder + "?service=Fan.CreateFan", data)
            .then(function ($rs) {
                console.log($rs);
                $('.pop-up').hide();
                $('.loading').hide();
                $scope.NewFanData = {};
                location.href = "Fan.MyFanList";
            });
    };



    $scope.adminMember = function () {
        $(".communityPopUp , .adminMember").show();
    };
    // adminMember end

    $scope.communityMemberDelete = function () {
        $(".communityPopUp , .deleteUser").show();
    };
    // communityMemberDelete end

    $scope.quitCommunity = function () {
        $(".communityPopUp , .quitCommnuity").show();
    };
    // quitCommunity end

    $scope.removeAdmin = function () {
        $(".communityPopUp , .removeAdmin").show();
    };
    // removeAdmin end

    $scope.adminMemberYes = function () {
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // adminMemberYes end

    $scope.deleteMenber = function () {
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // deleteMenber end

    $scope.quitYes = function () {
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // adminMemberYes end

    $scope.removeAdminYes = function () {
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // adminMemberYes end

    $scope.establishPopUp = function () {
        $(".communityPopUp , .removeAdmin").show();
    };
    // removeAdmin end

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video , .pop-up .v_iBox img').attr('src', '').hide();
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');

    };
    // colsePopUp end

    // left position fixed
    $scope.leftBoxFixed = function () {

        var position = $('.leftMenuFixd').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".left").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftMenuFixd").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftMenuFixd").addClass("boxFixed")
                    } else {
                        if ($(".leftMenuFixd").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftMenuFixd").height() + $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        }
                        if ($("body").scrollTop() <= 0) {
                            $(".leftMenuFixd").removeClass("boxFixed")
                            $(".leftMenuFixd").css("top", 0)
                        }
                    }

                } else {

                    if ($(".leftMenuFixd").height() < $(window).height()) {
                        if ($("body").scrollTop() > newY) {
                            $(".leftMenuFixd").addClass("boxFixed")
                            var _top = $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        } else {
                            if ($("body").scrollTop() > y) {
                                var _top2 = $("header").innerHeight()
                                $(".leftMenuFixd").css("top", _top2)
                            }
                            if ($("body").scrollTop() <= 0) {
                                $(".leftMenuFixd").removeClass("boxFixed")
                                $(".leftMenuFixd").css("top", 0)
                            }
                        }
                    }


                    if ($(".leftMenuFixd").height() > $(window).height() - $("footer").height() - $("header").height() - 30) {
                        if ($("body").scrollTop() > $(".right").height() - $(".leftMenuFixd").height()) {
                            var _newTop = $(".leftMenuFixd").height() - ($(window).height() - $("footer").height() - 50)
                            $(".leftMenuFixd").css("top", _newTop)
                        }
                    }
                }


                if ($("body").scrollTop() > $(".leftMenuFixd").height() / 2) {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("footer").innerHeight() + 40)
                    $(".leftMenuFixd").css("top", _top)
                } else {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("header").innerHeight())
                    $(".leftMenuFixd").css("top", _top)
                    if ($("body").scrollTop() <= 0) {
                        $(".leftMenuFixd").removeClass("boxFixed")
                        $(".leftMenuFixd").css("top", 0)
                    }
                }

                if ($("body").scrollTop() < 50) {
                    $(".leftMenuFixd").removeClass("boxFixed")
                }
            })
        }
    };
    // left position fixed END

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
    };

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "MyFanList.htm";
});

appH5.controller('FanMangeCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {


    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()
    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.fan_id = $routeParams.id;
    $scope.fan_id = $routeParams.id;
    data = formdata;


    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            if ($rs.data[0].f_backend == null) {
                location.href = 'Index';
            }
            $scope.seedata = $rs;
            console.log($scope.seedata);
        });
    TmpValue.post(APIForder + "?service=Fan.FanMange", data)
        .then(function ($rs) {
            if ($rs.ret == 400)
                location.href = "Index";


            console.log($rs);
            $scope.pagename = 'FanMange';
            if ($rs.data[0].msg_state == 'Y') {
                $scope.fan_manage_list = $rs.data["0"].fan_manage_list;

                $scope.fan_user_list = $rs.data["0"].fan_user_list;
                $scope.fan_wait_list = $rs.data["0"].fan_wait_list;
                $scope.fan_wait_list_count = $rs.data["0"].fan_wait_list_count;

                //leftmenu 
                $scope.fan_tmp_list = $rs.data["0"].fan_tmp_list;
                $scope.fan_tmp_list_count = $rs.data["0"].fan_tmp_list_count;

                if ($scope.fan_tmp_list[0].approval != 'Y' && $scope.fan_manage_list.length == 0) {
                    location.href = "Index";
                }
                if ($scope.fan_manage_list.length == 0) {
                    location.href = "Index";
                }

                $scope.fan_staff_list = $rs.data["0"].fan_staff_list;
                $scope.fan_man_list = $rs.data["0"].fan_man_list;
                $scope.fan_man_list_count = $rs.data["0"].fan_man_list_count;

                $scope.fan_subscript_list = $rs.data["0"].fan_subscript_list;
                $scope.fan_subscript_list_count = $rs.data["0"].fan_subscript_list_count;

                $scope.fan_Iin_list_count = $rs.data["0"].fan_Iin_list_count;
                $scope.fan_Iin_list = $rs.data["0"].fan_Iin_list;

                TmpObj.board_user_id = $scope.fan_tmp_list['0']['id'];
                TmpObj.friends_user_id = $scope.fan_tmp_list['0']['id'];

                //end leftmenu
                if ($scope.fan_Iin_list_count == 0) {
                    location.href = "Fan.FanText/" + $scope.fan_id;
                }

            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    //end main 

    $scope.cutbannershow = function () {
        $(".cutBannerImgBox , .moveBar , .cutBannerBTN , .up_img_files_li").show()
        $(".blackBg2 , .fanMange").show()
        $(".privatelistBtn").hide()
        $(".banner , .fanMange").css("z-index", "11")
        $(".right .banner , .fanMange").addClass("fanCutStyle")
    };
    // cutbannershow end

    $scope.showCutBanner = function () {
        $('.cut_pop_up , .banner_cut').show()
        $('body').css('overflow', 'hidden')

    };
    // showCutBanner end
    $scope.read_bg_image = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#target_bg_image').attr('src', e.target.result);
                $(".moveBar").show();
            }
            reader.readAsDataURL(input.files[0]);

        }

    }
    $scope.closeCutBanner = function () {
        $(".cutBannerImgBox , .moveBar , .cutBannerBTN , .up_img_files_li , .blackBg2 , .fanMange").hide();
        $(".privatelistBtn").show();
        $(".banner , .fanMange").css("z-index", "1")
        $(".right .banner , .fanMange").removeClass("fanCutStyle")
    };



    $scope.files_bg_image = function () {
        $("#files_bg_image").change(function () {
            console.log(this);
            $scope.read_bg_image(this);
            $("#holder img").css("top", $(".topBar").height())
        });
    }


    //mang func
    $scope.AgreeFanMember = function (mang_id, key) {
        data.id = mang_id;
        data.fan_id = $scope.fan_tmp_list[0].id;
        $scope.tmp_key = key;
        TmpValue.post(APIForder + "?service=Fan.AgreeFanMember", data)
            .then(function ($rs) {
                $scope.fan_wait_list[$scope.tmp_key].approval = 'Y'
                $scope.fan_user_list.push($scope.fan_wait_list[$scope.tmp_key]);
                $scope.fan_wait_list.splice($scope.tmp_key, 1);
                $scope.fan_wait_list_count--;
                $scope.tmp_key = null;
            });
    };

    $scope.FanEditUP = function (web_data) {
        if ($scope.seedata.data[0].f_backend == null) {
            Alertify.alert('Please login');
            retrun;
        }
        var data = {};
        data.id = web_data;
        data.text = $('#fan_text').text();
        TmpValue.post(APIForder + "?service=Fan.FanEditUP", data)
            .then(function ($rs) {
                $scope.closeChangeAbout();
                $scope.fan_tmp_list[0].text = data.text;
                //location.reload();
            });
    };

    $scope.RefuseFanMember = function (mang_id, key) {
        data.id = mang_id;
        $scope.tmp_key = key;
        data.fan_id = $scope.fan_tmp_list[0].id;

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 600);

        TmpValue.post(APIForder + "?service=Fan.DeleteFanMember", data)
            .then(function ($rs) {
                $scope.fan_wait_list[$scope.tmp_key].approval = 'N'
                $scope.fan_wait_list.splice($scope.tmp_key, 1);
                $scope.fan_wait_list_count--;
                $scope.tmp_key = null;
            });
    };

    $scope.RefuseInviteFanMember = function (mang_id, key) {
        data.id = mang_id;
        $scope.tmp_key = key;
        data.fan_id = $scope.fan_tmp_list[0].id;
        TmpValue.post(APIForder + "?service=Fan.DeleteFanMember", data)
            .then(function ($rs) {
                $scope.fan_user_list[$scope.tmp_key].approval = 'N'
                $scope.fan_user_list.splice($scope.tmp_key, 1);
                $scope.tmp_key = null;
            });
    };

    $scope.DeleteFanMember = function (data) {
        var data = {};
        data.id = $scope.fan_user_list[$scope.tmp_key].mang_id;
        data.fan_id = $scope.fan_tmp_list[0].id;
        //return;
        //delete $scope.fan_user_list[$scope.tmp_key];

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 600);

        TmpValue.post(APIForder + "?service=Fan.DeleteFanMember", data)
            .then(function ($rs) {
                console.log($rs);
                $scope.fan_user_list.splice($scope.tmp_key, 1);
                $scope.tmp_key = null;
                //$(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
            });
    };

    $scope.ChangeFanMemberType = function () {
        var data = {};
        $scope.tmp_key = this.$parent.$parent.key_this_fan_user_list;
        data.id = this.$parent.$parent.this_fan_user_list.mang_id;
        data.type = this.$parent.$parent.this_fan_user_list.type;
        data.fan_id = $scope.fan_tmp_list[0].id;


        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 600);

        TmpValue.post(APIForder + "?service=Fan.ChangeFanMemberType", data)
            .then(function ($rs) {
                //alert('Change Type done.');
                $scope.fan_user_list[$scope.tmp_key].type = data.type;
                $scope.tmp_key = null;
            });
    };





    $scope.DeleteFan = function (data) {
        data.id = this.$parent.$parent.this_fan_user_list.mang_id;
        TmpValue.post(APIForder + "?service=Fan.DeleteFan", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                console.log($scope.seedata);
            });
    };

    $scope.data = {};
    $scope.InviteFanMember = function (data) {
        var error_msg = "";
        if (data == undefined) {
            error_msg = error_msg + 'email';
        }
        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }
        data.fan_id = $scope.fan_tmp_list['0'].id;
        $('.pop-up').show();
        $('.loading').show();
        TmpValue.post(APIForder + "?service=Fan.InviteFanMember", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data["0"].msg_state == 'Y') {
                    $('.loading').hide();
                    //                    $('.loadingTrue').show();
                    $scope.fan_user_list.push($rs.data[0][0]);
                } else {
                    $('.loading').hide();
                    $('.loadingFalse').show();
                }

                setTimeout(function () {
                    //close ok box
                    $('.pop-up').hide();
                    $('.loading').hide();
                    //                    $('.loadingTrue').hide();
                    $('.loadingFalse').hide();
                }, 600);

                $scope.data.email = "";
            });



    };

    $scope.data = {};
    $scope.InviteMultiFanMember = function (data) {
        var fan_id = "";
        var url = "";
        var imbg_image_styleage = "";
        var filename = "";
        var file = "";
        fan_id = $scope.fan_tmp_list[0].id;
        filename = $('#file_excel').val();
        file = $('#file_excel')["0"].files["0"];
        url = "?service=Fan.InviteMultiFanMember";
        var formData = new FormData();
        formData.append('file_excel', file);
        formData.append('fan_id', fan_id);
        //console.log(blob);
        //return;
        if (filename == '') {
            Alertify.alert('Please select Excel file');
        }
        $('.pop-up').show();
        $('.loading').show();
        $.ajax({
            url: APIForder + url,
            type: 'POST',
            cache: false,
            data: formData,
            processData: false,
            contentType: false
        }).done(function (res) {
            $('.pop-up').hide();
            $('.loading').hide();
            console.log(res);
            if (res.data["0"].msg_state == 'N') {
                Alertify.alert('Please select Excel file');
                return;
            } else {
                console.log(res);
                if (res.data["0"].length > 0) {
                    i = 0;
                    for (i; i <= res.data["0"].length; i++) {
                        $scope.fan_user_list.push(res.data[i]["0"]);
                    }
                }

                Alertify.alert(res.data["0"].msg_text);
                return;
            }
            //var msg = alert('Upload Success');
            // window.location.reload();
        }).fail(function (res) {});





    };

    $scope.ChangeFanApproval = function () {
        var data = {};
        data.fan_id = $scope.fan_tmp_list[0].id;
        data.approval = this.tmp_fan.approval;

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 600);

        TmpValue.post(APIForder + "?service=Fan.ChangeFanApproval", data)
            .then(function ($rs) {
                console.log($rs);
                //alert('Change Approval done.');
                $scope.fan_tmp_list[0].approval = this.tmp_fan.approval;
            });
    };


    //end fan mang fun



    $scope.quitFanMember = function () {
        $scope.tmp_key = this.$parent.$parent.key_this_fan_user_list;
        console.log($scope.tmp_key);
        $(".communityPopUp , .quitCommnuity").show();
    };
    // quitCommunity end

    $scope.deleteMenber = function () {
        console.log(this)
            //$(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // deleteMenber end

    $scope.saveCut = function () {
        $('.cut_pop_up , .avatar_cut , .banner_cut').hide()
        $('body').css('overflow', 'auto')
    };
    // saveCut end

    $scope.closeCut = function () {
        $('.cut_pop_up , .avatar_cut , .banner_cut').hide()
        $('body').css('overflow', 'auto')
    };
    // closeCut end
    $scope.showCutAvatar = function () {
        $('.cut_pop_up , .avatar_cut').show()
        $('body').css('overflow', 'hidden')
    };
    // showCutAvatar end
    $scope.showCutBanner = function () {
        $('.cut_pop_up , .banner_cut').show()
        $('body').css('overflow', 'hidden')

    };
    // showCutBanner end

    $scope.packUp = function () {
        $(".msg_box, .key_in_box").slideToggle(0);
    };
    // packUp end

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
    };

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    $scope.changeAbout = function () {
        $('.textareaBox').attr("contenteditable", "true").addClass("canKeyIn");
        $(".changeBtn").hide()
        $(".saveBtn , .closeBtn").show()
    };
    $scope.closeChangeAbout = function () {
        $('.textareaBox').attr("contenteditable", "false").removeClass("canKeyIn");
        $(".changeBtn").show()
        $(".saveBtn , .closeBtn").hide()
    };

    // change about end

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video , .pop-up .v_iBox img').attr('src', '').hide();
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');

    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    // left position fixed
    $scope.leftBoxFixed = function () {

        var position = $('.leftMenuFixd').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".left").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftMenuFixd").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftMenuFixd").addClass("boxFixed")
                    } else {
                        if ($(".leftMenuFixd").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftMenuFixd").height() + $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        }
                        if ($("body").scrollTop() <= 0) {
                            $(".leftMenuFixd").removeClass("boxFixed")
                            $(".leftMenuFixd").css("top", 0)
                        }
                    }

                } else {

                    if ($(".leftMenuFixd").height() < $(window).height()) {
                        if ($("body").scrollTop() > newY) {
                            $(".leftMenuFixd").addClass("boxFixed")
                            var _top = $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        } else {
                            if ($("body").scrollTop() > y) {
                                var _top2 = $("header").innerHeight()
                                $(".leftMenuFixd").css("top", _top2)
                            }
                            if ($("body").scrollTop() <= 0) {
                                $(".leftMenuFixd").removeClass("boxFixed")
                                $(".leftMenuFixd").css("top", 0)
                            }
                        }
                    }


                    if ($(".leftMenuFixd").height() > $(window).height() - $("footer").height() - $("header").height() - 30) {
                        if ($("body").scrollTop() > $(".right").height() - $(".leftMenuFixd").height()) {
                            var _newTop = $(".leftMenuFixd").height() - ($(window).height() - $("footer").height() - 50)
                            $(".leftMenuFixd").css("top", _newTop)
                        }
                    }
                }


                if ($("body").scrollTop() > $(".leftMenuFixd").height() / 2) {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("footer").innerHeight() + 40)
                    $(".leftMenuFixd").css("top", _top)
                } else {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("header").innerHeight())
                    $(".leftMenuFixd").css("top", _top)
                    if ($("body").scrollTop() <= 0) {
                        $(".leftMenuFixd").removeClass("boxFixed")
                        $(".leftMenuFixd").css("top", 0)
                    }
                }

                if ($("body").scrollTop() < 50) {
                    $(".leftMenuFixd").removeClass("boxFixed")
                }
            })
        }
    };
    // left position fixed END

    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "FanMange.htm";
});

appH5.controller('FanMangeMemberCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {


    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()
    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    //console.log($scope.data);
    //$scope.data = TmpValue;
    $scope.today = new Date();
    //$scope.teacher_data = TmpObj;

    formdata = {};
    formdata.fan_id = $routeParams.id;
    $scope.fan_id = $routeParams.id;
    data = formdata;



    //main 
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            if ($rs.data[0].f_backend == null) {
                location.href = 'Index';
            }
            $scope.seedata = $rs;
            console.log($scope.seedata);
        });
    TmpValue.post(APIForder + "?service=Fan.FanMangeMember", data)
        .then(function ($rs) {
            console.log($rs);
            if ($rs.ret == 400)
                location.href = "Index";

            if ($rs.data[0].msg_state == 'Y') {
                $scope.fan_manage_list = $rs.data["0"].fan_manage_list;
                $scope.fan_user_list = $rs.data["0"].fan_user_list;

                //console.log($scope.fan_user_list);

                //leftmenu
                $scope.fan_tmp_list = $rs.data["0"].fan_tmp_list;
                $scope.fan_tmp_list_count = $rs.data["0"].fan_tmp_list_count;

                if ($scope.fan_tmp_list[0].approval != 'Y' && $scope.fan_manage_list.length == 0) {
                    location.href = "Index";
                }


                $scope.fan_staff_list = $rs.data["0"].fan_staff_list;
                $scope.fan_man_list = $rs.data["0"].fan_man_list;
                $scope.fan_man_list_count = $rs.data["0"].fan_man_list_count;
                $scope.fan_subscript_list = $rs.data["0"].fan_subscript_list;
                $scope.fan_subscript_list_count = $rs.data["0"].fan_subscript_list_count;
                $scope.fan_Iin_list_count = $rs.data["0"].fan_Iin_list_count;
                $scope.fan_Iin_list = $rs.data["0"].fan_Iin_list;
                //end leftmenu

                if ($scope.fan_Iin_list_count == 0) {
                    location.href = "Fan.FanText/" + $scope.fan_id;
                }
                TmpObj.board_user_id = $scope.fan_tmp_list['0']['id'];
                TmpObj.friends_user_id = $scope.fan_tmp_list['0']['id'];

            } else {
                Alertify.alert($rs.data[0].msg_text);
            }
        });
    //end main 
    $scope.RefuseFanMember = function (mang_id, key) {

        data.id = mang_id;
        $scope.tmp_key = key;
        data.fan_id = $scope.fan_tmp_list[0].id;

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $scope.deleteMenber();
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 600);

        TmpValue.post(APIForder + "?service=Fan.RefuseFanMember", data)
            .then(function ($rs) {
                console.log($rs);
                //$scope.fan_user_list.splice($scope.tmp_key, 1);
                $scope.tmp_key = null;
                $scope.tmp_mang_id = null;
                location.href = "Fan.FanText/" + $scope.fan_id;
            });
    };

    $scope.quitFanMember = function (id, key) {
        $scope.tmp_key = key;
        $scope.tmp_mang_id = id;
        $(".communityPopUp , .quitCommnuity").show();
    };
    // quitCommunity end

    $scope.deleteMenber = function () {
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
    };
    // deleteMenber end

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
    };

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo').hide();
        $(".v_iBox img").attr("src", "")
        $(".communityPopUp , .adminMember , .deleteUser , .deleteMenber , .removeAdmin , .quitCommnuity").hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video , .pop-up .v_iBox img').attr('src', '').hide();
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    // left position fixed
    $scope.leftBoxFixed = function () {

        var position = $('.leftMenuFixd').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".left").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftMenuFixd").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftMenuFixd").addClass("boxFixed")
                    } else {
                        if ($(".leftMenuFixd").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftMenuFixd").height() + $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        }
                        if ($("body").scrollTop() <= 0) {
                            $(".leftMenuFixd").removeClass("boxFixed")
                            $(".leftMenuFixd").css("top", 0)
                        }
                    }

                } else {

                    if ($(".leftMenuFixd").height() < $(window).height()) {
                        if ($("body").scrollTop() > newY) {
                            $(".leftMenuFixd").addClass("boxFixed")
                            var _top = $("header").innerHeight()
                            $(".leftMenuFixd").css("top", _top)
                        } else {
                            if ($("body").scrollTop() > y) {
                                var _top2 = $("header").innerHeight()
                                $(".leftMenuFixd").css("top", _top2)
                            }
                            if ($("body").scrollTop() <= 0) {
                                $(".leftMenuFixd").removeClass("boxFixed")
                                $(".leftMenuFixd").css("top", 0)
                            }
                        }
                    }


                    if ($(".leftMenuFixd").height() > $(window).height() - $("footer").height() - $("header").height() - 30) {
                        if ($("body").scrollTop() > $(".right").height() - $(".leftMenuFixd").height()) {
                            var _newTop = $(".leftMenuFixd").height() - ($(window).height() - $("footer").height() - 50)
                            $(".leftMenuFixd").css("top", _newTop)
                        }
                    }
                }


                if ($("body").scrollTop() > $(".leftMenuFixd").height() / 2) {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("footer").innerHeight() + 40)
                    $(".leftMenuFixd").css("top", _top)
                } else {
                    var _top = $(window).height() - ($(".leftMenuFixd").height() + $("header").innerHeight())
                    $(".leftMenuFixd").css("top", _top)
                    if ($("body").scrollTop() <= 0) {
                        $(".leftMenuFixd").removeClass("boxFixed")
                        $(".leftMenuFixd").css("top", 0)
                    }
                }

                if ($("body").scrollTop() < 50) {
                    $(".leftMenuFixd").removeClass("boxFixed")
                }
            })
        }
    };
    // left position fixed END

    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Fan/";
    $scope.ContentHtml = "FanMangeMember.htm";
});


function uploadFanPhoto() {
    var url = "";
    var image = "";
    var filename = "";
    var fan_id = "";
    fan_id = $("#FanPhotoSave").attr('fan_id');
    image = $('#imguserphoto64').attr('src');
    filename = $('#TmpphotoFile').val();
    url = "?service=Fan.PhotoUpload";

    var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
    var blob = base64ToBlob(base64ImageContent, 'image/png');
    var formData = new FormData();

    formData.append('photoFile', blob, filename);
    formData.append('fan_id', fan_id);
    //console.log(blob);
    //return;

    $.ajax({
        url: APIForder + url,
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        console.log(res);
        //var msg = alert('Upload Success');
        window.location.reload();
    }).fail(function (res) {});

}


function uploadFanBgPhoto() {
    var url = "";
    var imbg_image_styleage = "";
    var filename = "";
    var file = "";
    var fan_id = "";
    fan_id = $("#bgHeight").attr('fan_id');
    bg_image_style = $('#bgHeight').val();
    filename = $('#files_bg_image').val();
    file = $('#files_bg_image')["0"].files["0"];
    url = "?service=Fan.BgPhotoUpload";
    var formData = new FormData();

    formData.append('photoFile', file);
    formData.append('bg_image_style', bg_image_style);
    formData.append('fan_id', fan_id);
    //console.log(blob);
    //return;

    $.ajax({
        url: APIForder + url,
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        console.log(res);
        //var msg = alert('Upload Success');
        window.location.reload();
    }).fail(function (res) {});

}