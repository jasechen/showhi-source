//Index
appH5.controller('IndexCtrl', function ($scope, $location, $anchorScroll, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.scrollTo = function (id) {
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
        $location.hash(old);
    }

    var data = {};
    /*
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            if($scope.seedata.data["0"] && $scope.seedata.data["0"].f_backend  == null){
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                    s1.async=true;
                    s1.src='https://embed.tawk.to/5945c2d250fd5105d0c819c7/default';
                    s1.charset='UTF-8';
                    s1.setAttribute('crossorigin','*');
                    s0.parentNode.insertBefore(s1,s0);
                })();
            }
        });
        */


    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();

    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .member-box , .recommend-box , .nowUpVideo , .indexpopup , .newsList , .userList').hide();
        $('body').css('overflow', 'auto')
        $('.pop-up video').hide().attr('src', '')
        $('.mid').css('height', 'auto')
        $('.mid').css('overflow', 'auto')
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    $scope.verAccount = function () {
        $(".register-box").hide()
        $(".vaaccount-box").show()
    }
    
    $scope.indexFevent = function () {
        $("iframe").eq(3).css("z-index", "1")
    };

    // top header menu end

    $scope.showPrivateMsg = function () {
        $(".name_box").click(function () {
            var index = $(this).index() - 1
            $(".name_box").removeClass("nowSchoolMsg")
            $(this).addClass("nowSchoolMsg")
            $(".private_msg_pup").removeClass("nowSchoolMsgShow")
            $(".private_msg_pup").eq(index).addClass("nowSchoolMsgShow")
        })
    };
    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };
    // showPrivateBox end

    $scope.indexVideoBanner = function () {

        if ($(window).width() > 450) {
            var _n = 3;
            var _max = Math.round($(".video-group .sd_box").length / _n)
            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>")
            }
        } else {
            var _n = 2;
            var _max = Math.round($(".video-group .sd_box").length / _n)

            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>")
            }
        }

        var _max = Math.round($(".video-group .sd_box").length * $(".video-group .sd_box").width() / $(window).width())

        $(".btnG span").click(function () {
            var $this = $(this),
                _eq = $this.index(),
                _left = (100 * _eq) * -1 + "%";
            $(".btnG span").removeClass("now-group")
            $this.addClass("now-group")
            $(".video-wrap").animate({
                "margin-left": _left
            })
            $(".video-group").removeClass("nowVideoGroup")
            $(".video-group").eq(_eq).addClass("nowVideoGroup")
        })

        var hammer = new Hammer(document.getElementById("indexVideoGroup"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _equp = $(".btnG .now-group").index() + 1,
                _eqdown = $(".btnG .now-group").index() - 1

            if (_X < 0 && _equp < _max) {
                $(".video-wrap").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".btnG span").removeClass("now-group");
                $(".btnG span").eq(_equp).addClass("now-group")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".video-group").removeClass("nowVideoGroup");
                    $(".video-group").eq(_eqdown).addClass("nowVideoGroup");
                    $(".video-wrap").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".btnG span").removeClass("now-group");
                    $(".btnG span").eq(_eqdown).addClass("now-group");
                }
            }
        });

    };

    $scope.indexVideoShow = function () {
        $(".video-box").click(function () {
            var $this = $(this).find('.videolink'),
                _alt = $this.attr('alt'),
                _src = "assets/video/" + _alt + ".mp4"
            $('.pop-up , .v_iBox video').show()
            $('.v_iBox video').attr('src', _src)
            $("body").css("overflow", "hidden")
            $("iframe").eq(0).parent("div").css("z-index", "10")
            $(".v_iBox").css("display", "flex")
        })
    }

    $scope.bannerBtns = function () {
        $(".bannerBtns div").click(function () {
            $(".bannerBtns div").removeClass("nowThis");
            $(this).addClass("nowThis");
            var _eq = $(this).index()
            $(".bg-box .bannerImgBox").removeClass("now-img");
            $(".bg-box .bannerImgBox").eq(_eq).addClass("now-img");
            $(".bg-box").animate({
                'margin-left': 100 * _eq * -1 + "%"
            }, 250)
        })
    }
    $scope.schoolLinkClick = function () {
        $(".listBox a").click(function () {
            $("body").css("overflow", "auto")
        })
    }
    $scope.indexBannerApp = function () {

        var hammer = new Hammer(document.getElementById("indexBanner"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _Y = e.deltaY,
                _equp = $(".now-img").index() + 1,
                _eqdown = $(".now-img").index() - 1

            if (_X < 0 && _equp < $(".bg-box .bannerImgBox").length) {
                $(".bg-box .bannerImgBox").removeClass("now-img")
                $(".bg-box .bannerImgBox").eq(_equp).addClass("now-img")
                $(".bg-box").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".bannerBtns div").removeClass("nowThis");
                $(".bannerBtns div").eq(_equp).addClass("nowThis")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".bg-box .bannerImgBox").removeClass("now-img")
                    $(".bg-box .bannerImgBox").eq(_eqdown).addClass("now-img")
                    $(".bg-box").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".bannerBtns div").removeClass("nowThis");
                    $(".bannerBtns div").eq(_eqdown).addClass("nowThis")
                }
            }
        });


    }

    $scope.popupmorenews = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".newsList").show()
    }

    $scope.popupmoreuser = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".userList").show()
        if ($(".listBox > div").length > 20) {
            $(".indexpopup .userList").addClass("userBox")
            $(".indexpopup").css("display", "block")
        }
    }

    $scope.prev = function () {
        if ($("#indexBanner .bg-box").css("margin-left") == "0px") {

        } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() - 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")

        }
    }
    $scope.next = function () {
        var _length = $("#indexBanner .bg-box .bannerImgBox").length - 1,
            _maxMove = $("#indexBanner .bg-box .bannerImgBox").width() * _length
        if ($("#indexBanner .bg-box").css("margin-left") == _maxMove * -1 + "px") { } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() + 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")
        }
    }

    // calendar
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if ($(this).find("div").hasClass("event")) {
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            } else {
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }

        })
        $(".eventBox .subTitle").click(function () {
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }

    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }

    // calendar END

    $scope.pwdfun = function () {
        $(document).delegate('.pwdinput', 'change', function () {
            if ($(this).val().length > 0) {
                $(this).attr("type", "password")
            } else {
                $(this).attr("type", "")
            }
        });
    }

    //func
    //$scope.Search = function (data) {
        if (data == undefined)
            var data = {};
        data.this_day = $('#this_day').val();
        if (data.text == undefined)
            data.text = '';


        TmpValue.post(APIForder + "?service=Fan.ViewList", data)
            .then(function ($rs) {
                if ($rs.data[0] && $rs.data[0].msg_state == 'Y') {
                    $scope.fan_list = $rs.data["0"].tmp_fan_arr;

                } else {
                    if ($rs.data[0]) Alertify.alert($rs.data[0].msg_text);
                }
            });
    //};

    $scope.message = "Hello";
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Index/";
    $scope.ContentHtml = "ViewList.htm";
});


appH5.controller('page404Ctrl', function ($scope, $location, $anchorScroll, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.scrollTo = function (id) {
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
        $location.hash(old);
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            console.log($rs);
            $scope.seedata = $rs;
        });

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();

    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .member-box , .recommend-box , .nowUpVideo , .indexpopup , .newsList , .userList').hide();
        $('body').css('overflow', 'auto')
        $('.pop-up video').hide().attr('src', '')
        $('.mid').css('height', 'auto')
        $('.mid').css('overflow', 'auto')
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end


    $scope.indexFevent = function () {
        $("iframe").eq(3).css("z-index", "1")
    };

    // top header menu end

    $scope.showPrivateMsg = function () {
        $(".name_box").click(function () {
            var index = $(this).index() - 1
            $(".name_box").removeClass("nowSchoolMsg")
            $(this).addClass("nowSchoolMsg")
            $(".private_msg_pup").removeClass("nowSchoolMsgShow")
            $(".private_msg_pup").eq(index).addClass("nowSchoolMsgShow")
        })
    };
    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };
    // showPrivateBox end

    $scope.indexVideoBanner = function () {

        if ($(window).width() > 450) {
            var _n = 3;
            var _max = Math.round($(".video-group .sd_box").length / _n)
            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>")
            }
        } else {
            var _n = 2;
            var _max = Math.round($(".video-group .sd_box").length / _n)

            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>").trim()
            }
        }

        var _max = Math.round($(".video-group .sd_box").length * $(".video-group .sd_box").width() / $(window).width())

        $(".btnG span").click(function () {
            var $this = $(this),
                _eq = $this.index(),
                _left = (100 * _eq) * -1 + "%";
            $(".btnG span").removeClass("now-group")
            $this.addClass("now-group")
            $(".video-wrap").animate({
                "margin-left": _left
            })
            $(".video-group").removeClass("nowVideoGroup")
            $(".video-group").eq(_eq).addClass("nowVideoGroup")
        })

        var hammer = new Hammer(document.getElementById("indexVideoGroup"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _equp = $(".btnG .now-group").index() + 1,
                _eqdown = $(".btnG .now-group").index() - 1

            if (_X < 0 && _equp < _max) {
                $(".video-wrap").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".btnG span").removeClass("now-group");
                $(".btnG span").eq(_equp).addClass("now-group")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".video-group").removeClass("nowVideoGroup");
                    $(".video-group").eq(_eqdown).addClass("nowVideoGroup");
                    $(".video-wrap").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".btnG span").removeClass("now-group");
                    $(".btnG span").eq(_eqdown).addClass("now-group");
                }
            }
        });

    };

    $scope.indexVideoShow = function () {
        $(".video-box").click(function () {
            var $this = $(this).find('.videolink'),
                _alt = $this.attr('alt'),
                _src = "assets/video/" + _alt + ".mp4"
            $('.pop-up , .v_iBox video').show()
            $('.v_iBox video').attr('src', _src)
            $("body").css("overflow", "hidden")
            $("iframe").eq(0).parent("div").css("z-index", "10")
            $(".v_iBox").css("display", "flex")
        })
    }

    $scope.bannerBtns = function () {
        $(".bannerBtns div").click(function () {
            $(".bannerBtns div").removeClass("nowThis");
            $(this).addClass("nowThis");
            var _eq = $(this).index()
            $(".bg-box .bannerImgBox").removeClass("now-img");
            $(".bg-box .bannerImgBox").eq(_eq).addClass("now-img");
            $(".bg-box").animate({
                'margin-left': 100 * _eq * -1 + "%"
            }, 250)
        })
    }
    $scope.schoolLinkClick = function () {
        $(".listBox a").click(function () {
            $("body").css("overflow", "auto")
        })
    }
    $scope.indexBannerApp = function () {

        var hammer = new Hammer(document.getElementById("indexBanner"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _Y = e.deltaY,
                _equp = $(".now-img").index() + 1,
                _eqdown = $(".now-img").index() - 1

            if (_X < 0 && _equp < $(".bg-box .bannerImgBox").length) {
                $(".bg-box .bannerImgBox").removeClass("now-img")
                $(".bg-box .bannerImgBox").eq(_equp).addClass("now-img")
                $(".bg-box").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".bannerBtns div").removeClass("nowThis");
                $(".bannerBtns div").eq(_equp).addClass("nowThis")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".bg-box .bannerImgBox").removeClass("now-img")
                    $(".bg-box .bannerImgBox").eq(_eqdown).addClass("now-img")
                    $(".bg-box").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".bannerBtns div").removeClass("nowThis");
                    $(".bannerBtns div").eq(_eqdown).addClass("nowThis")
                }
            }
        });


    }

    $scope.popupmorenews = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".newsList").show()
    }

    $scope.popupmoreuser = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".userList").show()
        if ($(".listBox > div").length > 20) {
            $(".indexpopup .userList").addClass("userBox")
            $(".indexpopup").css("display", "block")
        }
    }

    $scope.prev = function () {
        if ($("#indexBanner .bg-box").css("margin-left") == "0px") {

        } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() - 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")

        }
    }
    $scope.next = function () {
        var _length = $("#indexBanner .bg-box .bannerImgBox").length - 1,
            _maxMove = $("#indexBanner .bg-box .bannerImgBox").width() * _length
        if ($("#indexBanner .bg-box").css("margin-left") == _maxMove * -1 + "px") { } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() + 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")
        }
    }

    // calendar
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if ($(this).find("div").hasClass("event")) {
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            } else {
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }

        })
        $(".eventBox .subTitle").click(function () {
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }

    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }

    // calendar END

    $scope.pwdfun = function () {
        $(document).delegate('.pwdinput', 'change', function () {
            if ($(this).val().length > 0) {
                $(this).attr("type", "password")
            } else {
                $(this).attr("type", "")
            }
        });
    }


    $scope.message = "Hello";
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/Index/";
    $scope.ContentHtml = "error404.htm";
});


appH5.controller('WebSubtion', function ($scope, $location, $anchorScroll, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    $scope.data = {};

    $scope.AddwebSubtion = function () {

        var data = {};
        var error_msg = "";
        data.email = $scope.data.email;

        if (typeof data.email == 'undefined' || data.email == null || data.email == "") {
            error_msg += "email \n";
        }

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        TmpValue.post(APIForder + "?service=CronSendMail.AddwebSubtion", data)
            .then(function ($rs) {
                if($rs.ret ==200){
                    if($rs.data["0"].msg_text =="dob_subtion"){
                        Alertify.alert("You have subscribed");
                    }else{
                        Alertify.alert("Subscription successful");
                    }
                }
                $scope.data ={};
            });



    }

});