//User
appH5.controller('UserLoginCtrl', function ($scope, $routeParams, $q, $http, APIForder, $sce,TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }



    //jquery ready
    //$(document).ready(init)
    angular.element(document).ready(function () {
        $(document).ready(init)
    });
    //jquery ready

    $scope.loginBtn = function () {
        $(".loading").addClass("login");
    }

    $scope.submit = function () {
        var data = {};
        var d = new Date();
        if ($scope.data === undefined) {
            //[{}] //陣列裡面的物件
            //{} 直接物件
            $scope.data = {
                loginid: null,
                loginpwd: null,
                time_set: null,
                identity: null
            };
        }

        $scope.data.time_set = d.getTimezoneOffset() / 60;
        $scope.data.identity = $('.identity').val();
        data = $scope.data;

        TmpValue.post(APIForder + "?service=User.logIn", data)
            .then(function ($rs) {
                console.log($rs);
                //window.localStorage.seeid =
                //return false;
                if ($rs.ret == 200) {
                    window.localStorage.sid = $rs.data[0].user_list[0].sid;
                    window.localStorage.sid;

                    if ($rs.data[0].user_list[0].msg_state == 'Y') {
                        if ($rs.data[0].user_list[0].identity == 'student') {
                            location.href = 'User.StudentMyProfile';
                        } else {
                            location.href = 'User.TeacherMyProfile';
                        }
                    } else if ($rs.data[0].user_list[0].msg_error_code == 'AccountError') {
                        Alertify.alert('Account or password incorrect');
                        $(".loading").removeClass("login");
                    } else if ($rs.data[0].user_list[0].msg_error_code == 'NoAccountError') {
                        Alertify.alert("Account or password incorrect");
                        $(".loading").removeClass("login");
                        // 新增註冊按鈕

                        // 註冊事件
                        $('.join').click(function () {
                            $("#alertify-ok").click();
                            $('.member-box').hide();
                            $('.register-box').show();
                        })
                    }
                } else {
                    //沒連到資料庫
                    Alertify.alert("Account or password incorrect");
                    $(".loading").removeClass("login");
                    // 新增註冊按鈕

                    // 註冊事件
                    $('.join').click(function () {
                        $("#alertify-ok").click();
                        $('.member-box').hide();
                        $('.register-box').show();
                    })
                }
            });



    };
    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "Login.htm";
});

appH5.controller('UserForgetPwCtrl', function ($scope, $routeParams, $q, $http, APIForder, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.ForgetPassword = function () {
        var data = {};
        if ($scope.data === undefined) {
            //[{}] //陣列裡面的物件
            //{} 直接物件
            $scope.data = {
                email: null
            };
        }
        data = $scope.data;
        console.log(data);

        TmpValue.post(APIForder + "?service=User.ForgetPw", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        //轉驗證設定密碼頁處
                        console.log($rs);
                        $('.general2').show()
                    } else {
                        Alertify.alert($rs.data[0].msg_text);
                    }
                } else {
                    Alertify.alert($rs.msg);
                }
            });
    };

    $scope.EmailPwUP = function () {
        var data = {};
        data = this.data;
        var error_msg = "";


        if (data.password == undefined)
            error_msg += "passowrd\n";
        if (data.cpassword == undefined)
            error_msg += "confirm password\n";
        if (data.forget_code == undefined)
            error_msg += "Verification code\n";
        if (data.email == undefined)
            error_msg += "email\n";

        if (data.password != data.cpassword) {
            error_msg += "passowrd and confirm passowrd different\n";
            Alertify.alert(error_msg);
            return;
        }

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }
        console.log(data);

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 800);

        TmpValue.post(APIForder + "?service=User.EditEmailPwUP", data)
            .then(function ($rs) {

                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        //alert($rs.data[0].msg_text);
                        location.reload();
                    } else {
                        Alertify.alert($rs.data[0].msg_text);
                    }
                } else {
                    Alertify.alert($rs.msg);
                }
            });
    };

    $scope.InSidebar = "N";
    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "Login.htm";
});

appH5.controller('UserRegisterCtrl', function ($scope, $routeParams, $q, $http, APIForder, VaAccountDa, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.data = VaAccountDa;
    $scope.submit_reg = function (formdata) {
        $(".loading").addClass("login")
        //$scope.data.identity =$('.identity').val();
        VaAccountDa.loginid = formdata.loginid;
        VaAccountDa.phone = formdata.phone;
        VaAccountDa.identity = 'teach';
        data = formdata;
        data.invite_url = $("#invite_url").val();
        data.invite_loginid = $("#invite_loginid").val();
        TmpValue.post(APIForder + "?service=User.RegisterTeacherUP", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    $(".pop-up").show()
                    $(".register-box").hide()
                    $('.vaaccount-box').show()
                    $(".close-btn").hide()
                    $(".loading").removeClass("login");

                } else {
                    Alertify.alert($rs.data[0].msg_text);
                    $('.pop-up').hide()
                    $(".register-box").hide()
                    $(".loading").removeClass("login");
                    //return false;
                }
            });

    };

    $scope.saveBtnEvent = function () {
        var strEmail = $(".email input").val();
        var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

        if ($(".password-again input").val().length < 1) {
            Alertify.success('Enter the Your password again');
            $(".password-again input").focus()
            return;
        }

        if ($(".password input").val().length < 1) {
            Alertify.success('Enter the Your password');
            $(".password input").focus()
            return;
        }

        if ($(".name input").val().length < 1) {
            Alertify.success('Enter the Your Name');
            $(".name input").focus()
            return;
        }

        if ($(".phone input").val().length < 1) {
            Alertify.success('Enter the phone number');
            $(".phone input").focus()
            return;
        }

        if (strEmail.search(emailRule) != -1) {
            //alert("true");
        } else {
            //alert("false");
            Alertify.success('Enter the email address in the format someone@example.com');
            $(".email input").focus()
            return;
        }

    }

});

appH5.controller('UserVaAccountCtrl', function ($scope, $routeParams, $q, $http, APIForder, VaAccountDa, TmpObj, TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }


    $scope.WakeupAccount = function () {
        var data = {};
        data.user_id = $routeParams.id;
        data.email_code = $routeParams.email_code;

        TmpValue.post(APIForder + "?service=User.WakeupAccount", data)
            .then(function ($rs) {
                console.log('now');
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    $scope.Vaaccount = '1';
                    //Alertify.alert($rs.data[0].msg_text);
                    //$(".pop-up").show()
                    //$(".vaaccount-box").hide()
                    //$('.login-box').show()
                } else {
                    $scope.Vaaccount = '0';
                    //Alertify.alert($rs.data[0].msg_text);
                    //$(".pop-up").show()
                    //$(".vaaccount-box").hide()
                    //$('.login-box').show()
                    //return false;
                }
            });
    }

    $scope.WakeupAccount();

    //QRCold 邀請

    $scope.openAnn = function ($event) {
        var _this = $($event.target)

        _this.parents(".annotation").find(".annotation_con").toggle()
    }

    $scope.showQRcode = function ($event) {
        var _this = $($event.target);
        _this.parents(".qrClde_data").find(".qrimg").toggleClass("openBigQRcode")
    }

    //QRCold 邀請 ＥＮＤ

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "WakeupAccount.htm";
    //$scope.ContentHtml = "qrcode_invite.htm";


});

appH5.controller('UserInviteMang', function ($scope, $routeParams, $q, $http, APIForder, VaAccountDa, TmpObj, TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    //main
    var data = {};
    $scope.tmp = {
        nick_name: null,
    };
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            //console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.QRLinkList", data);
            } else {
                Alertify.alert('login Error 1');
                location.href = 'Index';
            }

        }).then(function ($rs) {
            console.log('QRLinkList:');
            console.log($rs);
            $scope.qrlink_data = $rs;
            return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);

        }).then(function ($rs) {
            $scope.profile_data = $rs;
        })
    //end main

    //main func
    $scope.QRLinkCreate = function ($type) {

        $(".loading").addClass("login");

        var data = {};
        data.type = $type;

        TmpValue.post(APIForder + "?service=User.QRLinkCreate", data)
            .then(function ($rs) {
                console.log('QRLinkCreate:');
                console.log($rs);
                //新增進陣列 
                $scope.qrlink_data.data["0"].qrcode_list.splice(0, 0, $rs.data["0"]["0"]);
                $(".loading").removeClass("login")
            })

    }

    $scope.QRLinkAccountDelete = function (this_qrcode_data, key_this_qrcode_data, key_this_qrcode) {
        var data = {};
        data.id = this_qrcode_data.user_id;

        $(".loading").addClass("login")
        TmpValue.post(APIForder + "?service=User.QRLinkAccountDelete", data)
            .then(function ($rs) {
                console.log('QRLinkAccountDelete:');
                console.log($rs);
                $scope.qrlink_data.data["0"].qrcode_list[key_this_qrcode].qrcode_data.splice(key_this_qrcode_data, 1);
                $(".loading").removeClass("login")
                //刪除陣列 

            })

    }


    $scope.QRLinkApproval = function ($event, this_qrcode_data) {

        var _this = $($event.target);
        _this.hide();
        _this.parents(".user_iC").find(".approval_Y").show()

        var data = {};
        data.id = this_qrcode_data.user_id;
        data.approval = 'Y';
        var _this = $($event.target);
        $(".loading").addClass("login")
        TmpValue.post(APIForder + "?service=User.QRLinkApproval", data)
            .then(function ($rs) {
                $(".loading").removeClass("login")
                console.log('QRLinkApproval:');
                //Alertify.alert('Successful');
                console.log($rs);
                _this.hide();
                _this.parents(".user_iC").find(".approval_Y").show()
            })
    }

    $scope.QRLinkBlock = function (thisQRUser, block, $event) {
        var _this = $($event.target);
        _this.parents(".user_iC").find(".open_a").toggle();
        _this.parents(".user_iC").find(".close_a").toggle();
        //$(".loading").addClass("login");
        var data = {};
        data.id = thisQRUser.user_id;
        data.block = block;
        $(".loading").addClass("login");
        TmpValue.post(APIForder + "?service=User.QRLinkBlock", data)
            .then(function ($rs) {
                $(".loading").removeClass("login")
                console.log('QRLinkBlock:');
                //Alertify.alert('Successful');
                console.log($rs);
                //$(".loading").removeClass("login");
            })

    }

    $scope.QRLinkEditUP = function (thisQR) {

        var data = {};
        data.id = thisQR.id;
        data.note = thisQR.note;
        $(".loading").addClass("login");

        TmpValue.post(APIForder + "?service=User.QRLinkEditUP", data)
            .then(function ($rs) {
                $(".loading").removeClass("login");
                console.log('QRLinkEditUP:');
                console.log($rs);
                //editqrnote 
                //$scope.qrlink_data = $rs;
            })
    }

    $scope.QRLinkDeleteUP = function (thisQR, key_this_qrcode) {

        var data = {};
        data.id = thisQR.id;
        $(".loading").addClass("login")
        TmpValue.post(APIForder + "?service=User.QRLinkDeleteUP", data)
            .then(function ($rs) {
                $scope.qrlink_data.data["0"].qrcode_list.splice(key_this_qrcode, 1);
                $(".loading").removeClass("login")
                console.log('QRLinkDeleteUP:');
                console.log($rs);
                //editqrnote 
                //$scope.qrlink_data = $rs;
            })
    }

    $scope.QRLinkCheck = function ($event) {
        TmpValue.post(APIForder + "?service=User.QRLinkCheck", data)
            .then(function ($rs) {
                console.log('QRLinkCheck:');
                console.log($rs);
                //qrcheck 
                $scope.qrlink_data = $rs;
            })
    }

    $scope.QRLinkUserNoteEditUP = function (thisQRUser) {
        var data = {};
        data.id = thisQRUser.user_id;
        data.invite_note = thisQRUser.invite_note;
        $(".loading").addClass("login");
        TmpValue.post(APIForder + "?service=User.QRLinkUserNoteEditUP", data)
            .then(function ($rs) {
                $(".loading").removeClass("login");
                console.log('QRLinkUserNoteEditUP:');
                console.log($rs);
                //editusernote 
            })
    }

    //end main func

    //QRCold 邀請

    $scope.openAnn = function ($event) {
        var _this = $($event.target)

        _this.parents(".annotation").find(".annotation_con").toggle()
    }

    $scope.showQRcode = function ($event) {
        var _this = $($event.target);
        _this.parents(".qrClde_data").find(".qrimg").toggleClass("openBigQRcode")
    }

    //QRCold 邀請 ＥＮＤ

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "InviteMang.htm";


});

appH5.controller('UserQRLinkCheck', function ($scope, $routeParams, $q, $http, APIForder, VaAccountDa, TmpObj, TmpValue, Alertify) {

    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    //main
    var data = {};
    $scope.tmp = {
        nick_name: null,
    };




    //$scope.QRLinkCheck = function ($event) {
    data.loginid = $routeParams.loginid;
    data.post_date = $routeParams.post_date;
    data.url = 'http://test.show.co/User.QRLinkCheck/' + data.loginid + '/' + data.post_date;
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            console.log('notice');
            console.log($rs);
            $scope.seedata = $rs;
            if ($rs.data[0].f_backend != null) {
                alertify.alert("这个页面为注册页面，需要没有登入才可以使用", function () {
                    location.href = "Index";
                });
            } else {
                return TmpValue.post(APIForder + "?service=User.QRLinkCheck", data)
            }
        })

    //return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
    TmpValue.post(APIForder + "?service=User.QRLinkCheck", data)
        .then(function ($rs) {
            console.log('QRLinkCheck:1');
            console.log($rs);
            //qrcheck 
            $scope.qrlink_data = $rs;
            if ($scope.qrlink_data.data["0"].msg_state == 'N') {
                /*
                Alertify.alert('这个QRcode连结已经过期了',
                function () {
                    alert('1');
                    
                });
                */
                Alertify.alert("这个QRcode连结已经过期了", function () {
                    location.href = "Index"
                });
            }
        });
    //}

    //end main

    //func

    $scope.scrollTo = function (id) {
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
        $location.hash(old);
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            console.log($rs);
            $scope.seedata = $rs;
        });

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();

    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .member-box , .recommend-box , .nowUpVideo , .indexpopup , .newsList , .userList').hide();
        $('body').css('overflow', 'auto')
        $('.pop-up video').hide().attr('src', '')
        $('.mid').css('height', 'auto')
        $('.mid').css('overflow', 'auto')
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end

    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end


    $scope.indexFevent = function () {
        $("iframe").eq(3).css("z-index", "1")
    };

    // top header menu end

    $scope.showPrivateMsg = function () {
        $(".name_box").click(function () {
            var index = $(this).index() - 1
            $(".name_box").removeClass("nowSchoolMsg")
            $(this).addClass("nowSchoolMsg")
            $(".private_msg_pup").removeClass("nowSchoolMsgShow")
            $(".private_msg_pup").eq(index).addClass("nowSchoolMsgShow")
        })
    };
    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };
    // showPrivateBox end

    $scope.indexVideoBanner = function () {

        if ($(window).width() > 450) {
            var _n = 3;
            var _max = Math.round($(".video-group .sd_box").length / _n)
            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>")
            }
        } else {
            var _n = 2;
            var _max = Math.round($(".video-group .sd_box").length / _n)

            for (i = 2; i <= _max; i++) {
                $(".btnG").append("<span>●</span>").trim()
            }
        }

        var _max = Math.round($(".video-group .sd_box").length * $(".video-group .sd_box").width() / $(window).width())

        $(".btnG span").click(function () {
            var $this = $(this),
                _eq = $this.index(),
                _left = (100 * _eq) * -1 + "%";
            $(".btnG span").removeClass("now-group")
            $this.addClass("now-group")
            $(".video-wrap").animate({
                "margin-left": _left
            })
            $(".video-group").removeClass("nowVideoGroup")
            $(".video-group").eq(_eq).addClass("nowVideoGroup")
        })

        var hammer = new Hammer(document.getElementById("indexVideoGroup"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _equp = $(".btnG .now-group").index() + 1,
                _eqdown = $(".btnG .now-group").index() - 1

            if (_X < 0 && _equp < _max) {
                $(".video-wrap").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".btnG span").removeClass("now-group");
                $(".btnG span").eq(_equp).addClass("now-group")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".video-group").removeClass("nowVideoGroup");
                    $(".video-group").eq(_eqdown).addClass("nowVideoGroup");
                    $(".video-wrap").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".btnG span").removeClass("now-group");
                    $(".btnG span").eq(_eqdown).addClass("now-group");
                }
            }
        });

    };

    $scope.indexVideoShow = function () {
        $(".video-box").click(function () {
            var $this = $(this).find('.videolink'),
                _alt = $this.attr('alt'),
                _src = "assets/video/" + _alt + ".mp4"
            $('.pop-up , .v_iBox video').show()
            $('.v_iBox video').attr('src', _src)
            $("body").css("overflow", "hidden")
            $("iframe").eq(0).parent("div").css("z-index", "10")
            $(".v_iBox").css("display", "flex")
        })
    }

    $scope.bannerBtns = function () {
        $(".bannerBtns div").click(function () {
            $(".bannerBtns div").removeClass("nowThis");
            $(this).addClass("nowThis");
            var _eq = $(this).index()
            $(".bg-box .bannerImgBox").removeClass("now-img");
            $(".bg-box .bannerImgBox").eq(_eq).addClass("now-img");
            $(".bg-box").animate({
                'margin-left': 100 * _eq * -1 + "%"
            }, 250)
        })
    }
    $scope.schoolLinkClick = function () {
        $(".listBox a").click(function () {
            $("body").css("overflow", "auto")
        })
    }
    $scope.indexBannerApp = function () {

        var hammer = new Hammer(document.getElementById("indexBanner"));

        hammer.on("swipe", function (e) {
            var _X = e.deltaX,
                _Y = e.deltaY,
                _equp = $(".now-img").index() + 1,
                _eqdown = $(".now-img").index() - 1

            if (_X < 0 && _equp < $(".bg-box .bannerImgBox").length) {
                $(".bg-box .bannerImgBox").removeClass("now-img")
                $(".bg-box .bannerImgBox").eq(_equp).addClass("now-img")
                $(".bg-box").animate({
                    'margin-left': 100 * _equp * -1 + "%"
                }, 250);
                $(".bannerBtns div").removeClass("nowThis");
                $(".bannerBtns div").eq(_equp).addClass("nowThis")
            } else {
                if (_X > 0 && _eqdown > -1) {
                    $(".bg-box .bannerImgBox").removeClass("now-img")
                    $(".bg-box .bannerImgBox").eq(_eqdown).addClass("now-img")
                    $(".bg-box").animate({
                        'margin-left': 100 * _eqdown * -1 + "%"
                    }, 250);
                    $(".bannerBtns div").removeClass("nowThis");
                    $(".bannerBtns div").eq(_eqdown).addClass("nowThis")
                }
            }
        });


    }

    $scope.popupmorenews = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".newsList").show()
    }

    $scope.popupmoreuser = function () {
        $(".indexpopup").css("display", "block")
        $("body").css("overflow", "hidden")
        $(".userList").show()
        if ($(".listBox > div").length > 20) {
            $(".indexpopup .userList").addClass("userBox")
            $(".indexpopup").css("display", "block")
        }
    }

    $scope.prev = function () {
        if ($("#indexBanner .bg-box").css("margin-left") == "0px") {

        } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() - 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")

        }
    }
    $scope.next = function () {
        var _length = $("#indexBanner .bg-box .bannerImgBox").length - 1,
            _maxMove = $("#indexBanner .bg-box .bannerImgBox").width() * _length
        if ($("#indexBanner .bg-box").css("margin-left") == _maxMove * -1 + "px") { } else {
            var _nowThis = $(".bannerBtns").find(".nowThis").index() + 1,
                _move = -100 * _nowThis + "%"
            $("#indexBanner .bg-box").animate({
                "margin-left": _move
            }, 250)
            $(".bannerBtns").find("div").removeClass("nowThis")
            $(".bannerBtns").find("div").eq(_nowThis).addClass("nowThis")
            $(".bannerImgBox").removeClass("now-img")
            $(".bg-box").find(".bannerImgBox").eq(_nowThis).addClass("now-img")
        }
    }

    // calendar
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if ($(this).find("div").hasClass("event")) {
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            } else {
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }

        })
        $(".eventBox .subTitle").click(function () {
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }

    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }

    // calendar END

    $scope.pwdfun = function () {
        $(document).delegate('.pwdinput', 'change', function () {
            if ($(this).val().length > 0) {
                $(this).attr("type", "password")
            } else {
                $(this).attr("type", "")
            }
        });
    }


    //end func

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "JoinPage.htm";


});



appH5.controller('UserExCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    $scope.submit = function (formdata) {
        if (formdata === undefined) {
            formdata = {};
        }

        formdata.this_day = $('#this_day').val();
        formdata.text = $('#text').val();
        data = formdata;
        location.href = 'Course.ViewList/' + formdata.this_day;
    };

});

appH5.controller('UserLogoutCtrl', function ($scope, $routeParams, $q, $http, APIForder,TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    data.sid = window.localStorage.sid;
    
    TmpValue.post(APIForder + "?service=User.Logout", data)
        .then(function ($rs) {
            if ($rs.data[0].msg_state == 'Y')
                location.href = 'Index';
            else
                Alertify.alert('Logout Error');
        });

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "TeacherMyProfile.htm";
    //$scope.ContentHtml = $routeParams.Action+".htm";
});

appH5.controller('UserTeacherEdit', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    $scope.tmp = {
        nick_name: null,
    };
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            //console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
            } else {
                Alertify.alert('login Error 1');
                location.href = 'User.Login';
            }

        }).then(function ($rs) {
            console.log('tedit:');
            console.log($rs);

            $scope.profile_data = $rs;
            $scope.tmp.nick_name = angular.copy($scope.profile_data.data['0'].tmp_list['0'].nick_name);
            return TmpValue.post(APIForder + "?service=User.CanTutorTime", data);
        }).then(function ($rs) {
            $scope.can_tutor_time_data = $rs;
            //foreach($scope.can_tutor_time_data.tmp_list )
            for (var key in value = $scope.can_tutor_time_data.data['0'].tmp_list) {

                if (value[0]['can_tutor_time'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time'] = value[0]['can_tutor_time'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time'] = [{}];

                if (value[0]['can_tutor_time1'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time1'] = value[0]['can_tutor_time1'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time1'] = [{}];

                if (value[0]['can_tutor_time2'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time2'] = value[0]['can_tutor_time2'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time2'] = [{}];

                if (value[0]['can_tutor_time3'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time3'] = value[0]['can_tutor_time3'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time3'] = [{}];

                if (value[0]['can_tutor_time4'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time4'] = value[0]['can_tutor_time4'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time4'] = [{}];

                if (value[0]['can_tutor_time5'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time5'] = value[0]['can_tutor_time5'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time5'] = [{}];

                if (value[0]['can_tutor_time6'] != null)
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time6'] = value[0]['can_tutor_time6'].split(",");
                else
                    $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time6'] = [{}];

            }

            $scope.can_tutor_time = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time'])
            $scope.can_tutor_time1 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time1'])
            $scope.can_tutor_time2 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time2'])
            $scope.can_tutor_time3 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time3'])
            $scope.can_tutor_time4 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time4'])
            $scope.can_tutor_time5 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time5'])
            $scope.can_tutor_time6 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time6'])

            console.log($scope.can_tutor_time_data);
            //init();
            //TmpObj.BasicSetting ='on';

        });

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "TeacherEdit.htm";

    $scope.changeTimeEvent = function (obj, index) {

        var $this = $(obj.target)
        if ($this.parent().attr("class") != "week") {
            if ($this.attr("class") != "time") {
                $this.toggleClass("hasChange")


                if ($this.hasClass("open")) {
                    $this.removeClass("open")
                    $this.find('span').text("CLOSE")
                    $this.find('input').prop("checked", false)
                    var idx = $scope['can_tutor_time' + index].indexOf($this.find('input').val());
                    if (typeof $this.find('input').val() === 'undefined') {
                        return;
                    }
                    if (idx > -1) {
                        $scope['can_tutor_time' + index].splice(idx, 1);
                    }

                } else {
                    $this.addClass("open")
                    $this.find('span').text("OPEN")
                    $this.find('input').prop("checked", true)
                    if (typeof $this.find('input').val() === 'undefined') {
                        return;
                    }
                    $scope['can_tutor_time' + index].push($this.find('input').val());
                }
            }
        }
    }

    $scope.spanClick = function () {
        $("span").unbind().click(function () {
            $(this).parent().click()
        })
    }

    $scope.rewakeup = function (user_id) {
        var data = {};
        $scope.tmp = {
            nick_name: null,
        };
        data.user_id = user_id;
        TmpValue.post(APIForder + "?service=User.SendVaEmail", data)
            .then(function ($rs) {
                console.log('email');
                console.log($rs);
                console.log('寄信成功');
            })

    }

    $scope.submit_up = function (formdata) {

        if ($('#tmp_approval').prop("checked"))
            tmp_approval = $('#tmp_approval').val();
        else
            tmp_approval = 'N';

        var data = {
            start_date: null,
            end_date: null,
            can_tutor_time: null,
            can_tutor_time1: null,
            can_tutor_time2: null,
            can_tutor_time3: null,
            can_tutor_time4: null,
            can_tutor_time5: null,
            can_tutor_time6: null
        }

        var dataEdit = {
            phone: null,
            nationality: null,
            birth_day: null,
            gender: null,
            school_name: null,
            email: null,
            text: null,
            nick_name: null,
            approval: null
        }
        dataEdit.phone = $scope.profile_data.data['0'].tmp_list['0'].phone
        dataEdit.nationality = $scope.profile_data.data['0'].tmp_list['0'].nationality
        dataEdit.birth_day = $scope.profile_data.data['0'].tmp_list['0'].birth_day
        dataEdit.gender = $scope.profile_data.data['0'].tmp_list['0'].gender
        dataEdit.school_name = $scope.profile_data.data['0'].tmp_list['0'].school_name
        dataEdit.email = $scope.profile_data.data['0'].tmp_list['0'].email
        dataEdit.text = $scope.profile_data.data['0'].tmp_list['0'].text
        dataEdit.nick_name = $scope.tmp.nick_name;
        dataEdit.approval = tmp_approval;
        //console.log(dataEdit.approval);
        //return;

        for (var i = 0; i < 7; i++) {
            if (i == 0) {
                data.start_date = $('#start_date').val();
                data.end_date = $('#end_date').val();
                data.can_tutor_time = $scope['can_tutor_time'];
                //console.log($scope['can_tutor_time']);
            } else {
                data["can_tutor_time" + i] = $scope['can_tutor_time' + i];

                //console.log($scope['can_tutor_time'+i]);
            }
        }

        TmpValue.post(APIForder + "?service=User.UserEditUP", dataEdit)
            .then(function ($rs) {
                console.log($rs);
                //return false;
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        return TmpValue.post(APIForder + "?service=User.CanTutorTimeUP", data);
                    } else {
                        console.log($rs);
                        //alert($rs.data[0].msg_text);
                    }
                } else {
                    console.log($rs);
                    //alert($rs.msg);
                }
            })
            .then(function ($rs) { //CanTutorTimeUP
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        //alert($rs.data[0].msg_text);
                        if ($scope.seedata.data[0].f_backend.identity == 'student') {
                            location.href = 'User.StudentMyProfile';
                        } else {
                            location.href = 'User.TeacherMyProfile';
                        }
                    } else {
                        console.log($rs);
                        //alert($rs.data[0].msg_text);
                    }
                } else {
                    console.log($rs);
                    //alert($rs.msg);
                }
            });

    }

    $scope.CantutorTimeToggle = function (data) {
        $('.time_wrap').slideToggle(150);
        return false;
    };

    // CantutorTimeToggle END

    $scope.timePeriodSwitchM = function (event) {
        $('.class-time-box div').removeClass('nowTimeList ');
        $('.class-time-box div').eq(0).addClass('nowTimeList');
        if ($(".time-list .morning").hasClass('moving')) {
            $(".time-list .morning").removeClass("moving");
        };

    };
    $scope.timePeriodSwitchN = function (event) {
        $('.class-time-box div').removeClass('nowTimeList ');
        $('.class-time-box div').eq(1).addClass('nowTimeList');
        if ($(".time-list .morning").hasClass('moving')) { } else {
            $(".time-list .morning").addClass("moving");
        };

    };

    // top header menu 
    $scope.clickLi = function () {
        $(".time-list ul li span").click(function () {
            alert(0)
        })
    }
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();

    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo , .joinlivePopup').hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
        $(".v_iBox img").attr("src", "")
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end
    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    $scope.MessageCreateGroup = function (group_data) {
        console.log(group_data);
    };

    // showPrivateMsg end

    $scope.showPrivateBox = function () {
        $(".private_msg_list , .privateBtn .fa").slideToggle(0)
        $(".privateBar").toggleClass("shadowB")
    };



    // 手機驗證
    $scope.phoveVer = function () {

        //傳送驗證碼

        TmpValue.post(APIForder + "?service=User.SendPhoneCode", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    $(".phoneVER .phoneVERBtn").hide();
                    $(".phoneVER .pVerBox").show();
                } else {
                    $(".phoneVER .phoneVERBtn").hide();
                    $(".phoneVER .pVerBox").show();
                }
            });
    }

    $scope.ocInit = function () {

        if ($(".checkBtn input").attr('checked')) {
            $(".oc-open").show()
            $(".oc-close").hide()
        } else {
            $(".oc-close").show()
            $(".oc-open").hide()
        }
    }

    $scope.changeOC = function () {
        $(".oc-usbtitle").toggle()
    }

    $scope.verifydata = {};
    $scope.phoveVerSend = function (phone_code) {
        var data = {};
        data.user_id = $scope.seedata.data["0"].f_backend.user_id
        data.phone_code = $scope.verifydata.phone_code;
        console.log(data);
        return;
        TmpValue.post(APIForder + "?service=User.WakeupAccountPhone", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    $scope.Vaaccount = '1';
                    Alertify.alert('Mobile verification is successful');
                    $scope.seedata.data["0"].f_backend.phone_va = 'Y';
                    $(".phoneVER .pVerBox").hide();

                } else {
                    $scope.Vaaccount = '0';
                    Alertify.alert('Mobile verification failed');
                }
            });
        //$(".verF").show();
    }
    // 手機驗證

    // showPrivateBox end

    //$scope.ContentHtml = $routeParams.Action+".htm";


    //QRCold 邀請

    $scope.openAnn = function ($event) {
        var _this = $($event.target)

        _this.parents(".annotation").find(".annotation_con").toggle()
    }

    $scope.showQRcode = function ($event) {
        var _this = $($event.target);
        _this.parents(".qrClde_data").find(".qrimg").toggleClass("openBigQRcode")
    }

    //QRCold 邀請 ＥＮＤ

});

appH5.controller('UserTeacherMyProfile', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, $sce, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.LoadingOpen = function () {
        $('.pop-up').show()
        $('.loading').show()
    };
    $scope.LoadingClose = function () {
        $('.pop-up').hide()
        $('.loading').hide()
    };

    $scope.read_bg_image = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#target_bg_image').attr('src', e.target.result);
                $(".moveBar").show();
            }
            reader.readAsDataURL(input.files[0]);
        }

    };

    /*
    $scope.loadMsg = function () {
        $(document).scroll(function () {
            if ($(document).height() > $(window).height() * 2) {
                var scrollVal = $(this).scrollTop(),
                    docHeight = $(document).height() - 1000;

                if (scrollVal > docHeight) {
                    $(".right button.more").click()
                }
            }
        })
    };
*/
    $scope.$watch(function () { return TmpObj.page_num; }, function (newVal) {
        if (newVal != null && newVal != undefined) {
            if (TmpObj.old_page_num != null && TmpObj.old_page_num != undefined && TmpObj.old_page_num != newVal) {
                TmpObj.old_page_num = newVal;
                $scope.loadMsg();
            }
        }
    });

    $scope.loadMsg = function () {
        $(document).scroll(function (scrollBarA) {
            if ($(document).height() > $(window).height() * 2) {
                var scrollVal = $(this).scrollTop(),
                    docHeight = $(document).height() - 800;

                if (scrollVal > docHeight) {
                    console.log(TmpObj.page_num);
                    $(".right button.more").click();
                    $(document).unbind(scrollBarA)
                }
            }
        })


    };


    $scope.files_bg_image = function () {
        $("#files_bg_image").change(function () {
            console.log(this);
            $scope.read_bg_image(this);
            $("#target_bg_image").css("top", "12vw")
        });
    }


    //main
    var data = {};
    data.sid = window.localStorage.sid;
    $scope.LoadingOpen();
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($rs);
            //return;
            if ($rs.data[0].f_backend != null) {
                TmpObj.board_user_id = $scope.seedata.data["0"].f_backend.user_id;
                TmpObj.friends_user_id = $scope.seedata.data["0"].f_backend.user_id;

                if ($rs.data[0].f_backend.identity == 'teach') {
                    data.sid = window.localStorage.sid
                    return TmpValue.post(APIForder + "?service=User.TeacherEdit", data);
                } else {
                    location.href = 'Index';
                }
            } else {
                location.href = 'Index';
            }
        }).then(function ($rs) {
            $scope.profile_data = $rs;
            console.log($scope.profile_data);
            return TmpValue.post(APIForder + "?service=User.TeacherAptRecord", data);
        }).then(function ($rs) {
            data.sid = window.localStorage.sid
            $scope.apt_data = $rs;
            console.log($scope.apt_data);
            return TmpValue.post(APIForder + "?service=User.StudentAptRecord", data);
        }).then(function ($rs) {
            data.sid = window.localStorage.sid
            $scope.student_apt_data = $rs;
            //$('.pop-up').hide()
            //$('.loading').hide()
        }).then(function ($rs) {
            /*
            if ($scope.seedata.data["0"].f_backend == null) {
                angular.element(document).ready(function () {
                    $(".member").click();
                    //$scope.member();
                });
            }
            */
        });

    $scope.$watch(function () { return TmpObj.WaitFriendsList; }, function (newVal) {
        if (newVal != null && newVal != undefined) {
            $scope.WaitFriendsList = TmpObj.WaitFriendsList;
        }
        TmpObj.WaitFriendsList = null;
    });


    //pub func

    $scope.initL = function () {
        var offset = $('#numbers li').length;

        $('#numbers').endlessScroll({
            fireOnce: false,
            fireDelay: false,
            loader: '',
            insertAfter: '#numbers li:last',
            content: function (i) {
                alert(0)
                return '<li>' + (i + offset) + '</li>';
            }
        });
    };



    $scope.AgreeInvFan = function (YesorNo) {
        var error_msg = "";
        if (YesorNo != 'N' && YesorNo != 'Y') {
            error_msg = error_msg + "Yes or No";
        }
        if (error_msg != '') {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        $scope.key = this.key_this_fan_invite_notice;
        data.id = this.this_fan_invite_notice.mang_id;
        data.approval = YesorNo;
        $('#FanInviteNoticeItem').eq($scope.key).css("opacity", "0.5").css("pointer-events", "none");
        TmpValue.post(APIForder + "?service=Fan.AgreeInvFan", data)
            .then(function ($rs) {
                if ( $rs.ret != 200 ) return $('#FanInviteNoticeItem').eq($scope.key).css("opacity", "1").css("pointer-events", "auto");
                $scope.seedata.data[1].fan_invite_notice.splice($scope.key, 1);
                $('#FanInviteNoticeItem').eq($scope.key).hide();
                if ($scope.seedata.data[1].user_friends_notice.length == '0')
                    $('#FanInviteNoticeList').eq($scope.key).hide();
                $scope.key = null;
            });
    };

    $scope.ReadFriendsNote = function (id, user_id) {
        $scope.key = this.key_user_friends_notice;
        console.log(this.key_user_friends_notice);
        //return;
        data.id = id;
        data.user_id = user_id;
        TmpValue.post(APIForder + "?service=User.ReadFriendsNote", data)
            .then(function ($rs) {
                //console.log($rs);
                // console.log($scope.seedata.data[1].user_friends_notice.length);
                $scope.seedata.data[1].user_friends_notice.splice($scope.key, 1);
                $('.friend-apply').eq($scope.key).hide();
                if ($scope.seedata.data[1].user_friends_notice.length == '0')
                    $('.friend-msg').eq($scope.key).hide();
                $scope.key = null;
            });
    };

    $scope.ReadSysNote = function (id, user_id) {
        $scope.key = this.key_this_sys_notice;
        //return;
        data.id = id;
        data.user_id = user_id;
        TmpValue.post(APIForder + "?service=User.ReadSysInviteNotice", data)
            .then(function ($rs) {
                console.log($rs);
                $scope.seedata.data[1].sys_notice.splice($scope.key, 1);
                $('.friend-apply').eq($scope.key).hide();
                if ($scope.seedata.data[1].sys_notice.length == '0')
                    $('.friend-msg').eq($scope.key).hide();
                $scope.key = null;
            });
    };

    $scope.openMsgToggle = function (data) {
        $(".msg_warp:eq(" + this.$index + ")").toggleClass('pack-up');
    };

    $scope.TeacherwhyCancelBooking = function (data) {
        $scope.data = {};
        $scope.message = {};
        $scope.tmpfun = "teacher";
        $scope.data.can_tutor_time = this.apt_data1.can_tutor_time
        $scope.data.lesson_dt_id = this.apt_data1.id
        $scope.data.lesson_id = this.apt_data1.lesson_id
        $scope.message.user_id = this.apt_data1.student_image_id
        $('.pop-up').show()
        $('.why_cancel').show()
        $('body').css('overflow', 'hidden')
    };

    $scope.StudentwhyCancelBooking = function (data) {
        $scope.data = {};
        $scope.message = {};
        $scope.tmpfun = "student";
        $scope.data.can_tutor_time = this.apt_data1.can_tutor_time
        $scope.data.lesson_dt_id = this.apt_data1.id
        $scope.data.lesson_id = this.apt_data1.lesson_id
        $scope.message.user_id = this.apt_data1.teach_id
        console.log($scope.data);
        console.log($scope.message);
        $('.pop-up').show()
        $('.why_cancel').show()
        $('body').css('overflow', 'hidden')
    };

    $scope.LessonCancel = function (data) {
        if ($scope.tmpfun == 'teacher')
            $scope.TeacherLessonCancel();
        else if ($scope.tmpfun == 'student')
            $scope.StudentLessonCancel();
    };

    $scope.TeacherLessonCancel = function (data) {
        var error_msg = "";
        console.log(data);
        if ($scope.message == undefined)
            error_msg += "reason of cancellation\n";
        else if ($scope.message.cause == undefined || $scope.message.cause.trim() == '')
            error_msg += "reason of cancellation\n";

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        var data = {};
        data = $scope.data;
        //teacher cancel lesson
        $('.why_cancel').hide()
        $('.loading').show()
        TmpValue.post(APIForder + "?service=Course.TeacherLessonCancel", data)
            .then(function ($rs) {
                $('.loading').hide()
                console.log($rs);
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        //alert($rs.data['0'].msg_text);
                        return TmpValue.post(APIForder + "?service=User.TeacherAptRecord", data);
                    }
                } else {
                    return false;
                }
            }).then(function ($rs) {
                $scope.apt_data = $rs;
                $scope.MessageCreateGroup($scope.message.user_id);
            }).then(function () {

                $('.pop-up').hide()
                $('.why_cancel').hide()
                $('body').css('overflow', 'auto')
            });
        //end teacher cancel lesson
    };

    $scope.StudentLessonCancel = function (data) {
        var error_msg = "";
        console.log(data);
        if ($scope.message == undefined)
            error_msg += "reason of cancellation\n";
        else if ($scope.message.cause == undefined || $scope.message.cause.trim() == '')
            error_msg += "reason of cancellation\n";

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }
        var data = {};
        data = $scope.data;
        //student cancel lesson
        $('.why_cancel').hide()
        $('.loading').show()
        TmpValue.post(APIForder + "?service=Course.StudentLessonCancel", data)
            .then(function ($rs) {
                $('.loading').hide()
                console.log($rs);
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        //alert($rs.data['0'].msg_text);
                        return TmpValue.post(APIForder + "?service=User.StudentAptRecord", data);
                    }
                } else {
                    return false;
                }
            }).then(function ($rs) {
                $scope.student_apt_data = $rs;
                $scope.MessageCreateGroup($scope.message.user_id);
            }).then(function () {
                $('.pop-up').hide()
                $('.why_cancel').hide()
                $('body').css('overflow', 'auto')
            });
        //end student cancel lesson
    };


    //message send fun
    $scope.MessageCreateGroup = function (user_id) {
        var data = {};
        data.users_id = user_id;
        TmpValue.post(APIForder + "?service=Message.createGroup", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    $scope.MessageSendMessage($rs.data["0"].id);
                } else {
                    return false;
                }
            })
    };

    $scope.MessageSendMessage = function (groups_id) {
        var data = {};
        data.message_text = 'reason of cancellation:' + $scope.message.cause;
        data.groups_id = groups_id;
        TmpValue.post(APIForder + "?service=Message.sendMessage", data)
            .then(function ($rs) {
                if ($rs.ret == 200) { } else {
                    return false;
                }
            })
    };
    //end message send fun

    $scope.leftBoxShowMore = function () {
        $('.left_group').slideToggle(150);
    };

    $scope.blackBgShow = function () {
        $('.blackBg').show();
        $('.po-msg-box .title .fa').show();
        $(".po-msg-box").css("z-index", "15")
    };
    // blackBgShow end

    $scope.popUpMenuShow = function () {
        var _eq = this.$index;
        $(".popupMenu").eq(_eq).addClass("showthis");
        $(".post_box").eq(_eq).addClass("zIndexUp");
        $('.blackBg').css('z-index', '13').show();
    };

    // popUpMenuShow end


    $scope.saveCut = function () {
        $('.cut_pop_up , .avatar_cut , .banner_cut').hide()
        $('body').css('overflow', 'auto')
    };
    // saveCut end

    $scope.closeCut = function () {
        $('.cut_pop_up , .avatar_cut , .banner_cut').hide()
        $('body').css('overflow', 'auto')
    };
    // closeCut end
    $scope.showCutAvatar = function () {
        $('.cut_pop_up , .avatar_cut').show()
        $('body').css('overflow', 'hidden')
    };
    // showCutAvatar end
    $scope.showCutBanner = function () {
        $('.cut_pop_up , .banner_cut').show()
        $('body').css('overflow', 'hidden')

    };
    // showCutBanner end

    $scope.packUp = function () {
        $(".msg_box, .key_in_box").slideToggle(0);
    };
    // packUp end
    $scope.cutbannershow = function () {
        $(".cutBannerImgBox , .moveBar , .cutBannerBTN , .up_img_files_li").show()
        $(".privatelistBtn , .moveBar , .user-img").hide()
        $(".blackBg2").show()
        //$(".pageBanner").css("z-index", "20")
        //$(".pageBanner").height("47vw")
        $(".pageBanner").addClass("cutStyle")
        //$(".banner").height("44vw")
        //$(".cutBannerImgBox").height("44vw")
        $("body , .mid").css("overflow", "hidden")

        $(".mid").css("min-height", "70vh")
        $(".modify-img").hide()
        if ($(window) > "750px") {
            $(".pgemenubtn").hide()
        }
    };
    // cutbannershow end


    // packUp end

    $scope.showThisImg = function () {
        $('.left .videos .video-box').click(function () {
            $('body').css('overflow', 'hidden')
            var $this = $(this),
                $src = $this.find('img').attr('src')

            $('.member-box').hide()
            $('.pop-up').show()
            $('.pop-up video').attr('src', $src)
            $('.pop-up img').show().attr('src', $src)
        })
    };
    // showThisImg end

    $scope.showAll = function ($event) {
        $('.c_wrap').toggleClass('showAll')
        var _this = $($event.target);
        if (_this.text() == 'Show All') {
            _this.text('Close')
        } else {
            _this.text('Show All')
        }
    };

    $scope.showAllFriends = function ($event) {
        var _this = $($event.target);
        _this.parents(".friend-msg").toggleClass("showAll")
        if (_this.text() == 'Show All') {
            _this.text('Close')
        } else {
            _this.text('Show All')
        }
    }

    $scope.showAllMsg = function ($event) {
        var _this = $($event.target);
        $('.hasNewMsg').toggleClass('showAll')
        if (_this.text() == 'Show All') {
            _this.text('Close')
        } else {
            _this.text('Show All')
        }

    };

    // top header menu 
    $scope.member = function () {
        angular.element(document).ready(function () {
            $('body').css('overflow', 'hidden');
            $(".pop-up").show();
            $('.login-box').show();
        });

    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo , .joinlivePopup , .nowUpImage').hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
        $(".v_iBox img").attr("src", "")
        $(".blackBg, .po-msg-box span.title .fa").hide()
    };
    // colsePopUp end

    //colseThisMsg
    $scope.colseThisMsg = function () {
        $(".upVidepFinish").fadeOut()
    }
    //colseThisMsg END

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end
    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // showPrivateBox end

    $scope.saveCutBanner = function () {
        $(".cutBannerImgBox , .moveBar , .cutBannerBTN").hide();
        $(".privatelistBtn").show();
    };

    $scope.closeCutBanner = function () {
        $(".cutBannerImgBox , .moveBar , .cutBannerBTN , .up_img_files_li").hide();
        $(".privatelistBtn , .user-img").show();
        $(".blackBg2").hide()
        $(".pageBanner").removeClass("cutStyle")
        //$(".pageBanner").css("z-index", "0")
        //$(".pageBanner").height("20vw")
        //$(".banner").height("20vw")
        //$(".cutBannerImgBox").height("20vw")
        $("body").css("overflow", "auto")
        $(".mid").css("max-height", "initial")
        $(".mid").css("min-height", "79vh")
        $(".pgemenubtn , .modify-img").show()
        $("#holder img").attr("src", "")
        $("#files_bg_image").val("")
    };


    // top header menu end

    // left position fixed
    $scope.leftBoxFixed = function () {
        var position = $('.leftFixed').offset();
        var y = position.top
        if ($(window).width() > 1024) {
            $(document).scroll(function () {
                var leftHeight = $(".leftFixed").innerHeight(),
                    newY = leftHeight - y + $("header").innerHeight()
                if ($(".leftFixed").height() > $(window).height()) {
                    if ($("body").scrollTop() > newY) {
                        $(".leftFixed").addClass("boxFixed")
                    } else {
                        if ($(".leftFixed").height() > $(window).height()) {
                            var _top = $(window).height() - $(".leftFixed").height() + $("header").innerHeight()
                            $(".leftFixed").css("top", _top)
                        }
                        if ($("body").scrollTop() <= newY) {
                            $(".leftFixed").removeClass("boxFixed")
                        }
                    }
                    if ($("body").scrollTop() > $(document).height() - ($(window).height() + $("footer").innerHeight() + 20)) {
                        var _top = $(window).height() - ($(".leftFixed").height() + $("footer").innerHeight() + 100)
                        $(".leftFixed").css("top", _top)
                    } else {
                        var _top = $(window).height() - ($(".leftFixed").height() + $("header").innerHeight())
                        $(".leftFixed").css("top", _top)
                    }
                } else {

                    if ($("body").scrollTop() <= $('.right').offset().top) {
                        $(".leftFixed").removeClass("boxFixed")
                    } else {
                        if ($(".leftFixed").height() < $(window).height()) {
                            if ($("body").scrollTop() > newY) {
                                $(".leftFixed").addClass("boxFixed")
                            } else {
                                if ($(".leftFixed").height() < $(window).height()) {
                                    var _top2 = $("header").innerHeight()
                                    $(".leftFixed").css("top", _top2)
                                }
                                if ($("body").scrollTop() <= newY) {
                                    $(".leftFixed").removeClass("boxFixed")
                                }
                            }
                        }
                    }
                }



            })
        }
    };
    // left position fixed END

    //zoom_open
    $scope.tmp_zoomopen = function () {
        function PostJson(url, data) {
            var deferred = $q.defer();
            $http.post(url, data).success(function (d) {
                deferred.resolve(d);
            });
            return deferred.promise;
        }

        var data = {};
        TmpValue.post(APIForder + "?service=User.ZoomOpen", data)
            .then(function ($rs) {
                $scope.tmp_zoom = $rs;
                $('#zoom_link1').attr("href", $scope.tmp_zoom.data["0"].result.data.join_url);
            });

    };
    //end zoomopen

    // live video
    $scope.liveVideo = function () {
        $(".livePopup").show();
        $("body").css("overflow", "hidden");

        // 判斷 瀏覽器
        var userAgent = navigator.userAgent;
        var isChrome = userAgent.indexOf("Chrome") > -1;
        //var video = document.createElement('video');

        if (isChrome) {
            //var videoObj = { "video": true };
            if (1) {
                //if (navigator.getUserMedia) {
                // Put video listeners into place
                // Standard
                //$(".camera").css("display", "flex")// show 提示視窗顯示
                //$(".camera .camera_ready").show()// show 偵測到鏡頭提示
                /*
                navigator.getUserMedia(videoObj, function () {
                    //關閉鏡頭
                    //$(".goLive").hide();
                    //$("#highest").hide();

                    navigator.getUserMedia(videoObj, function () {
                        //alert(1);
                        $(".camera").css("display", "flex")// show 提示視窗顯示
                        $(".camera .camera_ready").show()// show 偵測到鏡頭提示
                        //$(".goLive").show()// hide 解析度 直撥按鈕
                        //$("#highest").show();
                        //$(".bottomBox select , .bottomBox button").show()// hide 解析度 直撥按鈕
                    }, function () {
                    });

                }, function () {
                    $(".camera").css("display", "flex")// show 提示視窗顯示
                    $(".camera .no_camera").show()// show 沒有鏡頭提示
                    $(".bottomBox select , .bottomBox button").hide()// hide 解析度 直撥按鈕
                });
                */

            } else {
                $(".camera").css("display", "flex") // show 提示視窗顯示
                $(".camera .no_camera").show() // show 沒有鏡頭提示
                $(".bottomBox select , .bottomBox button").hide() // hide 解析度 直撥按鈕 
            }
        } else {
            $(".bottomBox select , .bottomBox button").hide()
            $(".notchrome").css("display", "flex")
        }


    }


    // browser

    // joinLive
    $scope.joinLive = function (live_id) {
        console.log(this.$index);
        $('.joinlivebtn:eq(' + this.$index + ')').parents(".user-msg-box").addClass("joinlivePopup")
        $(".joinlivePopup , .closeJoinLive").show()
        if (live_id != null) {
            angular.element(document.getElementById('CommBoard')).scope().StartUserChatRoom('userchatroom', live_id);
        }
        $("body").css("overflow", "hidden")
    }
    // closeJoinLive
    $scope.closeJoinLive = function () {
        $('#userchatroom').html('');
        $(".user-msg-box").removeClass("joinlivePopup");
        $(".joinlivePopup , .closeJoinLive").hide();
        $("body").css("overflow", "auto")
    }


    $scope.goLive = function () {
        $(".livePopup .live .video ,  .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").removeClass("set");
        angular.element(document.getElementById('CommBoard')).scope().CreateLive();
        $('#self_iframe').find('video').muted = true;
        //前端load live 
        $(".camera").hide();
        $(".liveIframeBox").show();

        $(window).bind("beforeunload", function (event) {
            angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id);
            return '';
        });

    }; //開始直播

    $scope.hideLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").hide();
        $(".showIframe").show();
    }
    $scope.showLiveIframe = function () {
        $(".liveIframeBox iframe , .hideIframe").show();
        $(".showIframe").hide();
    }

    $scope.closeLive = function () {
        $(".livePopup , .joinlivePopup").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    };

    $scope.finishLive = function () {
        Alertify.confirm('End live video ?')
            .then(function () {
                if (TmpObj.tmp.stream_key != undefined)
                    angular.element(document.getElementById('CommBoard')).scope().StartLive(TmpObj.tmp.stream_key, 'N', TmpObj.tmp.live_id);
                else
                    window.reload();
                //Alertify.success('You are sure!');
            }, function () {
                return;
            });

        $(".finishText .spet1").css("display", "flex")
        //$(".finishText .spet2").css("display", "flex")
        //$(".finishLive").addClass("set")
    }; // 結束直撥流程 1.先顯示 spet1 loading 流程 loading 完後請 隱藏 spet1 顯示 spet2

    $scope.doneBtn = function () {
        $(".livePopup").hide()
        $(".finishText .spet1 , .finishText .spet2").hide()
        $("body").css("overflow", "auto")
        $(".livePopup .live .video , .livePopup .bottomBox select , .livePopup .live .video .liveTitle , .livePopup .bottomBox button.goLive , .livePopup .live .talkBox , .finishLive , .livePopup .live .video .nowislive , .closeLive").addClass("set")
    }; //  spet2 按鈕 儲存影片
    //end live

    // calendar
    $scope.calendarEvent = function () {
        $(".calendar ul li > div").each(function () {
            if ($(this).find(".newEvent div").length > 4) {
                $(this).find(".moreEvent").show()
            }
        })
        $(".calendar ul li > div").click(function () {
            $(".calendarDayBox").addClass("openDetailed")
            $(".calendarDetailed").show()
            if ($(this).find("div").hasClass("event")) {
                $(".allEventBox").show()
                $(".eventBox .subTitle").show()
                $(".editDetailed").hide()
            } else {
                $(".allEventBox").hide()
                $(".editDetailed").show()
            }

        })
        $(".eventBox .subTitle").click(function () {
            $(".eventBox .subTitle").toggle()
            $(this).show()
            $(this).parent().find(".content").toggle()
        })
    }


    $scope.colseC_D = function () {
        $(".calendarDayBox").removeClass("openDetailed")
        $(".calendarDetailed").hide()
    }
    $scope.closeE_D = function () {
        $(".eventBox .subTitle").show()
        $(".eventBox .content").hide()
        $(".editDetailed").hide()
    }
    $scope.editE_D = function () {
        $(".eventBox .subTitle").hide()
        $(".eventBox .content").hide()
        $(".editDetailed").show()
    }

    // calendar END
    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "TeacherMyProfile.htm";
    //$scope.ContentHtml = $routeParams.Action+".htm";
});

appH5.controller('UserStudentEdit', function ($scope, $routeParams, $q, $http, APIForder, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            console.log($scope.seedata);
            if ($rs.data[0].f_backend != null) {
                return TmpValue.post(APIForder + "?service=User.StudentEdit", data);
            } else {
                Alertify.alert('login Error3');
                //location.href = 'User.Login';
            }

        })
        .then(function ($rs) {
            $scope.profile_data = $rs;
            console.log($scope.profile_data);
            return TmpValue.post(APIForder + "?service=User.CanTutorTime", data);
        }).then(function ($rs) {
            $scope.can_tutor_time_data = $rs;
            //foreach($scope.can_tutor_time_data.tmp_list )
            for (var key in value = $scope.can_tutor_time_data.data['0'].tmp_list) {
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time'] = value[0]['can_tutor_time'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time1'] = value[0]['can_tutor_time1'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time2'] = value[0]['can_tutor_time2'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time3'] = value[0]['can_tutor_time3'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time4'] = value[0]['can_tutor_time4'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time5'] = value[0]['can_tutor_time5'].split(",");
                $scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time6'] = value[0]['can_tutor_time6'].split(",");
            }

            $scope.can_tutor_time = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time'])
            $scope.can_tutor_time1 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time1'])
            $scope.can_tutor_time2 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time2'])
            $scope.can_tutor_time3 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time3'])
            $scope.can_tutor_time4 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time4'])
            $scope.can_tutor_time5 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time5'])
            $scope.can_tutor_time6 = angular.copy($scope.can_tutor_time_data.data['0'].tmp_list['0']['can_tutor_time6'])

            console.log($scope.can_tutor_time_data);
        });

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "TeacherEdit.htm";

    $scope.changeTimeEvent = function (obj, index) {
        var $this = $(obj.target)
        if ($this.parent().attr("class") != "week") {
            if ($this.attr("class") != "time") {
                $this.toggleClass("hasChange")


                if ($this.hasClass("open")) {
                    $this.removeClass("open")
                    $this.find('span').text("CLOSE")
                    $this.find('input').prop("checked", false)
                    var idx = $scope['can_tutor_time' + index].indexOf($this.find('input').val());
                    if (typeof $this.find('input').val() === 'undefined') {
                        return;
                    }
                    if (idx > -1) {
                        $scope['can_tutor_time' + index].splice(idx, 1);
                    }

                } else {
                    $this.addClass("open")
                    $this.find('span').text("OPEN")
                    $this.find('input').prop("checked", true)
                    if (typeof $this.find('input').val() === 'undefined') {
                        return;
                    }
                    $scope['can_tutor_time' + index].push($this.find('input').val());
                }
            }
        }
    }

    $scope.submit_up = function (formdata) {

        var data = {
            start_date: null,
            end_date: null,
            can_tutor_time: null,
            can_tutor_time1: null,
            can_tutor_time2: null,
            can_tutor_time3: null,
            can_tutor_time4: null,
            can_tutor_time5: null,
            can_tutor_time6: null
        }

        var dataEdit = {
            phone: null,
            nationality: null,
            birth_day: null,
            gender: null,
            school_name: null,
            email: null,
            nick_name: null,
            text: null
        }
        dataEdit.phone = $scope.profile_data.data['0'].tmp_list['0'].phone
        dataEdit.nationality = $scope.profile_data.data['0'].tmp_list['0'].nationality
        dataEdit.birth_day = $scope.profile_data.data['0'].tmp_list['0'].birth_day
        dataEdit.gender = $scope.profile_data.data['0'].tmp_list['0'].gender
        dataEdit.school_name = $scope.profile_data.data['0'].tmp_list['0'].school_name
        dataEdit.email = $scope.profile_data.data['0'].tmp_list['0'].email
        dataEdit.text = $scope.profile_data.data['0'].tmp_list['0'].text

        for (var i = 0; i < 7; i++) {
            if (i == 0) {
                data.start_date = $('#start_date').val();
                data.end_date = $('#end_date').val();
                data.can_tutor_time = $scope['can_tutor_time'];
                //console.log($scope['can_tutor_time']);
            } else {
                data["can_tutor_time" + i] = $scope['can_tutor_time' + i];

                // console.log($scope['can_tutor_time'+i]);
            }

        }
        /*
                console.log(data);
                return false;*/
        TmpValue.post(APIForder + "?service=User.UserEditUP", dataEdit)
            .then(function ($rs) {
                //console.log($rs);
                //return false;
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        return TmpValue.post(APIForder + "?service=User.CanTutorTimeUP", data);
                    } else {
                        Alertify.alert($rs.data[0].msg_text);
                    }
                } else {
                    Alertify.alert($rs.msg);
                }
            })
            .then(function ($rs) { //CanTutorTimeUP
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        //alert($rs.data[0].msg_text);
                        if ($scope.seedata.data[0].f_backend.identity == 'student') {
                            location.href = 'User.StudentMyProfile';
                        } else {
                            location.href = 'User.TeacherMyProfile';
                        }
                    } else {
                        Alertify.alert($rs.data[0].msg_text);
                    }
                } else {
                    Alertify.alert($rs.msg);
                }
            });

    }


    //$scope.ContentHtml = $routeParams.Action+".htm";



});

appH5.controller('UserStudentEditUPCtrl', function ($scope, $routeParams, $q, $http, APIForder, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }


    $scope.submit = function () {
        var data = {};

        data.info_p_name = $scope.profile_data.data[0].tmp_list[0].info_p_name;
        data.phone = $scope.profile_data.data[0].tmp_list[0].phone;
        data.info_p_email = $scope.profile_data.data[0].tmp_list[0].info_p_email;

        TmpValue.post(APIForder + "?service=User.GetSession", data)
            .then(function ($rs) {
                $scope.seedata = $rs;
                //console.log($scope.seedata);
                if ($rs.data[0].f_backend.identity == 'student')
                    return TmpValue.post(APIForder + "?service=User.StudentEditUP", data);
                else
                    Alertify.alert('login Error 2');
            }).then(function ($rs) {
                if ($rs.data[0].msg_state == 'Y') {
                    Alertify.alert($rs.data[0].msg_text);
                }
            });
    };
});

appH5.controller('UserHomeCtrl', function ($scope, $routeParams, $q, $http, APIForder) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            //console.log($scope.seedata);
            if ($rs.data[0].f_backend.identity == 'student')
                location.href = 'User.StudentMyProfile';
            else if ($rs.data[0].f_backend.identity == 'teach')
                location.href = 'User.StudentMyProfile';
            else
                location.href = 'Index';
        })


    $scope.message = "Hello";
    $scope.InSidebar = "N";
    //ajax 確認是否登入是的話導入 學生頁 不是導入登入頁
    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "Login.htm";
});

appH5.controller('UserViewListCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }
    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
            $scope.Search();
        });
    //func
    $scope.Search = function (data) {
        var data = {};
        if ($routeParams.text == undefined) {
            $scope.teacher_data_empty = 1;
            return;
        } else {
            data.text = $routeParams.text;
        }

        TmpValue.post(APIForder + "?service=User.UserList", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.data[0].msg_state == 'Y') {
                    $scope.teacher_data = $rs;
                } else {
                    Alertify.alert($rs.data[0].msg_text);
                }
            });
    };

    // top header menu 
    $scope.member = function () {
        $('body').css('overflow', 'hidden');
        $(".pop-up").show();
        $('.login-box').show();
    };
    // menber end 

    $scope.colsePopUp = function () {
        $('.booking , .pop-up , .v_iBox , .v_iBox img , .member-box , .recommend-box , .nowUpVideo , .joinlivePopup').hide();
        $('body').css('overflow', 'auto');
        $('.pop-up video').hide().attr('src', '');
        $('.mid').css('height', 'auto');
        $('.mid').css('overflow', 'auto');
        $(".v_iBox img").attr("src", "")
    };
    // colsePopUp end

    $scope.langChange = function () {
        $('.lang-box').slideToggle(0);
    };
    // langChange end
    // langBoxHide
    $scope.langBoxHide = function () {
        $(document).scroll(function () {
            $('.lang-box').fadeOut()
        });
    };
    // langBoxHide end

    $scope.forgetPassword = function () {
        $('.member-box').hide();
        $('.forget-box').show();
    };
    // forgetPassword end

    $scope.noAccount = function () {
        $('.member-box').hide();
        $('.register-box').show();
    };
    // noAccount end

    $scope.backLogin = function () {
        $('.login-box').show();
        $('.register-box , .forget-box').hide();
    };
    // backLogin end

    // top header menu end

    $scope.userSearchText = function () {
        unwatch = $scope.$watch(function () { return $("#inputTarget").val(); }, function (newVal) {
            if (newVal != null && newVal != undefined) {
                $(".userSearchText").text(newVal);
                //清除監聽
                unwatch();
            }
        });
    }

    $scope.ContentForder = "templates/User/";
    $scope.ContentHtml = "ViewList.htm";

});

appH5.controller('CorpCtrl', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    //func
    $scope.fileChanged = function (evt) {
        /*
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
        */
        /*
        var files = e.target.files;
        var fileReader = new FileReader();
        fileReader.readAsDataURL(files[0]);

        fileReader.onload = function (e) {
            $scope.imgSrc = this.result;
            $scope.$apply();
        };
        */

    }
    $scope.imgSrc = '';
    $scope.myCroppedImage = '';
    var handleFileSelect = function (evt) {

        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.imgSrc = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#TmpphotoFile')).on('change', handleFileSelect);

    $scope.clear = function () {
        $('#TmpphotoFile').val('');
        $('#TmpbgphotoFile').val('');
        $scope.imageCropStep = 1;
        delete $scope.imgSrc;
        delete $scope.result;
        delete $scope.resultBlob;
    };

});




function uploadUserPhoto(imgType, resultBlob) {
    var url = "";
    var image = "";
    var filename = "";
    if (imgType == 'bg') {
        image = $('#imguserbg64').attr('src');
        filename = $('#TmpbgphotoFile').val();
        url = "?service=User.BgPhotoUpload";
    } else {
        image = $('#imguserphoto64').attr('src');
        filename = $('#TmpphotoFile').val();
        url = "?service=User.PhotoUpload";
    }

    var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
    var blob = base64ToBlob(base64ImageContent, 'image/png');
    var formData = new FormData();

    formData.append('photoFile', blob, filename);
    //console.log(blob);
    //return;

    $.ajax({
        url: APIForder + url,
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        console.log(res);
        //var msg = alert('Upload Success');
        window.location.reload();
    }).fail(function (res) { });

}


function uploadUserBgPhoto(imgType) {
    var url = "";
    var imbg_image_styleage = "";
    var filename = "";
    var file = "";
    bg_image_style = $('#bgHeight').val();
    filename = $('#files_bg_image').val();
    file = $('#files_bg_image')["0"].files["0"];
    url = "?service=User.BgPhotoUpload";
    var formData = new FormData();

    formData.append('photoFile', file);
    formData.append('bg_image_style', bg_image_style);
    //console.log(blob);
    //return;

    $.ajax({
        url: APIForder + url,
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        console.log(res);
        //var msg = alert('Upload Success');
        window.location.reload();
    }).fail(function (res) { });
    $(".pgemenubtn , .modify-img").show()
}

function setbgHeight() {
    var url = "";
    var imbg_image_styleage = "";

    bg_image_style = $('#setbgHeight').val();
    url = "?service=User.SetBgHeight";
    var formData = new FormData();
    formData.append('bg_image_style', bg_image_style);
    //console.log(blob);
    //return;

    $.ajax({
        url: APIForder + url,
        type: 'POST',
        cache: false,
        data: formData,
        processData: false,
        contentType: false
    }).done(function (res) {
        console.log(res);
        //var msg = alert('Upload Success');
        window.location.reload();
    }).fail(function (res) { });

}


function base64ToBlob(base64, mime) {
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {
        type: mime
    });
}