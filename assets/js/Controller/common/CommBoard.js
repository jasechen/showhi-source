appH5.controller('CommBoard', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue, $sce, Alertify) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }

    var data = {};
    TmpValue.post(APIForder + "?service=User.GetSession", data)
        .then(function ($rs) {
            $scope.seedata = $rs;
        });


    $scope.trustSrc = function (url) {
        return $sce.trustAsResourceUrl(url);
    }

    $scope.MessageCreateGroup = function (user_id) {
        console.log(user_id);
        var data = {};
        data.users_id = user_id;
        TmpValue.post(APIForder + "?service=Message.createGroup", data)
            .then(function ($rs) {
                console.log('1234');
                console.log($rs);
                if ($rs.ret == 200 && typeof $rs.data["0"].datas != 'undefined') {
                    //angular.element(document.getElementById('msgProcController')).scope().$apply(function () {
                    angular.element(document.getElementById('msgProcController')).scope().openPrivateMsgBox($rs.data["0"].datas);
                    //});

                } else {
                    return false;
                }
            })
    };

    //Board all
    //edit
    $scope.tmp_edit_img = [];
    $scope.formeditDataArr = [];
    $scope.formeditDataArrFilename = [];
    $scope.tmp_edit_size = 0;

    $scope.board_data_tmp_edit_img = [];
    $scope.board_data_formeditDataArr = [];
    $scope.board_data_formeditDataArrFilename = [];
    $scope.board_data_tmp_edit_size = 0;

    $scope.read_files_image = function (input) {
        console.log(input);
        var i = 0;
        var k = 0;
        $scope.tmp_img = [];
        $scope.formDataArr = [];
        $scope.formDataArrFilename = [];
        $scope.tmp_size = 0;

        $scope.board_data_tmp_img = [];
        $scope.board_data_formDataArr = [];
        $scope.board_data_formDataArrFilename = [];
        $scope.board_data_tmp_size = 0;


        for (; i < input.files.length; i++) {
            var files = input.files; //FileList object
            var file = input.files[i];
            if (!file.type.match('image')) continue;

            /*
            $scope.formDataArr.push(file);
            $scope.tmp_img[i] = angular.copy(URL.createObjectURL(file));
            $scope.formDataArrFilename.push(file.name);
            $scope.tmp_size = $scope.tmp_size + file.size;
            */
            console.log($scope.edit_data_key);
            if ($scope.edit_data_key == null || $scope.edit_data_key == undefined) {
                $scope.formDataArr.push(file);
                $scope.tmp_img[i] = angular.copy(URL.createObjectURL(file));
                $scope.formDataArrFilename.push(file.name);
                $scope.tmp_size = $scope.tmp_size + file.size;
            } else {
                $scope.formeditDataArr.push(file);
                $scope.tmp_edit_img.push(angular.copy(URL.createObjectURL(file)));
                $scope.formeditDataArrFilename.push(file.name);
                $scope.tmp_edit_size = $scope.tmp_size + file.size;
            }

            $scope.$apply();

            //$scope.formDataArr[i].filename=file.name;

            // picReader.readAsDataURL(file);
        }
        //clear input file
        $('#files_image').val('');
        $('#plusImg_fils').val('');
        $scope.CheckSize();


        //console.log($scope.formDataArr);
    }

    $scope.read_board_data_files_image = function (input) {
        console.log(input);
        var i = 0;
        var k = 0;
        $scope.board_data_tmp_img = [];
        $scope.board_data_formDataArr = [];
        $scope.board_data_formDataArrFilename = [];
        $scope.board_data_tmp_size = 0;


        for (; i < input.files.length; i++) {
            var files = input.files; //FileList object
            var file = input.files[i];
            if (!file.type.match('image')) continue;

            /*
            $scope.formDataArr.push(file);
            $scope.tmp_img[i] = angular.copy(URL.createObjectURL(file));
            $scope.formDataArrFilename.push(file.name);
            $scope.tmp_size = $scope.tmp_size + file.size;
            */
            console.log($scope.edit_data_key);
            if ($scope.edit_data_key == null || $scope.edit_data_key == undefined) {
                $scope.board_data_formDataArr.push(file);
                $scope.board_data_tmp_img[i] = angular.copy(URL.createObjectURL(file));
                $scope.board_data_formDataArrFilename.push(file.name);
                $scope.board_data_tmp_size = $scope.board_data_tmp_size + file.size;
            } else {
                $scope.board_data_formeditDataArr.push(file);
                $scope.board_data_tmp_edit_img.push(angular.copy(URL.createObjectURL(file)));
                $scope.board_data_formeditDataArrFilename.push(file.name);
                $scope.board_data_tmp_edit_size = $scope.board_data_tmp_edit_size + file.size;
            }

            $scope.$apply();

            //$scope.formDataArr[i].filename=file.name;

            // picReader.readAsDataURL(file);
        }
        //clear input file
        $('#files_image').val('');
        $('#plusImg_fils').val('');
        $scope.CheckSize();


        //console.log($scope.formDataArr);
    }

    $scope.read_edit_files_image = function (input) {
        var i = 0;
        var k = 0;

        //edit
        $scope.tmp_edit_img = [];
        $scope.formeditDataArr = [];
        $scope.formeditDataArrFilename = [];
        $scope.tmp_edit_size = 0;

        for (; i < input.files.length; i++) {
            var files = input.files; //FileList object
            var file = input.files[i];
            if (!file.type.match('image')) continue;

            /*
            $scope.formDataArr.push(file);
            $scope.tmp_img[i] = angular.copy(URL.createObjectURL(file));
            $scope.formDataArrFilename.push(file.name);
            $scope.tmp_size = $scope.tmp_size + file.size;
            */
            console.log($scope.edit_data_key);
            if ($scope.edit_data_key == null || $scope.edit_data_key == undefined) {
                $scope.formDataArr.push(file);
                $scope.tmp_img[i] = angular.copy(URL.createObjectURL(file));
                $scope.formDataArrFilename.push(file.name);
                $scope.tmp_size = $scope.tmp_size + file.size;
            } else {
                $scope.formeditDataArr.push(file);
                $scope.tmp_edit_img[i] = angular.copy(URL.createObjectURL(file));
                $scope.formeditDataArrFilename.push(file.name);
                $scope.tmp_edit_size = $scope.tmp_edit_size + file.size;
            }

            $scope.$apply();

            //$scope.formDataArr[i].filename=file.name;

            // picReader.readAsDataURL(file);
        }
        //clear input file
        $('#files_image').val('');
        $scope.CheckSize();


        //console.log($scope.formDataArr);
    }

    $scope.CheckSize = function () {
        if ($scope.tmp_size >= 400000000) {
            $scope.tmp_size_tobig = 'Y';
        } else {
            $scope.tmp_size_tobig = 'N';
        }
    }
    $scope.files_image = function () {
        $("#files_image").unbind().change(function () {
            //alert(1);
            $scope.RemoveVideo();
            $("#show_temp").val('image');
            $scope.read_files_image(this);
        });
        $("#show_temp").val('image');
        $scope.show_temp = 'text';
        $scope.date = new Date();
        if ($("#show_temp").val() != '')
            $scope.show_temp = $("#show_temp").val();
        return false;
    }

    $scope.board_data_files_image = function () {
        alert(1);
        $(".board_data_files_image").unbind().change(function () {
            //alert(1);
            $scope.RemoveVideo();
            $("#show_temp").val('image');
            $scope.read_files_image(this);
        });
        $("#show_temp").val('image');
        $scope.show_temp = 'text';
        $scope.date = new Date();
        if ($("#show_temp").val() != '')
            $scope.show_temp = $("#show_temp").val();
        return false;
    }

    $scope.plusImgfils = function () {
        $("#plusImg_fils").unbind().change(function () {
            $scope.read_files_image(this);
        });

        return false;
    }


    $scope.read_files_video = function (input) {
        var i = 0;
        $scope.tmp_img = [];
        $scope.formDataArr = [];
        $scope.formDataArrFilename = [];
        $scope.tmp_size = 0;
        for (; i < input.files.length; i++) {
            var files = input.files; //FileList object
            var file = input.files[i];
            if (!file.type.match('video')) continue;
            $('#target_video').attr('src', URL.createObjectURL(file));
            $scope.formDataArrFilename.push(file.name);
            $scope.tmp_size = $scope.tmp_size + file.size;

        }
        //clear input file
        $('#files_image').val('');
        $scope.CheckSize();
        //console.log($scope.formDataArr);
    }
    $scope.files_video = function () {
        $("#files_video").unbind().change(function () {
            $("#target_video").show();
            $scope.RemoveImage();
            $("#show_temp").val('video');
            //$("#files_image").prop("disabled", true);
            $scope.read_files_video(this);

        });
        $("#show_temp").val('video');
        $scope.show_temp = 'text';
        $scope.date = new Date();
        if ($("#show_temp").val() != '')
            $scope.show_temp = $("#show_temp").val();
        return false;
    }

    $scope.tRemoveImage = function (obj_name) {
        //obj_name ='board_data_';
        if ($scope.edit_data_key == null || $scope.edit_data_key == undefined) {
            $scope[obj_name + "tmp_size"] = $scope[obj_name + "tmp_size"] - $scope[obj_name + "formDataArr"][this.key_this_tmp_img].size;
            $scope.CheckSize();
            $scope[obj_name + "tmp_img"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name + "formDataArr"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name + "formDataArrFilename"].splice(this.key_this_tmp_img, 1);
        } else {
            $scope[obj_name + "tmp_eidt_size"] = $scope[obj_name + "tmp_eidt_size"] - $scope[obj_name + "formeditDataArr"][this.key_this_tmp_img].size;
            $scope.CheckSize();
            $scope[obj_name + "tmp_edit_img"].splice(this.key_this_edit_tmp_img, 1);
            $scope[obj_name + "formeditDataArr"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name + "formeditDataArrFilename"].splice(this.key_this_tmp_img, 1);
        }
    }

    /*
    $scope.board_data_tRemoveImage = function (obj_name) {
        //obj_name ='board_data_';
        if ($scope.edit_data_key == null || $scope.edit_data_key == undefined) {
            $scope[obj_name+"tmp_size"] = $scope[obj_name+"tmp_size"] - $scope[obj_name+"formDataArr"][this.key_this_tmp_img].size;
            $scope.CheckSize();
            $scope[obj_name+"tmp_img"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name+"formDataArr"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name+"formDataArrFilename"].splice(this.key_this_tmp_img, 1);
        } else {
            $scope[obj_name+"tmp_eidt_size"] = $scope[obj_name+"tmp_eidt_size"] - $scope[obj_name+"formeditDataArr"][this.key_this_tmp_img].size;
            $scope.CheckSize();
            $scope[obj_name+"tmp_edit_img"].splice(this.key_this_edit_tmp_img, 1);
            $scope[obj_name+"formeditDataArr"].splice(this.key_this_tmp_img, 1);
            $scope[obj_name+"formeditDataArrFilename"].splice(this.key_this_tmp_img, 1);
        }
    }
    */

    // see and like

    $scope.ivPopUpShow = function () {
        $(".pic_box img").unbind().click(function () {
            $("body").css("overflow", "hidden")
                //var _src = $(this).attr("src")
                //$(".pop-up , .v_iBox img").show()
                //$(".v_iBox").css("display", "flex")
                // $(".v_iBox img").attr("src", _src)
            $(this).parents(".pic_box").addClass("popUpthis")
            $(this).parents(".poimgbox").addClass("showThis")
        })
        $(".pn_btn").unbind().click(function () {
            if ($(this).hasClass("next")) {
                $(".showThis").next().addClass("showThis")
                $(".showThis img").animate({
                    "opacity": "0"
                }, 200)
                $(".showThis").eq(0).removeClass("showThis")
                $(".showThis img").animate({
                    "opacity": "1"
                }, 250)
            } else {
                $(".showThis").prev().addClass("showThis")
                $(".showThis img").animate({
                    "opacity": "0"
                }, 200)
                $(".showThis").eq(1).removeClass("showThis")
                $(".showThis img").animate({
                    "opacity": "1"
                }, 250)
            }
        })
    };


    $scope.closeClickImp = function () {
        $(".poimgbox img").animate({
            "opacity": "1"
        }, 0)
        $(".pic_box").removeClass("popUpthis")
        $(".poimgbox").removeClass("showThis")
        $("body").css("overflow", "auto")
    }


    $scope.add_board_like = function (id, key) {
        //console.log(id);
        var data = {};
        data.id = id;
        data.approval = 'Y';
        data.own = 'board';
        console.log('addlike');
        console.log(data);
        TmpValue.post(APIForder + "?service=Board.AddLike", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    $scope.BoardList.data[0].board[key].like_count++;
                    if ($scope.BoardList.data['0'].board[key].board_like_arr != undefined)
                        $scope.BoardList.data['0'].board[key].board_like_arr.splice(0, 0, $rs.data["0"]["0"])
                    else {
                        $scope.BoardList.data['0'].board[key].board_like_arr = new Array();
                        $scope.BoardList.data['0'].board[key].board_like_arr.splice(0, 0, $rs.data["0"]["0"]);
                        //$scope.BoardList.data['0'].board[key].board_like_arr.splice(0,0,$rs.data["0"]["0"])
                        //$scope.BoardList.data['0'].board[key].board_like_arr = $rs.data["0"];
                    }

                } else {
                    return false;
                }
            })

    }



    $scope.change_board_unlike = function (id, key) {
        //console.log(id);
        var data = {};
        data.id = id;
        data.approval = 'N';
        data.own = 'board';
        console.log('unlike');
        console.log(data);
        TmpValue.post(APIForder + "?service=Board.UnLike", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    $scope.BoardList.data[0].board[key].like_count--;
                    $scope.BoardList.data['0'].board[key].board_like_arr.splice(0, 1);
                    //delete $scope.BoardList.data['0'].board[key].board_like_arr;

                } else {
                    return false;
                }
            })

    }

    $scope.add_files_like = function (id, key, files_key) {
        //console.log(id);
        var data = {};
        data.id = id;
        data.approval = 'Y';
        data.own = 'files';
        console.log('addlike');
        console.log(data);

        TmpValue.post(APIForder + "?service=Board.AddLike", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    tmp_files_arr = "files_" + id + "_like_arr";
                    $scope.BoardList.data[0].board[key].file_like_count[files_key]++;
                    console.log('files_like');
                    //console.log($scope.BoardList.data['0'].board[key][tmp_files_arr]);

                    if ($scope.BoardList.data['0'].board[key][tmp_files_arr] != undefined)
                        $scope.BoardList.data['0'].board[key][tmp_files_arr].splice(0, 0, $rs.data["0"]["0"]);
                    else {
                        $scope.BoardList.data['0'].board[key][tmp_files_arr] = new Array();
                        $scope.BoardList.data['0'].board[key][tmp_files_arr].splice(0, 0, $rs.data["0"]["0"]);
                    }
                } else {
                    return false;
                }
                console.log($scope.BoardList.data['0'].board[key][tmp_files_arr]);
            })

    }

    $scope.change_files_unlike = function (id, key, files_key) {
        //console.log(id);
        var data = {};
        data.id = id;
        data.approval = 'N';
        data.own = 'files';
        console.log('unlike');
        console.log(data);

        TmpValue.post(APIForder + "?service=Board.UnLike", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    tmp_files_arr = "files_" + id + "_like_arr";
                    console.log('files_unlike');
                    console.log($scope.BoardList.data['0'].board[key]);

                    $scope.BoardList.data[0].board[key].file_like_count[files_key]--;
                    //delete $scope.BoardList.data['0'].board[key][tmp_files_arr][files_key];
                    $scope.BoardList.data['0'].board[key][tmp_files_arr].splice(0, 1);
                } else {
                    return false;
                }
                console.log($scope.BoardList.data['0'].board[key]);
            })

    }

    //end see and like

    $scope.RemoveImage = function () {
        //alert(this.$index.index())
        $scope.tmp_size = 0;
        $scope.tmp_img = [];
        $scope.formDataArr = [];
        $scope.formDataArrFilename = [];


        $("#files_image").val('')
        $("#show_temp").val('')
        $("#target_image").attr('src', '')
        $("#files_video").prop("disabled", false);
    }

    $scope.RemoveVideo = function () {
        $("#files_video").val('')
        $("#show_temp").val('')
        $("#target_video").attr('src', '')
        $("#target_video").hide();
        $("#files_image").prop("disabled", false);
    }

    $scope.BoardChangeRead = function (fan_id) {
        fan_id = fan_id || null ;
        var data = {};
        data.read_permi = this.this_BoardList.read_permi;
        data.id = this.this_BoardList.id;

        if (fan_id != '') {
            data.fan_id = fan_id;
        }

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 800);

        TmpValue.post(APIForder + "?service=Board.BoardChangeRead", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        //alert('Change permission' + data.read_permi + ' done.');
                        //$scope.FuncBoardList($scope.teacher_tmp_arr['0']['id']);
                    }
                } else {
                    return false;
                }
            });
    };


    $scope.FuncBoardList = function (user_id, page_num) {
        data = {};
        data.user_id = user_id;
        page_num = page_num || 1 ;
        data.page_num = page_num;
        data.sid = window.localStorage.sid;

        $scope.user_id = user_id;
        $scope.LoadingOpen();
        setTimeout(function () {
            //close ok box
            $scope.LoadingClose();
        }, 600);

        TmpValue.post(APIForder + "?service=Board.BoardList", data)
            .then(function ($rs) {
                //$scope.LoadingClose();
                console.log($rs);
                console.log('11111');
                for(key in $rs.data["0"].board){
                    var array =null;
                    if($rs.data["0"].board[key].attached_id !=null && $rs.data["0"].board[key].attached_id !="0" ){
                            array = $.map($rs.data["0"].board[key].attached_id, function(value, index) {
                            return [value];
                        });
                        $rs.data["0"].board[key].attached_id = array;
                    }

                }

                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        $scope.BoardList = $rs
                        TmpObj.old_page_num = 0;
                        TmpObj.page_num = $rs.data["0"].page.page_num;
                        //$rs.data["0"].board["0"].like_arr["0"].post_by;

                        $("textarea[title!='']").hint();


                        //console.log($scope.BoardList);
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.FuncPushBoardList = function (user_id, page_num) {
        //return;
        data = {};
        data.user_id = user_id;
        page_num = page_num || 1 ;
        data.page_num = page_num;
        data.sid = window.localStorage.sid;
        //$scope.LoadingOpen();
        $(".loadMsgBoxD").show()

        console.log('push');
        TmpValue.post(APIForder + "?service=Board.BoardList", data)
            .then(function ($rs) {
                console.log($rs);
                //$scope.LoadingClose();
                if ($rs.ret == 200) {
                    setTimeout(function () {
                        //close ok box
                        //$scope.LoadingClose();
                        $(".loadMsgBoxD").hide()
                    }, 600);
                    if ($rs.data['0'].msg_state == 'Y') {

                        if ($rs.data[0].board.length != 0) {
                            for (var index = 0; index < $rs.data[0].board.length; index++) {
                                $scope.BoardList.data[0].board.push($rs.data[0].board[index]);
                            }
                            for (var index = 0; index < $rs.data[0].board_data.length; index++) {
                                $scope.BoardList.data[0].board_data.push($rs.data[0].board_data[index]);
                            }
                            TmpObj.old_page_num = TmpObj.page_num;
                            TmpObj.page_num = $rs.data["0"].page.page_num;



                        } else {

                            $scope.boardLast = '1';
                        }

                        $scope.BoardList.data["0"].page.page_num = $rs.data["0"].page.page_num;
                        /*
                        console.log($scope.BoardList);
                        console.log($rs);
                        */
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.FuncBoardMedia = function (user_id, page_num) {
        data = {};
        data.user_id = user_id;
        page_num = page_num || 1 ;
        data.page_num = page_num;
        data.sid = window.localStorage.sid;
        $scope.LoadingOpen();
        setTimeout(function () {
            //close ok box
            $scope.LoadingClose();
        }, 600);
        TmpValue.post(APIForder + "?service=Board.BoardListMedia", data)
            .then(function ($rs) {
                //$scope.LoadingClose();
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        $scope.BoardMedia = $rs
                            //console.log($scope.BoardList);
                    }
                } else {
                    return false;
                }
            });
    };


    $scope.BoardCancel = function (data) {
        $('.po-msg-box .title .fa').hide()
        $('.blackBg').css('z-index', '11').hide();
        $('.po-msg-box').css('z-index', '1')
        $(".popupMenu").removeClass("showthis");
        $(".post_box").removeClass("zIndexUp");

        $('.blackBg , .po-msg-box .title .fa , #target_video').hide();
        $("#files_image , #files_video , #show_temp , .target_image1 , textarea").val('');
        $("#target_image , #target_video").attr('src', '');
        $("#files_video , #files_image").prop("disabled", false);
        $('.pop-up').hide();
        $('.loading').hide();

        $scope.tmp_size = 0;
        $scope.tmp_img = [];
        $scope.formDataArr = [];
        $scope.formDataArrFilename = [];

        //$scope.show_temp = '';
        return false;
        //data = {};
    };

    $scope.BoardDataToggle = function (data) {
        $('.user-po:eq(' + this.$index + ')').slideToggle(50);
        return false;
    };
    $scope.CheckVideo = function (video_id, board_id, cloud_down_sec) {
        data = {};
        data.video_id = video_id;
        data.sid = window.localStorage.sid;
        console.log(cloud_down_sec);
        console.log(board_id);
        console.log(video_id);
        $('.videouploading').show();
        $scope.colsePopUp();
        videouploading
        $scope.down_sec(cloud_down_sec);



        TmpValue.post(APIForder + "?service=Board.CheckVideo", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data["0"].status == 'completed') {
                        $('.nowUpVideo ,.blackBg').hide();
                        $('.upVidepFinish').show();
                        $("body").css("overflow", "auto")
                        data = {};
                        data.id = board_id;
                        $('.videouploading').hide();


                        /*
                        var video_notice = Array();
                        video_notice =$scope.getCookies('video_notice');

                        //事件發生刪除cookie
                        var i = 0;
                        for (; i < video_notice.length; i++) {
                            if ($rs.data['0'].id == video_notice[i].id) {
                                delete video_notice[i];
                                $scope.setCookies('video_notice',video_notice)
                                break;
                            }
                        }
                        */

                        

                        TmpValue.post(APIForder + "?service=Board.BoardEditUP", data)
                    } else {
                        var tmp_sec = cloud_down_sec * 1000;
                        setTimeout(function () {
                            console.log(video_id);
                            console.log(board_id);
                            console.log(cloud_down_sec);
                            $scope.CheckVideo(video_id, board_id, 10);
                        }, tmp_sec);
                    }


                }
            })


        return false;
    };

    $scope.down_sec = function (cloud_down_sec) {
        cloud_down_sec -= 1;
        //$('#down_sec').html(cloud_down_sec);
        if (cloud_down_sec == 0) {
            $('.videouploading').hide();
        } else {
            setTimeout(function () {
                $scope.down_sec(cloud_down_sec);
            }, 1000);

        }
    }

    $scope.getCookies = function (cookie_name) {
        //取出來準備PUSH
        var cookie_arr = Array();
        if ($.cookie(cookie_name) != 'undefined' && $.cookie(cookie_name) != null)
            cookie_arr = JSON.parse($.cookie(cookie_name));
        // video_notice = video_notice.push(data);
        //填入陣列
        return cookie_arr;
    }

    $scope.appendCookies = function (cookie_name, cookie_arr, append_arr) {
        //取出來準備PUSH
        if ($.cookie(cookie_name) != 'undefined' && $.cookie(cookie_name) != null)
            cookie_arr = JSON.parse($.cookie(cookie_name));
        // video_notice = video_notice.push(data);
        //填入陣列
        cookie_arr.push(append_arr);
        //console.log(video_notice);
        //取出

        return cookie_arr;
    }

    $scope.setCookies = function (cookie_name, cookie_arr) {
        //設定cookie
        $.cookie(cookie_name, JSON.stringify(cookie_arr), {
            path: '/',
            expires: 1
        });
        return cookie_arr;
    }

    $scope.removeCookies = function (cookie_name) {
        $.removeCookie(cookie_name, {
            path: '/'
        });
        return cookie_arr;
    }

    $scope.BoardPost = function (fan_id) {
        fan_id = fan_id || null ;

        //$("body").css("overflow", "hidden")

        var error_msg = "";
        if ($scope.formDataArr == null && $("#show_temp").val() == 'image')
            $("#show_temp").val('');
        if ($("#files_video").val() == "" && $("#show_temp").val() == 'video')
            $("#show_temp").val('');


        if (this.data == undefined) {
            data = {};
        } else {
            data = this.data;
            //this.data.read_permi = 'public';
        }

        if ((data.text == undefined || data.text.trim() == '') && $("#show_temp").val() != 'image' && $("#show_temp").val() != 'video')
            error_msg += "message\n";

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }

        if (data == undefined)
            data = {};
        if ($("#show_temp").val() == '') {
            data.type = 4;
            Fdata = new FormData();
        } else if ($("#show_temp").val() == 'image') {
            data.type = 1;
            Fdata = new FormData($('#FilesImageForm')[0]);
        } else if ($("#show_temp").val() == 'video') {
            data.type = 2;
            Fdata = new FormData($('#FilesVideoForm')[0]);
        }
        if (data.read_permi == undefined)
            data.read_permi = 'public';
        if (data.text == undefined)
            data.text = '';

        if (fan_id != '') {
            data.fan_id = fan_id;
        }

        $scope.CheckSize();

        Fdata.append("read_permi", data.read_permi);
        Fdata.append("text", data.text);
        Fdata.append("type", data.type);
        Fdata.append("fan_id", data.fan_id);
        Fdata.append("sid", window.localStorage.sid);
        var i = 0;

        if ($scope.formDataArr != undefined) {
            for (; i < $scope.formDataArr.length; i++) {
                Fdata.append('file' + i, $scope.formDataArr[i], $scope.formDataArrFilename[i]);
            }
        }


        //Fdata.append('photoFile', blob, filename);
        if ($("#show_temp").val() != 'video' && $("#show_temp").val() != 'image' ) {
            $('.pop-up').show();
            $('.loading').show();
        } else if($("#show_temp").val() == 'video') {
            $('.nowUpVideo').show();
        } else if($("#show_temp").val() == 'image') {
            $('.nowUpImage').show();
        }
        this.data.text ="";
        $.ajax({
            url: APIForder + "?service=Board.BoardPost",
            type: 'POST',
            cache: false,
            data: Fdata,
            processData: false,
            contentType: false
        }).done(function (res) {
            $('.po-msg-box').css("z-index","1");
            if (res.data["0"].in_cloud == 'Y') {
                $('.blackBg , .po-msg-box .title .fa , #target_video').hide();
                $("#files_image , #files_video , #show_temp , .target_image1 , textarea").val('');
                $("#target_image , #target_video").attr('src', '');
                $("#files_video , #files_image").prop("disabled", false);
                $('.pop-up').hide();
                $('.loading').hide();
                $scope.CheckVideo(res.data["0"].video_id, res.data["0"].id, res.data["0"].cloud_down_sec);
                return;
            } else if($("#show_temp").val() == 'image') {
                $('.nowUpImage , .blackBg').hide();
            }
            //res.data["0"].video_id
            this.data = {};
            //var msg = alert('Post down');
            //window.location.reload();
            $scope.FuncBoardList($scope.user_id);
            $scope.FuncBoardMedia($scope.user_id);
            $scope.formDataArr = null;

        }).fail(function (res) {});
        if ($("#show_temp").val() != 'video' && $("#show_temp").val() != 'image'  ) {
            $('.blackBg , .po-msg-box .title .fa , #target_video').hide();
            $("#files_image , #files_video , #show_temp , .target_image1 , textarea").val('');
            $("#target_image , #target_video").attr('src', '');
            $("#files_video , #files_image").prop("disabled", false);
            $('.pop-up').hide();
            $('.loading').hide();
            $("body").css("overflow", "auto")
        }

        $scope.tmp_img = {}
        $scope.show_temp = '';
        $scope.data = {};
        $scope.data.read_permi = data.read_permi;

        data = {};
    };


    $scope.ReBoardPost = function (data) {
        console.log($scope.board_data_formDataArr);
        var error_msg = "";
        if ($scope.board_data_formDataArr == undefined)
            $scope.re_show_temp = '';
        else
            $scope.re_show_temp = 'image';

        console.log($scope.re_show_temp);

        if (data == undefined) {
            data = {};
        } else {;
            //this.data.read_permi = 'public';
        }
        console.log(data.text);
        if ((data.text == undefined) && $scope.re_show_temp == '')
            error_msg += "message\n";

        if (error_msg != "") {
            Alertify.alert("Please fill in：\n" + error_msg);
            return;
        }


        if ($scope.re_show_temp == '' || $scope.re_show_temp == undefined) {
            data.type = 4;
            Fdata = new FormData();
        } else if ($scope.re_show_temp == 'image') {
            data.type = 1;
            Fdata = new FormData();
            //Fdata = new FormData($('#FilesImageForm')[0]);
        } else if ($scope.re_show_temp == 'video') {
            data.type = 2;
            Fdata = new FormData();
            //Fdata = new FormData($('#FilesVideoForm')[0]);
        }
        if (data.read_permi == undefined)
            data.read_permi = 'public';
        if (data.text == undefined)
            data.text = '';

        data.board_id = this.this_BoardList.id;
        this.Redata = {};
        var keydata = {};
        keydata.board_key = this.key_this_BoardList;

        Fdata.append("read_permi", data.read_permi);
        Fdata.append("board_id", data.board_id);
        Fdata.append("text", data.text);
        Fdata.append("type", data.type);
        var i = 0;
        if ($scope.board_data_formDataArr != undefined) {
            for (; i < $scope.board_data_formDataArr.length; i++) {
                Fdata.append('file' + i, $scope.board_data_formDataArr[i], $scope.board_data_formDataArrFilename[i]);
            }
        }


        if($scope.re_show_temp == 'image') {
            $('.nowUpImage').show();
        }else{
            $('.pop-up').show();
            $('.loading').show();
        }


        $.ajax({
            url: APIForder + "?service=Board.ReBoardPost",
            type: 'POST',
            cache: false,
            data: Fdata,
            processData: false,
            contentType: false
        }).done(function (res) {
            console.log(res);
            $scope.BoardList.data["0"].board_data.splice(0, 0, res.data["0"]['0']);
            $scope.$apply();
            $scope.openMsgToggle();
            if (res.ret != 200) {
                Alertify.alert(res.msg);
            }
                if($scope.re_show_temp == 'image') {
                    $('.nowUpImage').hide();
                }else{
                    $('.pop-up').hide();
                    $('.loading').hide();
                }

            //var msg = alert('Upload Success');
            //window.location.reload();
        }).fail(function (res) {});

        $scope.board_data_tmp_img = {}
        $scope.show_temp = '';
        $scope.re_show_temp = '';
        $scope.data = {};
        $scope.data.read_permi = data.read_permi;

        data = {};
    };


    $scope.editPost = function () {
        //console.log(this);
        var data = {};
        $scope.edit_data = {};
        /*
        data.text = this.this_BoardList.text;
        data.read_permi = this.this_BoardList.read_permi;
        data.attached_name = this.this_BoardList.attached_name;
        data.type = this.this_BoardList.type;
        data.id = this.this_BoardList.id;
        */
        $scope.edit_data_key = this.key_this_BoardList;
        $scope.edit_data = this.this_BoardList;
        $scope.tmpedit_data = angular.copy(this.this_BoardList);
        $('.change_msg_pup').show();
        $('.edit_msg').fadeIn(120)
        $('body').css('overflow', 'hidden')
        $('.del_msg').hide();
    }
    $scope.BoardEditUP = function (fan_id) {
            fan_id = fan_id || null ;

            //console.log($scope.edit_data);
            var data = {};
            data = $scope.edit_data;
            if (fan_id != '') {
                data.fan_id = fan_id;
            }
            Fdata = new FormData($('#FilesImageForm')[0]);

            console.log(data);

            $scope.CheckSize();

            Fdata.append("read_permi", data.read_permi);
            Fdata.append("id", data.id);
            Fdata.append("text", data.text);
            Fdata.append("type", data.type);
            Fdata.append("fan_id", data.fan_id);
            Fdata.append("delindex", $scope.delindex);
            Fdata.append("attached_id", data.attached_id);
            console.log(data.attached_id);

            var i = 0;
            if ($scope.formeditDataArr != undefined) {
                for (; i < $scope.formeditDataArr.length; i++) {
                    console.log($scope.formeditDataArr[i]);
                    Fdata.append('file' + i, $scope.formeditDataArr[i], $scope.formeditDataArrFilename[i]);
                }
            }

            /*
            console.log(Fdata);
            console.log($scope.formDataArr);
            return false;
            */
            //Fdata.append('photoFile', blob, filename);
            //console.log($scope.edit_data);
            //return
            $.ajax({
                    url: APIForder + "?service=Board.BoardEditUP",
                    type: 'POST',
                    cache: false,
                    data: Fdata,
                    processData: false,
                    contentType: false
                }).done(function (res) {

                    if (res.ret == 200) {
                        if (res.data['0'].msg_state == 'Y') {
                            //res.data["0"]["0"]
                            //$scope.LoadingClose();
                            //alert($rs.data[0].msg_text);
                            /*
                                for (var i=0; i<res.data.length; i++){
                                    console.log('editup1');
                                    var array =null;
                                    if(res.data["0"].attached_id !=undefined){
                                        array = $.map(res.data["0"].attached_id, function(value, index) {
                                            return [value];
                                        });
                                    }
                                res.data["0"].attached_id = array;
                                }
                            */
                                console.log('editup1');
                                for(key in res.data){
                                    var array =null;
                                    if(res.data[key].attached_id !=null && res.data[key].attached_id !="0"){
                                            array = $.map(res.data[key].attached_id, function(value, index) {
                                            return [value];
                                        });
                                        res.data[key].attached_id = array;
                                    }

                                }

                                console.log('editup');
                                console.log(res);
                                console.log(res.data["0"].attached_id);

                            $scope.BoardList.data["0"].board[$scope.edit_data_key] = res.data["0"];

                            //$scope.$apply();
                            $scope.edit_data = '';
                            $('.no_change_pup').hide();
                            $('.change_msg_pup , .edit_msg , .del_msg').hide();
                            $('body').css('overflow', 'auto')
                            $('.blackBg').css('z-index', '11').hide();
                            $(".popupMenu").removeClass("showthis");
                            $(".post_box").removeClass("zIndexUp");

                            //$scope.FuncBoardList($scope.seedata.data["0"].f_backend.user_id);
                            //console.log($scope.BoardList);

                        }
                    }
                }).fail(function (res) {})
                .then(function () {
                    //reset
                    $scope.delindex = '';
                    $scope.edit_data_key = null;
                    $scope.edit_data = null;
                    $scope.tmpedit_data = null;
                    $scope.tmp_edit_img = [];
                    $scope.formeditDataArr = [];
                    $scope.formeditDataArrFilename = [];
                    $scope.tmp_edit_size = 0;
                    $scope.$apply();
                    //reset
                });

            $('.blackBg').css('z-index', '11').hide();
            $(".popupMenu").removeClass("showthis");
            $(".post_box").removeClass("zIndexUp");
        }
        // editPost end

    $scope.deletePost = function () {
        console.log(this);
        $scope.del_data = {};
        $scope.del_data_key = this.key_this_BoardList;
        $scope.del_data.id = this.this_BoardList.id;
        $('.change_msg_pup').show();
        $('.del_msg').fadeIn(120);
        $('body').css('overflow', 'hidden')
    }

    $scope.DeleteUP = function () {
        console.log($scope.del_data);
        var data = {};
        data = $scope.del_data;

        $('.pop-up').show();
        $('.loading').show();
        setTimeout(function () {
            $('.loading').hide();
            //            $('.loadingTrue').show();
            $('.no_change_pup').hide();
            $('.change_msg_pup , .edit_msg , .del_msg').hide();
            $('body').css('overflow', 'auto')
            $('.blackBg').css('z-index', '11').hide();
            $(".popupMenu").removeClass("showthis");
            $(".post_box").removeClass("zIndexUp");
        }, 300);
        setTimeout(function () {
            //close ok box
            $('.pop-up').hide();
            $('.loading').hide();
            //            $('.loadingTrue').hide();
        }, 800);

        TmpValue.post(APIForder + "?service=Board.BoardDelete", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        //alert($rs.data[0].msg_text);
                        //delete $scope.BoardList.data["0"].board[$scope.del_data_key];
                        tmp_board_id = $scope.BoardList.data["0"].board[$scope.del_data_key].id;
                        $scope.BoardList.data["0"].board.splice($scope.del_data_key, 1);
                        var tmp_array = new Array();
                        for (i = 0; i < $scope.BoardList.data["0"].board_data.length; i++) {
                            if ($scope.BoardList.data["0"].board_data[i].board_id == tmp_board_id) {
                                tmp_array.push(i);
                            }
                        }
                        if (tmp_array.length > 0)
                            for (i = 0; i < tmp_array.length; i++) {
                                $scope.BoardList.data["0"].board_data.splice(i, 1);
                            }





                        $scope.del_data = '';
                        $('.no_change_pup').hide();
                        $('.change_msg_pup , .edit_msg , .del_msg').hide();
                        $('body').css('overflow', 'auto')
                        $('.blackBg').css('z-index', '11').hide();
                        $(".popupMenu").removeClass("showthis");
                        $(".post_box").removeClass("zIndexUp");
                        $('.pop-up').hide();
                        $('.loading').hide();
                        //$scope.FuncBoardMedia($scope.seedata.data["0"].f_backend.user_id);
                        //$scope.FuncBoardList($scope.seedata.data["0"].f_backend.user_id);
                        //console.log($scope.BoardList);
                    }
                } else {
                    return false;
                }
            });
        $('.blackBg').css('z-index', '11').hide();
        $(".popupMenu").removeClass("showthis");
        $(".post_box").removeClass("zIndexUp");
    }


    $scope.closeNoChange = function () {
        $('.no_change_pup').hide();

    };
    // closeNoChange end

    $scope.noChangePost = function () {
        $('.no_change_pup').show();
    };
    // noChangePost end


    //edit reply msg btn

    $scope.editMsgText = function ($event, data) {
        $($event.target).parent().find(".EdidReplyMenu").toggle()
    };
    $scope.tmp_this_BoardData = {};
    $scope.tmp_key_this_BoardData = {};
    $scope.deleteReply = function ($event, this_BoardData, key_this_BoardData) {
        $scope.tmp_this_BoardData = this_BoardData;
        $scope.tmp_key_this_BoardData = key_this_BoardData;


        $('.change_msg_pup').show();
        $('.del_reply').fadeIn(120);
        $('body').css('overflow', 'hidden')
    }; //移除回覆

    $scope.DeleteReplyUP = function () {

            //console.log($scope.tmp_this_BoardData);
            //return;
            var data = {};
            data = $scope.tmp_this_BoardData;

            $('.pop-up').show();
            $('.loading').show();
            setTimeout(function () {
                $('.loading').hide();
                $('.no_change_pup').hide();
                $('.change_msg_pup , .edit_msg , .del_msg').hide();
                $('body').css('overflow', 'auto')
                $('.blackBg').css('z-index', '11').hide();
                $(".popupMenu").removeClass("showthis");
                $(".post_box").removeClass("zIndexUp");
            }, 300);
            setTimeout(function () {
                //close ok box
                $('.pop-up').hide();
                $('.loading').hide();
                //            $('.loadingTrue').hide();
            }, 800);
            TmpValue.post(APIForder + "?service=Board.ReBoardDelete", data)
                .then(function ($rs) {
                    if ($rs.ret == 200) {

                        if ($rs.data['0'].msg_state == 'Y') {
                            //$scope.BoardList.data["0"].board.splice($scope.tmp_key_this_BoardData, 1);
                            $scope.BoardList.data["0"].board_data.splice($scope.tmp_key_this_BoardData, 1);
                            $scope.tmp_this_BoardData = '';
                            $scope.tmp_key_this_BoardData = '';
                            $('.no_change_pup').hide();
                            $('.change_msg_pup , .edit_msg , .del_msg').hide();
                            $('body').css('overflow', 'auto')
                            $('.blackBg').css('z-index', '11').hide();
                            $(".popupMenu").removeClass("showthis");
                            $(".post_box").removeClass("zIndexUp");
                            $('.pop-up').hide();
                            $('.loading').hide();

                        }
                    } else {
                        return false;
                    }
                });
            $('.blackBg').css('z-index', '11').hide();
            $(".popupMenu").removeClass("showthis");
            $(".post_box").removeClass("zIndexUp");




            $('.pop-up').show();
            $('.loading').show();
            setTimeout(function () {
                $('.loading').hide();
                $('.no_change_pup').hide();
                $('.change_msg_pup , .edit_msg , .del_msg , .del_reply , .EdidReplyMenu').hide();
                $('body').css('overflow', 'auto')
                $('.blackBg').css('z-index', '11').hide();
                $(".popupMenu").removeClass("showthis");
                $(".post_box").removeClass("zIndexUp");
            }, 300);
            setTimeout(function () {
                //close ok box
                $('.pop-up').hide();
                $('.loading').hide();
                //            $('.loadingTrue').hide();
            }, 800);

        } //end delete reboarddata

    $scope.edit_BoardData = {};
    $scope.editReply = function ($event, this_BoardData, key_this_BoardData) {
        console.log(this_BoardData);
        $scope.edit_BoardData = angular.copy(this_BoardData);
        $($event.target).parents(".u_text").find("p").hide()
        $(".EdidReplyMenu").hide()

        $($event.target).parents(".u_text").find(".changeUserPO").show()
    }

    $scope.noChangeReply = function () {
        $(".changeUserPO").hide()
        $(".user-po").find("p").show()
    }


    $scope.changeReplySave = function ($event, this_BoardData, key_this_BoardData) {
        console.log($scope.BoardList.data[0].board_data);
        console.log(key_this_BoardData);
        //return;

        //console.log($scope.edit_data);
        var data = {};
        data = $scope.edit_BoardData;
        //if (fan_id != '') {
        //   data.fan_id = fan_id;
        //}
        Fdata = new FormData($('#FilesImageForm')[0]);

        console.log(data);

        $scope.CheckSize();

        Fdata.append("read_permi", data.read_permi);
        Fdata.append("id", data.id);
        Fdata.append("text", data.text);
        Fdata.append("type", data.type);
        //Fdata.append("fan_id", data.fan_id);
        Fdata.append("delindex", $scope.delindex);
        Fdata.append("attached_id", data.attached_id);

        var i = 0;

        if ($scope.formeditDataArr != undefined) {
            for (; i < $scope.formeditDataArr.length; i++) {
                console.log($scope.formeditDataArr[i]);
                Fdata.append('file' + i, $scope.formeditDataArr[i], $scope.formeditDataArrFilename[i]);
            }
        }

        $.ajax({
                url: APIForder + "?service=Board.ReBoardEditUP",
                type: 'POST',
                cache: false,
                data: Fdata,
                processData: false,
                contentType: false
            }).done(function (res) {
                console.log('boardreeditup');
                console.log(key_this_BoardData);
                if (res.ret == 200) {
                    if (res.data['0'].msg_state == 'Y') {
                        $scope.BoardList.data[0].board_data[key_this_BoardData].text = data.text;
                        //.data["0"]["0"].text
                        $scope.edit_BoardData = '';
                    }
                }
            }).fail(function (res) {})
            .then(function () {
                //reset
                /*
                $scope.delindex = '';
                $scope.edit_data_key = null;
                $scope.edit_data = null;
                $scope.tmpedit_data = null;
                $scope.tmp_edit_img = [];
                $scope.formeditDataArr = [];
                $scope.formeditDataArrFilename = [];
                $scope.tmp_edit_size = 0;
                $scope.$apply();
                */
                //reset
                $scope.$apply();
            });

        $(".changeUserPO").hide()
        $(".user-po").find("p").show()

        // editPost end




    }

    //edit reply msg btn END

    //Reply msg text PO img or video
    $scope.postImages = function () {
        $scope.re_show_temp = 'image';
        $(".board_data_files_image:eq(" + this.$index + ")").click();
        this_file = $(".board_data_files_image:eq(" + this.$index + ")");
        $(".board_data_files_image:eq(" + this.$index + ")").unbind().change(function () {
            $scope.read_board_data_files_image(this_file["0"]);

        });

        $(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyVideo").hide();
        $(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyImg").show();
        //$(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyImg").text("IMG");

    };
    $scope.postVideo = function () {
        $(".post_box:eq(" + this.$index + ")").find(".postVideo").find("input").show();
        $(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyImg").hide();
        $(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyVideo").show();
        $(".post_box:eq(" + this.$index + ")").find(".replyPostIV .replyVideo").text("VIDEO");
    };
    //Reply msg text PO img or video END

    //Reply msg text change img or video
    $scope.postChangeImages = function () {
        $(".changeUserPO:eq(" + this.$index + ")").find(".postImages").find("input").click()
    };
    $scope.postChangeVideo = function () {
        $(".changeUserPO:eq(" + this.$index + ")").find(".postVideo").find("input").click()
    };
    //Reply msg text change img or video END

    $scope.delindex = '';
    $scope.editRemoveThisImg = function () {
        if ($scope.delindex === '') {
            $scope.delindex = this.$index;
        } else
            $scope.delindex = $scope.delindex + ',' + this.$index;

        $(".editBoxImg:eq(" + this.$index + ")").hide()
    }


    $scope.yesNoChange = function () {

        console.log($scope.tmpedit_data);
        //reset
        $scope.delindex = '';
        $scope.BoardList.data["0"].board[$scope.edit_data_key] = $scope.tmpedit_data;
        $scope.edit_data_key = null;
        $scope.edit_data = null;
        $scope.tmpedit_data = null;
        $scope.tmp_edit_img = [];
        $scope.formeditDataArr = [];
        $scope.formeditDataArrFilename = [];
        $scope.tmp_edit_size = 0;

        //reset

        //$scope.edit_data = $scope.tmpedit_data;

        $('.no_change_pup').hide();
        $('.change_msg_pup , .edit_msg , .del_msg').hide();
        $('body').css('overflow', 'auto')
        $('.blackBg').css('z-index', '11').hide();
        $(".popupMenu").removeClass("showthis");
        $(".post_box").removeClass("zIndexUp");
        $(".editBoxImg").show();

    };
    // yesNoChange end

    $scope.closeEditPost = function () {
        $('.change_msg_pup , .edit_msg , .del_msg').hide();
        $('body').css('overflow', 'auto');
        $('.blackBg').css('z-index', '11').hide();
        $(".popupMenu").removeClass("showthis");
        $(".post_box").removeClass("zIndexUp");
    };
    // closeEditPost end



    // deletePost end


    //end board all

    $scope.schoolPrivateShow = function () {
        var $schoolList = $(".private_msg_list .name_box"),
            thisIndex = this.$index,
            $this = $schoolList.eq(thisIndex)
        Alertify.alert($schoolList.find("div").index())

    };


    $scope.upIma = function () {
        var holder = document.getElementById('holder'),
            imgSRC = document.getElementById('target_bg_image'),
            tests = {
                filereader: typeof FileReader != 'undefined',
                dnd: 'draggable' in document.createElement('span'),
                formdata: !!window.FormData,
                progress: "upload" in new XMLHttpRequest
            },
            support = {
                filereader: document.getElementById('filereader'),
                formdata: document.getElementById('formdata'),
                progress: document.getElementById('progress')
            },
            acceptedTypes = {
                'image/png': true,
                'image/jpeg': true,
                'image/gif': true
            },
            progress = document.getElementById('uploadprogress'),
            fileupload = document.getElementById('upload');

        "filereader formdata progress".split(' ').forEach(function (api) {
            if ( !support[api] ) return;
            if (tests[api] === false) {
                support[api].className = 'fail';
            } else {
                // FFS. I could have done el.hidden = true, but IE doesn't support
                // hidden, so I tried to create a polyfill that would extend the
                // Element.prototype, but then IE10 doesn't even give me access
                // to the Element object. Brilliant.
                support[api].className = 'hidden';
            }
        });

        function previewfile(file) {
            if (tests.filereader === true && acceptedTypes[file.type] === true) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    var image = new Image();
                    image.src = event.target.result;
                    //image.width = 250; // a fake resize
                    //holder.appendChild(image);
                    $('#target_bg_image').attr('src', event.target.result);
                    $(".moveBar").append(event.target.result)
                    $(".files_bg_image").val(event.target.result)
                    $(".moveBar").show();

                };

                reader.readAsDataURL(file);
            } else {
                holder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size / 1024 | 0) + 'K' : '');
                console.log(file);
            }
        }

        function readfiles(files) {
            debugger;
            var formData = tests.formdata ? new FormData() : null;
            for (var i = 0; i < files.length; i++) {
                if (tests.formdata) formData.append('file', files[i]);
                previewfile(files[i]);
            }

            // now post a new XHR request
            if (tests.formdata) {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/devnull.php');
                xhr.onload = function () {
                    progress.value = progress.innerHTML = 100;
                };

                if (tests.progress) {
                    xhr.upload.onprogress = function (event) {
                        if (event.lengthComputable) {
                            var complete = (event.loaded / event.total * 100 | 0);
                            progress.value = progress.innerHTML = complete;
                        }
                    }
                }

                xhr.send(formData);
            }
        }

        if (tests.dnd) {
            holder.ondragover = function () {
                this.className = 'hover';
                return false;
            };
            holder.ondragend = function () {
                this.className = '';
                return false;
            };
            holder.ondrop = function (e) {
                this.className = '';
                e.preventDefault();
                readfiles(e.dataTransfer.files);
            }
        } else {
            fileupload.className = 'hidden';
            fileupload.querySelector('input').onchange = function () {
                readfiles(this.files);
            };
        }


    };

    //comm live info
    //

    $scope.CreateLive = function (fan_id) {
        fan_id = fan_id || null ;

        //broadcast();        
        //建立直播
        var highest = $('#highest').val();
        var title = $('#liveTitle').val();
        //alert(title);

        var data = {};
        if (fan_id != '') {
            data.fan_id = fan_id;
        }
        if (highest != '') {
            data.highest = highest;
        }
        if (title != '') {
            data.title = title;
        }
        //

        TmpValue.post(APIForder + "?service=Board.CreateLive", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    //直播初始化 --> 開啟直播 --> 建立聊天室 --> 崁入直播 ifsdk在畫面中 -->  結束直播後寫入VIDEOID 進留言板
                    //判斷當前是要顯示直播還是VIDEO
                    //return  TmpValue.post(APIForder + "?service=Board.CreateLive", data);
                    //開webrtc
                    tmp = {};
                    tmp.stream_key = $rs.data["0"].stream_key;
                    tmp.embed_url = $rs.data["0"].embed_url;
                    tmp.live_id = $rs.data["0"].live_id;
                    //傳給watch
                    TmpObj.tmp = tmp;
                    //return;
                    $scope.StartLive($rs.data["0"].stream_key, 'Y');
                    //$scope.StartLive('e4a8bafb321d47e482b8fbd0a4be5b9d', 'Y');
                    //關webrtc  要加血關直播
                    //$scope.StartLive(data.stream_key,'Y');
                    //end 直播初始化 --> 開啟直播 -->崁入直播 ifsdk在畫面中 -->  結束直播後寫入VIDEOID 進留言板

                }
            })
    }



    $scope.StartChatRoom = function (tmp_chatroomName) {
        tmp_chatroomName = tmp_chatroomName || null ;

        // 2. This code loads the iframe chatroom API code asynchronously.
        /*
        var tag = document.createElement('script');

        tag.src = "https://mgk.straas.net/sdk/1.5.0/iframe-sdk.js";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        */
        // 3. This function creates an <iframe> (and StraaS Chatroom)
        //    after the API code downloads.
        var chatroom;
        var tmp_account = 'showhi.club';
        // var tmp_chatroomName = 'johntest' //live_id
        function onStraaSChatroomIframeReady(tmp_chatroomName) {
            chatroom = new StraaSChatroom.Chatroom('chatroom', {
                accountId: tmp_account,
                showOnlineUserThreshold: false,
                showMessageSource: false,
                chatroomName: tmp_chatroomName,
                memberToken: $scope.seedata.data["0"].f_backend.chat_token,
                straasSupport: true,
                isPersonalChat: false,
                language: 'zh',
                height: '100%',
                width: '340',

                events: {
                    'onReady': onChatroomReady,
                },

                //styleURL: <URL of custom css file, please refer section CSS file example>,
                secureMode: false
            })
        }

        // 4. The API will call this function when the chatroom is ready.
        function onChatroomReady() {
            console.log('chatroom is ready')
        }

        onStraaSChatroomIframeReady(tmp_chatroomName);
    }

    $scope.StartUserChatRoom = function (gueser_id, tmp_chatroomName) {
        tmp_chatroomName = tmp_chatroomName || null ;
        $('#' + gueser_id).html('');
        // 2. This code loads the iframe chatroom API code asynchronously.
        /*
        var tag = document.createElement('script');

        tag.src = "https://mgk.straas.net/sdk/1.5.0/iframe-sdk.js";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        */
        // 3. This function creates an <iframe> (and StraaS Chatroom)
        //    after the API code downloads.
        var chatroom;
        var tmp_account = 'showhi.club';
        // var tmp_chatroomName = 'johntest' //live_id
        function onStraaSChatroomIframeReady(tmp_chatroomName) {
            chatroom = new StraaSChatroom.Chatroom(gueser_id, {
                accountId: tmp_account,
                showOnlineUserThreshold: false,
                showMessageSource: false,
                chatroomName: tmp_chatroomName,
                memberToken: $scope.seedata.data["0"].f_backend.chat_token,
                straasSupport: true,
                isPersonalChat: false,
                language: 'zh',
                height: '100%',
                width: '300',

                events: {
                    'onReady': onChatroomReady,
                },

                //styleURL: <URL of custom css file, please refer section CSS file example>,
                secureMode: false
            })
        }

        // 4. The API will call this function when the chatroom is ready.
        function onChatroomReady() {
            console.log('chatroom is ready')
        }

        onStraaSChatroomIframeReady(tmp_chatroomName);
    }

    $scope.StartLive = function (streamKey, StartliveOn, live_id, fan_id) {
        live_id = live_id || null;
        fan_id = fan_id || null;
        //直播初始化
        var stream;
        var video1 = $('#camara');
        const gumConstraints = {
            audio: true,
            video: {
                width: {
                    min: 1280
                },
                height: {
                    min: 720
                },
            }
        };
        //alert(1);
        //sleep(2000);
        //alert(2);
        var peerConn = new RTCPeerConnection({
/*
            iceServers: [
                {
                    urls: "stun:stun.l.google.com:19302"
                },
                {
                    urls: "turn:numb.viagenie.ca",
                    credential: "turnserver",
                    username: "john75116@gmail.com"
                }
            ]
            */
            //new 10/22 新版使用livehousein server 
            iceServers: [
            {urls: ['turn:35.194.239.30?transport=udp'], 
            username: 'showhi.club-test', 
            credential: 'ubBJzRbg'}
            ],
            iceTransportPolicy: 'relay',




        }, {
            optional: [{
                'DtlsSrtpKeyAgreement': false
                }]
        });
        var iceCandidates = '';
        var offerSDP;

        // RTCPeerConnection event handlers

        peerConn.onicecandidate = function (event) {
            if (event.candidate) {
                iceCandidates += ('a=' + event.candidate.candidate + '\r\n');
                return;
            }
            // ICE candidates gathering has finished
            var remoteCandidates;

            callStartBroadcastingAPI()
                .then(function (response) {
                    var remoteDesc = new RTCSessionDescription({
                        type: 'answer',
                        sdp: response.data.sdp
                    });
                    //console.log(response);
                    remoteCandidates = response.data.remoteCandidates;
                    return peerConn.setRemoteDescription(remoteDesc);
                })
                .then(function () {
                    var candidate;
                    for ( candidate of remoteCandidates) {
                        peerConn.addIceCandidate(new RTCIceCandidate({
                            candidate: candidate
                        }));
                    }
                });
        };
        peerConn.oniceconnectionstatechange = function (event) {
            console.log(event);

            if (peerConn.iceconnectionstate === 'failed' ||
                peerConn.iceconnectionstate === 'closed') {
                // peer connection encountered some errors.
                //callStopBroadcastingAPI();
                callStopBroadcastingAPI()
                    .then(function () {
                        callStartBroadcastingAPI();
                    });
                Alertify.alert('The connection failed, please reconnect after shutdown, or use a more stable network environment');
            }

            if (event.currentTarget.iceConnectionState === 'failed') {
                Alertify.alert('The connection failed, please reconnect after shutdown, or use a more stable network environment');
                callStopBroadcastingAPI();
                callStartBroadcastingAPI();
                // .then(() => {
                //     callStartBroadcastingAPI();

                // });

                //peerConn.removeStream(mediaStream);

                //peerConn.close();
                //disconnected
                //failed

            }



        };
        //end直播初始化

        // return;
        // Button click handlers
        //直播函數
        function broadcast() {
            return navigator.mediaDevices.getUserMedia(gumConstraints)
                .then(function (mediaStream) {
                    stream = mediaStream;
                    video1.attr('src', window.URL.createObjectURL(stream));

                    peerConn.addStream(mediaStream);
                    return peerConn.createOffer();
                })
                .then(function (offer) {

                    offerSDP = offer.sdp;
                    return peerConn.setLocalDescription(offer);
                });
        }

        function terminate() {
            return callStopBroadcastingAPI();
        }

        // StraaS WebRTC Broadcasting APIs
        function callStartBroadcastingAPI() {
            //alert(streamKey);
            const config = {
                method: 'post',
                url: 'https://rtc-middleware.straas.io/broadcasts/webrtc',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    stream_key: streamKey,
                    sdp: offerSDP + iceCandidates,
                }
            };

            return axios(config);
        }

        function callStopBroadcastingAPI() {
            const config = {
                method: 'delete',
                url: 'https://rtc-middleware.straas.io/broadcasts/webrtc',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    stream_key: streamKey,
                }
            };

            return axios(config);
        }


        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }



        //end 直播函數
        //setStream(streamKey);
        if (StartliveOn == 'Y') {
            broadcast();
        } else {
            terminate();

            var data = {};
            data.live_id = live_id;
            data.jsondata = {
                //"status": "ended",
                "vod_listed": true,
                "vod_available": true
            };

            TmpValue.post(APIForder + "?service=Board.UpdateLive", data)
                .then(function ($rs) {
                    console.log($rs);
                    data = {};
                    data.live_id = $rs.data["0"].live_id;
                    data.video_id = $rs.data["0"].video_id;

                    if (fan_id != '' && fan_id != null) {
                        data.fan_id = fan_id;
                    }

                    if ($rs.ret == 200 && $rs.data['0'].msg_state == 'Y') {
                        return TmpValue.post(APIForder + "?service=Board.BoardEndLiveUP", data)
                    } else {
                        console.log('error');
                        return false;
                    }
                })
                .then(function ($rs) {
                    if ($rs.ret == 200) {
                        if ($rs.data['0'].msg_state == 'Y') {
                            window.location.reload();
                            //alert('Change permission' + data.read_permi + ' done.');
                            //$scope.FuncBoardList($scope.teacher_tmp_arr['0']['id']);
                        }
                    } else {
                        window.location.reload();
                        return false;
                    }
                });
            TmpObj.tmp = null;
        }
    }


    //comm end live 





    //main
    //angular.element(document).ready(function () {
    $scope.$watch(function () { return TmpObj.board_user_id; }, function (newVal, oldVal) {
        if (newVal != null && newVal != undefined) {
            $scope.FuncBoardList(TmpObj.board_user_id);
            $("#hidden_option").remove()
            $scope.FuncBoardMedia(TmpObj.board_user_id);
        }
        TmpObj.board_user_id = null;
    });
    //});
    /*
    angular.element(document).ready(function () {
        $scope.$watch(() => TmpObj.board_user_id, function (newVal) {
            console.log('Name changed to ' + newVal);
        });
    });
    */
});


//$(function () {


//});