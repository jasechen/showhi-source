appH5.controller('FuncFriendsAndSubscript', function ($scope, $routeParams, $q, $http, APIForder, TmpObj, TmpValue) {
    function PostJson(url, data) {
        var deferred = $q.defer();
        $http.post(url, data).success(function (d) {
            deferred.resolve(d);
        });
        return deferred.promise;
    }



    //friendsAndsub all

    $scope.FuncFriendsList = function (user_id) {
        data = {};
        data.user_id = user_id;
        data.sid = window.localStorage.sid;
        $scope.user_id = user_id;
        TmpValue.post(APIForder + "?service=User.FriendsList", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        $scope.FriendsList = $rs.data['0']['FriendsList'];
                    } else {
                        $scope.FriendsList = $rs.data['0']['FriendsList'];
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.FuncSubscriptionList = function (user_id) {
        data = {};
        data.user_id = user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.SubscriptionList", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        $scope.SubscriptionList = $rs.data['0']['SubscriptionList'];
                    } else {
                        $scope.SubscriptionList = $rs.data['0']['SubscriptionList'];
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.AgreeFriends = function (option, user_id) {
        data = {};
        data.invite_user_id = user_id;
        data.approval = option;
        data.sid = window.localStorage.sid;
        console.log(data);
        // return false;
        TmpValue.post(APIForder + "?service=User.AgreeFriends", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        $scope.FuncWaitFriendsList($scope.user_id);
                        $scope.FuncFriendsList($scope.user_id);
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.FuncWaitFriendsList = function (user_id) {
        data = {};
        data.sid = window.localStorage.sid;
        data.who_user_id = user_id;
        TmpValue.post(APIForder + "?service=User.WaitFriendsList", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        TmpObj.WaitFriendsList = $rs.data['0']['WaitFriendsList'];
                    } else {
                        TmpObj.WaitFriendsList = $rs.data['0']['WaitFriendsList'];
                        //alert($rs.data[0].msg_text);
                    }
                } else {
                    //alert($rs.msg);
                }
            });
    };


    $scope.CheckFriendsSubscription = function (user_id) {
        data = {};
        data.sid = window.localStorage.sid;
        data.who_user_id = user_id;
        TmpValue.post(APIForder + "?service=User.CheckFriendsSubscription", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        $scope.Friends = $rs.data['0']['Friends'];
                        $scope.Subscription = $rs.data['0']['Subscription'];
                    } else {
                        alert($rs.data[0].msg_text);
                    }
                } else {
                    //alert($rs.msg);
                }
            });
    };


    $scope.CancelFriends = function () {
        data = {};
        data.who_user_id = $scope.user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.CancelFriends", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        return $scope.CheckFriendsSubscription(data.who_user_id);
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.CancelAddFriends = function () {
        data = {};
        data.who_user_id = $scope.user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.CancelFriends", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        return $scope.CheckFriendsSubscription(data.who_user_id);
                    }
                } else {
                    return false;
                }
            });
    };


    $scope.CancelSubscription = function () {
        data = {};
        data.who_user_id = $scope.user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.CancelSubscription", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        return $scope.CheckFriendsSubscription(data.who_user_id);
                        //return TmpValue.post(APIForder + "?service=User.CheckFriendsSubscription", data);            
                    }
                } else {
                    return false;
                }
            });

    };
    //end click
    //pub func 
    $scope.CheckFriendsSubscription = function (user_id) {
        data = {};
        data.who_user_id = user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.CheckFriendsSubscription", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data[0].msg_state == 'Y') {
                        $scope.Friends = $rs.data['0']['Friends'];
                        $scope.Subscription = $rs.data['0']['Subscription'];
                    } else {
                        alert($rs.data[0].msg_text);
                    }
                } else {
                    //alert($rs.msg);
                }
            });
    };

    $scope.AddFriends = function () {
        data = {};
        data.who_user_id = $scope.user_id;
        data.sid = window.localStorage.sid;
        TmpValue.post(APIForder + "?service=User.AddFriends", data)
            .then(function ($rs) {
                console.log($rs);
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        return $scope.CheckFriendsSubscription(data.who_user_id);
                    }
                } else {
                    return false;
                }
            });
    };

    $scope.AddSubscription = function () {
        data = {};
        data.who_user_id = $scope.user_id;
        data.sid = window.localStorage.sid;
        /*alert($scope.user_id);
        return;*/
        TmpValue.post(APIForder + "?service=User.AddSubscription", data)
            .then(function ($rs) {
                if ($rs.ret == 200) {
                    if ($rs.data['0'].msg_state == 'Y') {
                        return $scope.CheckFriendsSubscription(data.who_user_id);
                    }
                } else {
                    return false;
                }
            });

    };

    //end friendsAndsub all

    //main
    //angular.element(document).ready(function () {
        $scope.$watch(function () { return TmpObj.friends_user_id; }, function (newVal) {
            if (newVal != null && newVal != undefined) {
                $scope.CheckFriendsSubscription(TmpObj.friends_user_id);
                $scope.FuncFriendsList(TmpObj.friends_user_id);
                $scope.FuncSubscriptionList(TmpObj.friends_user_id);
                $scope.FuncWaitFriendsList(TmpObj.friends_user_id);
            }
            TmpObj.friends_user_id = null;
        });
    //});
    /*
    angular.element(document).ready(function () {
        $scope.$watch(() => TmpObj.friends_user_id, function (newVal) {
            console.log('Name changed to ' + newVal);
        });
    });
    */
});