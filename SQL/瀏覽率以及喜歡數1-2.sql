-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_like_share_datas 結構
DROP TABLE IF EXISTS `plf_like_share_datas`;
CREATE TABLE IF NOT EXISTS `plf_like_share_datas` (
  `id` varchar(50) DEFAULT NULL,
  `own_id` varchar(50) DEFAULT NULL COMMENT 'files_id,board_id',
  `type` enum('like','share') DEFAULT NULL COMMENT '類別 like,share',
  `approval` enum('Y','N') DEFAULT NULL COMMENT '開放',
  `post_date` varchar(50) DEFAULT NULL COMMENT '刊登時間',
  `update_date` varchar(50) DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='喜歡以及分享表';

-- 正在導出表  plf_api.plf_like_share_datas 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_like_share_datas` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_like_share_datas` ENABLE KEYS */;

-- 導出  表 plf_api.plf_view_share_count 結構
DROP TABLE IF EXISTS `plf_view_share_count`;
CREATE TABLE IF NOT EXISTS `plf_view_share_count` (
  `id` varchar(50) DEFAULT NULL,
  `own_id` varchar(50) DEFAULT NULL COMMENT 'files_id,board_id',
  `type` enum('like','share') DEFAULT NULL COMMENT '類別 like,share',
  `total_count` bigint(20) DEFAULT NULL COMMENT '總量',
  `week_count` bigint(20) DEFAULT NULL COMMENT '周量',
  `month_count` bigint(20) DEFAULT NULL COMMENT '月量',
  `year_count` bigint(20) DEFAULT NULL COMMENT '年量',
  `post_date` varchar(50) DEFAULT NULL COMMENT '刊登時間',
  `update_date` varchar(50) DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='喜歡以及分享表';

-- 正在導出表  plf_api.plf_view_share_count 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_view_share_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_view_share_count` ENABLE KEYS */;

-- 導出  觸發器 plf_api.plf_like_share_datas_before_insert 結構
DROP TRIGGER IF EXISTS `plf_like_share_datas_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_like_share_datas_before_insert` BEFORE INSERT ON `plf_like_share_datas` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_view_share_count_before_insert 結構
DROP TRIGGER IF EXISTS `plf_view_share_count_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_view_share_count_before_insert` BEFORE INSERT ON `plf_view_share_count` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
