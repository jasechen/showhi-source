ALTER TABLE `plf_files`
	ADD COLUMN `cloud_down_time` TIMESTAMP NULL DEFAULT NULL COMMENT '雲端影片可以看的時間';