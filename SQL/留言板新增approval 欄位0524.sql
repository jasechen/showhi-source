ALTER TABLE `plf_board`
	ADD COLUMN `approval` ENUM('Y','N') NULL DEFAULT 'Y' AFTER `update_date`;

ALTER TABLE `plf_board_data`
	ADD COLUMN `approval` ENUM('Y','N') NULL DEFAULT 'Y' AFTER `update_date`;