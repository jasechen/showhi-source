-- --------------------------------------------------------
-- 主機:                           showhi.club
-- 服務器版本:                        5.1.73 - Source distribution
-- 服務器操作系統:                      redhat-linux-gnu
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 showhi.plf_fan 結構
DROP TABLE IF EXISTS `plf_fan`;
CREATE TABLE IF NOT EXISTS `plf_fan` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `nick_name` text COMMENT '粉絲團名稱',
  `text` text COMMENT '學校簡介',
  `image` int(11) DEFAULT NULL COMMENT '圖片',
  `approval` enum('Y','A','N') DEFAULT 'A' COMMENT '是否開放',
  `bg_image` int(11) DEFAULT NULL COMMENT '背景照片',
  `bg_image_style` int(11) DEFAULT NULL COMMENT '背景圖高度',
  `fuzzy_name` text COMMENT '模糊比對',
  `school_id` int(11) DEFAULT NULL COMMENT '所屬學校id(給學校拉成員用)',
  `nationality` int(11) DEFAULT NULL,
  `post_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='粉絲團基本資料';

-- 正在導出表  showhi.plf_fan 的資料：~6 rows (大約)
/*!40000 ALTER TABLE `plf_fan` DISABLE KEYS */;
REPLACE INTO `plf_fan` (`id`, `nick_name`, `text`, `image`, `approval`, `bg_image`, `bg_image_style`, `fuzzy_name`, `school_id`, `nationality`, `post_date`, `update_date`) VALUES
	('7f33e38e-3c61-11e7-91f8-74d435c7f214', 'University of Wiscon', 'The University of Wisconsin-Eau Claire is consistently rated among the top 10 public Midwestern universities and recognized nationally for quality academics and high return on investment. Among our ranks are celebrated scholars and award winners, people who are passionate about their intellectual pursuits, critical thinkers and innovators deeply committed to interacting with and improving the world around us.', 112, 'Y', 9, NULL, 'uwec,威斯康辛,威斯康辛大學麥迪遜分校,威斯康辛大學麥迪遜分校,威斯康辛大学麦迪逊分校,威斯康辛大学', NULL, NULL, NULL, '2017-05-26 14:23:15'),
	('7f33e713-3c61-11e7-91f8-74d435c7f214', 'George Mason Univers', 'Mason is a young university that, in just a short time, has made impressive strides in size, stature and influence. Today, as Virginia’s largest public research university, we are setting the benchmark for a bold, progressive education that serves the needs of our students and communities. To that end, we have 10 schools and colleges devoted to a variety of areas of study.', 113, 'Y', 11, NULL, 'gmu,喬治梅森大學,喬治梅森,乔治梅森大学,乔治梅森', NULL, NULL, NULL, '2017-05-26 14:24:16'),
	('7f33e7ca-3c61-11e7-91f8-74d435c7f214', 'Ball State Universit', 'Ball State University, commonly referred to as Ball State or BSU, is a public coeducational research university in Muncie, Indiana, United States, with two satellite facilities in Fishers and Indianapolis. On July 25, 1917, the Ball brothers, industrialists and founders of the Ball Corporation, acquired the foreclosed Indiana Normal Institute for $35,100 and gifted the school and surrounding land to the State of Indiana. The Indiana General Assembly accepted the donation in the spring of 1918, with an initial 235 students enrolling at the Indiana State Normal School – Eastern Division on June 17, 1918.', 111, 'Y', 110, -163, 'bsu,波爾州立大學,波尔州立大学,波尔州,波爾州', NULL, NULL, NULL, '2017-05-25 15:25:17'),
	('7f33e85a-3c61-11e7-91f8-74d435c7f214', '北京师范大学', '北京师范大学是教育部直属重点大学，是一所以教师教育、教育科学和文理基础学科为主要特色的著名学府。学校的前身是1902年创立的京师大学堂师范馆，1908年改称京师优级师范学堂，独立设校，1912年改名为北京高等师范学校。1923年学校更名为北京师范大学，成为中国历史上第一所师范大学。1931年、1952年北平女子师范大学、辅仁大学先后并入北京师范大学。 \r\n', 114, 'Y', 15, NULL, 'bnu,北京师范,北京師範大學,北京師範', NULL, NULL, NULL, '2017-05-26 14:24:53'),
	('7f33e8e3-3c61-11e7-91f8-74d435c7f214', '北京交通大学', '　北京交通大学是教育部直属，教育部、中国铁路总公司、北京市人民政府共建的全国重点大学，是国家“211工程”“985工程优势学科创新平台”项目建设高校和具有研究生院的全国首批博士、硕士学位授予高校。学校牵头的“2011计划”“轨道交通安全协同创新中心”是国家首批14个认定的协同创新中心之一。2016年，学校跻身QS世界大学排行榜；QS2016金砖五国大学排名进入前60；3个学科保持ESI世界前1%；4个学科继续入围QS全球学科400强；两个专业接受《华盛顿协议》国际专家现场考察，为我国成为协议正式成员国提供了重要支撑。学校优势特色学科在全国第三轮学科评估中成绩不俗，系统科学排名第一，交通运输工程排名第三，信息与通信工程排名第八，其中系统科学在连续三轮学科评估中均名列全国第一。', 115, 'Y', 17, NULL, 'bju,北京交通大学', NULL, NULL, NULL, '2017-05-26 14:25:39'),
	('7f33e98a-3c61-11e7-91f8-74d435c7f214', '中国传媒大学', '中国传媒大学是教育部直属的国家“211工程”重点建设大学，已正式进入国家“985优势学科创新平台”项目重点建设高校行列，前身是创建于1954年的中央广播事业局技术人员训练班。1959年4月，经国务院批准，学校升格为北京广播学院。2004年8月，北京广播学院更名为中国传媒大学。学校位于中国北京城东古运河畔，校园占地面积46.37万平方米，总建筑面积49.98万平方米。', 116, 'Y', 22, NULL, 'cuc,中國傳媒大學', NULL, NULL, NULL, '2017-05-26 14:27:00');
/*!40000 ALTER TABLE `plf_fan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
