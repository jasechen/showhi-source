-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_like_count 結構
DROP TABLE IF EXISTS `plf_like_count`;
CREATE TABLE IF NOT EXISTS `plf_like_count` (
  `id` varchar(50) DEFAULT NULL,
  `own_id` varchar(50) DEFAULT NULL COMMENT 'files_id,board_id',
  `own` enum('files','board') DEFAULT NULL COMMENT '圖片還是留言板',
  `type` enum('like') DEFAULT NULL COMMENT '類別 like',
  `approval` enum('Y','N') DEFAULT NULL COMMENT 'like',
  `post_date` varchar(50) DEFAULT NULL COMMENT '刊登時間',
  `update_date` varchar(50) DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `post_by_loginid` varchar(50) DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的',
  `update_by_loginid` varchar(50) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='like 資料表';

-- 正在導出表  plf_api.plf_like_count 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_like_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_like_count` ENABLE KEYS */;

-- 導出  表 plf_api.plf_share_count 結構
DROP TABLE IF EXISTS `plf_share_count`;
CREATE TABLE IF NOT EXISTS `plf_share_count` (
  `id` varchar(50) DEFAULT NULL,
  `own_id` varchar(50) DEFAULT NULL COMMENT 'files_id,board_id',
  `type` enum('share') DEFAULT NULL COMMENT '類別 share',
  `own` enum('files','board') DEFAULT NULL COMMENT '圖片的id還是留言板的id',
  `total_count` bigint(20) DEFAULT '0' COMMENT '日總量',
  `post_day` timestamp NULL DEFAULT NULL COMMENT '刊登日',
  `post_date` timestamp NULL DEFAULT NULL COMMENT '刊登時間',
  `update_date` timestamp NULL DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `post_by_loginid` varchar(50) DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的',
  `update_by_loginid` varchar(50) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='view,like,share 資料表';

-- 正在導出表  plf_api.plf_share_count 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_share_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_share_count` ENABLE KEYS */;

-- 導出  表 plf_api.plf_view_count 結構
DROP TABLE IF EXISTS `plf_view_count`;
CREATE TABLE IF NOT EXISTS `plf_view_count` (
  `id` varchar(50) DEFAULT NULL,
  `own_id` varchar(50) DEFAULT NULL COMMENT 'files_id,board_id',
  `type` enum('view') DEFAULT NULL COMMENT '類別 view',
  `own` enum('files','board') DEFAULT NULL COMMENT '圖片的id還是留言板的id',
  `total_count` bigint(20) DEFAULT '0' COMMENT '日總量',
  `post_day` timestamp NULL DEFAULT NULL COMMENT '刊登日',
  `post_date` timestamp NULL DEFAULT NULL COMMENT '刊登時間',
  `update_date` timestamp NULL DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `post_by_loginid` varchar(50) DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的',
  `update_by_loginid` varchar(50) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='view,like,share 資料表';

-- 正在導出表  plf_api.plf_view_count 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_view_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_view_count` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
