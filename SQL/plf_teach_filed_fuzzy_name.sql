﻿ALTER TABLE `plf_teach`
	ADD COLUMN `fuzzy_name` TEXT NULL COMMENT '模糊資料' AFTER `fuzzy_name`;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='bju,北京交通大学' WHERE  `id`=18;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='cuc,中國傳媒大學' WHERE  `id`=19;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='bnu,北京师范,北京師範大學,北京師範' WHERE  `id`=17;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='bsu,波爾州立大學,波尔州立大学,波尔州,波爾州 ' WHERE  `id`=16;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='gmu,喬治梅森大學,喬治梅森,乔治梅森大学,乔治梅森' WHERE  `id`=15;
UPDATE `plf_api`.`plf_teach` SET `fuzzy_name`='uwec,威斯康辛,威斯康辛大學麥迪遜分校,威斯康辛大學麥迪遜分校,威斯康辛大学麦迪逊分校,威斯康辛大学 ' WHERE  `id`=14;