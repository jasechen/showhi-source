CREATE TABLE `plf_website_subtion` (
	`id` VARCHAR(50) NULL DEFAULT NULL,
	`email` TEXT NULL,
	`approval` ENUM('Y','N') NULL DEFAULT NULL,
	`post_date` TIMESTAMP NULL DEFAULT NULL,
	`update_date` TIMESTAMP NULL DEFAULT NULL
)
COMMENT='網站訂閱者'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
