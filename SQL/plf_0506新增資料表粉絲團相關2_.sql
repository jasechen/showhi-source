-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  過程 plf_api.ChangeRelTableUUID 結構
DROP PROCEDURE IF EXISTS `ChangeRelTableUUID`;
DELIMITER //
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `ChangeRelTableUUID`(
	IN `aid` VARCHAR(50),
	IN `bid` VARCHAR(50)

)
    COMMENT '修改相關性欄位的UUID (記得改table name)'
BEGIN
	update plf_board_data as a,
		(SELECT id from plf_board) AS b 
	set 
		a.board_id=b.id #a.board_uuid=b.uuid
	where 
		aid =bid;
	select * from plf_board_data;
END//
DELIMITER ;

-- 導出  過程 plf_api.ChangeToUUID 結構
DROP PROCEDURE IF EXISTS `ChangeToUUID`;
DELIMITER //
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `ChangeToUUID`()
    COMMENT '記得改table name'
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(*) FROM plf_board_data INTO n;
SET i=1;
WHILE i<n+1 DO 
	update plf_api.plf_board_data set id =uuid() where id=i;
	SET i = i + 1;
END WHILE;
select * from plf_board_data;

END//
DELIMITER ;

-- 導出  表 plf_api.plf_board 結構
DROP TABLE IF EXISTS `plf_board`;
CREATE TABLE IF NOT EXISTS `plf_board` (
  `id` varchar(50) NOT NULL DEFAULT '0' COMMENT 'uuid',
  `user_id` int(11) DEFAULT '0' COMMENT '刊登者id',
  `text` text COMMENT '內容',
  `type` int(11) DEFAULT NULL COMMENT '圖片 影片 課程 分享',
  `attached_id` int(11) DEFAULT NULL COMMENT '檔案id(圖片影片),board_id(分享id)',
  `read_permi` enum('public','private','friend','email') DEFAULT NULL COMMENT '''public'',''private'',''friend'',''email''',
  `post_date` datetime DEFAULT NULL COMMENT '刊登時間',
  `update_date` datetime DEFAULT NULL COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='留言板';

-- 正在導出表  plf_api.plf_board 的資料：~8 rows (大約)
DELETE FROM `plf_board`;
/*!40000 ALTER TABLE `plf_board` DISABLE KEYS */;
INSERT INTO `plf_board` (`id`, `user_id`, `text`, `type`, `attached_id`, `read_permi`, `post_date`, `update_date`) VALUES
	('0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 5, 'test1', 4, NULL, 'public', '2017-04-04 23:55:50', '2017-04-04 23:55:53'),
	('0d65270e-31c6-11e7-bfc8-74d435c7f214', 1, 'undefined', 1, 6, 'public', '2017-04-24 18:36:33', '2017-04-24 18:36:33'),
	('11659457-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 1, 6, 'private', '2017-04-25 08:44:58', '2017-05-01 14:22:03'),
	('14218878-31c6-11e7-bfc8-74d435c7f214', 1, 'dwewqe', 4, NULL, 'public', '2017-04-26 10:34:37', '2017-05-01 14:27:16'),
	('16c85c68-31c6-11e7-bfc8-74d435c7f214', 1, 'test', 4, NULL, 'public', '2017-05-03 09:52:23', '2017-05-03 09:52:23'),
	('1952c3fd-31c6-11e7-bfc8-74d435c7f214', 1, 'qqqq', 4, NULL, 'public', '2017-05-04 07:13:32', '2017-05-04 07:13:32'),
	('1be6daa1-31c6-11e7-bfc8-74d435c7f214', 1, 'qqq1234', 4, NULL, 'public', '2017-05-04 07:14:24', '2017-05-04 07:14:24'),
	('f12a54f0-31c5-11e7-bfc8-74d435c7f214', 1, 'test', 1, NULL, 'public', '2017-04-04 23:55:04', '2017-04-04 23:55:06');
/*!40000 ALTER TABLE `plf_board` ENABLE KEYS */;

-- 導出  表 plf_api.plf_board_data 結構
DROP TABLE IF EXISTS `plf_board_data`;
CREATE TABLE IF NOT EXISTS `plf_board_data` (
  `id` varchar(50) NOT NULL,
  `board_id` varchar(50) NOT NULL DEFAULT '0' COMMENT 'board_uuid',
  `re_user_id` int(11) DEFAULT '0' COMMENT '回覆者id',
  `text` text COMMENT '內容',
  `type` int(11) DEFAULT '0' COMMENT '圖片 影片 課程 分享',
  `attached_id` int(11) DEFAULT '0' COMMENT '檔案id(圖片影片),board_id(分享id)',
  `read_permi` enum('public','private','friend','email') DEFAULT NULL COMMENT '''public'',''private'',''friend'',''email''',
  `post_date` datetime DEFAULT NULL COMMENT '刊登時間',
  `update_date` datetime DEFAULT NULL COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='留言板詳細資料';

-- 正在導出表  plf_api.plf_board_data 的資料：~30 rows (大約)
DELETE FROM `plf_board_data`;
/*!40000 ALTER TABLE `plf_board_data` DISABLE KEYS */;
INSERT INTO `plf_board_data` (`id`, `board_id`, `re_user_id`, `text`, `type`, `attached_id`, `read_permi`, `post_date`, `update_date`) VALUES
	('3c20c5cd-3219-11e7-bfc8-74d435c7f214', 'f12a54f0-31c5-11e7-bfc8-74d435c7f214', 5, '1', 4, 0, 'public', '2017-04-04 23:59:52', '2017-04-04 23:59:54'),
	('3c20c919-3219-11e7-bfc8-74d435c7f214', '0d65270e-31c6-11e7-bfc8-74d435c7f214', 5, 'testre1', 4, 0, 'public', '2017-04-04 23:57:02', '2017-04-04 23:57:03'),
	('3c20ca01-3219-11e7-bfc8-74d435c7f214', '0d65270e-31c6-11e7-bfc8-74d435c7f214', 11, 'testre2', 4, 0, 'public', '2017-04-04 23:58:27', '2017-04-04 23:58:29'),
	('3c21039b-3219-11e7-bfc8-74d435c7f214', 'f12a54f0-31c5-11e7-bfc8-74d435c7f214', 13, '1', 4, 0, 'public', '2017-04-05 00:00:26', '2017-04-05 00:00:28'),
	('3c23cec7-3219-11e7-bfc8-74d435c7f214', '0d65270e-31c6-11e7-bfc8-74d435c7f214', 12, 'River', 4, 0, 'public', '2017-04-24 21:29:43', '2017-04-24 21:29:44'),
	('3c23ea90-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '123456789', 4, NULL, 'public', '2017-04-25 09:40:57', '2017-04-25 09:40:57'),
	('3c24029e-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 4, NULL, 'public', '2017-04-25 10:31:12', '2017-04-25 10:31:12'),
	('3c241cc0-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '6453312', 4, NULL, 'public', '2017-04-25 10:31:27', '2017-04-25 10:31:27'),
	('3c243548-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '3125321', 4, NULL, 'public', '2017-04-25 10:33:00', '2017-04-25 10:33:00'),
	('3c244d65-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, 'eqweqwe', 4, NULL, 'public', '2017-04-25 10:33:33', '2017-04-25 10:33:33'),
	('3c2465a9-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, 'ewqeee', 4, NULL, 'public', '2017-04-25 10:33:44', '2017-04-25 10:33:44'),
	('3c24828b-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, 'testtest', 4, NULL, 'public', '2017-04-25 10:33:56', '2017-04-25 10:33:56'),
	('3c249bf6-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '12344', 4, NULL, 'public', '2017-04-26 07:00:34', '2017-04-26 07:00:34'),
	('3c24b4c7-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '', 4, NULL, 'public', '2017-04-26 07:02:02', '2017-04-26 07:02:02'),
	('3c24ccfb-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, '0702', 4, NULL, 'public', '2017-04-26 07:02:20', '2017-04-26 07:02:20'),
	('3c24e92e-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 4, NULL, 'public', '2017-04-26 16:59:41', '2017-04-26 16:59:41'),
	('3c25025d-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 4, NULL, 'public', '2017-04-26 19:22:53', '2017-04-26 19:22:53'),
	('3c251c55-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 4, NULL, 'public', '2017-04-26 19:24:11', '2017-04-26 19:24:11'),
	('3c2537af-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '123456789', 4, NULL, 'public', '2017-04-26 19:33:06', '2017-04-26 19:33:06'),
	('3c25540c-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '12345678910', 4, NULL, 'public', '2017-04-26 19:41:40', '2017-04-26 19:41:40'),
	('3c256c53-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '1234', 4, NULL, 'public', '2017-04-26 19:42:15', '2017-04-26 19:42:15'),
	('3c2586b2-3219-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1, '1234567eeee', 4, NULL, 'public', '2017-04-26 19:44:06', '2017-04-26 19:44:06'),
	('3c259ec1-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, '312312', 4, NULL, 'public', '2017-04-27 16:41:37', '2017-04-27 16:41:37'),
	('3c25ba31-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, 'eqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqw', 4, NULL, 'public', '2017-04-27 17:20:34', '2017-04-27 17:20:34'),
	('3c2ac663-3219-11e7-bfc8-74d435c7f214', '0d65270e-31c6-11e7-bfc8-74d435c7f214', 1, 'weqrweq', 4, NULL, 'public', '2017-05-01 14:12:29', '2017-05-01 14:12:29'),
	('3c2f25aa-3219-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1, 'wwwwww', 4, NULL, 'public', '2017-05-01 14:12:49', '2017-05-01 14:12:49'),
	('3c305e0f-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, 'dadasdas', 4, NULL, 'public', '2017-05-02 01:48:46', '2017-05-02 01:48:46'),
	('3c30d727-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, '123456789', 4, NULL, 'public', '2017-05-02 01:51:31', '2017-05-02 01:51:31'),
	('3c31320b-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, '123456789', 4, NULL, 'public', '2017-05-02 22:53:52', '2017-05-02 22:53:52'),
	('3c3394bd-3219-11e7-bfc8-74d435c7f214', '14218878-31c6-11e7-bfc8-74d435c7f214', 1, '', 4, NULL, 'public', '2017-05-02 22:55:52', '2017-05-02 22:55:52');
/*!40000 ALTER TABLE `plf_board_data` ENABLE KEYS */;

-- 導出  表 plf_api.plf_board_group 結構
DROP TABLE IF EXISTS `plf_board_group`;
CREATE TABLE IF NOT EXISTS `plf_board_group` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `board_id` varchar(50) DEFAULT NULL COMMENT '留言板uuid',
  `user_id` int(11) DEFAULT NULL COMMENT 'user_id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='留言板的小圈圈';

-- 正在導出表  plf_api.plf_board_group 的資料：~3 rows (大約)
DELETE FROM `plf_board_group`;
/*!40000 ALTER TABLE `plf_board_group` DISABLE KEYS */;
INSERT INTO `plf_board_group` (`id`, `board_id`, `user_id`) VALUES
	('31dd599c-320f-11e7-bfc8-74d435c7f214', '0ad7ce68-31c6-11e7-bfc8-74d435c7f214', 1),
	('4475584c-320f-11e7-bfc8-74d435c7f214', '0d65270e-31c6-11e7-bfc8-74d435c7f214', 1),
	('beee0afd-320d-11e7-bfc8-74d435c7f214', '11659457-31c6-11e7-bfc8-74d435c7f214', 1);
/*!40000 ALTER TABLE `plf_board_group` ENABLE KEYS */;

-- 導出  表 plf_api.plf_board_type 結構
DROP TABLE IF EXISTS `plf_board_type`;
CREATE TABLE IF NOT EXISTS `plf_board_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '0' COMMENT '類別名稱',
  `rel_table` varchar(255) DEFAULT NULL COMMENT '相關資料表',
  `rel_filed` varchar(50) DEFAULT 'id' COMMENT '相關欄位(暫不使用)',
  `post_date` datetime DEFAULT NULL COMMENT '刊登時間',
  `update_date` datetime DEFAULT NULL COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='留言板裡面的類別(圖片影片分享等等)';

-- 正在導出表  plf_api.plf_board_type 的資料：~5 rows (大約)
DELETE FROM `plf_board_type`;
/*!40000 ALTER TABLE `plf_board_type` DISABLE KEYS */;
INSERT INTO `plf_board_type` (`id`, `name`, `rel_table`, `rel_filed`, `post_date`, `update_date`) VALUES
	(1, 'image', 'files', 'id', '2017-04-04 23:55:04', '2017-04-04 23:55:06'),
	(2, 'video', 'files', 'id', '2017-04-04 23:55:50', '2017-04-04 23:55:53'),
	(3, 'share', 'board', 'id', '2017-04-21 12:30:11', '2017-04-21 12:30:14'),
	(4, 'lesson', 'lesson_data', 'id', '2017-04-21 12:31:18', '2017-04-21 12:31:20'),
	(5, 'text', NULL, '', '2017-04-24 11:14:27', '2017-04-24 11:14:29');
/*!40000 ALTER TABLE `plf_board_type` ENABLE KEYS */;

-- 導出  表 plf_api.plf_fan 結構
DROP TABLE IF EXISTS `plf_fan`;
CREATE TABLE IF NOT EXISTS `plf_fan` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `name` int(11) DEFAULT NULL COMMENT '粉絲團名稱',
  `text` int(11) DEFAULT NULL COMMENT '學校簡介',
  `image` int(11) DEFAULT NULL COMMENT '圖片',
  `bg_imagae` int(11) DEFAULT NULL COMMENT '背景照片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='粉絲團基本資料';

-- 正在導出表  plf_api.plf_fan 的資料：~0 rows (大約)
DELETE FROM `plf_fan`;
/*!40000 ALTER TABLE `plf_fan` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_fan` ENABLE KEYS */;

-- 導出  表 plf_api.plf_fan_manage 結構
DROP TABLE IF EXISTS `plf_fan_manage`;
CREATE TABLE IF NOT EXISTS `plf_fan_manage` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `user_id` int(11) DEFAULT NULL COMMENT 'user_id',
  `school_id` int(11) DEFAULT NULL COMMENT '學校id',
  `type` enum('admin','editer','writer') DEFAULT NULL COMMENT 'admin,editer,writer',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='粉絲團管理清單';

-- 正在導出表  plf_api.plf_fan_manage 的資料：~0 rows (大約)
DELETE FROM `plf_fan_manage`;
/*!40000 ALTER TABLE `plf_fan_manage` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_fan_manage` ENABLE KEYS */;

-- 導出  表 plf_api.plf_school 結構
DROP TABLE IF EXISTS `plf_school`;
CREATE TABLE IF NOT EXISTS `plf_school` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `name` varchar(50) DEFAULT NULL COMMENT '學校名稱',
  `en_name` varchar(50) DEFAULT NULL COMMENT '學校en名稱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='學校資料表';

-- 正在導出表  plf_api.plf_school 的資料：~0 rows (大約)
DELETE FROM `plf_school`;
/*!40000 ALTER TABLE `plf_school` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_school` ENABLE KEYS */;

-- 導出  表 plf_api.plf_school_datas 結構
DROP TABLE IF EXISTS `plf_school_datas`;
CREATE TABLE IF NOT EXISTS `plf_school_datas` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `user_id` int(11) DEFAULT NULL COMMENT 'user_id',
  `school_id` varchar(50) DEFAULT NULL COMMENT '學校id',
  `start_date` timestamp NULL DEFAULT NULL COMMENT '入校日',
  `end_date` timestamp NULL DEFAULT NULL COMMENT '離開學校日',
  `type` enum('Original','admit') DEFAULT NULL COMMENT 'admin,editer,writer',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='學校資料表';

-- 正在導出表  plf_api.plf_school_datas 的資料：~0 rows (大約)
DELETE FROM `plf_school_datas`;
/*!40000 ALTER TABLE `plf_school_datas` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_school_datas` ENABLE KEYS */;

-- 導出  觸發器 plf_api.plf_board_before_insert 結構
DROP TRIGGER IF EXISTS `plf_board_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_board_before_insert` BEFORE INSERT ON `plf_board` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_board_data_before_insert 結構
DROP TRIGGER IF EXISTS `plf_board_data_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_board_data_before_insert` BEFORE INSERT ON `plf_board_data` FOR EACH ROW BEGIN
    SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_board_group_before_insert 結構
DROP TRIGGER IF EXISTS `plf_board_group_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_board_group_before_insert` BEFORE INSERT ON `plf_board_group` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_fan_before_insert 結構
DROP TRIGGER IF EXISTS `plf_fan_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_fan_before_insert` BEFORE INSERT ON `plf_fan` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_fan_manage_before_insert 結構
DROP TRIGGER IF EXISTS `plf_fan_manage_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_fan_manage_before_insert` BEFORE INSERT ON `plf_fan_manage` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_school_before_insert 結構
DROP TRIGGER IF EXISTS `plf_school_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_school_before_insert` BEFORE INSERT ON `plf_school` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_school_datas_before_insert 結構
DROP TRIGGER IF EXISTS `plf_school_datas_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_school_datas_before_insert` BEFORE INSERT ON `plf_school_datas` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
