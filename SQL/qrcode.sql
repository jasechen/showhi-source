-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_qrcode 結構
DROP TABLE IF EXISTS `plf_qrcode`;
CREATE TABLE IF NOT EXISTS `plf_qrcode` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `own` enum('teach','fan') DEFAULT NULL COMMENT '所屬',
  `own_id` text COMMENT 'uuid',
  `url` text COMMENT 'qrcode_連結',
  `image` varchar(200) DEFAULT NULL COMMENT 'qrcode_image',
  `expire_date` timestamp NULL DEFAULT NULL COMMENT 'qrcode 連結到期日',
  `note` text COMMENT '備註',
  `click_in` int(11) DEFAULT NULL COMMENT '點進來的次數',
  `post_date` timestamp NULL DEFAULT NULL COMMENT '刊登時間',
  `update_date` timestamp NULL DEFAULT NULL COMMENT '修改時間',
  `note_date` timestamp NULL DEFAULT NULL COMMENT '最後修改note 時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='id,own,own_id,url,expire_date';

-- 正在導出表  plf_api.plf_qrcode 的資料：~5 rows (大約)
DELETE FROM `plf_qrcode`;
/*!40000 ALTER TABLE `plf_qrcode` DISABLE KEYS */;
INSERT INTO `plf_qrcode` (`id`, `own`, `own_id`, `url`, `image`, `expire_date`, `note`, `click_in`, `post_date`, `update_date`, `note_date`) VALUES
	('bd2f05c3-9f68-11e7-a994-9ed75a3f2d23', 'teach', NULL, 'http://yahoo.com.tw', NULL, '2017-09-23 15:37:41', 'test', 1, NULL, NULL, NULL),
	('bd2f05c3-9f68-11e7-a994-9ed75a3f2d24', 'teach', NULL, 'http://google.com', NULL, '2017-09-23 15:37:41', 'test', 1, NULL, NULL, NULL),
	('c4720703-6697-4bd2-a1dc-938688127200', 'teach', '33e341aa-3225-11e7-bfc8-74d435c7f214', 'http://showhi.co/User.QRLinkCheck/john75116/1506311138', 'file_a9e346e45524e7c0a618ecfe47fcae74.png', '2017-09-26 11:45:38', NULL, NULL, '2017-09-25 11:45:38', '2017-09-25 11:45:38', NULL),
	('f4b61a8f-7a5b-4d1a-aa39-5747f4e88345', 'teach', '33e341aa-3225-11e7-bfc8-74d435c7f214', 'http://showhi.co/User.QRLinkCheck/john75116/1506077931', 'file_86a927fbcdd4c52d30817e94f71f5783.png', '2017-09-29 18:58:51', '12', NULL, '2017-09-24 17:08:50', '2017-09-25 10:19:49', '2017-09-25 10:19:49'),
	('f4b61a8f-7a5b-4d1a-aa39-5747f4e88346\r\n', 'teach', '33e341aa-3225-11e7-bfc8-74d435c7f214', 'http://showhi.co/User.QRLinkCheck/john75116/1506077999', 'file_51ad0aca3b6c2de68bb9df6268d9a156.png', '2017-09-29 18:58:51', '34', NULL, '2017-09-24 17:08:51', NULL, NULL);
/*!40000 ALTER TABLE `plf_qrcode` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
