-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_board_group 結構
DROP TABLE IF EXISTS `plf_board_group`;
CREATE TABLE IF NOT EXISTS `plf_board_group` (
  `uuid` char(50) NOT NULL COMMENT 'uuid',
  `board_id` int(11) DEFAULT NULL COMMENT '學校名稱',
  `user_id` int(11) DEFAULT NULL COMMENT '學校en名稱',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='留言板的小圈圈';

-- 正在導出表  plf_api.plf_board_group 的資料：~0 rows (大約)
DELETE FROM `plf_board_group`;
/*!40000 ALTER TABLE `plf_board_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_board_group` ENABLE KEYS */;

-- 導出  表 plf_api.plf_fan 結構
DROP TABLE IF EXISTS `plf_fan`;
CREATE TABLE IF NOT EXISTS `plf_fan` (
  `uuid` char(50) NOT NULL COMMENT 'uuid',
  `name` int(11) DEFAULT NULL COMMENT '粉絲團名稱',
  `text` int(11) DEFAULT NULL COMMENT '學校簡介',
  `image` int(11) DEFAULT NULL COMMENT '圖片',
  `bg_imagae` int(11) DEFAULT NULL COMMENT '背景照片',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='粉絲團基本資料';

-- 正在導出表  plf_api.plf_fan 的資料：~0 rows (大約)
DELETE FROM `plf_fan`;
/*!40000 ALTER TABLE `plf_fan` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_fan` ENABLE KEYS */;

-- 導出  表 plf_api.plf_fan_manage 結構
DROP TABLE IF EXISTS `plf_fan_manage`;
CREATE TABLE IF NOT EXISTS `plf_fan_manage` (
  `uuid` char(50) NOT NULL COMMENT 'uuid',
  `user_id` int(11) DEFAULT NULL COMMENT 'user_id',
  `school_id` int(11) DEFAULT NULL COMMENT '學校id',
  `type` enum('admin','editer','writer') DEFAULT NULL COMMENT 'admin,editer,writer',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='粉絲團管理清單';

-- 正在導出表  plf_api.plf_fan_manage 的資料：~0 rows (大約)
DELETE FROM `plf_fan_manage`;
/*!40000 ALTER TABLE `plf_fan_manage` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_fan_manage` ENABLE KEYS */;

-- 導出  表 plf_api.plf_school 結構
DROP TABLE IF EXISTS `plf_school`;
CREATE TABLE IF NOT EXISTS `plf_school` (
  `uuid` char(50) NOT NULL COMMENT 'uuid',
  `name` varchar(50) DEFAULT NULL COMMENT '學校名稱',
  `en_name` varchar(50) DEFAULT NULL COMMENT '學校en名稱',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='學校資料表';

-- 正在導出表  plf_api.plf_school 的資料：~0 rows (大約)
DELETE FROM `plf_school`;
/*!40000 ALTER TABLE `plf_school` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_school` ENABLE KEYS */;

-- 導出  表 plf_api.plf_school_datas 結構
DROP TABLE IF EXISTS `plf_school_datas`;
CREATE TABLE IF NOT EXISTS `plf_school_datas` (
  `uuid` char(50) NOT NULL COMMENT 'uuid',
  `user_id` int(11) DEFAULT NULL COMMENT 'user_id',
  `school_id` int(11) DEFAULT NULL COMMENT '學校id',
  `start_date` timestamp NULL DEFAULT NULL COMMENT '入校日',
  `end_date` timestamp NULL DEFAULT NULL COMMENT '離開學校日',
  `type` enum('Original','admit') DEFAULT NULL COMMENT 'admin,editer,writer',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='學校資料表';

-- 正在導出表  plf_api.plf_school_datas 的資料：~0 rows (大約)
DELETE FROM `plf_school_datas`;
/*!40000 ALTER TABLE `plf_school_datas` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_school_datas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
