ALTER TABLE `plf_board`
	CHANGE COLUMN `live_id` `live_id` TEXT NULL DEFAULT NULL COMMENT '直播ID',
	CHANGE COLUMN `stream_key` `stream_key` TEXT NULL DEFAULT NULL COMMENT '直播KEY' ,
	CHANGE COLUMN `live_embed_url` `live_embed_url` TEXT NULL DEFAULT NULL COMMENT '直播影片URL';
