ALTER TABLE `plf_files`
	CHANGE COLUMN `like_count` `like_count` BIGINT(20) NULL DEFAULT '0';
ALTER TABLE `plf_files`
	CHANGE COLUMN `view_count` `view_count` BIGINT(20) NULL DEFAULT '0';
	
ALTER TABLE `plf_board`
	CHANGE COLUMN `view_count` `view_count` BIGINT(20) NULL DEFAULT '0';
ALTER TABLE `plf_board`
	CHANGE COLUMN `like_count` `like_count` BIGINT(20) NULL DEFAULT '0';
	
	