-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_event 結構
DROP TABLE IF EXISTS `plf_event`;
CREATE TABLE IF NOT EXISTS `plf_event` (
  `id` varchar(50) DEFAULT NULL COMMENT 'UUID',
  `name` varchar(50) DEFAULT NULL COMMENT 'name',
  `text` text COMMENT '內容',
  `start_datetime` timestamp NULL DEFAULT NULL COMMENT '事件開始時間',
  `end_datetime` timestamp NULL DEFAULT NULL COMMENT '事件結束時間(先做單一日事件)',
  `own_id` varchar(50) DEFAULT NULL COMMENT '屬於誰的id',
  `pubilc_datetime` timestamp NULL DEFAULT NULL COMMENT '開放時間(cronjob)',
  `pubilc_in` enum('fan_board','board') DEFAULT NULL COMMENT '開放在哪(粉絲團留言板,個人留言板,...)',
  `type` enum('notice','crojob_board') DEFAULT NULL COMMENT '類別(提醒,預定刊登)',
  `post_date` timestamp NULL DEFAULT NULL COMMENT '刊登日時間',
  `update_date` timestamp NULL DEFAULT NULL COMMENT '修改時間',
  `post_by` varchar(50) DEFAULT NULL COMMENT '誰刊登的',
  `update_by` varchar(50) DEFAULT NULL COMMENT '誰修改的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活動表--行事曆:(預約呈現,事先公告,....)';

-- 正在導出表  plf_api.plf_event 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `plf_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `plf_event` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
