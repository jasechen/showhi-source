-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: showhi.club    Database: showhi
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plf_notice`
--

DROP TABLE IF EXISTS `plf_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plf_notice` (
  `id` varchar(50) DEFAULT NULL COMMENT 'uuid',
  `element_id` varchar(50) DEFAULT NULL COMMENT '元件id',
  `send_id` varchar(50) DEFAULT NULL COMMENT '發送的id',
  `receive_user_id` varchar(50) DEFAULT NULL COMMENT '接收的id',
  `text` varchar(50) DEFAULT NULL COMMENT '內文',
  `attached_id` varchar(255) DEFAULT NULL COMMENT 'fan_image,teach_image',
  `receive` enum('Y','A','N') DEFAULT NULL COMMENT 'Y =同意(讀取) A =等待  N =否',
  `send_table` enum('fan','teach') DEFAULT NULL COMMENT 'fan,teach 對應身分資料表',
  `receive_table` enum('fan','teach') DEFAULT NULL,
  `element` enum('board','activity','event') DEFAULT NULL COMMENT '''board'',''activity'',''event'' 元件放入任何可以通知的東西',
  `send_post_id` varchar(50) DEFAULT NULL,
  `send_loginid` varchar(50) DEFAULT NULL,
  `post_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通知表(各種通知)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plf_notice`
--

LOCK TABLES `plf_notice` WRITE;
/*!40000 ALTER TABLE `plf_notice` DISABLE KEYS */;
INSERT INTO `plf_notice` VALUES ('f3345a43-3e0b-11e7-a8bc-74d435c7f214','f42b7f33-3dde-11e7-a8bc-74d435c7f214  ','7f33e7ca-3c61-11e7-91f8-74d435c7f214','0cd8a0b0-3225-11e7-bfc8-74d435c7f214','12345678','12','A','fan','teach','board','33e341aa-3225-11e7-bfc8-74d435c7f214','john75116','2017-05-21 15:37:27','2017-05-21 15:37:28'),('19431784-3e3b-11e7-a8bc-74d435c7f214','1942b883-3e3b-11e7-a8bc-74d435c7f214','7f33e7ca-3c61-11e7-91f8-74d435c7f214','0cd8a0b0-3225-11e7-bfc8-74d435c7f214','just booking','12','A','fan','teach','board','33e341aa-3225-11e7-bfc8-74d435c7f214','john75116','2017-05-21 15:35:20','2017-05-21 15:35:20');
/*!40000 ALTER TABLE `plf_notice` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-22 10:40:37
