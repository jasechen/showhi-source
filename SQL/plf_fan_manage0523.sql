-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_fan_manage 結構
DROP TABLE IF EXISTS `plf_fan_manage`;
CREATE TABLE IF NOT EXISTS `plf_fan_manage` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `fan_id` varchar(50) DEFAULT NULL COMMENT 'fan_uuid',
  `user_id` varchar(50) DEFAULT NULL COMMENT 'user_id',
  `school_id` int(11) DEFAULT NULL COMMENT '學校id',
  `approval` enum('Y','A','N') DEFAULT NULL,
  `type` enum('admin','board','staff','nomal','teach') DEFAULT 'nomal' COMMENT 'admin,editer,writer',
  `o_type` enum('admin','board','staff','nomal','teach') DEFAULT 'nomal' COMMENT '原始權限(紀錄第一個admin)',
  `post_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='粉絲團管理清單';

-- 正在導出表  plf_api.plf_fan_manage 的資料：~12 rows (大約)
/*!40000 ALTER TABLE `plf_fan_manage` DISABLE KEYS */;
REPLACE INTO `plf_fan_manage` (`id`, `fan_id`, `user_id`, `school_id`, `approval`, `type`, `o_type`, `post_date`, `update_date`) VALUES
	('06be6f5f-3dc8-11e7-a8bc-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:42', '2017-05-21 09:52:43'),
	('40ae86cb-3e2b-11e7-a8bc-74d435c7f214', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:42', '2017-05-21 09:52:43'),
	('4f449cda-3e2b-11e7-a8bc-74d435c7f214', '7f33e713-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:42', '2017-05-21 09:52:43'),
	('63ef7e24-3e2a-11e7-a8bc-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('65b66452-3e2b-11e7-a8bc-74d435c7f214', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:42', '2017-05-21 09:52:43'),
	('93c31d3b-3e2a-11e7-a8bc-74d435c7f214', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('9eb0d3b1-3e2a-11e7-a8bc-74d435c7f214', '7f33e713-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('ad050c19-3e2a-11e7-a8bc-74d435c7f214', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('b3b6c521-3e2a-11e7-a8bc-74d435c7f214', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('b5d27438-3c69-11e7-91f8-74d435c7f214', '7f33e98a-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('d43a715e-3e2b-11e7-a8bc-74d435c7f214', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('ea1290cc-3e2b-11e7-a8bc-74d435c7f214', '7f33e98a-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', NULL, '2017-05-21 09:52:45', '2017-05-21 09:52:47');
/*!40000 ALTER TABLE `plf_fan_manage` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
