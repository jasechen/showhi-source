-- --------------------------------------------------------
-- 主機:                           showhi.club
-- 服務器版本:                        5.1.73 - Source distribution
-- 服務器操作系統:                      redhat-linux-gnu
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 showhi.plf_teach 結構
DROP TABLE IF EXISTS `plf_teach`;
CREATE TABLE IF NOT EXISTS `plf_teach` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `loginid` text NOT NULL COMMENT '帳號 email',
  `loginpwd` text NOT NULL COMMENT '密碼',
  `approval` enum('Y','N') DEFAULT 'N' COMMENT '開放',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '暱稱',
  `en_nick_name` varchar(20) DEFAULT NULL COMMENT '英文暱稱',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真實名字',
  `image` text COMMENT '圖片',
  `bg_image` text,
  `sml_image` text COMMENT '1:1小圖',
  `birth_day` date DEFAULT NULL COMMENT '生日',
  `exp` int(11) DEFAULT NULL COMMENT '教學經驗',
  `gender` enum('M','F') DEFAULT NULL COMMENT '男女',
  `nationality` varchar(20) DEFAULT NULL COMMENT '國籍',
  `email` text COMMENT 'email',
  `can_tutor_lang` text COMMENT '可以教學語言',
  `can_tutor_time` text COMMENT '可以教學時間週1',
  `can_tutor_time1` text COMMENT '可以教學時間週2',
  `can_tutor_time2` text,
  `can_tutor_time3` text,
  `can_tutor_time4` text,
  `can_tutor_time5` text,
  `can_tutor_time6` text,
  `start_date` date DEFAULT NULL COMMENT '開始日期',
  `end_date` date DEFAULT NULL COMMENT '結束日期',
  `tag_id` varchar(20) DEFAULT NULL COMMENT '標籤id',
  `accent` varchar(20) DEFAULT NULL COMMENT '腔調',
  `major` varchar(20) DEFAULT NULL COMMENT '主修',
  `expertise` text COMMENT '專長',
  `occupation` int(11) DEFAULT NULL COMMENT '職業',
  `interest` text COMMENT '興趣',
  `education` varchar(20) DEFAULT NULL COMMENT '學歷',
  `rate_count` int(11) DEFAULT '0' COMMENT '平果總數',
  `type_id` int(11) DEFAULT '1' COMMENT '類別id',
  `licenes_image_count` int(11) DEFAULT NULL COMMENT '證照數量',
  `email_code` varchar(20) DEFAULT NULL COMMENT 'email驗證碼',
  `email_va` enum('Y','N') DEFAULT 'N' COMMENT 'email驗證',
  `phone` varchar(50) DEFAULT NULL COMMENT '電話號碼',
  `phone_va` enum('Y','N') DEFAULT NULL,
  `phone_code` varchar(50) DEFAULT NULL,
  `paypal_acount` text COMMENT '銀行帳號',
  `interview_procs` text COMMENT '面試進程',
  `post_date` date DEFAULT NULL COMMENT '建立時間',
  `text` text COMMENT '中文簡介',
  `en_text` text COMMENT '英文簡介',
  `update_date` datetime DEFAULT NULL COMMENT '修改時間',
  `teacher_badnum` int(11) DEFAULT NULL COMMENT '老師未找到替代老師次數限制',
  `school_name` text COMMENT '畢業學校',
  `skype` varchar(50) DEFAULT NULL COMMENT 'skype 帳號',
  `leave_count` int(11) DEFAULT '0' COMMENT '請假數',
  `need_point` int(11) DEFAULT '1' COMMENT '需要點數',
  `forget_code` varchar(100) DEFAULT NULL COMMENT '忘記密碼code\n',
  `youtube_url` text,
  `r_youtube_url` text,
  `hot_teacher` enum('Y','N') DEFAULT NULL COMMENT '熱門老師',
  `hot_sort` int(11) DEFAULT NULL COMMENT '排序',
  `use_oth_login` enum('fb','twitter','google') DEFAULT NULL,
  `fb_email` text,
  `fb_id` text,
  `fb_first_name` text,
  `fb_last_name` text,
  `fb_locale` text,
  `fb_timezone` int(11) DEFAULT NULL,
  `fb_verified` enum('true','false') DEFAULT NULL,
  `fb_gender` text,
  `twitter_id` text,
  `teacher_category` text,
  `close_teacher` enum('Y','N') DEFAULT NULL,
  `agree_update` datetime DEFAULT NULL,
  `agree` enum('Y','N') DEFAULT NULL,
  `fuzzy_name` text COMMENT '模糊資料',
  `bg_image_style` text COMMENT '背景圖高度',
  `point` int(11) DEFAULT NULL COMMENT '點數',
  `product_id` int(11) DEFAULT NULL COMMENT '購買的產品id',
  `point_date_limit` date DEFAULT NULL COMMENT '點數到期日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='老師資料表';

-- 正在導出表  showhi.plf_teach 的資料：~15 rows (大約)
/*!40000 ALTER TABLE `plf_teach` DISABLE KEYS */;
REPLACE INTO `plf_teach` (`id`, `loginid`, `loginpwd`, `approval`, `nick_name`, `en_nick_name`, `real_name`, `image`, `bg_image`, `sml_image`, `birth_day`, `exp`, `gender`, `nationality`, `email`, `can_tutor_lang`, `can_tutor_time`, `can_tutor_time1`, `can_tutor_time2`, `can_tutor_time3`, `can_tutor_time4`, `can_tutor_time5`, `can_tutor_time6`, `start_date`, `end_date`, `tag_id`, `accent`, `major`, `expertise`, `occupation`, `interest`, `education`, `rate_count`, `type_id`, `licenes_image_count`, `email_code`, `email_va`, `phone`, `phone_va`, `phone_code`, `paypal_acount`, `interview_procs`, `post_date`, `text`, `en_text`, `update_date`, `teacher_badnum`, `school_name`, `skype`, `leave_count`, `need_point`, `forget_code`, `youtube_url`, `r_youtube_url`, `hot_teacher`, `hot_sort`, `use_oth_login`, `fb_email`, `fb_id`, `fb_first_name`, `fb_last_name`, `fb_locale`, `fb_timezone`, `fb_verified`, `fb_gender`, `twitter_id`, `teacher_category`, `close_teacher`, `agree_update`, `agree`, `fuzzy_name`, `bg_image_style`, `point`, `product_id`, `point_date_limit`) VALUES
	('33e341aa-3225-11e7-bfc8-74d435c7f214', 'john75116', '4a7d1ed414474e4033ac29ccb8653d9b', 'Y', 'John', NULL, 'Tony Stark', '93', '30', NULL, '1986-01-01', 1992, 'M', '25', 'john75116@gmail.com', '73', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '2017-04-07', '2019-04-28', '', NULL, '201', '', NULL, ',', '24', 42, 2, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', '', '2016-07-13', '12345', '12345', '2017-05-21 22:18:44', 0, '1234', NULL, 0, 1, '3075', '', '', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, '0', 8993, 1, '2018-04-19'),
	('0cd8881c-3225-11e7-bfc8-74d435c7f214', 'river75', '4a7d1ed414474e4033ac29ccb8653d9b', 'Y', 'River', NULL, 'River', '90', '56', NULL, '1981-06-13', 2004, 'M', '4', 'a0935628366@gmail.com', '73', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '2016-07-13', '2018-12-31', '', NULL, '44', '', NULL, '', '21', 0, 2, NULL, '97b691cbe6a457fe1de5', 'Y', '19810613', 'Y', '1234', 'Rogers', '', '2016-07-13', '12345', '12345', '2017-05-21 14:42:15', 0, NULL, NULL, 0, 1, NULL, '', '', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, NULL, NULL, NULL, NULL, 9000, 1, '2018-04-19'),
	('0cd8a0b0-3225-11e7-bfc8-74d435c7f214', 'harry.wu', '4a7d1ed414474e4033ac29ccb8653d9b', 'Y', 'Harry', NULL, 'Harry', 'tp1.jpg', NULL, NULL, '1965-04-04', 1992, 'M', '4', 'harry.wu@psycomputing.com', '73', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '2016-07-13', '2018-12-31', '', NULL, '201', NULL, NULL, ',', '24', 20, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', '12345', '12345', '2017-03-15 12:04:14', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdbda97-3225-11e7-bfc8-74d435c7f214', 'james.yeh', '4a7d1ed414474e4033ac29ccb8653d9b', 'Y', 'James.yeh', NULL, 'James.yeh', '106', '109', NULL, '1981-06-13', 2004, 'M', '4', 'James.yeh@psycomputing.com', '73', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '2016-07-13', '2018-12-31', '', NULL, '44', NULL, NULL, NULL, '21', 0, 2, NULL, '97b691cbe6a457fe1de5', 'Y', '19810613', 'Y', '1234', 'Rogers', NULL, '2016-07-13', 'hi I\'m James', '12345', '2017-05-22 13:23:01', 0, 'GaLiDu', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, NULL, NULL, NULL, '-180', 9000, 1, '2018-04-19'),
	('0cdbf064-3225-11e7-bfc8-74d435c7f214', 'sonicweng623', '4a7d1ed414474e4033ac29ccb8653d9b', 'Y', 'Sonicweng623', NULL, 'Sonicweng623', 'tp4.jpg', NULL, NULL, '1981-06-13', 2004, 'M', '4', 'sonicweng623@yahoo.com', '73', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '86,87,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121', '2016-07-13', '2018-12-31', '', NULL, '44', NULL, NULL, NULL, '21', 0, 2, NULL, '97b691cbe6a457fe1de5', 'Y', '19810613', 'Y', '1234', 'Rogers', NULL, '2016-07-13', '12345', '12345', '2016-07-13 14:34:00', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, NULL, NULL, NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdc98f8-3225-11e7-bfc8-74d435c7f214', 'cu_001', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_001', NULL, 'cu_001', '103', '104', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test10@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-22 11:18:39', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, '-212', 9000, 1, '2018-04-19'),
	('0cdcade4-3225-11e7-bfc8-74d435c7f214', 'cu_002', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_002', NULL, 'cu_002', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test09@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdcc6e2-3225-11e7-bfc8-74d435c7f214', 'cu_003', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_003', NULL, 'cu_003', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test08@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdcdd70-3225-11e7-bfc8-74d435c7f214', 'cu_004', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_004', NULL, 'cu_004', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test07@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdcf313-3225-11e7-bfc8-74d435c7f214', 'cu_005', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_005', NULL, 'cu_005', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test06@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdd084f-3225-11e7-bfc8-74d435c7f214', 'cu_006', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_006', NULL, 'cu_006', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test05@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdd1e3a-3225-11e7-bfc8-74d435c7f214', 'cu_007', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_007', NULL, 'cu_007', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test04@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdd3501-3225-11e7-bfc8-74d435c7f214', 'cu_008', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_008', NULL, 'cu_008', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test03@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdd4ae4-3225-11e7-bfc8-74d435c7f214', 'cu_009', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_009', NULL, 'cu_009', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test02@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19'),
	('0cdd6078-3225-11e7-bfc8-74d435c7f214', 'cu_010', 'b59c67bf196a4758191e42f76670ceba', 'Y', 'cu_010', NULL, 'cu_010', '23', '4', NULL, '1986-01-01', 1992, 'M', '25', 'john75116test01@gmail.com', '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '201', NULL, NULL, ',', '24', 37, 1, NULL, 'abc7dc9687c643abead5', 'Y', '0919900398', 'Y', '1234', 'Stark', NULL, '2016-07-13', NULL, NULL, '2017-05-01 14:40:47', 0, '1234', NULL, 0, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '247,248,249,250,251,252,253,254,255', NULL, '2017-02-16 03:00:49', 'Y', NULL, NULL, 9000, 1, '2018-04-19');
/*!40000 ALTER TABLE `plf_teach` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
