-- --------------------------------------------------------
-- 主機:                           gotp.com.tw
-- 服務器版本:                        5.5.52-MariaDB - MariaDB Server
-- 服務器操作系統:                      Linux
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 test.games_score 結構
DROP TABLE IF EXISTS `games_score`;
CREATE TABLE IF NOT EXISTS `games_score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `reviewer_user_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '評審使用者編號',
  `games_step_index_id` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '競賽階段編號',
  `games_index_id` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '競賽表頭編號',
  `games_team_index_id` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '競賽隊伍編號',
  `games_score_status` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '競賽分數狀態 , Y:評分 , D:待修改 , P:草稿',
  `score_data` longtext CHARACTER SET utf8 NOT NULL COMMENT '競賽分數(JSON格式儲存)',
  `ranking` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '排名',
  `created_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '建立者',
  `updated_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '更新者',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '建立時間',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新時間',
  `is_next_step` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score_comment` longtext CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='競賽分數';

-- 正在導出表  test.games_score 的資料：~47 rows (大約)
DELETE FROM `games_score`;
/*!40000 ALTER TABLE `games_score` DISABLE KEYS */;
INSERT INTO `games_score` (`id`, `reviewer_user_id`, `games_step_index_id`, `games_index_id`, `games_team_index_id`, `games_score_status`, `score_data`, `ranking`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_next_step`, `score_comment`) VALUES
	(2, '23', '4', '12', '13', 'N', '', NULL, '23', '23', '2017-06-07 11:09:06', '2017-06-07 11:09:06', NULL, NULL),
	(5, '23', '5', '17', '18', 'N', '', NULL, '1', '1', '2017-06-07 13:40:27', '2017-06-07 13:40:27', NULL, NULL),
	(7, '19', '7', '8', '23', 'N', '', NULL, '1', '1', '2017-06-07 16:56:40', '2017-06-07 16:56:40', NULL, NULL),
	(8, NULL, '9', '24', '27', 'N', '', NULL, '1', '1', '2017-06-08 16:50:41', '2017-06-08 16:50:43', NULL, NULL),
	(9, '19', '9', '24', '28', 'N', '', NULL, '1', '1', '2017-06-08 16:54:11', '2017-06-08 16:54:33', NULL, NULL),
	(10, NULL, '1', '2', '26', 'N', '', NULL, '1', '1', '2017-06-08 16:59:54', '2017-06-08 17:03:06', NULL, NULL),
	(11, NULL, '1', '2', '2', 'N', '', NULL, '1', '1', '2017-06-08 17:00:37', '2017-06-08 17:02:27', NULL, NULL),
	(12, NULL, '1', '2', '5', 'N', '', NULL, '1', '1', '2017-06-08 17:03:15', '2017-06-08 17:03:37', NULL, NULL),
	(13, NULL, '1', '2', '25', 'N', '', NULL, '1', '1', '2017-06-08 17:03:56', '2017-06-08 17:08:31', NULL, NULL),
	(14, NULL, '11', '25', '29', 'N', '', NULL, '1', '1', '2017-06-08 17:14:27', '2017-06-08 17:25:54', NULL, NULL),
	(15, '35', '11', '25', '30', 'N', '', NULL, '1', '1', '2017-06-08 17:25:58', '2017-06-08 17:25:58', NULL, NULL),
	(16, NULL, '13', '28', '32', 'N', '', NULL, '20', '20', '2017-06-08 18:36:26', '2017-06-08 18:37:04', NULL, NULL),
	(17, NULL, '13', '28', '33', 'N', '', NULL, '20', '20', '2017-06-08 18:39:59', '2017-06-08 18:42:54', NULL, NULL),
	(18, '35', '13', '28', '35', 'N', '', NULL, '20', '20', '2017-06-08 18:42:59', '2017-06-08 18:42:59', NULL, NULL),
	(19, NULL, '14', '31', '38', 'N', '', NULL, '23', '23', '2017-06-08 19:02:56', '2017-06-08 19:03:09', NULL, NULL),
	(20, NULL, '15', '34', '42', 'N', '', NULL, '20', '20', '2017-06-08 19:30:15', '2017-06-08 19:30:34', NULL, NULL),
	(21, '36', '15', '34', '43', 'N', '', NULL, '20', '20', '2017-06-08 19:31:56', '2017-06-08 19:31:56', NULL, NULL),
	(22, '35', '15', '34', '44', 'N', '', NULL, '20', '20', '2017-06-08 19:35:41', '2017-06-08 19:35:41', NULL, NULL),
	(23, NULL, '16', '36', '46', 'N', '', NULL, '1', '1', '2017-06-08 19:58:15', '2017-06-08 20:02:09', NULL, NULL),
	(24, '22', '17', '39', '48', 'Y', '{"score_data":"80","score_comment":"gggg"}', NULL, '1', '22', '2017-06-08 22:49:56', '2017-06-11 05:27:59', NULL, NULL),
	(25, '22', '17', '39', '49', 'Y', '{"score_data":"100","score_comment":"gg"}', NULL, '1', '22', '2017-06-09 02:34:36', '2017-06-11 05:27:47', NULL, NULL),
	(26, '35', '19', '41', '53', 'N', '', NULL, '20', '20', '2017-06-10 07:00:47', '2017-06-10 07:18:42', NULL, NULL),
	(27, '35', '21', '44', '59', 'N', '', NULL, '40', '40', '2017-06-10 11:03:33', '2017-06-10 11:04:01', NULL, NULL),
	(28, '40', '21', '44', '60', 'N', '', NULL, '40', '40', '2017-06-10 11:03:33', '2017-06-10 11:03:55', NULL, NULL),
	(29, '19', '22', '24', '27', 'N', '', NULL, '1', '1', '2017-06-10 20:44:08', '2017-06-10 20:44:08', NULL, NULL),
	(30, '33', '22', '24', '28', 'N', '', NULL, '1', '1', '2017-06-10 20:44:08', '2017-06-10 20:44:21', NULL, NULL),
	(31, '44', '25', '57', '66', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-09 23:19:08', NULL, NULL),
	(32, '54', '25', '57', '67', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-09 23:19:08', NULL, NULL),
	(33, '52', '25', '57', '68', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-09 23:19:08', NULL, NULL),
	(34, '51', '25', '57', '69', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-09 23:19:08', NULL, NULL),
	(35, '44', '25', '57', '70', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(36, '54', '25', '57', '71', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(37, '52', '25', '57', '72', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(38, '51', '25', '57', '73', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(39, '49', '25', '57', '74', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(40, '50', '25', '57', '75', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(41, '50', '25', '57', '76', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(42, '54', '25', '57', '77', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(43, '52', '25', '57', '78', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(44, '51', '25', '57', '79', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(45, '49', '25', '57', '80', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(46, '44', '25', '57', '82', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:21:56', NULL, NULL),
	(47, '44', '25', '57', '83', 'N', '', NULL, '1', '1', '2017-08-09 23:11:22', '2017-08-10 20:11:16', NULL, NULL),
	(48, '54', '25', '57', '86', 'N', '', NULL, '1', '1', '2017-08-10 20:11:16', '2017-08-10 20:11:16', NULL, NULL),
	(49, '52', '25', '57', '89', 'N', '', NULL, '1', '1', '2017-08-10 20:11:16', '2017-08-10 20:11:16', NULL, NULL),
	(50, '51', '25', '57', '92', 'N', '', NULL, '1', '1', '2017-08-10 20:11:16', '2017-08-10 20:11:16', NULL, NULL),
	(51, '49', '25', '57', '96', 'N', '', NULL, '1', '1', '2017-08-10 20:11:16', '2017-08-10 20:11:16', NULL, NULL);
/*!40000 ALTER TABLE `games_score` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
