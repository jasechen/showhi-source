ALTER TABLE `plf_files`
	ADD COLUMN `share_count` BIGINT(20) NULL DEFAULT '0';
	
ALTER TABLE `plf_board`
	ADD COLUMN `share_count` BIGINT(20) NULL DEFAULT '0' AFTER `view_count`;