ALTER TABLE `plf_teach` ADD COLUMN 
`invite_loginid` TEXT NULL COMMENT '邀請人loginid';
ALTER TABLE `plf_teach`  ADD COLUMN
   `invite_user_id` TEXT NULL COMMENT '邀請人id';
ALTER TABLE `plf_teach`  ADD COLUMN
	`invite_qrcode_id` TEXT NULL COMMENT 'qrcode_id';
ALTER TABLE `plf_teach`  ADD COLUMN
	`invite_url` TEXT NULL COMMENT '當初邀請進來的連結(產生qrcode)';
ALTER TABLE `plf_teach`  ADD COLUMN
	`block` ENUM('Y','N') NULL DEFAULT NULL COMMENT '邀請人封鎖這個用戶帳號';
ALTER TABLE `plf_teach`  ADD COLUMN
	`invite_note` TEXT NULL COMMENT '邀請人針對這個人的備註';
ALTER TABLE `plf_teach`  ADD COLUMN
	`approval_date` TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `plf_teach`  ADD COLUMN
	`block_date` TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `plf_teach`  ADD COLUMN
	`invite_note_date` TIMESTAMP NULL DEFAULT NULL COMMENT '最後修改note 時間';

ALTER TABLE `plf_qrcode`ADD COLUMN 
`type` ENUM('S','A') NULL COMMENT 'Student = S , Agent  = A';
