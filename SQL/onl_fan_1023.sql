-- --------------------------------------------------------
-- 主機:                           showhi.club
-- 服務器版本:                        5.1.73 - Source distribution
-- 服務器操作系統:                      redhat-linux-gnu
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 showhi.plf_fan 結構
DROP TABLE IF EXISTS `plf_fan`;
CREATE TABLE IF NOT EXISTS `plf_fan` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `nick_name` text COMMENT '粉絲團名稱',
  `text` text COMMENT '學校簡介',
  `image` int(11) DEFAULT NULL COMMENT '圖片',
  `approval` enum('Y','A','N') DEFAULT 'A' COMMENT '是否開放',
  `bg_image` int(11) DEFAULT NULL COMMENT '背景照片',
  `bg_image_style` int(11) DEFAULT NULL COMMENT '背景圖高度',
  `fuzzy_name` text COMMENT '模糊比對',
  `school_id` int(11) DEFAULT NULL COMMENT '所屬學校id(給學校拉成員用)',
  `nationality` int(11) DEFAULT NULL,
  `post_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `fan_type` enum('school','department','community') DEFAULT NULL COMMENT '粉絲團類別',
  `email` tinyint(1) DEFAULT NULL COMMENT '空欄位 給全月搜尋用，避免error',
  `loginid` tinyint(1) DEFAULT NULL COMMENT '空欄位 給全月搜尋用，避免error'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='粉絲團基本資料';

-- 正在導出表  showhi.plf_fan 的資料：27 rows
DELETE FROM `plf_fan`;
/*!40000 ALTER TABLE `plf_fan` DISABLE KEYS */;
INSERT INTO `plf_fan` (`id`, `nick_name`, `text`, `image`, `approval`, `bg_image`, `bg_image_style`, `fuzzy_name`, `school_id`, `nationality`, `post_date`, `update_date`, `fan_type`, `email`, `loginid`) VALUES
	('236c2afe-45e6-11e7-8e89-0a715d5224b9', 'test531粉絲團創建功能', 'test', 301, 'N', 300, 44, 'ts,ts531', NULL, NULL, '2017-05-31 17:47:19', '2017-07-09 17:15:02', 'school', NULL, NULL),
	('7f33e38e-3c61-11e7-91f8-74d435c7f214', 'University of Wiscon', '\n                        \n                    \n                    The University of Wisconsin-Eau Claire is consistently rated among the top 10 public Midwestern universities and recognized nationally for quality academics and high return on investment. Among our ranks are celebrated scholars and award winners, people who are passionate about their intellectual pursuits, critical thinkers and innovators deeply committed to interacting with and improving the world around us.\n\n                \n\n                \n\n                    ', 118, 'Y', 9, NULL, 'uwec,威斯康辛,威斯康辛大學麥迪遜分校,威斯康辛大學麥迪遜分校,威斯康辛大学麦迪逊分校,威斯康辛大学', NULL, NULL, NULL, '2017-09-19 20:44:20', 'school', NULL, NULL),
	('7f33e713-3c61-11e7-91f8-74d435c7f214', 'George Mason Univers', '\n                        \n                        \n                        Mason is a young university that, in just a short time, has made impressive strides in size, stature and influence. Today, as Virginia’s largest public research university, we are setting the benchmark for a bold, progressive education that serves the needs of our students and communities. To that end, we have 10 schools and colleges devoted to a variety of areas of study.', 119, 'Y', 363, 0, 'gmu,喬治梅森大學,喬治梅森,乔治梅森大学,乔治梅森', NULL, NULL, NULL, '2017-09-10 13:41:03', 'school', NULL, NULL),
	('7f33e7ca-3c61-11e7-91f8-74d435c7f214', 'Ball State Universit', '美国鲍尔州立大学位于印第安纳州中西部核心区的曼西市，离印第安纳州首府印第安纳波利斯只有一小时左右的车程。学校始建于1899年，是一所国家公立型大学。学校每年约有21,000位来自全美以及70多个国家和地区的在校学生在众多的专业领域学习，包含本科、研究生以及博士生等学位项目。我校研究生学科中的商科专业，教育专业和护理专业在全美排名前20名，并且在“美国新闻和世界报道”最佳远程课程评比中排名全美前30名。', 117, 'N', 442, 77, 'bsu,波爾州立大學,波尔州立大学,波尔州,波爾州', NULL, NULL, NULL, '2017-09-14 01:59:57', 'school', NULL, NULL),
	('7f33e85a-3c61-11e7-91f8-74d435c7f214', '北京师范大学', '北京师范大学是教育部直属重点大学，是一所以教师教育、教育科学和文理基础学科为主要特色的著名学府。学校的前身是1902年创立的京师大学堂师范馆，1908年改称京师优级师范学堂，独立设校，1912年改名为北京高等师范学校。1923年学校更名为北京师范大学，成为中国历史上第一所师范大学。1931年、1952年北平女子师范大学、辅仁大学先后并入北京师范大学。 \r\n', 120, 'Y', 15, NULL, 'bnu,北京师范,北京師範大學,北京師範', NULL, NULL, NULL, '2017-05-27 01:03:19', 'school', NULL, NULL),
	('7f33e8e3-3c61-11e7-91f8-74d435c7f214', '北京交通大学', '　北京交通大学是教育部直属，教育部、中国铁路总公司、北京市人民政府共建的全国重点大学，是国家“211工程”“985工程优势学科创新平台”项目建设高校和具有研究生院的全国首批博士、硕士学位授予高校。学校牵头的“2011计划”“轨道交通安全协同创新中心”是国家首批14个认定的协同创新中心之一。2016年，学校跻身QS世界大学排行榜；QS2016金砖五国大学排名进入前60；3个学科保持ESI世界前1%；4个学科继续入围QS全球学科400强；两个专业接受《华盛顿协议》国际专家现场考察，为我国成为协议正式成员国提供了重要支撑。学校优势特色学科在全国第三轮学科评估中成绩不俗，系统科学排名第一，交通运输工程排名第三，信息与通信工程排名第八，其中系统科学在连续三轮学科评估中均名列全国第一。', 121, 'Y', 17, NULL, 'bju,北京交通大学', NULL, NULL, NULL, '2017-05-27 01:04:19', 'school', NULL, NULL),
	('7f33e98a-3c61-11e7-91f8-74d435c7f214', '中国传媒大学', '中国传媒大学是教育部直属的国家“211工程”重点建设大学，已正式进入国家“985优势学科创新平台”项目重点建设高校行列，前身是创建于1954年的中央广播事业局技术人员训练班。1959年4月，经国务院批准，学校升格为北京广播学院。2004年8月，北京广播学院更名为中国传媒大学。学校位于中国北京城东古运河畔，校园占地面积46.37万平方米，总建筑面积49.98万平方米。', 123, 'Y', 22, NULL, 'cuc,中國傳媒大學', NULL, NULL, NULL, '2017-05-27 01:06:25', 'school', NULL, NULL),
	('c3a138b8-4238-11e7-8e89-0a715d5224b9', 'testCommunity', '', 319, 'N', 320, 53, 'tc', NULL, NULL, '2017-05-27 01:28:42', '2017-10-17 16:23:53', 'school', NULL, NULL),
	('5a1b0c00-4681-11e7-8e89-0a715d5224b9', 'Test Group Create', '\n                    This group is test group test~~~~~~~~~~~~~~~~~~~', 167, 'N', 166, -91, 'Tgc', NULL, NULL, '2017-06-01 12:18:23', '2017-06-04 20:07:53', 'school', NULL, NULL),
	('b0523376-4921-11e7-8e89-0a715d5224b9', '6/4 test US', '123456', 173, 'N', 172, -94, '64t', NULL, NULL, '2017-06-04 20:31:09', '2017-06-04 20:32:43', 'school', NULL, NULL),
	('52475d96-629f-11e7-8a3d-0a715d5224b9', '四川师范大学', '四川师范大学是四川省人民政府举办的全日制综合性省属重点大学，是四川省举办师范类本科专业最早、师范类院校中办学历史最为悠久的大学。学校位于四川省省会——成都市，校园占地面积3000余亩。现有全日制本专科学生37000余人，博士与硕士研究生4000余人。现有各类教学、科研人员3000余人，其中，具有高级专业技术职务的教师约1100人，具有博士和硕士学位的教师1900余人。', 285, 'Y', NULL, NULL, 'sicnu', NULL, NULL, '2017-07-07 07:03:27', '2017-07-07 07:12:22', 'school', NULL, NULL),
	('ba9e127c-629f-11e7-8a3d-0a715d5224b9', '温州医科大学', '温州医科大学是浙江省省属普通高等学校。学校可溯源至创办于1912年的浙江医学专门学校，1958年8月由浙江医学院从杭州分迁至温州建立，初名“浙江第二医学院”，后以校址所在地改名为“温州医学院”，2013年更名为“温州医科大学”，2015年成为浙江省政府、国家卫生计生委和教育部共建高校。', 286, 'Y', NULL, NULL, 'wmu', NULL, NULL, '2017-07-07 07:06:22', '2017-07-07 07:13:32', 'school', NULL, NULL),
	('de24477a-629f-11e7-8a3d-0a715d5224b9', '淮阴师范学院', '淮阴师范学院是一所具有深厚历史文化底蕴和优良校风的江苏省属知名高等学府，坐落在风光秀美、人文荟萃的国家历史文化名城、周恩来总理故乡江苏省淮安市。', 287, 'Y', NULL, NULL, 'hytc', NULL, NULL, '2017-07-07 07:07:22', '2017-07-07 07:15:26', 'school', NULL, NULL),
	('fa615f9a-629f-11e7-8a3d-0a715d5224b9', '哈尔滨师范大学', '哈尔滨师范大学坐落于素有“冰城夏都”美誉的历史文化名城哈尔滨，是一所具有悠久办学历史、以教师教育为特色的高水平教学研究型大学,是黑龙江省教育、艺术、人文社会科学和自然科学的重要人才培养基地和科学研究基地，是黑龙江省属重点建设的高水平大学，是教育部本科教学工作水平评估优秀学校。学校是国家中西部高校基础能力建设重点院校，是“十三五”时期国家重点支持建设的百所高校之一。 \n\n　　  学校前身是1946年我党在东北解放区建立的松江省立行知师范学校，1951年成立哈尔滨师范专科学校，1956年扩建为哈尔滨师范学院，1980年更名为哈尔滨师范大学。2000年，呼兰师范专科学校、黑龙江农垦师范专科学校并入，2002年，黑龙江省物资学校（黑龙江省物资职工大学）并入，组建成新的哈尔滨师范大学。', 288, 'Y', NULL, NULL, 'hrbnu', NULL, NULL, '2017-07-07 07:08:09', '2017-09-09 14:51:54', 'school', NULL, NULL),
	('0d4a0bb6-62a0-11e7-8a3d-0a715d5224b9', '东北农业大学', '东北农业大学是一所“以农科为优势，以生命科学和食品科学为特色，农、工、理、经、管等多学科协调发展”的国家“211工程”重点建设大学，是黑龙江省人民政府与农业部省部共建大学、国家“中西部高校基础能力建设工程”项目入选高校、教育部本科教学工作水平评估优秀院校。\n\n学校1948年创建于哈尔滨，始称东北农学院，是中国共产党在解放区创办的第一所普通高等农业院校，知名教育家、哈尔滨市第一任市长刘达为学校第一任院长。建校以来，先后隶属于东北行政委员会农业部、国家高等教育部、国家农业部、黑龙江省农业委员会、黑龙江省教育厅，1981年被确定为黑龙江省省属重点院校，1994年与黑龙江省农业管理干部学院合并组建东北农业大学。', 289, 'Y', NULL, NULL, 'neau', NULL, NULL, '2017-07-07 07:08:41', '2017-09-09 14:54:50', 'school', NULL, NULL),
	('73e00264-6588-11e7-8a3d-0a715d5224b9', '7/10 test', '\n                        testasdasdasdasdasdasdasdasda', 323, 'N', 322, 54, 'test', NULL, NULL, '2017-07-10 23:57:19', '2017-07-10 23:58:50', 'community', NULL, NULL),
	('8ab7af12-6602-11e7-8a3d-0a715d5224b9', '7/11 測試社團', '\n                        test 7/11 change', 332, 'N', 331, 29, '711', NULL, NULL, '2017-07-11 14:31:16', '2017-07-11 14:34:39', 'school', NULL, NULL),
	('ad18b984-6bf2-11e7-8768-0a715d5224b9', 'University of Southern California', 'The University of Southern California (USC[a] or SC) is a private research university located in Los Angeles, California. Founded in 1880, it is the oldest private research university in California.[9] USC has historically educated a large number of the region\'s business leaders and professionals. In recent decades, the university has also leveraged its location in Los Angeles to establish relationships with research and cultural institutions throughout Asia and the Pacific Rim. An engine for economic activity, USC contributes $8 billion annually to the economy of the Los Angeles metropolitan area and California.', 382, 'N', 383, -6, 'USC', NULL, NULL, '2017-07-19 03:52:48', '2017-09-09 14:41:09', 'department', NULL, NULL),
	('d4029610-6c28-11e7-8768-0a715d5224b9', '坪林國中', '\n                        本校創立於民國五十七年，是新北市坪林區唯一的一所國中，學區包括坪林國小及漁光國小，亦有學生來自鄰近的雲海國小、雙峰國小等等。\n坪中是一所迷你的偏遠小型學校，然師生互動密切，相處和樂，宛如一個大家庭。多數教師住在學校宿舍，課餘時間仍抽空指導學生功課或進行家庭訪問，其犧牲奉獻的精神令人感動。學校自八十七年三月起，開辦學生住宿方案，讓住在偏遠山區的學生，從此可以專心向學。 \n然而自翡翠水庫興建後，全鄉受限於水源保護區的管制，導致產業發展受限，年輕人紛紛到外地發展，青壯年人口日益減少，高齡化的現象與日俱增，單親家庭、隔代教養等問題一一浮現。我們深感學校教育有責任引領學生重新體認社區對其生活的重大意義，應該喚醒學生的社區意識，俾能讓學生找回對家鄉的認同與關愛。\n\n                    ', 396, 'Y', 397, 26, '坪林,', NULL, NULL, '2017-07-19 10:20:26', '2017-09-09 14:58:40', 'school', NULL, NULL),
	('74d4cf08-7837-11e7-8768-0a715d5224b9', 'Guest_10 Test', '社群管理員功能測試', NULL, 'N', NULL, NULL, 'TS', NULL, NULL, '2017-08-03 18:35:23', '2017-08-03 18:36:29', 'community', NULL, NULL),
	('942fe3ec-7855-11e7-8768-0a715d5224b9', 'UUSS', 'The King\'s School is a selective British co-educational independent school for both day and boarding pupils in the English city of Canterbury in Kent. It is a member of the Headmasters\' and Headmistresses\' Conference and the Eton Group. It is held to be the oldest continuously operating school in the world, having been founded in 597 AD.', 436, 'Y', 421, 100, 'The King\'s School is a selective British co-educational independent school for both day and boarding pupils in the English city of Canterbury in Kent. It is a member of the Headmasters\' and Headmistresses\' Conference and the Eton Group. It is held to be the oldest continuously operating school in the world, having been founded in 597 AD.', NULL, NULL, '2017-08-03 22:11:01', '2017-08-30 09:50:38', 'department', NULL, NULL),
	('08424810-7e00-11e7-8768-0a715d5224b9', 'APLUSTART', 'APLUSTART', 438, 'Y', 427, 55, 'APLUSTART', NULL, NULL, '2017-08-11 03:13:46', '2017-08-30 09:54:25', 'community', NULL, NULL),
	('929df164-9532-11e7-8768-0a715d5224b9', 'Ball State University', '美国鲍尔州立大学位于印第安纳州中西部核心区的曼西市，离印第安纳州首府印第安纳波利斯只有一小时左右的车程。学校始建于1899年，是一所国家公立型大学。学校每年约有21,000位来自全美以及70多个国家和地区的在校学生在众多的专业领域学习，包含本科、研究生以及博士生等学位项目。我校研究生学科中的商科专业，教育专业和护理专业在全美排名前20名，并且在“美国新闻和世界报道”最佳远程课程评比中排名全美前30名。\n                        \n\n                    ', 443, 'Y', 444, 75, 'BSU', NULL, NULL, '2017-09-09 15:43:29', '2017-09-09 15:46:46', 'school', NULL, NULL),
	('9b0e6ed4-b880-11e7-8768-0a715d5224b9', 'A大學', 'AC created by Robert_agent', NULL, 'Y', NULL, NULL, 'AC', NULL, NULL, '2017-10-24 14:00:15', '2017-10-24 14:56:13', 'school', NULL, NULL),
	('648fc1cc-b886-11e7-8768-0a715d5224b9', 'Reb大學', '相親相愛', NULL, 'N', NULL, NULL, 'reb', NULL, NULL, '2017-10-24 14:41:41', '2017-10-24 14:41:41', 'school', NULL, NULL),
	('6047ecf6-b887-11e7-8768-0a715d5224b9', 'Reb院', 'reb學院', NULL, 'N', NULL, NULL, 'rc', NULL, NULL, '2017-10-24 14:48:43', '2017-10-24 14:48:43', 'department', NULL, NULL),
	('73d5c586-b887-11e7-8768-0a715d5224b9', 'reb社團', 'reg社團', NULL, 'N', NULL, NULL, 'rg', NULL, NULL, '2017-10-24 14:49:16', '2017-10-24 14:49:16', 'community', NULL, NULL);
/*!40000 ALTER TABLE `plf_fan` ENABLE KEYS */;

-- 導出  表 showhi.plf_fan_manage 結構
DROP TABLE IF EXISTS `plf_fan_manage`;
CREATE TABLE IF NOT EXISTS `plf_fan_manage` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `fan_id` varchar(50) DEFAULT NULL COMMENT 'fan_uuid',
  `user_id` varchar(50) DEFAULT NULL COMMENT 'user_id',
  `school_id` int(11) DEFAULT NULL COMMENT '學校id',
  `approval` enum('Y','A','N','I') DEFAULT 'A' COMMENT '''A'' =等待管理者開放 ''I''=等邀請者同意',
  `type` enum('admin','board','staff','nomal','teach') DEFAULT 'nomal' COMMENT 'admin,editer,writer',
  `o_type` enum('admin','board','staff','nomal','teach') DEFAULT 'nomal' COMMENT '原始權限(紀錄第一個admin)',
  `post_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='粉絲團管理清單';

-- 正在導出表  showhi.plf_fan_manage 的資料：87 rows
DELETE FROM `plf_fan_manage`;
/*!40000 ALTER TABLE `plf_fan_manage` DISABLE KEYS */;
INSERT INTO `plf_fan_manage` (`id`, `fan_id`, `user_id`, `school_id`, `approval`, `type`, `o_type`, `post_date`, `update_date`) VALUES
	('06be6f5f-3dc8-11e7-a8bc-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'teach', 'teach', '2017-05-21 09:52:42', '2017-06-12 01:17:41'),
	('40ae86cb-3e2b-11e7-a8bc-74d435c7f214', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', 'staff', '2017-05-21 09:52:42', '2017-05-21 09:52:43'),
	('4f449cda-3e2b-11e7-a8bc-74d435c7f214', '7f33e713-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', 'staff', '2017-05-21 09:52:42', '2017-05-31 13:47:33'),
	('63ef7e24-3e2a-11e7-a8bc-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('65b66452-3e2b-11e7-a8bc-74d435c7f214', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', 'staff', '2017-05-21 09:52:42', '2017-05-24 10:53:24'),
	('93c31d3b-3e2a-11e7-a8bc-74d435c7f214', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('9eb0d3b1-3e2a-11e7-a8bc-74d435c7f214', '7f33e713-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('a7db2d76-400a-11e7-9905-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-05-24 06:53:36', '2017-05-28 12:40:02'),
	('a8c439cd-4009-11e7-9905-74d435c7f214', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '0cd8a0b0-3225-11e7-bfc8-74d435c7f214', NULL, 'N', 'nomal', 'nomal', '2017-05-24 06:46:28', '2017-05-24 08:31:38'),
	('ad050c19-3e2a-11e7-a8bc-74d435c7f214', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('b3b6c521-3e2a-11e7-a8bc-74d435c7f214', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', 'staff', '2017-05-21 09:52:45', '2017-07-06 21:24:59'),
	('b5d27438-3c69-11e7-91f8-74d435c7f214', '7f33e98a-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'staff', 'staff', '2017-05-21 09:52:45', '2017-06-07 22:33:11'),
	('d43a715e-3e2b-11e7-a8bc-74d435c7f214', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('ea1290cc-3e2b-11e7-a8bc-74d435c7f214', '7f33e98a-3c61-11e7-91f8-74d435c7f214', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 1, 'Y', 'admin', 'admin', '2017-05-21 09:52:45', '2017-05-21 09:52:47'),
	('c3a241c2-4238-11e7-8e89-0a715d5224b9', 'c3a138b8-4238-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-05-27 01:28:42', '2017-05-27 01:28:42'),
	('236cb9e2-45e6-11e7-8e89-0a715d5224b9', '236c2afe-45e6-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-05-31 17:47:19', '2017-05-31 17:47:19'),
	('5a1be882-4681-11e7-8e89-0a715d5224b9', '5a1b0c00-4681-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-06-01 12:18:23', '2017-06-01 12:18:23'),
	('28cf2612-4768-11e7-8e89-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'staff', 'nomal', '2017-06-02 15:50:34', '2017-07-07 08:54:38'),
	('81d95d06-491e-11e7-8e89-0a715d5224b9', '5a1b0c00-4681-11e7-8e89-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'N', 'nomal', 'nomal', '2017-06-04 20:08:23', '2017-06-04 20:08:57'),
	('b0534cb6-4921-11e7-8e89-0a715d5224b9', 'b0523376-4921-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-06-04 20:31:09', '2017-06-04 20:31:09'),
	('cd372cce-4ad1-11e7-8a3d-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cdd4ae4-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'teach', 'nomal', '2017-06-07 00:04:20', '2017-07-07 08:54:31'),
	('073f6d8c-5c2f-11e7-8a3d-0a715d5224b9', '7f33e713-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-06-29 02:24:31', '2017-08-03 13:13:04'),
	('113852cc-5c2f-11e7-8a3d-0a715d5224b9', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-06-29 02:24:48', '2017-09-09 13:13:53'),
	('f6836e8e-625b-11e7-8a3d-0a715d5224b9', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-07-06 23:01:17', '2017-07-07 00:07:11'),
	('61712b06-6279-11e7-8a3d-0a715d5224b9', '7f33e713-3c61-11e7-91f8-74d435c7f214', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'staff', 'nomal', '2017-07-07 02:31:52', '2017-07-07 08:54:45'),
	('6a96d42e-6279-11e7-8a3d-0a715d5224b9', '7f33e7ca-3c61-11e7-91f8-74d435c7f214', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'staff', 'nomal', '2017-07-07 02:32:07', '2017-07-07 09:00:06'),
	('524802dc-629f-11e7-8a3d-0a715d5224b9', '52475d96-629f-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-07 07:03:27', '2017-07-07 07:03:27'),
	('ba9ec7b2-629f-11e7-8a3d-0a715d5224b9', 'ba9e127c-629f-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-07 07:06:22', '2017-07-07 07:06:22'),
	('de24f1b6-629f-11e7-8a3d-0a715d5224b9', 'de24477a-629f-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-07 07:07:22', '2017-07-07 07:07:22'),
	('fa64752c-629f-11e7-8a3d-0a715d5224b9', 'fa615f9a-629f-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-07 07:08:09', '2017-07-07 07:08:09'),
	('0d4dc396-62a0-11e7-8a3d-0a715d5224b9', '0d4a0bb6-62a0-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-07 07:08:41', '2017-07-07 07:08:41'),
	('665bae16-62ab-11e7-8a3d-0a715d5224b9', '52475d96-629f-11e7-8a3d-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'nomal', 'nomal', '2017-07-07 08:29:55', '2017-07-07 08:41:24'),
	('c8c12eea-62ac-11e7-8a3d-0a715d5224b9', '52475d96-629f-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'admin', 'nomal', '2017-07-07 08:39:50', '2017-07-07 08:41:29'),
	('cefd683c-62ac-11e7-8a3d-0a715d5224b9', 'ba9e127c-629f-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'admin', 'nomal', '2017-07-07 08:40:00', '2017-07-07 08:41:41'),
	('d3c2616a-62ac-11e7-8a3d-0a715d5224b9', 'de24477a-629f-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'admin', 'nomal', '2017-07-07 08:40:08', '2017-07-07 08:41:52'),
	('d8e13158-62ac-11e7-8a3d-0a715d5224b9', 'fa615f9a-629f-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'admin', 'nomal', '2017-07-07 08:40:17', '2017-07-07 08:42:04'),
	('dde3e556-62ac-11e7-8a3d-0a715d5224b9', '0d4a0bb6-62a0-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'admin', 'nomal', '2017-07-07 08:40:25', '2017-07-07 08:42:26'),
	('1d95b182-62b4-11e7-8a3d-0a715d5224b9', '52475d96-629f-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-07-07 09:32:18', '2017-07-07 09:41:41'),
	('2d0bb4ea-62b4-11e7-8a3d-0a715d5224b9', 'ba9e127c-629f-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-07-07 09:32:44', '2017-07-07 09:41:48'),
	('396a84d2-62b4-11e7-8a3d-0a715d5224b9', 'de24477a-629f-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-07-07 09:33:05', '2017-07-07 09:41:38'),
	('7442c054-649b-11e7-8a3d-0a715d5224b9', 'c3a138b8-4238-11e7-8e89-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'nomal', 'nomal', '2017-07-09 19:40:49', '2017-10-03 16:17:26'),
	('73e0a958-6588-11e7-8a3d-0a715d5224b9', '73e00264-6588-11e7-8a3d-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-10 23:57:19', '2017-07-10 23:57:19'),
	('8ab91f14-6602-11e7-8a3d-0a715d5224b9', '8ab7af12-6602-11e7-8a3d-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-11 14:31:16', '2017-07-11 14:31:16'),
	('ad193b02-6bf2-11e7-8768-0a715d5224b9', 'ad18b984-6bf2-11e7-8768-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-19 03:52:48', '2017-07-19 03:52:48'),
	('caf1c2d6-6bf5-11e7-8768-0a715d5224b9', 'ad18b984-6bf2-11e7-8768-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'staff', 'nomal', '2017-07-19 04:15:07', '2017-07-19 05:12:06'),
	('d40694ea-6c28-11e7-8768-0a715d5224b9', 'd4029610-6c28-11e7-8768-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-07-19 10:20:26', '2017-07-19 10:20:26'),
	('61165730-6c29-11e7-8768-0a715d5224b9', 'd4029610-6c28-11e7-8768-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'staff', 'nomal', '2017-07-19 10:24:23', '2017-07-19 10:25:29'),
	('9825b326-6d30-11e7-8768-0a715d5224b9', 'fa615f9a-629f-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'nomal', 'nomal', '2017-07-20 17:48:33', '2017-09-09 13:11:58'),
	('74d4f1c2-7837-11e7-8768-0a715d5224b9', '74d4cf08-7837-11e7-8768-0a715d5224b9', '71245186-7836-11e7-8768-0a715d5224b9', NULL, 'Y', 'admin', 'admin', '2017-08-03 18:35:23', '2017-08-03 18:35:23'),
	('ad6fe1b0-7853-11e7-8768-0a715d5224b9', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-08-03 21:57:24', '2017-09-09 13:17:51'),
	('94301600-7855-11e7-8768-0a715d5224b9', '942fe3ec-7855-11e7-8768-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-08-03 22:11:01', '2017-08-03 22:11:01'),
	('88bd27b8-7b34-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '7c5bcf2f-0fdc-4b5c-89bb-031e9f7cad22', NULL, 'I', 'nomal', 'nomal', '2017-08-07 13:52:02', '2017-08-07 13:52:02'),
	('88bdaba2-7b34-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '8d2a67f1-ed66-4299-9017-b20c6b52472e', NULL, 'N', 'nomal', 'nomal', '2017-08-07 13:52:02', '2017-08-16 12:58:31'),
	('084268c2-7e00-11e7-8768-0a715d5224b9', '08424810-7e00-11e7-8768-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-08-11 03:13:46', '2017-08-11 03:13:46'),
	('cb8c12be-7e01-11e7-8768-0a715d5224b9', '08424810-7e00-11e7-8768-0a715d5224b9', '0cd8a0b0-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'staff', 'nomal', '2017-08-11 03:26:23', '2017-08-11 03:27:47'),
	('dde81cb4-7e01-11e7-8768-0a715d5224b9', '08424810-7e00-11e7-8768-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-08-11 03:26:54', '2017-08-11 03:27:43'),
	('5f113eac-91fc-11e7-8768-0a715d5224b9', '0d4a0bb6-62a0-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, 'N', 'nomal', 'nomal', '2017-09-05 13:37:57', '2017-09-05 13:40:47'),
	('0bc5d27a-951d-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:24', '2017-09-09 13:17:24'),
	('137e9da8-951d-11e7-8768-0a715d5224b9', '7f33e8e3-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:37', '2017-09-09 13:17:40'),
	('14ecacde-951d-11e7-8768-0a715d5224b9', '7f33e98a-3c61-11e7-91f8-74d435c7f214', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:39', '2017-09-09 13:18:00'),
	('175e5ba2-951d-11e7-8768-0a715d5224b9', '52475d96-629f-11e7-8a3d-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:43', '2017-09-09 13:11:11'),
	('192abe76-951d-11e7-8768-0a715d5224b9', 'ba9e127c-629f-11e7-8a3d-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:46', '2017-09-09 13:11:43'),
	('1b317c96-951d-11e7-8768-0a715d5224b9', 'de24477a-629f-11e7-8a3d-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:50', '2017-09-09 13:11:22'),
	('1ec4cf0c-951d-11e7-8768-0a715d5224b9', 'fa615f9a-629f-11e7-8a3d-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:56', '2017-09-09 13:11:55'),
	('207136b0-951d-11e7-8768-0a715d5224b9', '0d4a0bb6-62a0-11e7-8a3d-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:58', '2017-09-09 13:11:33'),
	('2072b3a0-951d-11e7-8768-0a715d5224b9', 'ad18b984-6bf2-11e7-8768-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:09:58', '2017-09-09 13:18:39'),
	('217b176a-951d-11e7-8768-0a715d5224b9', 'd4029610-6c28-11e7-8768-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:10:00', '2017-09-09 13:18:11'),
	('23f032e6-951d-11e7-8768-0a715d5224b9', '942fe3ec-7855-11e7-8768-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:10:04', '2017-09-09 13:18:31'),
	('25731304-951d-11e7-8768-0a715d5224b9', '08424810-7e00-11e7-8768-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'nomal', '2017-09-09 13:10:07', '2017-09-09 13:18:22'),
	('929e141e-9532-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '0cdc98f8-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'admin', 'admin', '2017-09-09 15:43:29', '2017-09-09 15:43:29'),
	('16a3af92-955d-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '70930fa0-7836-11e7-8768-0a715d5224b9', NULL, 'Y', 'board', 'nomal', '2017-09-09 20:47:50', '2017-09-28 19:38:31'),
	('62687a9e-9a5c-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-09-16 05:25:23', '2017-09-28 19:27:18'),
	('e92f090a-9a5f-11e7-8768-0a715d5224b9', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '35b3260a-4676-11e7-8e89-0a715d5224b9', NULL, 'A', 'nomal', 'nomal', '2017-09-16 05:50:38', '2017-09-16 05:50:38'),
	('dd136d08-9d04-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', 'bb1aa3cb-5113-4abc-9480-88da4a0b7848', NULL, 'Y', 'admin', 'nomal', '2017-09-19 14:36:27', '2017-09-19 21:10:22'),
	('2a6afdbe-9d05-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '7baa78b7-f9d9-4a8e-85fa-dda8bb115976', NULL, 'Y', 'nomal', 'nomal', '2017-09-19 14:38:37', '2017-09-19 14:40:33'),
	('44e38732-a436-11e7-8768-0a715d5224b9', '7f33e38e-3c61-11e7-91f8-74d435c7f214', '70a24a9c-7836-11e7-8768-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-09-28 18:17:44', '2017-09-28 18:21:25'),
	('4df6b06a-a436-11e7-8768-0a715d5224b9', '7f33e713-3c61-11e7-91f8-74d435c7f214', '70a24a9c-7836-11e7-8768-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-09-28 18:18:00', '2017-09-28 18:19:32'),
	('54538c30-a436-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '70a24a9c-7836-11e7-8768-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-09-28 18:18:10', '2017-09-28 20:04:28'),
	('2bd364ac-a43a-11e7-8768-0a715d5224b9', '7f33e713-3c61-11e7-91f8-74d435c7f214', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'Y', 'nomal', 'nomal', '2017-09-28 18:45:40', '2017-09-28 18:47:47'),
	('074d40da-a444-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '48f55536-7839-11e7-8768-0a715d5224b9', NULL, 'Y', 'nomal', 'nomal', '2017-09-28 19:56:14', '2017-09-28 20:04:27'),
	('8d016540-a812-11e7-8768-0a715d5224b9', '929df164-9532-11e7-8768-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, 'A', 'nomal', 'nomal', '2017-10-03 16:12:08', '2017-10-03 16:12:08'),
	('c2dc3954-acf1-11e7-8768-0a715d5224b9', '7f33e85a-3c61-11e7-91f8-74d435c7f214', '33e341aa-3225-11e7-bfc8-74d435c7f996', NULL, 'A', 'nomal', 'nomal', '2017-10-09 21:00:01', '2017-10-09 21:00:01'),
	('9b0e8fa4-b880-11e7-8768-0a715d5224b9', '9b0e6ed4-b880-11e7-8768-0a715d5224b9', 'c70a62b0-5f09-4abc-9dfd-532efe93beb9', NULL, 'Y', 'admin', 'admin', '2017-10-24 14:00:15', '2017-10-24 14:00:15'),
	('d4845376-b881-11e7-8768-0a715d5224b9', '9b0e6ed4-b880-11e7-8768-0a715d5224b9', '811b462a-4578-4993-9cad-f430fcea1c1e', NULL, 'Y', 'nomal', 'nomal', '2017-10-24 14:09:01', '2017-10-24 14:24:13'),
	('649007c2-b886-11e7-8768-0a715d5224b9', '648fc1cc-b886-11e7-8768-0a715d5224b9', '70930fa0-7836-11e7-8768-0a715d5224b2', NULL, 'Y', 'admin', 'admin', '2017-10-24 14:41:41', '2017-10-24 14:41:41'),
	('6048149c-b887-11e7-8768-0a715d5224b9', '6047ecf6-b887-11e7-8768-0a715d5224b9', '70930fa0-7836-11e7-8768-0a715d5224b2', NULL, 'Y', 'admin', 'admin', '2017-10-24 14:48:43', '2017-10-24 14:48:43'),
	('73d5e912-b887-11e7-8768-0a715d5224b9', '73d5c586-b887-11e7-8768-0a715d5224b9', '70930fa0-7836-11e7-8768-0a715d5224b2', NULL, 'Y', 'admin', 'admin', '2017-10-24 14:49:16', '2017-10-24 14:49:16');
/*!40000 ALTER TABLE `plf_fan_manage` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
