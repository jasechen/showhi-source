-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.5.42 - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win32
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出  表 plf_api.plf_messages 結構
DROP TABLE IF EXISTS `plf_messages`;
CREATE TABLE IF NOT EXISTS `plf_messages` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '編號',
  `groups_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '群組編號',
  `message` longtext CHARACTER SET utf8 COMMENT '文字訊息',
  `files_id` int(10) DEFAULT NULL COMMENT '檔案編號',
  `created_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '建立者',
  `updated_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '更新者',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '建立時間',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='訊息內容';

-- 正在導出表  plf_api.plf_messages 的資料：~9 rows (大約)
/*!40000 ALTER TABLE `plf_messages` DISABLE KEYS */;
REPLACE INTO `plf_messages` (`id`, `groups_id`, `message`, `files_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	('1', '1', 'test', NULL, '14', '14', '2017-05-01 10:13:28', '2017-05-01 10:13:28'),
	('2', '1', '123456', NULL, '14', '14', '2017-05-01 11:29:26', '2017-05-01 11:29:26'),
	('3', '1', 'john test', NULL, '14', '14', '2017-05-01 11:41:18', '2017-05-01 11:41:18'),
	('4', '1', 'johntest 2', NULL, '14', '14', '2017-05-01 11:43:03', '2017-05-01 11:43:03'),
	('5', '1', 'sorry teacher', NULL, '1', '1', '2017-05-01 12:12:26', '2017-05-01 12:12:26'),
	('6', '1', '1234', NULL, '1', '1', '2017-05-01 12:12:53', '2017-05-01 12:12:53'),
	('7', '1', 'sorry teacher 12345', NULL, '1', '1', '2017-05-01 12:33:18', '2017-05-01 12:33:18'),
	('8', '1', 'ewqewqrw', NULL, '1', '1', '2017-05-02 01:48:38', '2017-05-02 01:48:38'),
	('9', '4', 'reason of cancellation:1234567', NULL, '1', '1', '2017-05-12 00:57:17', '2017-05-12 00:57:17');
/*!40000 ALTER TABLE `plf_messages` ENABLE KEYS */;

-- 導出  表 plf_api.plf_message_groups 結構
DROP TABLE IF EXISTS `plf_message_groups`;
CREATE TABLE IF NOT EXISTS `plf_message_groups` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '編號',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '群組名稱',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '群組詳細資訊',
  `created_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '建立者',
  `updated_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '更新者',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '建立時間',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='訊息群組';

-- 正在導出表  plf_api.plf_message_groups 的資料：~4 rows (大約)
/*!40000 ALTER TABLE `plf_message_groups` DISABLE KEYS */;
REPLACE INTO `plf_message_groups` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	('1', 'John , University of Wiscon', NULL, '14', '14', '2017-05-01 10:12:43', '2017-05-01 10:12:43'),
	('2', 'John , Harry', NULL, '1', '1', '2017-05-08 09:11:24', '2017-05-08 09:11:24'),
	('3', 'John , River', NULL, '1', '1', '2017-05-10 23:25:36', '2017-05-10 23:25:36'),
	('4', 'John , George Mason Univers', NULL, '1', '1', '2017-05-10 23:26:06', '2017-05-10 23:26:06');
/*!40000 ALTER TABLE `plf_message_groups` ENABLE KEYS */;

-- 導出  表 plf_api.plf_message_members 結構
DROP TABLE IF EXISTS `plf_message_members`;
CREATE TABLE IF NOT EXISTS `plf_message_members` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '編號',
  `user_id` int(10) NOT NULL COMMENT '會員編號',
  `groups_id` int(10) NOT NULL COMMENT '群組編號',
  `created_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '建立者',
  `updated_by` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '更新者',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '建立時間',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='訊息群組成員';

-- 正在導出表  plf_api.plf_message_members 的資料：~8 rows (大約)
/*!40000 ALTER TABLE `plf_message_members` DISABLE KEYS */;
REPLACE INTO `plf_message_members` (`id`, `user_id`, `groups_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	('1', 1, 1, '14', '14', '2017-05-01 10:12:43', '2017-05-01 10:12:43'),
	('2', 14, 1, '14', '14', '2017-05-01 10:12:43', '2017-05-01 10:12:43'),
	('3', 11, 2, '1', '1', '2017-05-08 09:11:24', '2017-05-08 09:11:24'),
	('4', 1, 2, '1', '1', '2017-05-08 09:11:24', '2017-05-08 09:11:24'),
	('5', 5, 3, '1', '1', '2017-05-10 23:25:36', '2017-05-10 23:25:36'),
	('6', 1, 3, '1', '1', '2017-05-10 23:25:36', '2017-05-10 23:25:36'),
	('7', 15, 4, '1', '1', '2017-05-10 23:26:06', '2017-05-10 23:26:06'),
	('8', 1, 4, '1', '1', '2017-05-10 23:26:06', '2017-05-10 23:26:06');
/*!40000 ALTER TABLE `plf_message_members` ENABLE KEYS */;

-- 導出  觸發器 plf_api.plf_messages_before_insert 結構
DROP TRIGGER IF EXISTS `plf_messages_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_messages_before_insert` BEFORE INSERT ON `plf_messages` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_message_groups_before_insert 結構
DROP TRIGGER IF EXISTS `plf_message_groups_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_message_groups_before_insert` BEFORE INSERT ON `plf_message_groups` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 plf_api.plf_message_members_before_insert 結構
DROP TRIGGER IF EXISTS `plf_message_members_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_message_members_before_insert` BEFORE INSERT ON `plf_message_members` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
