ALTER TABLE `plf_board`
	ADD COLUMN `like_count` BIGINT NULL ;
	
ALTER TABLE `plf_board`
	ADD COLUMN `view_count` BIGINT NULL ;
	
	
ALTER TABLE `plf_files`
	ADD COLUMN `like_count` BIGINT NULL ;
	
ALTER TABLE `plf_files`
	ADD COLUMN `view_count` BIGINT NULL ;
	