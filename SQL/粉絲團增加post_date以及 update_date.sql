ALTER TABLE `plf_fan`
	ADD COLUMN `post_date` TIMESTAMP NULL AFTER `nationality`,
	ADD COLUMN `update_date` TIMESTAMP NULL AFTER `post_date`;
