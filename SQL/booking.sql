-- --------------------------------------------------------
-- 主機:                           showhi.club
-- 服務器版本:                        5.1.73 - Source distribution
-- 服務器操作系統:                      redhat-linux-gnu
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 導出 showhi 的資料庫結構
CREATE DATABASE IF NOT EXISTS `showhi` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `showhi`;

-- 導出  表 showhi.plf_lesson 結構
DROP TABLE IF EXISTS `plf_lesson`;
CREATE TABLE IF NOT EXISTS `plf_lesson` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `teach_id` varchar(50) DEFAULT NULL COMMENT '老師id',
  `type` int(11) DEFAULT NULL COMMENT '課程類別(沒用到)',
  `lesson_day` datetime DEFAULT '1915-01-01 00:00:00' COMMENT '課程日期',
  `post_date` datetime DEFAULT '1915-01-01 00:00:00' COMMENT '建立時間',
  `update_date` datetime DEFAULT '1915-01-01 00:00:00' COMMENT '修改日期',
  `hand_admin_id` int(11) DEFAULT NULL COMMENT '手動排課人員id',
  `last_sql` text COMMENT '最後一筆sql',
  `last_time` datetime DEFAULT NULL COMMENT '最後一筆時間',
  `teach_state` int(11) NOT NULL COMMENT '老師狀態,到,未到,代課',
  `teach_leavel` enum('Y','N') DEFAULT NULL COMMENT '老師全天請假'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所有老師每日上課總表';

-- 正在導出表  showhi.plf_lesson 的資料：~15 rows (大約)
DELETE FROM `plf_lesson`;
/*!40000 ALTER TABLE `plf_lesson` DISABLE KEYS */;
INSERT INTO `plf_lesson` (`id`, `teach_id`, `type`, `lesson_day`, `post_date`, `update_date`, `hand_admin_id`, `last_sql`, `last_time`, `teach_state`, `teach_leavel`) VALUES
	('d7f512a9-3cba-11e7-baba-74d435c7f214', '0cdc0b6e-3225-11e7-bfc8-74d435c7f214', NULL, '2017-05-31 00:00:00', '2017-05-20 01:44:44', '2017-05-20 01:44:44', NULL, NULL, NULL, 0, NULL),
	('a1714dd5-3cbd-11e7-baba-74d435c7f214', '0cdc0b6e-3225-11e7-bfc8-74d435c7f214', NULL, '2017-05-29 00:00:00', '2017-05-20 02:04:41', '2017-05-20 02:04:41', NULL, NULL, NULL, 0, NULL),
	('5e5bb3b3-3cc4-11e7-baba-74d435c7f214', '0cdc0b6e-3225-11e7-bfc8-74d435c7f214', NULL, '2017-05-26 00:00:00', '2017-05-20 02:52:55', '2017-05-20 02:52:55', NULL, NULL, NULL, 0, NULL),
	('548c6ccc-3cc7-11e7-baba-74d435c7f214', '0cdc0b6e-3225-11e7-bfc8-74d435c7f214', NULL, '2017-05-27 00:00:00', '2017-05-20 03:14:07', '2017-05-20 03:14:07', NULL, NULL, NULL, 0, NULL),
	('4f06ba4a-4545-11e7-8e89-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-02 00:00:00', '2017-05-30 22:36:03', '2017-05-30 22:36:03', NULL, NULL, NULL, 0, NULL),
	('181f511a-45b1-11e7-8e89-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-30 00:00:00', '2017-05-31 11:27:37', '2017-05-31 11:27:37', NULL, NULL, NULL, 0, NULL),
	('e009bd90-45c6-11e7-8e89-0a715d5224b9', '0cd8a0b0-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-15 00:00:00', '2017-05-31 14:03:32', '2017-05-31 14:03:32', NULL, NULL, NULL, 0, NULL),
	('c2da00ec-46be-11e7-8e89-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-13 00:00:00', '2017-06-01 19:37:58', '2017-06-01 19:37:58', NULL, NULL, NULL, 0, NULL),
	('7e6d63b8-4844-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-05 00:00:00', '2017-06-03 18:07:47', '2017-06-03 18:07:47', NULL, NULL, NULL, 0, NULL),
	('7653cd3e-4920-11e7-8e89-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-15 00:00:00', '2017-06-04 20:22:23', '2017-06-04 20:22:23', NULL, NULL, NULL, 0, NULL),
	('199be9aa-4a92-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-09 00:00:00', '2017-06-06 16:28:21', '2017-06-06 16:28:21', NULL, NULL, NULL, 0, NULL),
	('bbb1a02a-4b8e-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, '2017-07-19 00:00:00', '2017-06-07 22:36:46', '2017-06-07 22:36:46', NULL, NULL, NULL, 0, NULL),
	('67b20afc-4ff1-11e7-8a3d-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-21 00:00:00', '2017-06-13 12:33:10', '2017-06-13 12:33:10', NULL, NULL, NULL, 0, NULL),
	('67457a9a-5a23-11e7-8a3d-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, '2017-06-29 00:00:00', '2017-06-26 11:56:16', '2017-06-26 11:56:16', NULL, NULL, NULL, 0, NULL),
	('f2a21468-6108-11e7-8a3d-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', NULL, '2017-07-27 00:00:00', '2017-07-05 06:34:31', '2017-07-05 06:34:31', NULL, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `plf_lesson` ENABLE KEYS */;

-- 導出  表 showhi.plf_lesson_data 結構
DROP TABLE IF EXISTS `plf_lesson_data`;
CREATE TABLE IF NOT EXISTS `plf_lesson_data` (
  `id` varchar(50) NOT NULL COMMENT 'uuid',
  `lesson_id` varchar(50) DEFAULT NULL COMMENT '課程id',
  `student_id` varchar(50) DEFAULT NULL COMMENT '學生id',
  `can_tutor_time` int(11) DEFAULT NULL COMMENT '教學時間（rel_id）',
  `post_date` datetime DEFAULT NULL COMMENT '建立時間',
  `update_date` datetime DEFAULT NULL COMMENT '修改時間',
  `admin_id` int(11) DEFAULT NULL COMMENT '排課人員id',
  `student_state` int(11) DEFAULT NULL COMMENT '學生狀態 ,到 未到 取消',
  `last_sql` text COMMENT '最後sql',
  `student_note` int(11) DEFAULT NULL COMMENT '學生上課錢給給老師的備註',
  `student_cancel` enum('Y','N') DEFAULT 'N' COMMENT '學生取消',
  `teach_leave` enum('Y','N') DEFAULT 'N' COMMENT '老師請假',
  `teach_score` int(11) DEFAULT NULL COMMENT '5,4,3,2,1,0',
  `lesson_rate_note` text COMMENT '學生給老師的評語',
  `lesson_rate_date` datetime DEFAULT NULL COMMENT '給評價的時間',
  `student_cancel_date` datetime DEFAULT NULL COMMENT '學生請假日期',
  `teach_leave_date` datetime DEFAULT NULL COMMENT '老師請假日期',
  `type` int(11) DEFAULT NULL COMMENT '類別',
  `who_textbook` int(11) DEFAULT NULL COMMENT '誰準備教材',
  `use_point` int(11) DEFAULT NULL COMMENT '點數',
  `article_link` text,
  `article_title` text,
  `confirm_lesson` enum('Y','N') DEFAULT NULL,
  `teach_room_url` text,
  `student_room_url` text,
  `room_id` int(11) DEFAULT NULL,
  `id_sha1` text COMMENT '加密',
  `id_sha1_key` text COMMENT '位移加密_+id',
  `teach_first_in_room_time` datetime DEFAULT NULL COMMENT '老師第一次進教室時間',
  `teach_in_room` enum('Y','N') DEFAULT NULL COMMENT '老師進入教室',
  `freetalk_article_link` text,
  `cause` text COMMENT '面談原因'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所有老師上課詳細表(學生id)';

-- 正在導出表  showhi.plf_lesson_data 的資料：~5 rows (大約)
DELETE FROM `plf_lesson_data`;
/*!40000 ALTER TABLE `plf_lesson_data` DISABLE KEYS */;
INSERT INTO `plf_lesson_data` (`id`, `lesson_id`, `student_id`, `can_tutor_time`, `post_date`, `update_date`, `admin_id`, `student_state`, `last_sql`, `student_note`, `student_cancel`, `teach_leave`, `teach_score`, `lesson_rate_note`, `lesson_rate_date`, `student_cancel_date`, `teach_leave_date`, `type`, `who_textbook`, `use_point`, `article_link`, `article_title`, `confirm_lesson`, `teach_room_url`, `student_room_url`, `room_id`, `id_sha1`, `id_sha1_key`, `teach_first_in_room_time`, `teach_in_room`, `freetalk_article_link`, `cause`) VALUES
	('7e6e8a9a-4844-11e7-8e89-0a715d5224b9', '7e6d63b8-4844-11e7-8e89-0a715d5224b9', '33e341aa-3225-11e7-bfc8-74d435c7f214', 116, '2017-06-03 18:07:47', '2017-06-03 18:07:47', NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'cb8372af935d8b5c9b02f1046e0672f3b74807f0', NULL, NULL, NULL, NULL, 'test1234'),
	('76552e9a-4920-11e7-8e89-0a715d5224b9', '7653cd3e-4920-11e7-8e89-0a715d5224b9', '0cd8881c-3225-11e7-bfc8-74d435c7f214', 116, '2017-06-04 20:22:23', '2017-06-04 20:22:23', NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '465da885252bb85ca1d2a6629aa4b3e657373eff', NULL, NULL, NULL, NULL, '6/4 booking test'),
	('199d04a2-4a92-11e7-8a3d-0a715d5224b9', '199be9aa-4a92-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 103, '2017-06-06 16:28:21', '2017-06-06 16:28:21', NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '568198d0ec5b47491aa56d4947de83fd036d8d87', NULL, NULL, NULL, NULL, 'Test'),
	('67b364ec-4ff1-11e7-8a3d-0a715d5224b9', '67b20afc-4ff1-11e7-8a3d-0a715d5224b9', '0cdbda97-3225-11e7-bfc8-74d435c7f214', 108, '2017-06-13 12:33:10', '2017-06-13 12:33:10', NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'eb7b1c43a58cb36d25ae9c7e5891759dde03739d', NULL, NULL, NULL, NULL, 'Hello'),
	('6746a168-5a23-11e7-8a3d-0a715d5224b9', '67457a9a-5a23-11e7-8a3d-0a715d5224b9', '35b3260a-4676-11e7-8e89-0a715d5224b9', 102, '2017-06-26 11:56:16', '2017-06-26 11:56:16', NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '37015001c5e70513a2aa675ce197ab2ff7d83b15', NULL, NULL, NULL, NULL, 'Test');
/*!40000 ALTER TABLE `plf_lesson_data` ENABLE KEYS */;

-- 導出  觸發器 showhi.plf_lesson_before_insert 結構
DROP TRIGGER IF EXISTS `plf_lesson_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_lesson_before_insert` BEFORE INSERT ON `plf_lesson` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- 導出  觸發器 showhi.plf_lesson_data_before_insert 結構
DROP TRIGGER IF EXISTS `plf_lesson_data_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `plf_lesson_data_before_insert` BEFORE INSERT ON `plf_lesson_data` FOR EACH ROW BEGIN
  SET new.id = uuid();
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
