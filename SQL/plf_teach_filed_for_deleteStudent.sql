﻿ALTER TABLE `plf_teach`
	ADD COLUMN `point` INT NULL COMMENT '點數' AFTER `bg_image_style`,
	ADD COLUMN `product_id` INT NULL COMMENT '購買的產品id' AFTER `point`,
	ADD COLUMN `point_date_limit` DATE NULL COMMENT '點數到期日' AFTER `product_id`;