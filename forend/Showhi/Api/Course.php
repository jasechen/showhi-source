<?php

class Api_Course extends Common_ApiDefaultList {

    public function getRules() {
        return array(
            'StudentBooking' => array(
                'article_link' => array('name' => 'article_link', 'type' => 'string','require' => false, 'desc' => '文章連結編號'),
                'lesson_day' => array('name' => 'lesson_day', 'type' => 'string','require' => true, 'desc' => '上課日期'),
                'teach_id' => array('name' => 'teach_id', 'type' => 'string','require' => true, 'desc' => '老師id'),
                'can_tutor_time' => array('name' => 'can_tutor_time', 'type' => 'string','require' => true, 'desc' => '上課時間'),    
            ),
            'StudentBookingChoose' => array( 
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '類別'),
                'tag' => array('name' => 'tag', 'type' => 'string', 'require' => true, 'desc' => 'Tag'),
            ),
            
            'StudentLessonCancel' => array( 
                'lesson_dt_id' => array('name' => 'lesson_dt_id', 'type' => 'int', 'require' => true, 'desc' => '上課id'),
            ),
            'TeacherLessonCancel' => array( 
                'lesson_id' => array('name' => 'lesson_id', 'type' => 'int', 'require' => true, 'desc' => '老師上課id'),
                'can_tutor_time' => array('name' => 'can_tutor_time', 'type' => 'int', 'require' => true, 'desc' => '老師上課時間'),
            ),
            'TeacherList' => array( 
                'nationality' => array('name' => 'nationality', 'type' => 'string','desc' => '國籍'),
                'gender' => array('name' => 'gender', 'type' => 'string','desc' => '男女'),
                'exp' => array('name' => 'exp', 'type' => 'string','desc' => '教育年資 2007;2017'),
                'teacher_category' => array('name' => 'teacher_category', 'type' => 'string','desc' => '老師分類'),
                'text' => array('name' => 'text', 'type' => 'string','desc' => '老師編號以及名稱'),
                'order' => array('name' => 'order', 'type' => 'string','desc' => '排序'),
            ),
            'TeacherText' => array( 
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '文章編號'),
            ),
            'ViewList' => array( 
            ),
            
        );
  
    }
    
/*
-SelectResources
-StudentBooking
-StudentBookingChoose
-StudentLessonCancel
-TeacherLessonCancel
-TeacherList    
-TeacherText        
-ViewList     
*/
    
    public function __construct() {  
        parent::__construct(); 
         //exit;
    }
    public function __destruct(){
        //exit;
    } 
        
    /**
     * 學生修改
     * @desc 回傳修改資料所需欄位
     * 
     * 
     */
    public function StudentBookingChoose() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentBookingChoose();
        return $this-> laout_arr;
    } 

    /**
     * 執行學生修改
     * @desc 執行修改資料
     * 
     * 
     * 
     */
    public function StudentBooking() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->StudentBooking($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function StudentLessonCancel() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->StudentLessonCancel($this->query_arr);
        return $this-> laout_arr;
    }     
    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function TeacherLessonCancel() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->TeacherLessonCancel($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function TeacherList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->TeacherList($this->query_arr);
        return $this-> laout_arr;
    } 

    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function ViewList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->ViewList($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function TeacherText() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->TeacherText($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    
    
    
//----------    --------------
    /**
     * 老師同意內容
     * @desc 老師同意內容
     * 
     * 
     * 
     */
    public function Agree() {
        /*
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,2);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentPwEdit($this->query_arr);
        
        return $this-> laout_arr;
        */
        return 'AgreeUP';
    }

}
