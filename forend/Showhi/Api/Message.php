<?php

class Api_Message extends Common_ApiDefaultList
{

    public function getRules()
    {
        return array(
            'readMessageList' => array(),
            'readMessageByGroupId' => array(
                'group_id' => array('name' => 'group_id', 'type' => 'string', 'require' => true, 'desc' => '對話視窗ID'),
            ),
            'sendMessage' => array(
                'groups_id' => array('name' => 'groups_id', 'type' => 'string', 'require' => true, 'desc' => '對話視窗ID'),
                'message_text' => array('name' => 'message_text', 'type' => 'string', 'require' => false, 'desc' => '文字訊息'),
                'message_file' => array('name' => 'message_file', 'type' => 'file', 'require' => false, 'desc' => '檔案訊息'),
            ),
            'createGroup' => array(
                'users_id' => array('name' => 'users_id', 'type' => 'array', 'require' => true, 'desc' => '對話對象'),
                'group_name' => array('name' => 'message_text', 'type' => 'string', 'require' => false, 'desc' => '群組名稱'),
                'group_description' => array('name' => 'message_file', 'type' => 'file', 'require' => false, 'desc' => '群組詳細資訊'),
            ),
            'EditGroup' => array(
                'group_id' => array('name' => 'group_id', 'type' => 'string', 'require' => true, 'desc' => '對話對象'),
                'group_name' => array('name' => 'group_name', 'type' => 'string', 'require' => false, 'desc' => '群組名稱'),
            ),

            'AddGroupMember' => array(
                'users_id' => array('name' => 'users_id', 'type' => 'string', 'require' => true, 'desc' => '對話對象'),
                'group_id' => array('name' => 'group_id', 'type' => 'string', 'require' => false, 'desc' => '群組名稱'),
            ),
            'RemoveGroupMember' => array(
                'users_id' => array('name' => 'users_id', 'type' => 'string', 'require' => true, 'desc' => '對話對象'),
                'group_id' => array('name' => 'group_id', 'type' => 'string', 'require' => false, 'desc' => '群組名稱'),
            ),

            'GetNameEmail' => array(
                'text' => array('name' => 'text', 'type' => 'string', 'require' => true, 'desc' => '搜尋'),
            ),

            'fileUpload' => array(
                'message_file' => array('name' => 'message_file', 'type' => 'file', 'require' => false, 'desc' => '檔案訊息'),
            ),
        );
    }

    public function __construct()
    {
        parent::__construct();
        //exit;
    }

    public function __destruct()
    {
        //exit;
    }


    //讀取這個人的清單
    public function readMessageList()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr = $domain->readMessageList();
        return $this->laout_arr;
    }

    //這個人會選擇要跟誰對話
    //把這個人選擇的誰建立群組
    public function createGroup()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->createGroup($this->users_id, $this->group_name, $this->group_description);
        return $this->laout_arr;
    }

    
    //改變群組名稱
    public function EditGroup()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->EditGroup($this->group_id, $this->group_name);
        return $this->laout_arr;
    }

    //拉社群人員進來
    public function AddGroupMember()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->AddGroupMember($this->users_id, $this->group_id);
        return $this->laout_arr;
    }

    //刪除群人員進來
    public function RemoveGroupMember()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->RemoveGroupMember($this->users_id, $this->group_id);
        return $this->laout_arr;
    }




    //取得email name 清單
    public function GetNameEmail()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->GetNameEmail($this->text);
        return $this->laout_arr;
    }



    //帶這個人到對話筐

    //讀取之前的對話紀錄
    public function readMessageByGroupId()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = $domain->readMessageByGroupId($this->group_id);
        return $this->laout_arr;
    }

    //這個人打字到對話筐，並且按下送出或Enter
    //這個人傳檔案到對話筐並且按下送出
    public function sendMessage()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr[] = $domain->sendMessage($this->message_text, $this->message_file, $this->groups_id);
        return $this->laout_arr;
    }

    public function fileUpload()
    {
        $tmp_domain_class = DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this->laout_arr = array();
        $this->laout_arr['file_id'] = $domain->uploadFile($this->message_file);
        return $this->laout_arr;
    }


}
