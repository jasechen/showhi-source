<?php
class Api_Fan extends Common_ApiDefaultList {

    public function getRules() {
        return array(
            'FanText' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '社群id'),
            ),      
            'MyFanList' => array(
            ),
            'FanMange' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '社群id'),
            ),      
            'FanMangeMember' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '社群id'),
            ),      
            'AgreeFanMember' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => 'user_id'),
            ),   
            'AgreeInvFan' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => 'user_id'),
            ),   
               
            'RefuseFanMember' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => 'user_id'),
            ),      
            
            'DeleteFanMember' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => 'user_id'),
            ),   
            'ChangeFanMemberType' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => 'user_id'),
            ),  
            'ChangeFanApproval' => array(
                'approval' => array('name' => 'approval', 'type' => 'string',  'desc' => '是否開放'),
            ),

            'DeleteFan' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '社群id'),
            ), 
            'FanEditUP' => array(
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '社群id'),
            ),    
            
            'InviteFanMember' => array(
            ), 
            'InviteMultiFanMember' => array(
            ), 
            
            'PhotoUpload' => array(
                'photoFile' => array('name' => 'photoFile', 'type' => 'file', 'require' => true, 'desc' => '相片'),
                'fan_id' => array('name' => 'fan_id', 'type' => 'string', 'require' => true, 'desc' => '粉絲團id'),
            ),
            'BgPhotoUpload' => array(
                'photoFile' => array('name' => 'photoFile', 'type' => 'file', 'require' => true, 'desc' => '相片'),
                'fan_id' => array('name' => 'fan_id', 'type' => 'string', 'require' => true, 'desc' => '粉絲團id'),
                'bg_image_style' => array('name' => 'bg_image_style', 'type' => 'string', 'require' => true, 'desc' => '背景圖片起始高度'),
            ),
            'ViewList' => array(
            ),      
            'CreateFan' => array(
                'nick_name' => array('name' => 'nick_name', 'type' => 'string', 'require' => true,  'desc' => '粉絲團名稱'),
                'text' => array('name' => 'text', 'type' => 'string',  'desc' => '粉絲團簡介'),
                'fuzzy_name' => array('name' => 'fuzzy_name', 'type' => 'string',  'desc' => '模糊名稱'),
                'fan_type' => array('name' => 'type', 'type' => 'string',  'desc' => '粉絲團類別'),
            ), 
            
            
        );
  
    }
    
    public function __construct() {  
        parent::__construct(); 
         //exit;
    }
    public function __destruct(){
        //exit;
    } 

    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function FanText() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->FanText($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    public function MyFanList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->MyFanList($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function FanMange() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->FanMange($this->query_arr);
        return $this-> laout_arr;
    } 

    public function FanReport() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->FanReport($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function FanMangeMember() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->FanMangeMember($this->query_arr);
        return $this-> laout_arr;
    } 
    

    public function AgreeFanMember() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->AgreeFanMember($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function AgreeInvFan() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->AgreeInvFan($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    public function RefuseFanMember() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->RefuseFanMember($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    public function DeleteFanMember() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->DeleteFanMember($this->query_arr);
        return $this-> laout_arr;
    } 


    public function AddFanMember() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;    
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->AddFanMember($this->query_arr);
        return $this-> laout_arr;
    } 

    public function ChangeFanMemberType() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->ChangeFanMemberType($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function ChangeFanApproval() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->ChangeFanApproval($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function DeleteFan() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->DeleteFan($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function InviteFanMember() {

        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->InviteFanMember($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function InviteMultiFanMember() {
        

        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->InviteMultiFanMember($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function PhotoUpload() {
        //print_r($_FILES);
        //exit;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->PhotoUpload('image',$this->photoFile);
        return $this-> laout_arr;
    }
    
    public function BgPhotoUpload() {
        //print_r($_FILES);
        //exit;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BgPhotoUpload('bg_image',$this->photoFile,$this->bg_image_style);
        return $this-> laout_arr;
    }

    
    public function ViewList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->ViewList($this->query_arr);
        return $this-> laout_arr;
    } 
    
    public function FanEditUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->FanEditUP($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    public function CreateFan() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->CreateFan($this->query_arr);
        return $this-> laout_arr;
    }   
    
    

}
