<?php
class Api_User extends Common_ApiDefaultList {

    public function getRules() {
        return array(
            'logIn' => array(
                'loginid' => array('name' => 'loginid', 'type' => 'string',  'desc' => '登入帳號'),
                'phone' => array('name' => 'phone', 'type' => 'string',  'desc' => '登入電話'),
                'email' => array('name' => 'email', 'type' => 'string',  'desc' => '登入email'),
                'loginpwd' => array('name' => 'loginpwd', 'type' => 'string', 'require' => true, 'desc' => '登入密碼'),
                'identity' => array('name' => 'identity', 'type' => 'string', 'require' => true, 'desc' => 'identity'),
                'time_set' => array('name' => 'time_set', 'type' => 'int', 'require' => true, 'desc' => 'time set'),
            ),
            'getSession' => array(
            ),
            'logOut' => array(

            ),
            'Agree' => array( 
            ),
            
            'AgreeUP' => array( 
                'agree' => array('name' => 'agree', 'type' => 'enum','range' => array('Y', 'N'), 'require' => true, 'desc' => 'Y'),
            ),
            'StudentBookingRecord' => array( 
            ),
            'StudentAptRecord' => array( 
            ),
            'CanTutorTime' => array( 
            ),
            'CanTutorTimeUP' => array( 
                'can_tutor_time' => array('name' => 'can_tutor_time', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time1' => array('name' => 'can_tutor_time1', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time2' => array('name' => 'can_tutor_time2', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time3' => array('name' => 'can_tutor_time3', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time4' => array('name' => 'can_tutor_time4', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time5' => array('name' => 'can_tutor_time5', 'type' => 'array',  'desc' => 'array'),
                'can_tutor_time6' => array('name' => 'can_tutor_time6', 'type' => 'array',  'desc' => 'array'),
            ),
            'CourseContent' => array( 
            ),
            
            'EditEmailPwUP' => array( 
                'email' => array('name' => 'email', 'type' => 'string', 'require' => true, 'desc' => 'email'),
                'forget_code' => array('name' => 'forget_code', 'type' => 'string', 'require' => true, 'desc' => '修改密碼驗證'),
                'loginpwd' => array('name' => 'password', 'type' => 'string', 'require' => true, 'desc' => '修改密碼'),
            ),
            
            'EditPwUP' => array( 
                'old_loginpwd' => array('name' => 'old_loginpwd', 'type' => 'string', 'require' => true, 'desc' => '舊密碼'),
                'loginpwd' => array('name' => 'loginpwd', 'type' => 'string', 'require' => true, 'desc' => '修改密碼'),
            ),
            
            'ForgetEmail' => array(
                'identity' => array('name' => 'identity', 'type' => 'string', 'require' => true, 'desc' => '身分'),
                'loginid' => array('name' => 'loginid', 'type' => 'string', 'require' => true, 'desc' => 'Loginid'),
            ),        
            'ForgetPw' => array(
                'email' => array('name' => 'email', 'type' => 'string', 'require' => true, 'desc' => '信箱'),

            ),       
                
            'ExUP' => array(
            ),

            'ZoomOpen' => array(
            ),

            'register_fb_update' => array( 
            ),
                        
            'RegisterTeacher' => array( 
            ),
                        
            'RegisterTeacherUP' => array( 
                'loginid' => array('name' => 'loginid', 'type' => 'string', 'require' => true, 'desc' => '帳號'),
                'loginpwd' => array('name' => 'loginpwd', 'type' => 'string', 'require' => true,  'desc' => '密碼'),
                'nick_name' => array('name' => 'nick_name', 'type' => 'string', 'require' => true,  'desc' => 'Nick Name'),
                'phone' => array('name' => 'phone', 'type' => 'string',  'desc' => '名稱'),
                'email' => array('name' => 'loginid', 'type' => 'string', 'require' => true,  'desc' => 'last name'),
                //'nationality' => array('name' => 'nationality', 'type' => 'int', 'require' => true,  'desc' => '國籍'),
                //'education' => array('name' => 'education', 'type' => 'int', 'require' => true,  'desc' => '學歷'),
                //'major' => array('name' => 'major', 'type' => 'int', 'require' => true,  'desc' => '主修'),
                //'teacher_category' => array('name' => 'teacher_category',  'type' => 'int',  'desc' => '老師擅長'),
                //'birth_day' => array('name' => 'birth_day', 'type' => 'string',  'desc' => '生日年'),
                //'exp' => array('name' => 'exp', 'type' => 'int', 'require' => true,  'desc' => '教學年資'),
                //'text' => array('name' => 'text', 'type' => 'string',  'desc' => '簡介'),
                //'agree' => array('name' => 'agree', 'type' => 'string', 'require' => true  ,'desc' => '同意條款'),            
            ),
                        
            'RegisterTwitterUP' => array( 
            ),
                                    
            'RegisterStudentUP' => array(
                'loginid' => array('name' => 'loginid', 'type' => 'string', 'require' => true, 'desc' => '帳號'),
                'loginpwd' => array('name' => 'loginpwd', 'type' => 'string', 'require' => true,  'desc' => '密碼'),
                'phone' => array('name' => 'phone', 'type' => 'string', 'require' => true,  'desc' => '電話'),
                'birth_day' => array('name' => 'birth_day', 'type' => 'string', 'require' => false,  'desc' => '生日'),
                'gender' => array('name' => 'gender', 'type' => 'string', 'require' => false,  'desc' => '性別'),
            ),
                                    
            'RequestHelp' => array( 
            ),
                                    
            'ResetPw' => array( 
                'identity' => array('name' => 'identity', 'type' => 'string', 'require' => true, 'desc' => '身分'),
                'email_code' => array('name' => 'email_code', 'type' => 'string', 'require' => true, 'desc' => '取回密碼驗證碼'),
            ),
                                    
            'StudentPwEdit' => array( 
            ),
            'TeacherEdit' => array( 
            ),
                                    
            'StudentPointRecord' => array(
            
            ),
            'RateList' => array( 
            ),                             
            'RateClass' => array( 
            ),                              
            'RateClassUP' => array(
                'code' => array('name' => 'code', 'type' => 'string', 'require' => true, 'desc' => '課程唯一碼'),
                'lesson_rate_note' => array('name' => 'lesson_rate_note', 'type' => 'string',  'desc' => '課程編號'),
                'teach_score' => array('name' => 'teach_score', 'type' => 'string', 'require' => true, 'desc' => '星星數'),
            ),
                                                
            'TeacherInRoom' => array( 
                'lesson_dt_id' => array('name' => 'lesson_dt_id', 'type' => 'string', 'require' => true, 'desc' => '課程編號'),
            ),
                                                
            'TeacherBookingRecord' => array( 
            ),
            'TeacherAptRecord' => array( 
            ),                                                
            'TeacherEdit' => array( 
            ),
                                                            
            'TeacherOpneLessonRoom' => array( 
                'lesson_dt_id' => array('name' => 'lesson_dt_id', 'type' => 'string', 'require' => true, 'desc' => '課程編號'),
            ),
                                                            
            'TeacherPwEdit' => array( 
            ),
                                                            
            'Text' => array( 
                'id' => array('name' => 'id', 'type' => 'string',  'desc' => '文章編號'),
                'code_name' => array('name' => 'code_name', 'type' => 'string',  'desc' => '文章編碼'),
            ),

                //回饋修改user需要的"欄位資料"+選擇修改的資料
            'StudentEdit' => array(
            ),
            
            'UserEditUP' => array(
                'nick_name' => array('name' => 'nick_name', 'type' => 'string', 'desc' => '暱稱'),
                'phone' => array('name' => 'phone', 'type' => 'string', 'desc' => '電話號碼'),
                'nationality' => array('name' => 'nationality', 'type' => 'string', 'desc' => '國籍'),
                'birth_day' => array('name' => 'birth_day', 'type' => 'string', 'desc' => '生日'),
                'school_name' => array('name' => 'school_name', 'type' => 'string', 'desc' => '學校名稱'),
                'email' => array('name' => 'email', 'type' => 'string', 'desc' => 'email'),
                'text' => array('name' => 'text', 'type' => 'string', 'desc' => '簡介'),
            ),
            'MaterialsUP' => array(
                'lesson_id' => array('name' => 'lesson_id', 'type' => 'string','require' => true, 'desc' => '課程id'),
                'article_link' => array('name' => 'article_link', 'type' => 'string','require' => true, 'desc' => '課程連結'),
            ),
            'CreateRoomUrl' => array(

            ),      
            'WakeupAccount' => array(

            ),      
            'WakeupAccountPhone' => array(

            ),  
            'SendPhoneCode' => array(

            ),  

            'ReadFriendsNote' => array(

            ), 
            'DeleteUP' => array(
                //'pemi_loginid' => array('name' => 'pemi_loginid', 'type' => 'string', 'require' => true, 'desc' => '權限帳號'),
                'id' => array('name' => 'id', 'type' => 'string', 'require' => true, 'desc' => '刪除id'),
            ),
           'ChangeStudent' => array(
            ),
           'ChangeTeacher' => array(
            ),
            'PhotoUpload' => array(
                'photoFile' => array('name' => 'photoFile', 'type' => 'file', 'require' => true, 'desc' => '相片'),
            ),
            'BgPhotoUpload' => array(
                'photoFile' => array('name' => 'photoFile', 'type' => 'file', 'require' => true, 'desc' => '相片'),
                'bg_image_style' => array('name' => 'bg_image_style', 'type' => 'string', 'require' => true, 'desc' => '背景圖片起始高度'),
            ),
            'SetBgHeight' => array(
                'bg_image_style' => array('name' => 'bg_image_style', 'type' => 'string', 'require' => true, 'desc' => '背景圖片起始高度'),
            ),
            'AddFriends' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => '被邀請人'),
            ),
            'AddSubscription' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => '被邀請人'),
            ),
            'AgreeFriends' => array(
                'invite_user_id' => array('name' => 'invite_user_id', 'type' => 'string', 'require' => true, 'desc' => '邀請人'),
                'approval' => array('name' => 'approval', 'type' => 'string', 'require' => true, 'desc' => '是否同意好友'),
            ),
            'CancelFriends' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => '被邀請人'),
            ),
            'CancelSubscription' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => '被邀請人'),
            ),
            'CheckFriendsSubscription' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => '被邀請人'),
            ),  
            'SubscriptionList' => array(
                'user_id' => array('name' => 'user_id', 'type' => 'string', 'require' => true, 'desc' => 'user_id'),
            ),  
            'FriendsList' => array(
                'user_id' => array('name' => 'user_id', 'type' => 'string', 'require' => true, 'desc' => 'user_id'),
            ),  
            'WaitFriendsList' => array(
                'who_user_id' => array('name' => 'who_user_id', 'type' => 'string', 'require' => true, 'desc' => 'user_id'),
            ),
            
            'UserList' => array( 
                'nationality' => array('name' => 'nationality', 'type' => 'string','desc' => '國籍'),
                'gender' => array('name' => 'gender', 'type' => 'string','desc' => '男女'),
                'exp' => array('name' => 'exp', 'type' => 'string','desc' => '教育年資 2007;2017'),
                'teacher_category' => array('name' => 'teacher_category', 'type' => 'string','desc' => '老師分類'),
                'text' => array('name' => 'text', 'type' => 'string','desc' => '老師編號以及名稱'),
                'order' => array('name' => 'order', 'type' => 'string','desc' => '排序'),
            ),

            'EventBookingRecord' => array( 
                'this_day' => array('name' => 'this_day', 'type' => 'string','desc' => '行事曆開始月'),
            ),
            'QRLinkList' => array(
            ), 
            'QRLinkCreate' => array(
            ), 
            'QRLinkEditUP' => array(
                'note' => array('name' => 'note', 'type' => 'string','desc' => 'QR備註'),
            ), 
            'QRLinkApproval' => array(
            ), 
            'QRLinkBlock' => array(
            ), 
            'QRLinkDeleteUP' => array(
            ), 
            'QRLinkCheck' => array(
            ), 
            'QRLinkUserNoteEditUP' => array(
                'invite_note' => array('name' => 'invite_note', 'type' => 'string','desc' => 'user備註'),
            ), 
            'QRLinkAccountDelete' => array(

            ), 

            'ReadSysInviteNotice' => array(

            ), 

            'SendVaEmail' => array(

            ), 


        );
  
    }
    
    public function __construct() {  
        parent::__construct(); 
         //exit;
    }
    public function __destruct(){
        //exit;
    } 
        
    /**
     * 登入
     * @desc 登入帳號
     * 
     * @return array data[].pemi[] 權限列表
     * @return array data[].user_list[] 用戶列表
     * @return string data[].msg_text 訪問敘述
     * @return string data[].msg_state 訪問狀態 'Y' 代表成功
     */
     
    public function logIn() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        //電話以及email 同欄位
        $this->phone = $this->loginid;
        $this->email = $this->loginid;
        //end 電話以及email 同欄位
        $this-> laout_arr =array();
        $this-> laout_arr[] = $domain->login($this->loginid,$this->phone,$this->loginpwd,$this->identity,$this->time_set,$this->email);
        //exit;
        return $this-> laout_arr;
    }
    
    /**
     * 登出
     * @desc 登出帳號
     * 
     * 
     * 
     */
    public function logOut() {
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[] = $domain->logout();
        return $this-> laout_arr;
    }


    /**
     * 學生修改
     * @desc 回傳修改資料所需欄位
     * 
     * 
     */
    public function StudentEdit() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentEdit();
        return $this-> laout_arr;
    } 
    

    /**
     * 執行學生修改
     * @desc 執行修改資料
     * 
     * 
     * 
     */
    public function UserEditUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->UserEditUP($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 執行學生修改
     * @desc 執行修改資料
     * 
     * 
     * 
     */
    public function GetSession() {
        $this-> laout_arr =array();
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr[] =$_SESSION;
        $this-> laout_arr[] =$domain->GetSession();

        /*
        if($_SESSION['f_backend']['block'] =='Y'){
            $this->logOut();
        }
        */
        
        $this-> laout_arr[0]['msg_state']  = 'Y';
        $this-> laout_arr[0]['msg_text']  = 'session';
      //  $this-> laout_arr[]  =
        return $this-> laout_arr;
        
    } 
    
    /**
     * 執行教材修改
     * @desc 執行教材修改資料
     * 
     * 
     * 
     */
    public function MaterialsUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->MaterialsUP($this->query_arr);
        return $this-> laout_arr;
    } 

    /**
     * 老師同意內容
     * @desc 老師同意內容
     * 
     * 
     * 
     */
    public function Agree() {
        /*
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentPwEdit($this->query_arr);
        
        return $this-> laout_arr;
        */
        return 'AgreeUP';
    }
    
    /**
     * 老師同意內容更新
     * @desc 老師同意內容更新
     * 
     * 
     * 
     */
    public function AgreeUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AgreeUP($this->query_arr);
        return $this-> laout_arr;
    }    
    
    
    /**
     * 學生booking列表
     * @desc 學生booking列表
     * 
     * 
     * 
     */
    public function StudentBookingRecord() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentBookingRecord();
        return $this-> laout_arr;
    }    

    /**
     * 學生booking列表
     * @desc 學生booking列表
     * 
     * 
     * 
     */
    public function StudentAptRecord() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentAptRecord();
        return $this-> laout_arr;
    }    
    
    /**
     * 老師可以上課時間表
     * @desc 老師可以上課時間表
     * 
     * 
     * 
     */
    public function CanTutorTime() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CanTutorTime();
        return $this-> laout_arr;
    }    
    
    
    /**
     * 更新老師可以上課時間表
     * @desc 更新老師可以上課時間表
     * 
     * 
     * 
     */
    public function CanTutorTimeUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CanTutorTimeUP($this->query_arr);
        return $this-> laout_arr;
    }  
    
    /**
     * 課程內容(教材)
     * @desc 課程內容(教材)
     * 
     * 
     * 
     */
    public function CourseContent() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CourseContent();
        return $this-> laout_arr;
    }
    
    /**
     * 忘記帳號使用寄信重新寄信驗證
     * @desc 忘記帳號使用寄信重新寄信驗證
     * 
     * 
     * 
     */
    public function EditEmailPwUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,2);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->EditEmailPwUP($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 忘記帳號的更新密碼
     * @desc 忘記帳號的更新密碼
     * 
     * 
     * 
     */
    public function EditPwUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->EditPwUP($this->query_arr);
        return $this-> laout_arr;
    } 
    
    /**
     * 忘記帳號(信箱)
     * @desc 忘記帳號(信箱)
     * 
     * 
     * 
     */
    public function ForgetEmail() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,2);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ForgetEmail($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    
    
    
    /**
     * 範例
     * @desc 範例
     * 
     * 
     * 
     */
    public function ExUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,2);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ExUP($this->query_arr);
        return $this-> laout_arr;
    } 

    public function ZoomOpen() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,2);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ZoomOpen($this->query_arr);
        return $this-> laout_arr;
    } 
    
    
    /**
     * 老師註冊
     * @desc 老師註冊
     * 
     * 
     * 
     */
    public function RegisterTeacher() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RegisterTeacher();
        return $this-> laout_arr;
    } 
    
    
    /**
     * 老師註冊(寫入)
     * @desc 老師註冊(寫入)
     * 
     * 
     * 
     */
    public function RegisterTeacherUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RegisterTeacherUP($this->query_arr);
        return $this-> laout_arr;
    }
    
    
    /**
     * 學生註冊(寫入)
     * @desc 學生註冊(寫入)
     * 
     * 
     * 
     */
    public function RegisterStudentUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RegisterStudentUP($this->query_arr);
        return $this-> laout_arr;
    }
    
    
    /**
     * FAQ
     * @desc FAQ
     * 
     * 
     * 
     */
    public function RequestHelp() {
        return T('CourseCancellationNotice', array('nick_name' => 'dogstar'));
    }
    
    /**
     * 重設密碼
     * @desc 重設密碼
     * 
     * 
     * 
     */
    public function ResetPw() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ResetPw($this->query_arr);
        return $this-> laout_arr;
    }
    
    /**
     * 學生密碼修改
     * @desc 學生密碼修改
     * 
     * 
     * 
     */
    public function StudentPwEdit() {
        /*
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentPwEdit($this->query_arr);
        
        return $this-> laout_arr;
        */
        return 'EditPwUP';
    }
    
    
    /**
     * 學生點數紀錄
     * @desc 學生點數紀錄
     * 
     * 
     * 
     */
    public function StudentPointRecord() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentPointRecord($this->query_arr);
        return $this-> laout_arr;
    }
    
    /**
     * 學生評價
     * @desc 學生評價
     * 
     * 
     * 
     */
    public function RateClass() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RateClass($this->query_arr);
        return $this-> laout_arr;
    }
    
    /**
     * 評價列表
     * @desc 學生評價
     * 
     * 
     * 
     */
    public function RateList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RateList($this->query_arr);
        return $this-> laout_arr;
    }
    
    /**
     * 學生評價(寫入)
     * @desc 學生評價(寫入)
     * 
     * 
     * 
     */
    public function RateClassUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->RateClassUP($this->query_arr);
        return $this-> laout_arr;
    }
    
    /**
     * 老師進入教室
     * @desc 老師進入教室
     * 
     * 
     * 
     */
    public function TeacherInRoom() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->TeacherInRoom($this->query_arr);
        return $this-> laout_arr;
    }
    
    
    /**
     * 老師課程紀錄
     * @desc 老師課程紀錄
     * 
     * 
     * 
     */
    public function TeacherBookingRecord() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->TeacherBookingRecord();
        return $this-> laout_arr;
    }  

    /**
     * 老師課程紀錄
     * @desc 老師課程紀錄
     * 
     * 
     * 
     */
    public function TeacherAptRecord() {
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->TeacherAptRecord();
        return $this-> laout_arr;
    }  
    
    /**
     * 老師修改個人資料
     * @desc 老師修改個人資料
     * 
     * 
     * 
     */
    public function TeacherEdit() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->TeacherEdit($this->query_arr);
        return $this-> laout_arr;
    }  
    
    /**
     * 老師開啟教室(用不到)
     * @desc 老師開啟教室
     * 
     * 
     * 
     */
    public function TeacherOpneLessonRoom() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->TeacherOpneLessonRoom($this->query_arr);
        return $this-> laout_arr;
    }  

    /**
     * 老師修改密碼
     * @desc 老師修改密碼
     * 
     * 
     * 
     */
    public function TeacherPwEdit() {
        /*
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->StudentPwEdit($this->query_arr);
        
        return $this-> laout_arr;
        */
        return 'TeacherPwEdit';
    }
    
    
    /**
     * 網站文章內容
     * @desc 網站文章內容
     * 
     * 
     * 
     */
    public function Text() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->Text($this->query_arr);
        return $this-> laout_arr;
    } 

    public function WakeupAccount() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->WakeupAccount($this->query_arr);
        return $this-> laout_arr;
    }

    public function SendPhoneCode() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->SendPhoneCode($this->query_arr);
        return $this-> laout_arr;
    }

    public function WakeupAccountPhone() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->WakeupAccountPhone($this->query_arr);
        return $this-> laout_arr;
    }


    public function ReadFriendsNote() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ReadFriendsNote($this->query_arr);
        return $this-> laout_arr;
    }

    public function ReadSysInviteNotice() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ReadSysInviteNotice($this->query_arr);
        return $this-> laout_arr;
    }


    public function ChangeStudent() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ChangeStudent($this->query_arr);
        return $this-> laout_arr;
    }



    public function ChangeTeacher() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ChangeTeacher($this->query_arr);
        return $this-> laout_arr;
    }

    public function PhotoUpload() {    
        //print_r($_FILES);    
        //exit;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->PhotoUpload('image',$this->photoFile);
        return $this-> laout_arr;
    }

    public function BgPhotoUpload() {
        //print_r($_FILES);
        //exit;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BgPhotoUpload('bg_image',$this->photoFile,$this->bg_image_style);
        return $this-> laout_arr;
    }
    public function SetBgHeight() {
        //print_r($_FILES);
        //exit;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->SetBgHeight($this->bg_image_style);
        return $this-> laout_arr;
    }
    
    public function AddFriends() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AddFriends($this->query_arr);
        return $this-> laout_arr;
    }

    public function AddSubscription() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AddSubscription($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function AgreeFriends() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AgreeFriends($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function CancelFriends() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CancelFriends($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function CancelSubscription() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CancelSubscription($this->query_arr);
        return $this-> laout_arr;
    }

    public function CheckFriendsSubscription() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CheckFriendsSubscription($this->query_arr);
        return $this-> laout_arr;
    }
    public function FriendsList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->FriendsList($this->query_arr);
        return $this-> laout_arr;
    }
    public function SubscriptionList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->SubscriptionList($this->query_arr);
        return $this-> laout_arr;
    }
    public function WaitFriendsList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->WaitFriendsList($this->query_arr);
        return $this-> laout_arr;
    }
    

    public function UserList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();

        $this-> laout_arr[]  = $domain->UserList($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function ForgetPw() {
        $tmp_this = (array)$this;
        //array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ForgetPw($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function EventBookingRecord() {
        $tmp_this = (array)$this;
        //array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->EventBookingRecord($this->query_arr);
        return $this-> laout_arr;
    }
    
    //QRLink
    public function QRLinkList() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkList($this->query_arr);
        return $this-> laout_arr;
    }

    public function QRLinkEditUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkEditUP($this->query_arr);
        return $this-> laout_arr;
    }

    public function QRLinkUserNoteEditUP() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkUserNoteEditUP($this->query_arr);
        return $this-> laout_arr;
    }

    public function QRLinkCreate() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $type = $this->laout_check(DI()->request->get('type'));
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        if($type =='A'){
            $this-> laout_arr[]  = $domain->QRLinkACreate($this->query_arr);
        }else{
            $this-> laout_arr[]  = $domain->QRLinkSCreate($this->query_arr);
        }
        return $this-> laout_arr;
    }

    public function QRLinkApproval() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkApproval($this->query_arr);
        return $this-> laout_arr;
    }

    public function QRLinkAccountDelete() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkAccountDelete($this->query_arr);
        return $this-> laout_arr;
    }


    public function QRLinkBlock() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkBlock($this->query_arr);
        return $this-> laout_arr;
    }

    public function QRLinkDeleteUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkDeleteUP();

        return $this-> laout_arr;
    }


    public function QRLinkCheck() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->QRLinkCheck($this->query_arr);
        return $this-> laout_arr;
    }

    public function SendVaEmail() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->SendVaEmail($this->query_arr);
        return $this-> laout_arr;
    }

    

    


}
