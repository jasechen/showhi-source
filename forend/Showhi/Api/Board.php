<?php
class Api_Board extends Common_ApiDefaultList {

    public function getRules() {
        return array(
            'BoardList' => array(
            ),    
            'BoardListMedia' => array(
            ),
            'CheckVideo' => array(
            ),

            'BoardPost' => array(
               // 'file' => array('name' => 'file', 'type' => 'file', 'require' => true, 'desc' => '相片,影片'),
                'text' => array('name' => 'text', 'type' => 'string', 'desc' => '留言'),
                'read_permi' => array('name' => 'read_permi', 'type' => 'string', 'desc' => '權限'),
                'type' => array('name' => 'type', 'type' => 'string', 'desc' => '類別'),

            ),     
            'CreateLive' => array(
            ),    
            'UpdateLive' => array(
            ),  
            
            'BoardEditUP' => array(
               // 'file' => array('name' => 'file', 'type' => 'file', 'require' => true, 'desc' => '相片,影片'),
                'text' => array('name' => 'text', 'type' => 'string', 'desc' => '留言'),
                'read_permi' => array('name' => 'read_permi', 'type' => 'string', 'desc' => '權限'),
                'type' => array('name' => 'type', 'type' => 'string', 'desc' => '類別'),

            ),  
            'ReBoardDelete' => array(
                'id' => array('name' => 'id', 'type' => 'string', 'desc' => '留言板id'),
            ),

            
            'BoardEndLiveUP' => array(
            ), 
            

            'BoardChangeRead' => array(
                'id' => array('name' => 'id', 'type' => 'string', 'desc' => '留言板id'),
                'read_permi' => array('name' => 'read_permi', 'type' => 'string', 'desc' => '權限'),
            ),
            'BoardDelete' => array(
                'id' => array('name' => 'id', 'type' => 'string', 'desc' => '留言板id'),
            ),
            
            'ReBoardPost' => array(
                'text' => array('name' => 'text', 'type' => 'string', 'desc' => '留言'),
                'read_permi' => array('name' => 'read_permi', 'type' => 'string', 'desc' => '權限'),
                'type' => array('name' => 'type', 'type' => 'string', 'desc' => '類別'),
                'board_id' => array('name' => 'board_id', 'type' => 'string', 'desc' => '留言板id'),
            ),   

            'ReBoardEditUP' => array(
                'text' => array('name' => 'text', 'type' => 'string', 'desc' => '留言'),
                'read_permi' => array('name' => 'read_permi', 'type' => 'string', 'desc' => '權限'),
                'type' => array('name' => 'type', 'type' => 'string', 'desc' => '類別'),
                'id' => array('name' => 'id', 'type' => 'string', 'desc' => '留言板id'),
            ),    


            'AddLike' => array(

            ), 
            'UnLike' => array(

            ), 
            'AddShare' => array(

            ), 
        );
  
    }
    
    public function __construct() {  
        parent::__construct(); 
         //exit;
    }
    public function __destruct(){
        //exit;
    } 

    public function BoardList() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardList($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function BoardListMedia() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardListMedia($this->query_arr);
        return $this-> laout_arr;
    }
    

    public function BoardPost() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardPost($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function CreateLive() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $tmp_rs = $domain->CreateLive($this->query_arr);
        
        
        $this->query_arr = null;
        $this->query_arr['text'] =$tmp_rs['title'];
        $this->query_arr['read_permi'] ='public';
        $this->query_arr['type'] = '2';
        $this->query_arr['live_embed_url'] = $tmp_rs['embed_url'];
        $this->query_arr['live_id'] = $tmp_rs['live_id'];
        $this->query_arr['stream_key'] = $tmp_rs['stream_key'];
        $this->query_arr['in_cloud'] = 'Y';
        $domain->BoardPost($this->query_arr);
        
        $this-> laout_arr[] = $tmp_rs;
        return $this-> laout_arr;
    }

    public function UpdateLive() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $tmp_rs = $domain->UpdateLive($this->query_arr);
        
        /*
        $this->query_arr = null;
        $this->query_arr['text'] =$tmp_rs['title'];
        $this->query_arr['read_permi'] ='public';
        $this->query_arr['type'] = '2';
        $this->query_arr['live_embed_url'] = $tmp_rs['embed_url'];
        $this->query_arr['live_id'] = $tmp_rs['live_id'];
        $this->query_arr['stream_key'] = $tmp_rs['stream_key'];
        $this->query_arr['in_cloud'] = 'Y';
        $domain->BoardPost($this->query_arr);
        */
        
        $this-> laout_arr[] = $tmp_rs;
        return $this-> laout_arr;
    }

    public function CheckVideo() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->CheckVideo($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function BoardEditUP() {
        $tmp_this = (array)$this;
        //array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardEditUP($this->query_arr);
        return $this-> laout_arr;
    }

    public function ReBoardEditUP() {
        $tmp_this = (array)$this;
        //array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ReBoardEditUP($this->query_arr);
        return $this-> laout_arr;
    }



    public function BoardEndLiveUP() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardEndLiveUP($this->query_arr);
        return $this-> laout_arr;
    }
    
    
    
    public function BoardChangeRead() {
        $tmp_this = (array)$this;
        array_splice($tmp_this,0,1);
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        echo $this-> laout_arr[]  = $domain->BoardChangeRead($this->query_arr);
        exit;
        return $this-> laout_arr;
    }
    
    public function BoardDelete() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->BoardDelete($this->query_arr);
        return $this-> laout_arr;
    }

    public function ReBoardDelete() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;
        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ReBoardDelete($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function ReBoardPost() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->ReBoardPost($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function AddLike() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AddLike($this->query_arr);
        return $this-> laout_arr;
    }
    
    public function UnLike() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->UnLike($this->query_arr);
        return $this-> laout_arr;
    }

    public function AddShare() {
        $tmp_this = (array)$this;
        $this->query_arr = $tmp_this;

        $tmp_domain_class =DOMAIN_CLASS_NAME;
        $domain = new $tmp_domain_class();
        $this-> laout_arr =array();
        $this-> laout_arr[]  = $domain->AddShare($this->query_arr);
        return $this-> laout_arr;
    }

}
