<?php

class Model_UserModel extends Common_ModelDefaultList {
    
    //ex 有用到
    public function getByUserId($userId) {
        return $this->getORM()
            ->select('*')
            ->where('id = ?', $userId)
            ->fetch();
    }


    public function getByUserIdWithCache($userId) {
        $key = 'userbaseinfo_' . $userId;
        //要自己寫 原本的沒有宣告 cache
        DI()->cache = new PhalApi_Cache_File(array('path' => API_ROOT . '/Runtime', 'prefix' => 'demo'));
        //
        $rs = DI()->cache->get($key);
        if ($rs === NULL) {
            $rs = $this->getByUserId($userId);
            DI()->cache->set($key, $rs, 600);
        }
        return $rs;
    }
    //end ex
    
    //建構子
    public function __construct() {   
        parent::__construct(); 
    }
    
    public function __destruct(){

    }
    
    
    public function getTableName($id) {
        return "student";
    }
    

    public function ZoomOpen() {
        $tmp_room =$this->ZoomApiTw->meeting_create('Demo Room','WYB0SNRoQfSs1qrFgd5jkg');

        return $tmp_room;
    }

    public function Login($loginid,$phone,$loginpwd,$identity,$time_set,$email) {
            //$this->ZoomApiTw->meeting_create($subject);
            //$this->ZoomApiTw->user_get();
            //$this->ZoomApiTw->user_list();
            //$room_list =$this->ZoomApiTw->meeting_delete('403869474');
            //$room_list =$this->ZoomApiTw->meeting_delete('403869474');
            //$room_list =$this->ZoomApiTw->meeting_get('262705744','9375a4cf36f94f5f52a3f32ea497eb');
            //$room_list =$this->ZoomApiTw->meeting_list('145b09d78715823f997e8f20564ec4');
            //$tmp_room =$this->ZoomApiTw->meeting_create('Demo Room','WYB0SNRoQfSs1qrFgd5jkg');
            //print_r($tmp_room);
            //exit;
        //雙登入

            $identity='teach';
            $tmp_table='teach';

            $tmp_arr = DI()->notorm->$tmp_table->select('*')
            ->where(" ( loginid = ? || phone = ?  ) and ( block !='Y' or isnull(block) ) ", $loginid,$phone)->fetchAll();
            if(count($tmp_arr) ==0){

                    $tmp_arr[0]['msg_text'] =T('NoAccountError');
                    $tmp_arr[0]['msg_error_code'] ='NoAccountError';
                    $tmp_arr[0]['update_time'] =date('Y-m-d H:i:s');
                    $tmp_arr[0]['msg_state'] ='N';
                 return $tmp_arr;
            }

            $tmp_arr = DI()->notorm->$tmp_table->select('*')
            ->where(" ( loginid = ? || phone = ?  ) and ( block !='Y' or isnull(block) ) and loginpwd  = md5(?) ", $loginid,$phone,$loginpwd)->fetchAll();



        //end 雙登入
        
        //寫入seesion
                if(count($tmp_arr) ==1){

                    if($tmp_arr[0]['type_id'] >=2){
                        $tmp_room =$this->ZoomApiTw->meeting_create('Demo Room','WYB0SNRoQfSs1qrFgd5jkg');
                        $tmp_room = json_decode(json_encode($tmp_room), True);
                    }
                    $replace_arr=array('bg_image','image');
                    $lifetime = 24 * 3600;
                    //session_save_path($savepath); 
                    session_set_cookie_params($lifetime);
                    //login
                    $tmp_arr =  $this->RePlaceFilename($tmp_arr,$replace_arr,'id');
                    $_SESSION['f_backend']['identity']=$identity;
                    $_SESSION['f_backend']['login']=1;
                    $_SESSION['f_backend']['level']='1';   
                    $_SESSION['f_backend']['time_set']=$time_set;   
                    $_SESSION['f_backend']['loginid']=$tmp_arr[0]['loginid'];                
                    $_SESSION['f_backend']['user_id']=$tmp_arr[0]['id']; 
                    $_SESSION['f_backend']['type_id']=$tmp_arr[0]['type_id']; 
                    $_SESSION['f_backend']['name']=$tmp_arr[0]['nick_name'];  
                    $_SESSION['f_backend']['email_va']=$tmp_arr[0]['email_va'];     
                    $_SESSION['f_backend']['email']=$tmp_arr[0]['email'];     
                    $_SESSION['f_backend']['phone']=$tmp_arr[0]['phone'];     
                    $_SESSION['f_backend']['phone_va']=$tmp_arr[0]['phone_va'];     
                    $_SESSION['f_backend']['approval']=$tmp_arr[0]['approval'];     
                    $_SESSION['f_backend']['rel_teach_id']=$tmp_arr[0]['rel_teach_id'];     
                    $_SESSION['f_backend']['rel_student_id']=$tmp_arr[0]['rel_student_id'];     
                    $_SESSION['f_backend']['image']=$tmp_arr[0]['image'];     
                    $_SESSION['f_backend']['bg_image']=$tmp_arr[0]['bg_image'];     
                    $_SESSION['f_backend']['def_page']='user.php?action/'.$identity.'er_edit';

                    $_SESSION['f_backend']['teach_room_url']=$tmp_room['result']['data']['start_url'];

                    $_SESSION['f_backend']['create_at']=$tmp_room['result']['data']['create_at'];
                    $_SESSION['f_backend']['room_id']=$tmp_room['result']['data']['id'];

                    $_SESSION['f_backend']['agree']=$tmp_arr[0]['agree'];  
                    $_SESSION['f_backend']['block']=$tmp_arr[0]['block'];  
                    $_SESSION['f_backend']['seesion_id']=session_id();  
                    $tmp_arr[0]['sid']=session_id();  

                    //get chatmenber token  
                    $sendResult = @$this->Livehousein->GetChatMembersToken();
                    if($sendResult['result']['token'] != null)
                        $_SESSION['f_backend']['chat_token']=$sendResult['result']['token']; 
                    else{
                        @$this->Livehousein->CreateChatMembers();  
                        $sendResult = @$this->Livehousein->GetChatMembersToken(); 
                        $_SESSION['f_backend']['chat_token']=$sendResult['result']['token']; 
                    }
                    //end get chatmenber token
                    
                    setcookie("user_id", $tmp_arr[0]['id'], time()+86400);
                }else{
                    $tmp_arr[0]['msg_text'] =T('AccountError');
                    $tmp_arr[0]['msg_error_code'] ='AccountError';
                    $tmp_arr[0]['update_time'] =date('Y-m-d H:i:s');
                    $tmp_arr[0]['msg_state'] ='N';
                    
                }
                //  header("Location: ".$_SESSION['backend']['def_page']);    
                    
        //end 寫入seesion
        $tmp_arr = $this->Takeout_Password($tmp_arr);

         return $tmp_arr;
        
        /*ex 
        fetchAll,fetchRows 抓全部
        fetchOne,fetchRow  抓一筆
        fetch 抓出所有資料(不建議多次訪問資料庫)
        
         用 fetch 抓出所有資料(不建議多次訪問資料庫)
        */
        /*
        $b =$this->getORM()->select('*')
        ->where('loginid = ? and loginpwd  = md5(?)', $loginid,$loginpwd)
        ->where('1')->limit(2);
        while(($row = $b->fetch())) {
            var_dump($row);
         }
         */ 
    }
    

    public function ChangeIdentity($loginid,$phone,$loginpwd,$identity,$time_set) {
            $tmp_table=$identity;
            $tmp_arr = DI()->notorm->$tmp_table->select('*')
            ->where(' ( loginid = ? || phone = ? )  ', $loginid,$phone)->fetchAll();
            
        //寫入seesion
                if(count($tmp_arr) ==1){
                  $_SESSION['f_backend']['identity']=$identity;
                  $_SESSION['f_backend']['login']=1;
                  $_SESSION['f_backend']['level']='1';   
                  $_SESSION['f_backend']['time_set']=$time_set;   
                  $_SESSION['f_backend']['loginid']=$tmp_arr[0]['loginid'];                
                  $_SESSION['f_backend']['user_id']=$tmp_arr[0]['id']; 
                  $_SESSION['f_backend']['name']=$tmp_arr[0]['nick_name'];  
                  $_SESSION['f_backend']['email']=$tmp_arr[0]['email'];     
                  $_SESSION['f_backend']['email_va']=$tmp_arr[0]['email_va'];     
                  $_SESSION['f_backend']['phone']=$tmp_arr[0]['phone'];     
                  $_SESSION['f_backend']['phone_va']=$tmp_arr[0]['phone_va'];     
                  $_SESSION['f_backend']['approval']=$tmp_arr[0]['approval'];        
                  if($identity == 'student'){
                      $tmp_arr[0]['identity'] = $identity;
                      $_SESSION['f_backend']['def_page']='user.php?action/student_edit';    
                  }
                  elseif($identity == 'teach'){
                      $tmp_arr[0]['identity'] = $identity;
                      $_SESSION['f_backend']['def_page']='user.php?action/'.$identity.'er_edit';
                      $_SESSION['f_backend']['agree']=$tmp_arr[0]['agree'];
                  }
                }else{
                    $tmp_arr[0]['msg_text'] =T('AccountError');
                    $tmp_arr[0]['update_time'] =date('Y-m-d H:i:s');
                    $tmp_arr[0]['msg_state'] ='N';
                    
                }
                //  header("Location: ".$_SESSION['backend']['def_page']);    
                    
        //end 寫入seesion
        $tmp_arr = $this->Takeout_Password($tmp_arr);
         return $tmp_arr;
        
        /*ex 
        fetchAll,fetchRows 抓全部
        fetchOne,fetchRow  抓一筆
        fetch 抓出所有資料(不建議多次訪問資料庫)
        
         用 fetch 抓出所有資料(不建議多次訪問資料庫)
        */
        /*
        $b =$this->getORM()->select('*')
        ->where('loginid = ? and loginpwd  = md5(?)', $loginid,$loginpwd)
        ->where('1')->limit(2);
        while(($row = $b->fetch())) {
            var_dump($row);
         }
         */ 
    }


    //*
    public function oth_login(){
        
    }
    //url 確認問題 要怎麼做
    public function get_pemi($loginid,$rs,$url = 'article'){ //確認權限
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "select a.*,b.name,b.title,b.keywords,b.description,b.fb_title,b.fb_keywords,b.fb_description,b.fb_image,b.url,b.approval as func_approval   from ".$table_first_name."admin_func_detail   as a 
            left join ".$table_first_name."admin_func   as b on a.func_id =b.id  
            left join ".$table_first_name."admin_user   as c on a.user_id =c.id 
             where a.group_id =:group_id and b.url =:url";
        $params = array(':group_id' => $rs[0]['group_id'] ,':url' => $url );
        
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
         return $tmp_arr;
        /** ex
        * queryAll  抓所有
        * queryRows 抓所有
        */
        
    }
    
    public function getUserInfo($loginid) {
        $tmp_arr[] = $this->getORM()
            ->select('*')
            ->where('loginid = ?', $loginid)
            ->fetch();
            
        $tmp_arr = $this->Takeout_Password($tmp_arr);
        
        return $tmp_arr;
    }

    

    
    
    public function StudentBookingRecord(){
        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];    
       //end get
       
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where student_id=:user_id and unix_timestamp(now()) 
         > unix_timestamp(CONCAT(DATE_FORMAT(c.lesson_day,'%Y-%m-%d'),' ',f.name))+1800
            and a.student_cancel !='Y' and a.teach_leave !='Y' ";

        $tmp_order = " order by id desc ";
        //分頁用sql
        $sql_count = "select 
                h.lesson_count,a.*, 
                c.lesson_day, c.teach_id, 
                d.nationality, d.nick_name, d.loginid, d.loginpwd, d.image,
                e.link,
                f.name as time_name,
                g.name as category_name
                from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."teach as b on a.student_id = b.id
                left join ".$table_first_name."lesson as c on a.lesson_id = c.id
                left join ".$table_first_name."teach as d on c.teach_id = d.id
                left join ".$table_first_name."teach_article as e on a.article_link = e.id
                left join ".$table_first_name."realtion_datas as f on a.can_tutor_time = f.id
                left join ".$table_first_name."realtion_datas as g on a.type = g.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order." ";
            
        //抓資料sql
        $sql = $sql_count." limit :limit_start , :limit_num ";

        //分頁

        $params = array(':user_id' => $user_id);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 10;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁

        $params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);

        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
        
    }

    public function StudentAptRecord(){
        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];    
        $loginid=$_SESSION['f_backend']['loginid'];    
        $time_set=$_SESSION['f_backend']['time_set']+8;    
        
       //end get

        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->realtion_datas->select('*')->where('rel_id = ?', 12)->fetchAll();

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where b.loginid=:loginid and unix_timestamp(now())  
        <= unix_timestamp(CONCAT(DATE_FORMAT(c.lesson_day,'%Y-%m-%d'),' ',f.name))+1800
            and a.student_cancel !='Y' and a.teach_leave !='Y' ";

        $tmp_order = " order by id desc ";
        $tmp_limit = " ";
        $sql_count = "select 
                d.image as teach_image,h.lesson_count,a.*,
                c.lesson_day, c.teach_id, 
                d.nationality, d.nick_name, d.loginid, d.loginpwd, d.image,
                e.link,
                f.name as time_name,
                g.name as category_name
                from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."teach as b on a.student_id = b.id
                left join ".$table_first_name."lesson as c on a.lesson_id = c.id
                left join ".$table_first_name."teach as d on c.teach_id = d.id
                left join ".$table_first_name."teach_article as e on a.article_link = e.id
                left join ".$table_first_name."realtion_datas as f on a.can_tutor_time = f.id
                left join ".$table_first_name."realtion_datas as g on a.type = g.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                where student_cancel !='Y' and teach_leave !='Y'
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order.$tmp_limit;
        
                
        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;

        //分頁

        $params = array(':loginid' => $loginid);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 1;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁


                
        //$params = array(':loginid' => $loginid,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $params = array(':loginid' => $loginid);
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);
        foreach($tmp_arr['tmp_list'] as $key => $value ){
            foreach($tmp_arr['tmp_arr_can_tutor_time'] as $key1 => $value1 ){
                    if($value['can_tutor_time'] == $value1['id']){
                            $tmp_date = date('Y-m-d',strtotime($value['lesson_day'])).' '.$value1['name'];
                            $tmp_date = date('Y-m-d H:i:s',strtotime("$tmp_date -$time_set hour"));
                            $tmp_arr['tmp_list'][$key]['lesson_day'] = date('Y-m-d',strtotime("$tmp_date"));
                            $tmp_arr['tmp_list'][$key]['lesson_time'] = date('H:i',strtotime("$tmp_date"));
                    }
                }
        }

        $replace_arr=array('teach_image');
        $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'teach_id');
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
        
    }


    public function EventBookingRecord(){
        //抓出行事曆哪一個月所有的booking
        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];    
        $loginid=$_SESSION['f_backend']['loginid']; 
        
        $this_day = $this->laout_check(DI()->request->get('this_day'));
        if($this_day == null)
        $this_day == strtotime(date('Y-m-d',strtotime("now")));
        
        $this_start_mon = strtotime(date('Y-m-01',strtotime($this_day)));
        $this_end_mon = strtotime(date('Y-m-t',strtotime($this_day)));
        
        
        
       //end get
       
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where b.loginid=:loginid and 
        $this_start_mon  <= unix_timestamp(CONCAT(DATE_FORMAT(c.lesson_day,'%Y-%m-%d'),' ',f.name))+1800
        and
        $this_end_mon  >= unix_timestamp(CONCAT(DATE_FORMAT(c.lesson_day,'%Y-%m-%d'),' ',f.name))+1800
            and a.student_cancel !='Y' and a.teach_leave !='Y' ";

        $tmp_order = " order by id desc ";
        $tmp_limit = " ";
        $sql_count = "select 
                d.image as teach_image,h.lesson_count,a.*,
                c.lesson_day, c.teach_id, 
                d.nationality, d.nick_name, d.loginid, d.loginpwd, d.image,
                e.link,
                f.name as time_name,
                g.name as category_name
                from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."teach as b on a.student_id = b.id
                left join ".$table_first_name."lesson as c on a.lesson_id = c.id
                left join ".$table_first_name."teach as d on c.teach_id = d.id
                left join ".$table_first_name."teach_article as e on a.article_link = e.id
                left join ".$table_first_name."realtion_datas as f on a.can_tutor_time = f.id
                left join ".$table_first_name."realtion_datas as g on a.type = g.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                where student_cancel !='Y' and teach_leave !='Y'
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order.$tmp_limit;
        
        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;

        //分頁

        $params = array(':loginid' => $loginid);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 1;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁


                
        //$params = array(':loginid' => $loginid,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $params = array(':loginid' => $loginid);
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);
        $replace_arr=array('teach_image');
        $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'teach_id');
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
        
    }

    
    public function CanTutorTime(){

        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $loginid=$_SESSION['f_backend']['loginid'];    
       //end get
        $tmp_table='teach';
        $tmp_arr['tmp_list'] = DI()->notorm->$tmp_table->select('*')
        ->where(' loginid=? ', $loginid )->fetchAll();
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        
           for ($i = 0; $i <= 6; $i++) {
               if($i == 0){
                $k =null;
            }else{
                $k =$i;
            }
            //todo: time_set
       //    $this->obj_tmp1 ->laout_arr['tmp_arr'][0]['can_tutor_time'.$k]=
       //    $this->change_time_set($this->obj_tmp1 ->laout_arr['tmp_arr'][0]['can_tutor_time'.$k]);
       
       //時區返算
        $tmp_arr['tmp_list'][0]['can_tutor_time'.$k]=
           $tmp_arr['tmp_list'][0]['can_tutor_time'.$k]?$this->redo_time_set($tmp_arr['tmp_list'][0]['can_tutor_time'.$k]):null;    
               
        }
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_list_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        return $tmp_arr;
        
    }
    
    function CourseContent(){   
    //get
        $orderby = $this->laout_check(DI()->request->get('orderby'));
        $tag = $this->laout_check(DI()->request->get('tag'));
   //end get
   
            if($orderby == ''){
               $orderby ='id';
           }elseif($orderby == 'hot'){
               $orderby ='choose_count';
           }elseif($orderby == 'time'){
               $orderby ='id';
           }
           
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where LOWER(CONCAT(',',tag,',')) like LOWER('%,$tag,%') and approval ='Y' ";
        $tmp_order = " order by $orderby desc ";
        $tmp_limit = " ";
        $sql = "select * from ".$table_first_name."teach_article    
        ".$tmp_where.$tmp_order.$tmp_limit;

        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql);
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr; 

    }
    

    public function StudentEdit(){
        $tmp_table='student';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
       //end get
       
        $tmp_arr['tmp_list'] = DI()->notorm->$tmp_table->select('*')
        ->where('id = ?', $user_id)->fetchAll();
        $tmp_arr['tmp_list'][0]['age'] = date('Y') - date('Y',strtotime($tmp_arr['tmp_list'][0]['birth_day']));
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        //type           
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where  a.id =:user_id and c.confirm_lesson ='Y' and c.student_cancel !='Y' and isnull(c.teach_score) ";
        $tmp_order = " order by a.id asc ";
        $tmp_limit = " ";
        $sql = "select a.id
            from ".$table_first_name."student 
            as a 
            left join ".$table_first_name."lesson_data as c on c.student_id =a.id
            left join ".$table_first_name."lesson as b on b.id =c.lesson_id
            left join ".$table_first_name."teach as d on d.id =b.teach_id    
            left join ".$table_first_name."realtion_datas as e on c.can_tutor_time =e.id
            ".$tmp_where.$tmp_order.$tmp_limit;
            
        $params = array(':user_id' => $user_id);
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_nat'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 2)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teach_score'] = $tmp_obj->queryAll($sql,$params);
           $tmp_arr['teach_score']  = count($tmp_arr['tmp_arr_teach_score']);
        return $tmp_arr;
            
    }

    
    function RegisterTeacher(){   
    //get

   //end get
   
   
        //type
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_nat'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 2)->fetchAll();
        $tmp_arr['tmp_arr_education'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 9)->fetchAll();
        $tmp_arr['tmp_arr_expertise'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 3)->fetchAll();
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 13)->fetchAll();
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 4)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();


        return $tmp_arr; 

    }
    
    
    public function TeacherBookingRecord(){
        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];  
        $loginid=$_SESSION['f_backend']['loginid']; 
       //end get
       
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.student_cancel !='Y'  and teach_leave !='Y'
                and unix_timestamp(now())  > unix_timestamp(CONCAT(DATE_FORMAT(b.lesson_day,'%Y-%m-%d'),' ',e.name))+1800
                and  ( c.id =:user_id )";

        $tmp_order = " order by id desc ";
        $tmp_limit = " ";
        $sql_count = "select h.lesson_count,a.*,c.id as teach_id,d.nick_name,d.skype,b.lesson_day from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."lesson as b on a.lesson_id =b.id
                left join ".$table_first_name."teach as c on c.id =b.teach_id
                left join ".$table_first_name."teach as d on a.student_id = d.id
                left join ".$table_first_name."realtion_datas as e on a.can_tutor_time =e.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order.$tmp_limit;

        //抓資料sql
        $sql = $sql_count." limit :limit_start , :limit_num ";

        //分頁

        $params = array(':user_id' => $user_id);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 10;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁


                
        $params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
        
    }

    public function TeacherAptRecord(){
        //get //重新判斷登入 todo lgoin
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];    
        $loginid=$_SESSION['f_backend']['loginid'];    
        $time_set=$_SESSION['f_backend']['time_set']+8;

       //end get
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->realtion_datas->select('*')->where('rel_id = ?', 12)->fetchAll();

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.student_cancel !='Y'  and teach_leave !='Y'
                and unix_timestamp(now())  <= unix_timestamp(CONCAT(DATE_FORMAT(b.lesson_day,'%Y-%m-%d'),' ',e.name))+1800
                and  ( c.id =:user_id ) ";

    
        $tmp_order = " order by id desc ";
        $tmp_limit = " ";
        $sql_count = "select e.name as time_name,d.id as student_id,d.image as student_image,h.lesson_count,a.*,c.id as teach_id,c.image as teach_image,d.image as student_image ,d.nick_name,d.skype,b.lesson_day from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."lesson as b on a.lesson_id =b.id
                left join ".$table_first_name."teach as c on c.id =b.teach_id
                left join ".$table_first_name."teach as d on a.student_id = d.id
                left join ".$table_first_name."teach as g on g.loginid = d.loginid
                left join ".$table_first_name."realtion_datas as e on a.can_tutor_time =e.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                where student_cancel !='Y' and teach_leave !='Y'
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order.$tmp_limit;


        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;

        //分頁

        $params = array(':user_id' => $user_id);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 1;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁


                
        //$params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $params = array(':user_id' => $user_id);
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);
        foreach($tmp_arr['tmp_list'] as $key => $value ){
            foreach($tmp_arr['tmp_arr_can_tutor_time'] as $key1 => $value1 ){
                    if($value['can_tutor_time'] == $value1['id']){
                            $tmp_date = date('Y-m-d',strtotime($value['lesson_day'])).' '.$value1['name'];
                            $tmp_date = date('Y-m-d H:i:s',strtotime("$tmp_date -$time_set hour"));
                            $tmp_arr['tmp_list'][$key]['lesson_day'] = date('Y-m-d',strtotime("$tmp_date"));
                            $tmp_arr['tmp_list'][$key]['lesson_time'] = date('H:i',strtotime("$tmp_date"));
                    }
                }
        }

        $replace_arr=array('student_image');
        $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'student_id');


        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
        
    }
    
    
    
    public function TeacherEdit(){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
        
        $last_month_start_day = date('Y-m-01',strtotime("now -1 year"));
        $last_month_end_day = date('Y-m-01',strtotime("now"));
        $this_month_start_day = date('Y-m-01',strtotime("now"));
        
       //end get

        $tmp_arr['tmp_list'] = DI()->notorm->$tmp_table->select('*')->where('id = ?', $user_id)->fetchAll();
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        
        $replace_arr=array('image','bg_image');
        $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'id');
        
        //粉絲團身分是否有行政人員
        $tmp_arr['fan_manage'] = DI()->notorm->fan_manage->select('*')
        ->where("user_id = ? && ( `type` ='staff' || `type` ='admin'  ) ", $user_id)->fetchOne();
            if($tmp_arr['fan_manage']['type'] == 'staff' || $tmp_arr['fan_manage']['type'] == 'admin' ){
            $tmp_arr['tmp_list'][0]['fan_ident'] ='staff';
        }else{
            $tmp_arr['tmp_list'][0]['fan_ident'] =null;
        }
        
        //取得上課次數
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.id =:user_id and
            unix_timestamp(b.lesson_day) >= unix_timestamp(:last_month_start_day)  
            and 
            unix_timestamp(b.lesson_day) < unix_timestamp(:last_month_end_day)
            and  (student_cancel !='Y'  and teach_leave !='Y')";
            
        $tmp_order = " ";
        $tmp_limit = " ";
        $sql = "select count(*) as count from 
                ".$table_first_name."teach as a left join
                ".$table_first_name."lesson as b on a.id =b.teach_id left join 
                ".$table_first_name."lesson_data as c on b.id =c.lesson_id
            ".$tmp_where.$tmp_order.$tmp_limit;


   
        $params = array(':user_id' => $user_id ,
        ':last_month_start_day' => $last_month_start_day,
        ':last_month_end_day' => $last_month_end_day
        );

        $tmp_arr['last_count'] = $tmp_obj->queryAll($sql,$params);

        

        
        $tmp_where = " where a.id =:user_id and
            unix_timestamp(b.lesson_day) >= unix_timestamp(:last_month_start_day)  
            and 
            unix_timestamp(b.lesson_day) < unix_timestamp(:last_month_end_day)
            and  (student_cancel !='Y'  and teach_leave !='Y') and teach_in_room ='Y' ";
            
        $tmp_order = " ";
        $tmp_limit = " ";
        $sql = "select count(*) as count from 
                ".$table_first_name."teach as a left join
                ".$table_first_name."lesson as b on a.id =b.teach_id left join 
                ".$table_first_name."lesson_data as c on b.id =c.lesson_id
            ".$tmp_where.$tmp_order.$tmp_limit;

            
        $params = array(':user_id' => $user_id ,
        ':last_month_start_day' => $last_month_start_day,
        ':last_month_end_day' => $last_month_end_day
        );

        $tmp_arr['last_real_count'] = $tmp_obj->queryAll($sql,$params);
        //end 取得上課次數
        
        $tmp_where = " where a.id =:user_id
            and
            unix_timestamp(b.lesson_day) <= unix_timestamp()  
            and  (student_cancel !='Y'  and teach_leave !='Y') and !isnull(c.teach_score) ";
            
        $tmp_order = " group by b.teach_id order by b.teach_id asc ";
        $tmp_limit = " ";
        $sql = "select count(*) as count from 
                ".$table_first_name."teach as a left join
                ".$table_first_name."lesson as b on a.id =b.teach_id left join 
                ".$table_first_name."lesson_data as c on b.id =c.lesson_id
            ".$tmp_where.$tmp_order.$tmp_limit;

            
        $params = array(':user_id' => $user_id );

        $tmp_arr['score_count'] = $tmp_obj->queryAll($sql,$params);
        //type    
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_nat'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 2)->fetchAll();
        $tmp_arr['tmp_arr_education'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 9)->fetchAll();
        $tmp_arr['tmp_arr_expertise'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 3)->fetchAll();
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 13)->fetchAll();
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 4)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();
        //end type
        
        return $tmp_arr;
            
    }
    
    public function TeacherOpneLessonRoom(){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
        $lesson_dt_id = $this->laout_check(DI()->request->get('lesson_dt_id'));
       //end get
       
        $tmp_arr['tmp_list'] = DI()->notorm->$tmp_table->select('*')
        ->where('id = ?', $user_id)->fetchAll();
        
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        


        //自己的上課畫面
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = "where a.student_cancel !='Y'  and teach_leave !='Y'
                and
                c.id = ':user_id' and a.id=':lesson_dt_id' ";
            
        $tmp_order = " ";
        $tmp_limit = " ";
        $sql = "select a.*,d.nick_name,d.skype,b.lesson_day from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."lesson as b on a.lesson_id =b.id
                left join ".$table_first_name."teach as c on c.id =b.teach_id
                left join ".$table_first_name."teach as d on a.student_id = d.id
                left join ".$table_first_name."realtion_datas as e on c.can_tutor_time =e.id
            ".$tmp_where.$tmp_order.$tmp_limit;

            
        $params = array(
        ':user_id' => $user_id ,
        ':lesson_dt_id' => $lesson_dt_id
        );

        $tmp_arr['last_count'] = $tmp_obj->queryAll($sql,$params);
        //end 自己的上課畫面

        //type    
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 14)->fetchAll();
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 15)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_article'] = DI()->notorm->$tmp_table->select('*')->fetchAll();        
        //end type
        
        return $tmp_arr;
            
    }
    
    public function Text(){
        $tmp_table='article';
        
        //get todo login
        $id = $this->laout_check(DI()->request->get('id'));
        $code_name = $this->laout_check(DI()->request->get('code_name'));
       //end get
            $tmp_sql = DI()->notorm->$tmp_table->select('*');
            if(!empty($code_name)){
            $tmp_arr['tmp_list'] =$tmp_sql->where(' code_name = ?', $code_name)->fetchAll();
           }else{
            $tmp_arr['tmp_list'] =$tmp_sql->where('id = ?', $id)->fetchAll();
           }

        return $tmp_arr;
            
    }
    
    public function RateList(){
        //get //重新判斷登入 todo lgoin
        $teach_id = $this->laout_check(DI()->request->get('teach_id'));


       //end get
       
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.student_cancel !='Y'  and teach_leave !='Y'
                and unix_timestamp(now())  > unix_timestamp(CONCAT(DATE_FORMAT(b.lesson_day,'%Y-%m-%d'),' ',e.name))+1800
                and  ( c.id =:user_id )
                and
                    !isnull(a.teach_score)  and !isnull(a.lesson_rate_note)
                ";

        $tmp_order = " order by id desc ";
        $tmp_limit = " ";
        $sql_count = "select g.title as class_name,d.image as student_image ,d.nick_name as student_nick_name, h.lesson_count,a.*,c.id as teach_id,d.nick_name,d.skype,b.lesson_day from ".$table_first_name."lesson_data as a 
                left join ".$table_first_name."lesson as b on a.lesson_id =b.id
                left join ".$table_first_name."teach as c on c.id =b.teach_id
                left join ".$table_first_name."teach as d on a.student_id = d.id
                left join ".$table_first_name."realtion_datas as e on a.can_tutor_time =e.id
                left join ".$table_first_name."teach_article as g on a.article_link =g.id
                left join  ( select id,count(*) as lesson_count  
                from  ".$table_first_name."lesson_data 
                group by lesson_id,can_tutor_time
                ) as h on h.id = a.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
        //抓資料sql
        $sql = $sql_count." limit :limit_start , :limit_num ";

        //分頁

        $params = array(':user_id' => $teach_id);
        $tmp_count['tmp_list'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['tmp_list']);
           $this -> page_rec = 10;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁



                
        $params = array(':user_id' => $teach_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $tmp_arr['setting']['lesson_student_count'] = $this->setting['setting']['lesson_student_count']['value'];
        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql,$params);
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        $replace_arr=array('student_image');
        $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'id');
        
        
        //get class_list
        $tmp_table='teach_article';
        $tmp_sql = DI()->notorm->$tmp_table->select('*');
        $tmp_arr['teach_class_list'] =$tmp_sql->fetchAll();
        $replace_arr=array('image','bg_image');
        $tmp_arr['teach_class_list'] =  $this->RePlaceFilename($tmp_arr['teach_class_list'],$replace_arr,'id');
        
        return $tmp_arr;
        
    }

    public function UserList(){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
       //end get

        //自己的上課畫面
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //抓取搜尋
        $tmp_where = $this->search();
        //end 抓取搜尋
        
           if($_REQUEST['orderby'] == null)
                $tmp_orderby = 'count';
            else
                $order = $this->laout_check(DI()->request->get('order'));

            if($order =='count')    
                $order = ' order by lesson_count desc ';
            elseif($order =='exp')
                $order = ' order by exp asc '; 
            else
                $order = ' order by a.type_id asc, a.id asc ';    
                    
                
        $tmp_order = $order;
        $tmp_limit = " ";
        $sql = "SELECT a.* , b.id as teach_id,b.count as lesson_count FROM ".$table_first_name."teach as a 
                left join (
                    select 
                    a.id ,count(*) as count 
                    from ".$table_first_name."teach 
                    as a left join ".$table_first_name."lesson as b on a.id =b.teach_id 
                    left join ".$table_first_name."lesson_data as c on b.id =c.lesson_id 
                    where unix_timestamp(b.lesson_day) <= unix_timestamp() 
                    and (student_cancel !='Y' and teach_leave !='Y') 
                    and !isnull(c.teach_score) group by b.teach_id order by b.teach_id asc 
                ) as b on a.id=b.id
                where approval ='Y'
            ".$tmp_where.$tmp_order.$tmp_limit;



        $tmp_arr['tmp_teach_arr'] = $tmp_obj->queryAll($sql);
        
        $sql = "select * from ".$table_first_name."fan as a where approval='Y' 
            ".$tmp_where;
        $tmp_arr['tmp_fan_arr'] = $tmp_obj->queryAll($sql);
        
        
        //end 自己的上課畫面

        //type 
        /*   
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 14)->fetchAll();
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 15)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_article'] = DI()->notorm->$tmp_table->select('*')->fetchAll(); 
        */       
        //end type
        $replace_arr=array('image','bg_image');
        $tmp_arr['tmp_teach_arr'] =  $this->RePlaceFilename($tmp_arr['tmp_teach_arr'],$replace_arr);
        $tmp_arr['tmp_teach_arr'] = $this->Takeout_Password($tmp_arr['tmp_teach_arr']);
        
        $replace_arr=array('image','bg_image');
        $tmp_arr['tmp_fan_arr'] =  $this->RePlaceFilename($tmp_arr['tmp_fan_arr'],$replace_arr);
        $tmp_arr['tmp_fan_arr'] = $this->Takeout_Password($tmp_arr['tmp_fan_arr']);
        
        return $tmp_arr;
    }

    public function QRLinkList(){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
       //end get
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;

                //確定權限
                $tmp_arr = DI()->notorm->teach->where('id =? && type_id >=2 ',$user_id)->fetchAll();
                
                //update note
                if(count($tmp_arr)){
                    $rs['qrcode_list'] = DI()->notorm->qrcode->where('own_id =? ',$user_id)->fetchAll();

                    $tmp_order = $order;
                    $tmp_limit = " ";
                    $tmp_where = " where a.invite_user_id ='$user_id' and expire_date >=now() ";
                    $sql = "SELECT a.id as user_id,a.loginid,a.nick_name,a.approval,a.block,a.invite_note,a.post_date qrpost_date,a.image,a.email_va,b.* FROM ".$table_first_name."teach as a
                            left join ".$table_first_name."qrcode  as b on a.invite_qrcode_id =b.id

                        ".$tmp_where.$tmp_order.$tmp_limit;

                    $rs['qrcode_data'] = $tmp_obj->queryAll($sql);

                    foreach($rs['qrcode_list'] as $key => $value ){
                        foreach($rs['qrcode_data'] as $key1 => $value1 ){
                            if($value['id'] == $value1['id']){
                                $rs['qrcode_list'][$key]['qrcode_data'][] = $value1;
                            }
                        }
                    }
                    $rs['qrcode_data'] =null;

                }
                if(count($rs) >=1){
                    $rs['msg_text'] =T('QRLinkList');
                    $rs['msg_state'] ='Y';
                }else{
                    $rs['msg_text'] ='QRLinkList CodeError';
                    $rs['msg_state'] ='N';
                }
            return $rs;
    }
    
    public function GetSession(){

        $receive_user_id=$_SESSION['f_backend']['user_id']; 
        $tmp_time = $_SESSION['f_backend']['create_at'];
        if($_SESSION['f_backend']['type_id'] >=2 && strtotime("$tmp_time +1 day") <=strtotime('now')){
                $room_list =$this->ZoomApiTw->meeting_get($_SESSION['f_backend']['room_id'],'WYB0SNRoQfSs1qrFgd5jkg');
                    $tmp_room =$this->ZoomApiTw->meeting_create('Demo Room','WYB0SNRoQfSs1qrFgd5jkg');
                    $tmp_room = json_decode(json_encode($tmp_room), True);
                    $_SESSION['f_backend']['teach_room_url']=$tmp_room['result']['data']['start_url'];
                    $_SESSION['f_backend']['create_at']=$tmp_room['result']['data']['create_at'];
                    $_SESSION['f_backend']['room_id']=$tmp_room['result']['data']['id'];
        }

        $tmp_arr['notice'] = $this->Notice->notice($receive_user_id);
        $tmp_arr['sys_notice'] = $this->Notice->sys_invite_notice($receive_user_id);
        $tmp_arr['fan_invite_notice'] = $this->Notice->fan_invite_notice($receive_user_id);
        $tmp_arr['user_friends_notice'] = $this->Notice->user_friends_notice($receive_user_id);

        $replace_arr=array('attached_id');
        $tmp_arr['notice'] =  $this->RePlaceFilename($tmp_arr['notice'],$replace_arr,'send_id');
        $replace_arr=array('image');
        //print_r()
        $tmp_arr['fan_invite_notice'] =  $this->RePlaceFilename($tmp_arr['fan_invite_notice'],$replace_arr,'id');
        $tmp_arr['user_friends_notice'] =  $this->RePlaceFilename($tmp_arr['user_friends_notice'],$replace_arr,'who_user_id');
        return $tmp_arr;
            
    }

}
