<?php
class Model_CronSendMailModel extends Common_ModelDefaultList {

    //建構子
    public function __construct() {  
        parent::__construct(); 

    }
    
    public function __destruct(){

    }
    
    
    public function getTableName($id) {
        return "student";
    }
    
    public function ViewList(){

        $this->notic_teach_minute(5);
        $this->notice_teach(1);
        $this->notic_rate(-25);
        $this->close_meeting_room(30);


        return $tmp_arr;
            
    }

    public function AddwebSubtion(){
        $email = $this->laout_check(DI()->request->get('email'));

        $table_name="website_subtion";  
        $rs = DI()->notorm->$table_name->where('email',$email)->fetchAll();

        if(count($rs) ==0){
        //set query
            $query_arr['approval'] = 'Y';
            $query_arr['id'] = $this->get_uuid();
            $query_arr['email'] = $email;
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query
            $rs = DI()->notorm->$table_name->insert($query_arr);
            $rs =array();
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('subtion');
            $rs['post_time'] =date('Y-m-d H:i:s');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='Y';
            return $rs;
        }else{           
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('dob_subtion');
            $rs['msg_state'] ='Y';
            $rs['post_time'] =date('Y-m-d H:i:s');
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
            }  
        
    }

    //前5分鐘通知
    function notic_teach_minute($minute=5){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $befault_sec = $minute*60+400;

        $sql="select b.type as article_type,d.email as student_email , e.email as teach_email,d.phone as student_phone, e.phone as teach_phone,b.freetalk_article_link,h.en_name as article_category,g.title as article_title,
                g.link as article_link ,g.level as article_level ,
                b.can_tutor_time,c.time_name,d.loginid student_loginid,d.nick_name student_nick_name ,
                e.loginid teach_loginid ,e.nick_name teach_nick_name ,b.id as lesson_id,a.*
           from ".$table_first_name."lesson as a  inner join  ".$table_first_name."lesson_data as b  on 
           a.id=b.lesson_id 
                left join 
                (select b.id as rel_id ,b.name as time_name   from  ".$table_first_name."realtion as a inner join ".$table_first_name."realtion_datas as b on a.id= b.rel_id 
                where a.id ='12'  ) 
                as c on b.can_tutor_time=c.rel_id
                left join ".$table_first_name."teach as d on d.id =b.student_id
                left join ".$table_first_name."teach as e on e.id =a.teach_id
                LEFT JOIN ".$table_first_name."teach_article AS g ON b.article_link =g.id
                LEFT JOIN ".$table_first_name."realtion_datas AS h ON h.id =b.`type`
             where 
                unix_timestamp(now()) < unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))
                and
                unix_timestamp(now())+$befault_sec > unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))   
              and 
            student_cancel !='Y'
            and 
            teach_leave !='Y'        
           ";
           


        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
          //end set
          
          
        if(isset($tmp_arr['tmp_arr']))foreach($tmp_arr['tmp_arr']  as $key => $value){ 



            //send mail send sms
            $host_path =HOST_PATH;
            $subject = T('CronNoticeMinuteSubject');
            $body = T('CronNoticeMinute', array(
                'subject' => $subject ,
                'nick_name' => $value['student_nick_name'] ,
                'host_path' => $host_path
                ));
                if($value['student_email'] !=''){
                        $tmp_email = $value['student_email'];
                }else{
                        $tmp_email = $value['student_loginid'];
                }

            $send_mail = $this->Mail->send($tmp_email, $subject, $body);

            $body = T('CronNoticeMinute', array(
                'subject' => $subject ,
                'nick_name' => $value['teach_nick_name'] ,
                'host_path' => $host_path
                ));

                if($value['teach_email'] !=''){
                        $tmp_email = $value['teach_email'];
                }else{
                        $tmp_email = $value['teach_loginid'];
                }

            $send_mail = $this->Mail->send($tmp_email, $subject, $body);


            $Smsbody = T('SmsCronNoticeMinute', array(
                'subject' => $subject ,
                'nick_name' => $value['student_nick_name'] 
                ));
            $SmsTeachbody = T('SmsCronNoticeMinute', array(
                'subject' => $subject ,
                'nick_name' => $value['teach_nick_name'] 
                ));

            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($query_arr['student_phone'],$Smsbody);
                $sendCodeResult = $this->Sms->sendSMS($query_arr['teach_phone'],$Smsbody);
            }
            //send mail send sms

        }

    }

    function close_meeting_room($minute=30){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $befault_sec = $minute*60+400;
        $tmp_befault_sec = $befault_sec*2;

        $sql="select b.room_id, b.type as article_type,d.email as student_email , e.email as teach_email,d.phone as student_phone, e.phone as teach_phone,b.freetalk_article_link,h.en_name as article_category,g.title as article_title,
                g.link as article_link ,g.level as article_level ,
                b.can_tutor_time,c.time_name,d.loginid student_loginid,d.nick_name student_nick_name ,
                e.loginid teach_loginid ,e.nick_name teach_nick_name ,b.id as lesson_dt_id,b.id_sha1 as code ,a.*
           from ".$table_first_name."lesson as a  inner join  ".$table_first_name."lesson_data as b  on 
           a.id=b.lesson_id 
                left join 
                (select b.id as rel_id ,b.name as time_name   from  ".$table_first_name."realtion as a inner join ".$table_first_name."realtion_datas as b on a.id= b.rel_id 
                where a.id ='12'  ) 
                as c on b.can_tutor_time=c.rel_id
                left join ".$table_first_name."teach as d on d.id =b.student_id
                left join ".$table_first_name."teach as e on e.id =a.teach_id
                LEFT JOIN ".$table_first_name."teach_article AS g ON b.article_link =g.id
                LEFT JOIN ".$table_first_name."realtion_datas AS h ON h.id =b.`type`
             where 
                unix_timestamp(now()) > unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name)) + $befault_sec  
                and
                unix_timestamp(now()) < unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name)) + $tmp_befault_sec  

              and 
            student_cancel !='Y'
            and 
            teach_leave !='Y'        
           ";
           
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
          //end set
          
          
        if(isset($tmp_arr['tmp_arr']))foreach($tmp_arr['tmp_arr']  as $key => $value){ 
                $room_list =$this->ZoomApiTw->meeting_delete($value['room_id']);
            }

            $room_list =$this->ZoomApiTw->meeting_list('WYB0SNRoQfSs1qrFgd5jkg');
            foreach($room_list['result']['data']->meetings as $key => $value){
                if(strtotime("$value->created_at +1 day ") <=strtotime('now'))
                    $this->ZoomApiTw->meeting_just_delete($value->id,'WYB0SNRoQfSs1qrFgd5jkg');
            }
            

    }

    //前一個小時通知
    function notice_teach($hour){
        //自己的上課畫面
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
         $sec = $hour*60*60;
         $sec_limit = $sec+400;

        $sql="select b.type as article_type,d.email as student_email , e.email as teach_email,d.phone as student_phone, e.phone as teach_phone,b.freetalk_article_link,h.en_name as article_category,g.title as article_title,
                g.link as article_link ,g.level as article_level ,
         b.can_tutor_time,c.time_name,d.loginid student_loginid,d.nick_name student_nick_name ,
e.loginid teach_loginid ,e.nick_name teach_nick_name,b.id as lesson_id,a.*
           from ".$table_first_name."lesson as a  inner join  ".$table_first_name."lesson_data as b  on 
           a.id=b.lesson_id 
                left join 
                (select b.id as rel_id ,b.name as time_name   from  ".$table_first_name."realtion as a inner join ".$table_first_name."realtion_datas as b on a.id= b.rel_id 
                where a.id ='12'  ) 
                as c on b.can_tutor_time=c.rel_id
                left join ".$table_first_name."teach as d on d.id =b.student_id
                left join ".$table_first_name."teach as e on e.id =a.teach_id
                LEFT JOIN ".$table_first_name."teach_article AS g ON b.article_link =g.id
                LEFT JOIN ".$table_first_name."realtion_datas AS h ON h.id =b.`type`
                
             where 
                unix_timestamp(now())+$sec < unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))                   
                and
                unix_timestamp(now())+$sec_limit > unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))   
              and 
            student_cancel !='Y'
            and 
            teach_leave !='Y'
           ";

/*test
        $sql="select b.type as article_type,d.email as student_email , e.email as teach_email,d.phone as student_phone, e.phone as teach_phone,b.freetalk_article_link,h.en_name as article_category,g.title as article_title,
                g.link as article_link ,g.level as article_level ,
         b.can_tutor_time,c.time_name,d.loginid student_loginid,d.nick_name student_nick_name ,
e.loginid teach_loginid ,e.nick_name teach_nick_name,b.id as lesson_id,a.*
           from ".$table_first_name."lesson as a  inner join  ".$table_first_name."lesson_data as b  on 
           a.id=b.lesson_id 
                left join 
                (select b.id as rel_id ,b.name as time_name   from  ".$table_first_name."realtion as a inner join ".$table_first_name."realtion_datas as b on a.id= b.rel_id 
                where a.id ='12'  ) 
                as c on b.can_tutor_time=c.rel_id
                left join ".$table_first_name."student as d on d.id =b.student_id
                left join ".$table_first_name."teach as e on e.id =a.teach_id
                LEFT JOIN ".$table_first_name."teach_article AS g ON b.article_link =g.id
                LEFT JOIN ".$table_first_name."realtion_datas AS h ON h.id =b.`type`
                
             where 

            student_cancel !='Y'
            and 
            teach_leave !='Y'
           limit 1 " ;*/

        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
          
          
    if(isset($tmp_arr['tmp_arr']))foreach($tmp_arr['tmp_arr']  as $key => $value){



//send mail send sms
            $host_path =HOST_PATH;
            $subject = T('CronNoticeHourSubject' ,array(
                'hour' => $hour 
                ));
            $body = T('CronNoticeHour', array(
                'subject' => $subject ,
                'hour' => $hour ,
                'nick_name' => $value['student_nick_name'] ,
                'host_path' => $host_path
                ));
                if($value['student_email'] !=''){
                        $tmp_email = $value['student_email'];
                }else{
                        $tmp_email = $value['student_loginid'];
                }

            $send_mail = $this->Mail->send($tmp_email, $subject, $body);

            $body = T('CronNoticeHour', array(
                'subject' => $subject ,
                'nick_name' => $value['teach_nick_name'] ,
                'hour' => $hour ,
                'host_path' => $host_path
                ));

                if($value['teach_email'] !=''){
                        $tmp_email = $value['teach_email'];
                }else{
                        $tmp_email = $value['teach_loginid'];
                }

            $send_mail = $this->Mail->send($tmp_email, $subject, $body);

            $Smsbody = T('SmsCronNoticeMinute', array(
                'subject' => $subject ,
                'hour' => $hour ,
                'nick_name' => $value['student_nick_name'] 
                ));
            $SmsTeachbody = T('SmsCronNoticeMinute', array(
                'subject' => $subject ,
                'hour' => $hour ,
                'nick_name' => $value['teach_nick_name'] 
                ));

            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($query_arr['student_phone'],$Smsbody);
                $sendCodeResult = $this->Sms->sendSMS($query_arr['teach_phone'],$Smsbody);
            }
//send mail send sms

          }
    
    }
//前25分鐘通知
function notic_rate($minute=-25){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $befault_sec = $minute*60-400;

        $sql="select b.type as article_type,d.email as student_email , e.email as teach_email,d.phone as student_phone, e.phone as teach_phone,b.freetalk_article_link,h.en_name as article_category,g.title as article_title,
                g.link as article_link ,g.level as article_level ,
                b.can_tutor_time,c.time_name,d.loginid student_loginid,d.nick_name student_nick_name ,
                e.loginid teach_loginid ,e.nick_name teach_nick_name ,b.id as lesson_dt_id,b.id_sha1 as code ,a.*
           from ".$table_first_name."lesson as a  inner join  ".$table_first_name."lesson_data as b  on 
           a.id=b.lesson_id 
                left join 
                (select b.id as rel_id ,b.name as time_name   from  ".$table_first_name."realtion as a inner join ".$table_first_name."realtion_datas as b on a.id= b.rel_id 
                where a.id ='12'  ) 
                as c on b.can_tutor_time=c.rel_id
                left join ".$table_first_name."teach as d on d.id =b.student_id
                left join ".$table_first_name."teach as e on e.id =a.teach_id
                LEFT JOIN ".$table_first_name."teach_article AS g ON b.article_link =g.id
                LEFT JOIN ".$table_first_name."realtion_datas AS h ON h.id =b.`type`
             where 
                unix_timestamp(now()) < unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))
                and
                unix_timestamp(now())+$befault_sec > unix_timestamp(CONCAT(DATE_FORMAT(lesson_day,'%Y-%m-%d'),' ',time_name))   
              and 
            student_cancel !='Y'
            and 
            teach_leave !='Y'        
           ";
           
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
          //end set
          
          
        if(isset($tmp_arr['tmp_arr']))foreach($tmp_arr['tmp_arr']  as $key => $value){ 



            //send mail send sms
            $host_path =HOST_PATH;
            $subject = T('CronNoticeRateSubject');
            $body = T('CronNoticeRate', array(
                'subject' => $subject ,
                'nick_name' => $value['student_nick_name'] ,
                'code' => $value['code'] ,
                'lesson_dt_id' => $value['lesson_dt_id'] ,
                'host_path' => $host_path
                ));
                if($value['student_email'] !=''){
                        $tmp_email = $value['student_email'];
                }else{
                        $tmp_email = $value['student_loginid'];
                }

            $send_mail = $this->Mail->send($tmp_email, $subject, $body);
            
            $Smsbody = T('CronNoticeRate', array(
                'subject' => $subject ,
                'code' => $value['code'] ,
                'lesson_dt_id' => $value['lesson_dt_id'] ,
                'nick_name' => $value['student_nick_name'],
                'host_path' => $host_path
                ));

                if(isset($this->Sms)){
                    $sendCodeResult = $this->Sms->sendSMS($query_arr['student_phone'],$Smsbody);

                }
            //send mail send sms

            }
        
        
}




}
