<?php

class Model_FanModel extends Common_ModelDefaultList {
    
    //ex 有用到
    public function getByUserId($userId) {
        return $this->getORM()
            ->select('*')
            ->where('id = ?', $userId)
            ->fetch();
    }


    public function getByUserIdWithCache($userId) {
        $key = 'userbaseinfo_' . $userId;
        //要自己寫 原本的沒有宣告 cache
        DI()->cache = new PhalApi_Cache_File(array('path' => API_ROOT . '/Runtime', 'prefix' => 'demo'));
        //
        $rs = DI()->cache->get($key);
        if ($rs === NULL) {
            $rs = $this->getByUserId($userId);
            DI()->cache->set($key, $rs, 600);
        }
        return $rs;
    }
    //end ex
    
    //建構子
    public function __construct() {   
        parent::__construct(); 
		$this->PHPExcel = new PHPExcel_Lite();
    }
    
    public function __destruct(){

    }
    
    
    public function getTableName($id) {
        return "student";
    }
    
    public function FanText($query_arr){
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get
       
       //左邊列
       $tmp_arr = $this->FanLeftMenu();

            //fan fan manage
            $sql = "select a.id,a.fuzzy_name,b.`type`,c.loginid,c.id as board_man_id  from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where a.id='$fan_id'
                    and c.id ='$board_man_id' 
                    and  ( b.type ='admin'  || b.`type`='board' ) ";
                    
            $tmp_arr['fan_boardmanage_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_boardmanage_list_count'] = count($tmp_arr['fan_boardmanage_list']); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_boardmanage_list'] =  $this->RePlaceFilename($tmp_arr['fan_boardmanage_list'],$replace_arr);
            //end fan manage
            
    
        return $tmp_arr;
    }
    
    
    public function MyFanList($query_arr){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get

            // myfanlist 粉絲團清單
            $sql = "select a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as user_id  from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where c.id='$board_man_id'
                    and  ( b.type !='admin' )  and b.approval !='N'
                    ";

            $tmp_arr['fan_myfanlist'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_myfanlist'] =  $this->RePlaceFilename($tmp_arr['fan_myfanlist'],$replace_arr);
            // myfanlist
            
            //fan fan manage
            $sql = "select a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as board_man_id,d.count  from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    left join (
								select a.id,count(*) as count  from plf_fan as a 
								inner join plf_fan_manage as b on b.fan_id =a.id 
                    		inner join  plf_teach as c on c.id =b.user_id
                    		where b.approval='A' group by a.id
						  ) as d on d.id =a.id
                    where c.id ='$board_man_id' 
                    and  ( b.type ='admin' ) ";
            $tmp_arr['fan_manage_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_manage_list'] =  $this->RePlaceFilename($tmp_arr['fan_manage_list'],$replace_arr);
            //end fan manage
            
    
        return $tmp_arr;
    }
    
    
    public function FanMangeMember($query_arr){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get
       //左邊列
       $tmp_arr = $this->FanLeftMenu();


            //fan_user_list 成員清單 
            $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                    FROM ".$table_first_name."fan AS a
                    left JOIN ".$table_first_name."fan_manage AS b ON b.fan_id =a.id
                    left JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                    WHERE   a.id ='$fan_id' and b.approval !='N'
                        order 
                        by b.o_type='admin' desc, b.type ='admin' desc ,b.type ='board' desc , 
                        b.type ='staff' desc ,b.type ='teach' desc 
                        ,b.type ='nomal' desc  
                    ";
            $tmp_arr['fan_user_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_user_list'] =  $this->RePlaceFilename($tmp_arr['fan_user_list'],$replace_arr,'user_id');
            // fan_user_list
 
            
            //fan fan manage
            $sql = "select a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as board_man_id from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where c.id ='$board_man_id' and  a.id ='$fan_id'
                    and  ( b.type ='admin' ) ";
            $tmp_arr['fan_manage_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_manage_list'] =  $this->RePlaceFilename($tmp_arr['fan_manage_list'],$replace_arr);
            //end fan manage
            
        return $tmp_arr;
    }
    
    
    
    public function FanMange($query_arr){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get
       //左邊列
       $tmp_arr = $this->FanLeftMenu();


            //fan_user_list 成員清單 
            $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                    FROM ".$table_first_name."fan AS a
                    left JOIN ".$table_first_name."fan_manage AS b ON b.fan_id =a.id
                    left JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                    WHERE   a.id ='$fan_id' and b.approval !='N' and b.approval !='A'
                        order 
                        by b.o_type='admin' desc , b.type ='admin' desc ,b.type ='board' desc , 
                        b.type ='staff' desc ,b.type ='teach' desc 
                        ,b.type ='nomal' desc 
                    ";
            $tmp_arr['fan_user_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_user_list'] =  $this->RePlaceFilename($tmp_arr['fan_user_list'],$replace_arr,'user_id');
            // fan_user_list
            
            //fan_wait_list 成員清單 
            
            $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                    FROM ".$table_first_name."fan AS a
                    left JOIN ".$table_first_name."fan_manage AS b ON b.fan_id =a.id
                    left JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                    WHERE   a.id ='$fan_id' and  b.approval ='A'
                        order 
                        by b.type ='admin' desc ,b.type ='board' desc , 
                        b.type ='staff' desc ,b.type ='teach' desc 
                        ,b.type ='nomal' desc 
                    ";
            $tmp_arr['fan_wait_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_wait_list_count'] = count($tmp_arr['fan_wait_list']); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_wait_list'] =  $this->RePlaceFilename($tmp_arr['fan_wait_list'],$replace_arr,'user_id');
            // fan_wait_list
 
            
            //fan fan manage
            $sql = "select a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as board_man_id from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where c.id ='$board_man_id' and  a.id ='$fan_id'
                    and  ( b.type ='admin' ) ";
            $tmp_arr['fan_manage_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_manage_list'] =  $this->RePlaceFilename($tmp_arr['fan_manage_list'],$replace_arr);
            //end fan manage
            
        return $tmp_arr;
    }

    public function FanReport($query_arr){

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        //$fan_id = '33e341aa-3225-11e7-bfc8-74d435c7f214';
       //end get
       //左邊列
       $tmp_arr = $this->FanLeftMenu();

            //fan board view article
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id
                and
                (
                UNIX_TIMESTAMP(b.post_date) > UNIX_TIMESTAMP(now())-(86400*:day_count)
                )
            group by b.id order by count desc
            limit 50";

            $params = array(':user_id' => $fan_id,
            ':day_count' => 30 );
            $tmp_arr['fan_view']['article']['day30'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['article']['day30'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['article']['day30'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 7 );
            $tmp_arr['fan_view']['article']['day7'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['article']['day7'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['article']['day7'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 3 );
            $tmp_arr['fan_view']['article']['day3'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['article']['day3'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['article']['day3'],$replace_arr,'post_by');
            //end fan board view article 

            //fan board view video 
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id and  b.in_cloud='Y' and isnull(b.live_id) 
                and
                (
                UNIX_TIMESTAMP(b.post_date) > UNIX_TIMESTAMP(now())-(86400*:day_count)
                )
            group by b.id order by count desc
            limit 50";

            $params = array(':user_id' => $fan_id,
            ':day_count' => 30 );
            $tmp_arr['fan_view']['video']['day30'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['video']['day30'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['video']['day30'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 7 );
            $tmp_arr['fan_view']['video']['day7'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['video']['day7'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['video']['day7'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 3 );
            $tmp_arr['fan_view']['video']['day3'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['video']['day3'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['video']['day3'],$replace_arr,'post_by');
            //end fan board view video 


            //fan board view live 
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id and  b.in_cloud='Y' and !isnull(b.live_id) 
                and
                (
                UNIX_TIMESTAMP(b.post_date) > UNIX_TIMESTAMP(now())-(86400*:day_count)
                )
            group by b.id order by count desc
            limit 50";

            $params = array(':user_id' => $fan_id,
            ':day_count' => 30 );
            $tmp_arr['fan_view']['live']['day30'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['live']['day30'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['live']['day30'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 7 );
            $tmp_arr['fan_view']['live']['day7'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['live']['day7'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['live']['day7'],$replace_arr,'post_by');

            $params = array(':user_id' => $fan_id,
            ':day_count' => 3 );
            $tmp_arr['fan_view']['live']['day3'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['live']['day3'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['live']['day3'],$replace_arr,'post_by');
            //end fan board view live 



            //fan board image
            $sql = "select count(*) as count,c.id board_id, c.text as board_text, b.name attached_name,b.created_at post_date,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."files as b on b.id = a.own_id
            left join plf_board as c on  ( CONCAT(',',c.attached_id,',') like CONCAT('%',b.id,'%')  )
            where  a.own ='files' and  a.post_by =:user_id and b.`type`='1'
                and
                (
                UNIX_TIMESTAMP(b.created_at) > UNIX_TIMESTAMP(now())-(86400*:day_count)
                )
            group by b.id order by count desc
            limit 50";

            $params = array(':user_id' => $fan_id,
            ':day_count' => 30 );
            $tmp_arr['fan_view']['image']['day30'] = $tmp_obj->queryAll($sql,$params); 


            $params = array(':user_id' => $fan_id,
            ':day_count' => 7 );
            $tmp_arr['fan_view']['image']['day7'] = $tmp_obj->queryAll($sql,$params); 


            $params = array(':user_id' => $fan_id,
            ':day_count' => 3 );
            $tmp_arr['fan_view']['image']['day3'] = $tmp_obj->queryAll($sql,$params); 
             //end fan board image




            //fan board view allarticle
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id
            group by b.id order by b.post_date desc
            ";

            $params = array(':user_id' => $fan_id
             );
            $tmp_arr['fan_view']['article']['allday'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['article']['allday'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['article']['allday'],$replace_arr,'post_by');
            
            //fan board view allvideo 
            
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id and  b.in_cloud='Y' and isnull(b.live_id) 

            group by b.id order by b.post_date desc
            ";

            $params = array(':user_id' => $fan_id
             );
            $tmp_arr['fan_view']['video']['allday'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['video']['allday'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['video']['allday'],$replace_arr,'post_by');
            
            //end fan board view allvideo 


            //fan board view alllive 
            
            $sql = "select count(*) as count ,b.text,b.attached_id,b.attached_id attached_name,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."board as b on b.id = a.own_id
            where  a.own ='board' and  a.post_by =:user_id and  b.in_cloud='Y' and !isnull(b.live_id) 

            group by b.id order by b.post_date desc
            ";

            $params = array(':user_id' => $fan_id
             );
            $tmp_arr['fan_view']['live']['allday'] = $tmp_obj->queryAll($sql,$params); 
            $replace_arr=array('attached_name');
            $tmp_arr['fan_view']['live']['allday'] =  $this->RePlaceMutileFilename($tmp_arr['fan_view']['live']['allday'],$replace_arr,'post_by');
            
            //end fan board view alllive 



            //fan board image
            
            $sql = "select count(*) as count,c.id board_id, c.text as board_text, b.name attached_name,b.created_at post_date,b.id,b.`type`,a.* from  ".$table_first_name."view_count as a 
            inner join ".$table_first_name."files as b on b.id = a.own_id
            left join plf_board as c on  ( CONCAT(',',c.attached_id,',') like CONCAT('%',b.id,'%')  )
            where  a.own ='files' and  a.post_by =:user_id and b.`type`='1'

            group by b.id order by b.created_at desc
            ";

            $params = array(':user_id' => $fan_id
             );
            $tmp_arr['fan_view']['image']['allday'] = $tmp_obj->queryAll($sql,$params); 
            
             //end fan board image


            //fan teacher booking list
            
            $sql = "select count(*) as count,e.fan_id ,a.* from ".$table_first_name."teach as a
            left join ".$table_first_name."lesson as b on a.id =b.teach_id
            left join 
            (
				select c.id as fan_id,d.user_id from ".$table_first_name."fan as c 
					left join ".$table_first_name."fan_manage as d on c.id = d.fan_id
					where 
					c.id ='$fan_id'
					and ( d.`type`='admin'  or d.`type` ='staff' )

				) as e on e.user_id =a.id

            where fan_id =:user_id
            group by a.id order by count desc ";
            //exit;



            $params = array(':user_id' => $fan_id
             );
            $tmp_arr['fan_view']['booking']['allday'] = $tmp_obj->queryAll($sql,$params); 
            
             //end fan teacher booking list
             



            //fan_user_list 成員清單 
            $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                    FROM ".$table_first_name."fan AS a
                    left JOIN ".$table_first_name."fan_manage AS b ON b.fan_id =a.id
                    left JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                    WHERE   a.id ='$fan_id' and b.approval !='N' and b.approval !='A'
                        order 
                        by b.o_type='admin' desc , b.type ='admin' desc ,b.type ='board' desc , 
                        b.type ='staff' desc ,b.type ='teach' desc 
                        ,b.type ='nomal' desc 
                    ";
            $tmp_arr['fan_user_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_user_list'] =  $this->RePlaceFilename($tmp_arr['fan_user_list'],$replace_arr,'user_id');
            // fan_user_list
 
            //fan fan manage
            $sql = "select a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as board_man_id from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where c.id ='$board_man_id' and  a.id ='$fan_id'
                    and  ( b.type ='admin' ) ";
            $tmp_arr['fan_manage_list'] = $tmp_obj->queryAll($sql); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_manage_list'] =  $this->RePlaceFilename($tmp_arr['fan_manage_list'],$replace_arr);
            //end fan manage
            
        return $tmp_arr;
    }
    
    public function ViewList($query_arr){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
       //end get

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        
           if($_REQUEST['orderby'] == null)
                $tmp_orderby = 'count';
            else
                $order = $this->laout_check(DI()->request->get('order'));

            if($order =='count')    
                $order = ' order by lesson_count desc ';
            elseif($order =='exp')
                $order = ' order by exp asc '; 
            else
                $order = ' order by a.type_id asc, a.id asc ';    
                    
                
        $tmp_order = $order;
        $tmp_limit = " ";
        $sql = "select * from ".$table_first_name."fan as a where approval ='Y'
            ".$tmp_where;
        $tmp_arr['tmp_fan_arr'] = $tmp_obj->queryAll($sql);

        //type 
        /*   
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 14)->fetchAll();
        */       
        //end type

        $replace_arr=array('image','bg_image');
        $tmp_arr['tmp_fan_arr'] =  $this->RePlaceFilename($tmp_arr['tmp_fan_arr'],$replace_arr);
        $tmp_arr['tmp_fan_arr'] = $this->Takeout_Password($tmp_arr['tmp_fan_arr']);
        
        return $tmp_arr;
    }
    
    public function read_notice(){
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;        
        //get todo login
        $receive_user_id=$_SESSION['f_backend']['user_id'];         
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $teach_id = $this->laout_check(DI()->request->get('teach_id'));
       //end get
       //粉絲版一班user 通用撰寫
       if($fan_id =='' && $teach_id ==''){
           return;
       }elseif($fan_id ==''){
            $send_id = $teach_id;
           
       }else{
            $send_id = $fan_id;
       }
       
        //已讀通知 一次就把哪一個大類變成已讀而不只一次只能已讀一篇文章
        $tmp_arr = $this->Notice->read_notice($receive_user_id,$send_id,'Y','board');
        return $tmp_arr;
    }
    
    public function FanLeftMenu(){
        
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $board_man_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get   

            //左邊列
            
            //fan 粉絲團
            $sql = "select a.*  from plf_fan as a 
                    where a.id='$fan_id'";

            $tmp_arr['fan_tmp_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_tmp_list_count'] = count($tmp_arr['fan_tmp_list']); 
              
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_tmp_list'] =  $this->RePlaceFilename($tmp_arr['fan_tmp_list'],$replace_arr);
            //end fan
            
            // fan staff list 可以預約的人清單
            $sql = "select a.id,a.fuzzy_name,b.`type`,c.loginid,c.id as user_id , c.image,c.nick_name  from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where a.id='$fan_id'
                    and b.type ='staff' and b.approval ='Y' ";
            $tmp_arr['fan_staff_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_staff_list_count'] = count($tmp_arr['fan_staff_list']); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_staff_list'] =  $this->RePlaceFilename($tmp_arr['fan_staff_list'],$replace_arr,'user_id');
            // fan staff  list
            
            // fan man list 成員清單
            $sql = "select a.id,a.fuzzy_name,b.`type`,c.loginid,c.id as user_id , c.image,c.nick_name  from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where a.id='$fan_id' and b.approval ='Y'
                        order 
                        by b.o_type='admin' desc, b.type ='admin' desc ,b.type ='board' desc , 
                        b.type ='staff' desc ,b.type ='teach' desc 
                        ,b.type ='nomal' desc 
                    ";
                 
            $tmp_arr['fan_man_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_man_list_count'] = count($tmp_arr['fan_man_list']); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_man_list'] =  $this->RePlaceFilename($tmp_arr['fan_man_list'],$replace_arr,'user_id');
            // fan man  list
            
            
            //fan I in Fan check
            $sql = "select b.approval,a.id,a.fuzzy_name,a.nick_name,a.image,b.`type`,c.loginid,c.id as board_man_id from ".$table_first_name."fan as a 
                    inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                    inner join  ".$table_first_name."teach as c on c.id =b.user_id
                    where c.id ='$board_man_id' and  a.id ='$fan_id' and b.approval !='N' ";
            $tmp_arr['fan_Iin_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_Iin_list_count'] = count($tmp_arr['fan_Iin_list']); 
            

            //end I in Fan check
            
            //fan Subscript 關注這個粉絲團的人
            $sql = "select a.*,b.email,b.image  from ".$table_first_name."user_friends as a 
                    inner join  ".$table_first_name."teach as b on b.id =a.user_id 
                    inner join  ".$table_first_name."fan as c on c.id =a.who_user_id
                    where who_user_id ='$fan_id' and type=2 and a.approval ='Y'";
                    
            $tmp_arr['fan_subscript_list'] = $tmp_obj->queryAll($sql); 
            $tmp_arr['fan_subscript_list_count'] = count($tmp_arr['fan_subscript_list']); 
            $replace_arr=array('image','bg_image');
            $tmp_arr['fan_subscript_list'] =  $this->RePlaceFilename($tmp_arr['fan_subscript_list'],$replace_arr,'user_id');
            //end fan Subscript 關注這個粉絲團的人
            return $tmp_arr;
            

    }


    public function AddFanMember($query_arr){
        //get 
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $user_id=$_SESSION['f_backend']['user_id'];         
        $loginid = $_SESSION['f_backend']['loginid'];
       //end get

        //set query
            $query_arr['approval'] = 'A';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('user_id',$user_id)->where('fan_id',$fan_id)->fetchAll();

        if(count($rs) ==0){
        //set query
            $query_arr['approval'] = 'A';
            $query_arr['user_id'] = $user_id;
            $query_arr['fan_id'] = $fan_id;
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query
            $rs = DI()->notorm->$table_name->insert($query_arr);
        }else{           
            $rs = DI()->notorm->$table_name->where('user_id',$user_id)->where('fan_id',$fan_id)->update($query_arr);
        }
        
        //$rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('AddFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }        
    }

    public function AgreeFanMember($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get
       
        //確定權限
        
        $fan_id =$fan_id;
        $check_type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$check_type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('AgreeFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        
        //end 確定權限
       

        //set query
            $query_arr['approval'] = 'Y';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('AgreeFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }        
    }

    public function AgreeInvFan($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get

        //set query
            $query_arr['approval'] = 'Y';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('id',$id)->where('user_id',$user_id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('AgreeInvFan');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }        
    }



    public function RefuseFanMember($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get


        //set query
            $query_arr['approval'] = 'N';
            $query_arr['type'] = 'Nomal';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 

        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('id',$id)->where('user_id',$user_id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('DeleteFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }   
                
    }

    public function DeleteFanMember($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get


        //確定權限
        
        $fan_id =$fan_id;
        $check_type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$check_type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('DeleteFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        
        //end 確定權限


        //set query
            $query_arr['approval'] = 'N';
            $query_arr['type'] = 'Nomal';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        
        
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('DeleteFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }   
                
    }
    
    

    public function CreateFan($query_arr){
        //get 
            $user_id=$_SESSION['f_backend']['user_id'];         
            $type_id=$_SESSION['f_backend']['type_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get
        if($type_id <2){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('CreateFan');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;   
        }
        
        //set query
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['approval'] = 'N';
        //end query 



        $table_name="fan";  
        $rs = DI()->notorm->$table_name->insert($query_arr);
        $rs = DI()->notorm->$table_name->order('post_date desc')->limit(1)->fetchAll();
        $fan_id = $rs['0']['id'];
        //set query
            $query_arr =null;
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['approval'] = 'Y';
            $query_arr['fan_id'] = $fan_id;
            $query_arr['user_id'] = $user_id;
            $query_arr['type'] = 'admin';
            $query_arr['o_type'] = 'admin';
        //end query 
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->insert($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('CreateFan');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }   
                
    }


    public function ChangeFanMemberType($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $type = $this->laout_check(DI()->request->get('type'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get
       
        //確定權限
        $fan_id =$fan_id;
        $check_type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$check_type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('ChangeFanMemberType');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
       

        //set query
            $query_arr['type'] = $type;
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        
        
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('ChangeFanMemberType');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }   
                
    }
    
    public function ChangeFanApproval($query_arr){
        //get 
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $user_id=$_SESSION['f_backend']['user_id'];         
            $loginid = $_SESSION['f_backend']['loginid'];
       //end get
        //確定權限
        $fan_id =$fan_id;
        $check_type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$check_type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('ChangeFanApproval');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
       

        //set query
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        

        $table_name="fan";  
        $rs = DI()->notorm->$table_name->where('id',$fan_id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }
    
    public function DeleteFan($query_arr){
        
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
       //end get

       //upload
            $field_name = 'BoardChangeRead:user_id:'.$user_id;
            if($_FILES["file"]["size"] !=0)
            $file_id = $this->FileUpload->fileUpload($_FILES["file"],$field_name);
       //end upload
       
        //set query
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        

        $table_name="board";  
        $rs = DI()->notorm->$table_name->where('id',$id)->delete($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }
    
    
    public function InviteFanMember($query_arr){
        

        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $email = $this->laout_check(DI()->request->get('email'));
        $user_id=$_SESSION['f_backend']['user_id'];         
        $loginid = $_SESSION['f_backend']['loginid'];
       //end get

        //get 
        //set query
            $query_arr['approval'] = 'I';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query
        
        //確定權限
        $type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('InviteFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
         
        
        //確認帳號是否存在
        $table_name="teach";  
        $rs = DI()->notorm->$table_name->where('email',$email)->fetchAll();
        if(count($rs) ==0){
            $rs['sql_state'] ='No Match';
            $rs['msg_text'] =T('No Match');
            $rs['msg_state'] ='N';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;     
        }
        $inv_user_id =$rs['0']['id'];
        
        
        $table_name="fan_manage";  
        $rs = DI()->notorm->$table_name->where('user_id',$inv_user_id)->where('fan_id',$fan_id)->fetchAll();
        
        if(count($rs) ==0){
        //set query
            $query_arr['id'] = $this->get_uuid();
            $query_arr['approval'] = 'I';
            $query_arr['user_id'] = $inv_user_id;
            $query_arr['fan_id'] = $fan_id;
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query
        
            $rs = DI()->notorm->$table_name->insert($query_arr);
                $tmp_obj = DI()->notorm->fan;
                $table_first_name = DI()->TABLE_FIRST_NAME;
                $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                        FROM ".$table_first_name."fan AS a
                        LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                        LEFT JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                        WHERE 
                        b.user_id =:user_id 
                        AND b.`fan_id`=:fan_id 
                        ";
                $params = array(':user_id' => $inv_user_id ,':fan_id' => $fan_id);
                $rs = $tmp_obj->queryAll($sql,$params);
                $replace_arr=array('image');
                $rs =  $this->RePlaceFilename($rs,$replace_arr,'user_id');
                
                $rs['sql_state'] ='Success';
                $rs['msg_text'] =T('InviteFanMember');
                $rs['msg_state'] ='Y';
                $rs['update_time'] =date('Y-m-d H:i:s');
                return $rs;
        }else{           
            $rs = DI()->notorm->$table_name->where('user_id',$inv_user_id)->where('fan_id',$fan_id)->where('approval','N')->update($query_arr);
            if($rs ==1){
                $tmp_obj = DI()->notorm->fan;
                $table_first_name = DI()->TABLE_FIRST_NAME;
                $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                        FROM ".$table_first_name."fan AS a
                        LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                        LEFT JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                        WHERE 
                        b.user_id =:user_id 
                        AND b.`fan_id`=:fan_id 
                        ";
                $params = array(':user_id' => $inv_user_id ,':fan_id' => $fan_id);
                $rs = $tmp_obj->queryAll($sql,$params);
                $replace_arr=array('image');
                $rs =  $this->RePlaceFilename($rs,$replace_arr,'user_id');
                
                $rs['sql_state'] ='Success';
                $rs['msg_text'] =T('InviteFanMember');
                $rs['msg_state'] ='Y';
                $rs['update_time'] =date('Y-m-d H:i:s');
                return $rs;
            }else{
                $rs['sql_state'] ='No Match';
                $rs['msg_text'] =T('No Match');
                $rs['msg_state'] ='N';
                $rs['update_time'] =date('Y-m-d H:i:s');
                return $rs;     
            }                       
        }


          
                
    }


    public function InviteMultiFanMember($query_arr){
		 $excel_arr =$this->PHPExcel->importExcel($_FILES['file_excel']['tmp_name']);
         
		 //echo "<pre>";
		 //print_r($excel_arr);
		 //echo "</pre>";
		 //exit;



         foreach($excel_arr as $key =>$value){
                $host_path =HOST_PATH;
                $query_arr =null;
                $query_arr['id']  = $this->get_uuid();
                $query_arr['loginid']  = $value['email'];
                $query_arr['nick_name']  = $value['email'];
                $query_arr['approval']  = 'N';
                $query_arr['email']  = $value['email'];
                $query_arr['phone'] = str_replace("\"","",$value['phone']);
                //$query_arr['forget_code']=mt_rand(1000, 9999);
                $query_arr['email_code']=mt_rand(1000, 9999);
                $query_arr['phone_code']=mt_rand(1000, 9999);
                $query_arr['forget_code']=mt_rand(1000, 9999);
                $tmp_pwd  = mt_rand(1000, 9999);
                $query_arr['loginpwd']  = md5($tmp_pwd);

                $rs = DI()->notorm->teach->where('email =? || phone =?',$query_arr['email'],$query_arr['phone'])->fetchAll();
                if(count($rs) ==0){
                    
                    $rs = DI()->notorm->teach->insert($query_arr);
                    //寄邀請信+重設密碼信
                    
                    //todo sendmail
                    $send_mail = $this->Mail->send(
                            $query_arr['email'], 
                        T('AccountVA').$query_arr['email_code'], 
                        T('AccountVANote',
                        array(
                            'loginid' => $query_arr['loginid'] ,
                            'loginpwd' => $tmp_pwd ,
                            'host_path' => $host_path ,
                            'user_id' => $query_arr['id'] ,
                            'email_code' => $query_arr['email_code']
                            )
                        )
                    );
                    //end todo sendmail    
                    if(isset($this->Sms)){
                        $sendCodeResult = $this->Sms->sendSMS($query_arr['phone'],
                        T('AccountVANote',
                                array(
                                    'loginid' => $query_arr['loginid'] ,
                                    'loginpwd' => $tmp_pwd ,
                                    'host_path' => $host_path ,
                                    'user_id' => $query_arr['id'] ,
                                    'email_code' => $query_arr['email_code']
                                    )
                                )
                        );
                    }
                    if($tmp_msg1 == null)
                    $tmp_msg1 =$tmp_msg1.'Successfully Import List :'.'<br><br>';
                    
                    $tmp_msg1 =$tmp_msg1.'email:'.$query_arr['email'].'<br>';

                }else{
                    if($tmp_msg ==null)
                    $tmp_msg ="<br>".$tmp_msg.'Error Import List :'.'<br><br>';

                    if($rs[0]['loginid'] == $query_arr['email'] || $rs[0]['email'] == $query_arr['email']  ){
                        $tmp_msg =$tmp_msg.'email:'.$query_arr['email'].' repeat'.'<br>';
                        
                    }
                    if( $rs[0]['phone'] == $query_arr['phone']  ){
                        $tmp_msg =$tmp_msg.'phone:'.$query_arr['phone'].' repeat'.'<br>';
                    }

                }
            
         }
        $tmp_msg = $tmp_msg.$tmp_msg1;
        
        $send_mail = $this->Mail->send(
            $_SESSION['f_backend']['email'], 
            'Import Excel repeat', 
            $tmp_msg
        );
            

            
            
        //echo $tmp_msg;
        //exit;
        //邀請

        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $user_id=$_SESSION['f_backend']['user_id'];         
        $loginid = $_SESSION['f_backend']['loginid'];
       //end get

        //get 
        //set query
            $query_arr =null;
            $query_arr['approval'] = 'I';
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query
        
        //確定權限
        $type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('InviteMultiFanMember');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
        
         
         // 批次加入邀請
         /*
         foreach($excel_arr as $key =>$value){

             }
             exit;
             */
         foreach($excel_arr as $key =>$value){
             //echo $value['email'];
             //exit;
                //確認帳號是否存在
                $table_name="teach";  
                $rs = DI()->notorm->$table_name->where('email',$value['email'])->fetchAll();
                if(count($rs) ==0){
                    $rs['sql_state'] ='No Match';
                    $rs['msg_text'] =T('No Match');
                    $rs['msg_state'] ='N';
                    $rs['update_time'] =date('Y-m-d H:i:s');
                    continue;
                    //return $rs;     
                }
                
                $inv_user_id =$rs['0']['id'];

                $table_name="fan_manage";  
                $rs = DI()->notorm->$table_name->where('user_id',$inv_user_id)->where('fan_id',$fan_id)->fetchAll();
                
                if(count($rs) ==0){
                //set query
                    $query_arr =null;
                    $query_arr['id'] = $this->get_uuid();
                    $query_arr['approval'] = 'I';

                    $query_arr['user_id'] = $inv_user_id;
                    $query_arr['fan_id'] = $fan_id;
                    $query_arr['post_date'] = date('Y-m-d H:i:s');
                    $query_arr['update_date'] = date('Y-m-d H:i:s');
                //end query
                
                    $rs = DI()->notorm->$table_name->insert($query_arr);
                        $tmp_obj = DI()->notorm->fan;
                        $table_first_name = DI()->TABLE_FIRST_NAME;
                        $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                                FROM ".$table_first_name."fan AS a
                                LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                                LEFT JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                                WHERE 
                                b.user_id =:user_id 
                                AND b.`fan_id`=:fan_id 
                                ";
                        $params = array(':user_id' => $inv_user_id ,':fan_id' => $fan_id);
                        $rs = $tmp_obj->queryAll($sql,$params);
                        $replace_arr=array('image');
                        $tmp_rs[] =  $this->RePlaceFilename($rs,$replace_arr,'user_id');
                        

                }else{           
                    $rs = DI()->notorm->$table_name->where('user_id',$inv_user_id)->where('fan_id',$fan_id)->where('approval','N')->update($query_arr);
                    if($rs ==1){
                        $tmp_obj = DI()->notorm->fan;
                        $table_first_name = DI()->TABLE_FIRST_NAME;
                        $sql = "SELECT a.id,b.id AS mang_id,b.user_id,c.nick_name,c.image,b.`type`,b.`o_type`,b.post_date,b.approval
                                FROM ".$table_first_name."fan AS a
                                LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                                LEFT JOIN ".$table_first_name."teach AS c ON c.id =b.user_id
                                WHERE 
                                b.user_id =:user_id 
                                AND b.`fan_id`=:fan_id 
                                ";
                        $params = array(':user_id' => $inv_user_id ,':fan_id' => $fan_id);
                        $rs = $tmp_obj->queryAll($sql,$params);
                        $replace_arr=array('image');
                        $tmp_rs[] =  $this->RePlaceFilename($rs,$replace_arr,'user_id');

                    }else{
                        ;
                    }                       
                }

            }
                
                $rs['data'] =$tmp_rs;
                $rs['sql_state'] ='Success';
                $rs['msg_text'] =$tmp_msg;
                $rs['msg_state'] ='Y';
                $rs['update_time'] =date('Y-m-d H:i:s');
                return $rs;
            //end 批次加入邀請
               
    }
    
    public function FanEditUP($query_arr){
        //get 
        $id = $this->laout_check(DI()->request->get('id'));
        $text = $this->laout_check(DI()->request->get('text'));
        $user_id=$_SESSION['f_backend']['user_id'];         
        $loginid = $_SESSION['f_backend']['loginid'];
       //end get

        //set query
            $query_arr['text'] = $text;
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        //確定權限
        $fan_id =$id;
        $type ='admin';
        $Permi=$this->FanCheckPermi($user_id,$type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('FanEditUP');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
        
        $table_name="fan";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('FanEditUP');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }   

         
    }
    
    
    public function FanCheckPermi($user_id,$type,$fan_id){
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "SELECT *
                FROM ".$table_first_name."fan AS a
                LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                WHERE 
                b.user_id =:user_id 
                AND b.`type`=:type 
                AND b.fan_id =:fan_id";
        $params = array(':user_id' => $user_id ,':type' => $type,':fan_id' => $fan_id );
        
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
         return count($tmp_arr);
                
    }
    
    
    
    
}
