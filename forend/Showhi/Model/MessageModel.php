<?php

class Model_MessageModel extends Common_ModelDefaultList
{

    //建構子
    public function __construct()
    {
        parent::__construct();

    }

    public function __destruct()
    {

    }


    public function getTableName($id)
    {
        return "message_members";
    }

    public function readMessageList()
    {
        //1.抓取這個User下有多少群組，群組成員有誰，最後對話時間，最後是誰發話，最後發話內容是什麼
        $user_id = $_SESSION['f_backend']['user_id'];
        $tmp_obj = $this->getORM();
        $table_first_name = 'plf_message_members';
        $tmp_where = "WHERE {$table_first_name}.user_id = :user_id ";
        $tmp_group = "GROUP BY b.groups_id  ";
        $tmp_order = "ORDER BY b.created_at DESC ";
        $sql = "select plf_message_groups.id as group_id , plf_message_groups.name as group_name , b.created_at as last_message_time , b.message , b.path , b.original_name , plf_teach.nick_name as last_messager 
        ,plf_message_members.updated_at , if(UNIX_TIMESTAMP(plf_message_members.updated_at) < UNIX_TIMESTAMP(b.created_at),1,0 ) room_Noread_number " .

            "from {$table_first_name} " .
            "LEFT JOIN " .
            "plf_message_groups ON {$table_first_name}.groups_id = plf_message_groups.id " .
            "LEFT JOIN " .
            "(SELECT " .
            "c.*, d.path, d.original_name " .
            "FROM " .
            "plf_messages c " .
            "LEFT JOIN ".
            "plf_files d ON c.files_id = d.id ".
            "ORDER BY c.created_at DESC
            ) b ON b.groups_id = plf_message_groups.id " .
            "LEFT JOIN " .
            "plf_teach ON b.created_by = plf_teach.id "
            . $tmp_where . $tmp_group . $tmp_order;

        $params = array(
            ':user_id' => $user_id
        );

        return $tmp_obj->queryAll($sql, $params);

    }

    public function readMessageByGroupId($group_id)
    {
        $tmp_obj = $this->getORM('plf_messages');
        $table_first_name = 'plf_messages';
        $tmp_where = "WHERE {$table_first_name}.groups_id = :groups_id ";
        $tmp_order = "ORDER BY {$table_first_name}.created_at desc ";
        $sql = "select plf_messages.*, plf_teach.nick_name , plf_files.path,plf_files.type, plf_files.original_name,plf_files.cloud_id,plf_files.cloud_id,cloud_down_time " .
            "from {$table_first_name} " .
            "LEFT JOIN " .
            "plf_teach ON {$table_first_name}.created_by = plf_teach.id " .
            "LEFT JOIN ".
            "plf_files ON {$table_first_name}.files_id = plf_files.id ".
            $tmp_where . $tmp_order ." limit 15 ";


        $params = array(
            ':groups_id' => $group_id
        );

        $messageList = $tmp_obj->queryAll($sql, $params);
        $i=count($messageList)-1;

        foreach($messageList as $key =>$value){

            if($value['cloud_id'] !=null && $value['cloud_down_time'] !=null ){
                    $cloud_down_time = strtotime($value['cloud_down_time']);
                    $now_time = strtotime("now");  
                    if($cloud_down_time > $now_time){
                        $messageList[$key]['cloud_id'] =null;
                    }else{
                        $messageList[$key]['cloud_id'] =null;
                    }

            }

                //$value[$key]['created_at'].'<br>';
                $tmp_mesg[$i-$key] = $messageList[$key]; 
        }

        $messageList = $tmp_mesg;
        ksort($messageList);
        //print_r($messageList); 
        //exit;

        

        foreach( $messageList as $key => $value ) {
            $match = $this->Linkinfo->checkurl($value['message']);
            if($match !=null){
                
                $rs = $this->Linkinfo->getLinkinfo($match[0]);    
                $messageList[$key]['web_title'] = $rs['title'];
                $messageList[$key]['web_desc']  = $rs['description'];
                $messageList[$key]['web_image'] = $rs['image'];
                $messageList[$key]['web_url'] = $rs['web_url'];
                $tmp="<a target='_blank' href=".$rs['web_url']."> <div class='append_image'>".
                    "<div><img src='".$rs['image']."' /></div>".
                    "<div>".$rs['title']."</div>".
                    "<div>".$rs['description']."</div>".   
                "</div></a>";
                $messageList[$key]['message'] =$messageList[$key]['message'].$tmp;       
            }  
        }
        

        $tmp_obj = $this->getORM('plf_messages');
        $table_first_name = 'plf_message_members';
        $tmp_where = "WHERE {$table_first_name}.groups_id = :groups_id ";
        $tmp_order = "ORDER BY plf_teach.nick_name DESC ";
        $sql = "select plf_teach.nick_name " .
            "from {$table_first_name} " .
            "LEFT JOIN " .
            "plf_teach ON {$table_first_name}.user_id = plf_teach.id " .
            $tmp_where . $tmp_order;

        $memberList = $tmp_obj->queryAll($sql, $params);

        $transMember = array();
        foreach ($memberList as $member){
            array_push($transMember,$member['nick_name']);
        }

        $user_id = $_SESSION['f_backend']['user_id'];

        return array('messageList'=>$messageList,'members'=>implode(" , ", $transMember),'nowID' => $user_id,'nowGroup'=>$group_id);
    }

    public function insertMessage($message_text, $message_file_id, $groups_id)
    {
        $user_id = $_SESSION['f_backend']['user_id'];
        $query_arr = null;
        $query_arr['id'] = $this->gen_uuid();
        $query_arr['groups_id'] = $groups_id;
        if (!empty($message_text)) {
            $query_arr['message'] = $message_text;
        }
        if (!empty($message_file_id)) {
            $query_arr['files_id'] = $message_file_id;
        }

        $query_arr['groups_id'] = $groups_id;
        $query_arr['groups_id'] = $groups_id;
        $query_arr['created_at'] = date('Y-m-d H:i:s');
        $query_arr['updated_at'] = date('Y-m-d H:i:s');
        $query_arr['created_by'] = $user_id;
        $query_arr['updated_by'] = $user_id;
        //end get
        $table_name = "messages";
        $rs = DI()->notorm->$table_name->insert($query_arr);

        return $rs;
    }

    public function createGroup($users_id, $group_name, $group_description)
    {
        $created_user_id = $_SESSION['f_backend']['user_id'];
        if (count($users_id) > 1) {
            //1對多
            array_push($users_id, $created_user_id);
        } else {
            //1對1
            foreach ($users_id as $user_id) {
                if ($user_id == $created_user_id) {
                    return array('saveResult' => false, 'message' => '不可選擇自己開新對話窗');
                }
            }
            array_push($users_id, $created_user_id);
        }

        //判斷這些名單有沒有已經開了一個群組了
        $rs = DI()->notorm->message_members->where('user_id', $users_id)->fetchAll();
        $groups_id = "";
        if (count($rs) != 0) {
            foreach ($rs as $memberObj) {
                $groupData = DI()->notorm->message_members->where('user_id', $users_id)->where('groups_id = ?', $memberObj['groups_id'])->fetchAll();
                if (count($groupData) == count($users_id)) {
                    $groups_id = $memberObj['groups_id'];
                    break;
                }
            }
        }

        //如果有開就回傳那個群組的資料
        if (!empty($groups_id)) {
            $rs = DI()->notorm->message_groups->where('id = ?', $groups_id)->fetchOne();
            return array('saveResult' => true, 'message' => '', datas => $rs);
        }

        //如果沒開就新增群組的資料

        //判斷所有選擇的人是否都是會員

        foreach ($users_id as $user_id) {

            $rs = DI()->notorm->teach->where('id = ?', $user_id)->fetchOne();
            if (empty($rs['id'])) {
                return array('saveResult' => false, 'message' => "會員編號{$user_id}不存在");
            }
        }
        $query_arr = array();

        //群組資料
        if (!empty($group_name)) {
            $query_arr['name'] = $group_name;
        } else {
            $rs = DI()->notorm->teach->select('nick_name')->where('id', $users_id)->fetchAll();
            $nicknameArr = array();
            foreach ($rs as $dataObj) {
                array_push($nicknameArr, $dataObj['nick_name']);
            }
            $query_arr['name'] = implode(" , ", $nicknameArr);
        }
        if (!empty($group_description)) {
            $query_arr['description'] = $group_description;
        }
        $query_arr['id'] = $this->gen_uuid();
        $query_arr['created_at'] = date('Y-m-d H:i:s');
        $query_arr['updated_at'] = date('Y-m-d H:i:s');
        $query_arr['created_by'] = $created_user_id;
        $query_arr['updated_by'] = $created_user_id;

        $table_name = "message_groups";
        $groups_data = DI()->notorm->$table_name->insert($query_arr);
        $groups_id = $groups_data['id'];
        if (empty($groups_id)) {
            return array('saveResult' => false, 'message' => '建立新對話窗失敗' , 'data' => $query_arr);
        }

        //群組成員資料
        foreach ($users_id as $user_id) {

            $table_name = "message_members";
            $rs = DI()->notorm->$table_name->where('id = ? and groups_id = ?', $user_id, $groups_id)->fetchOne();
            if (!empty($rs['id'])) {
                continue;
            }
            $query_arr = array();
            $query_arr['id'] = $this->gen_uuid();
            $query_arr['user_id'] = $user_id;
            $query_arr['groups_id'] = $groups_id;
            $query_arr['created_at'] = date('Y-m-d H:i:s');
            $query_arr['updated_at'] = date('Y-m-d H:i:s');
            $query_arr['created_by'] = $created_user_id;
            $query_arr['updated_by'] = $created_user_id;
            //end get
            DI()->notorm->$table_name->insert($query_arr);
        }

        return array('saveResult' => true, 'message' => '', 'data' => $groups_data);

    }

    public function EditGroup($group_id, $group_name)
    {

        $created_user_id = $_SESSION['f_backend']['user_id'];


        //判斷這些名單有沒有已經開了一個群組了
        $rs = DI()->notorm->message_groups->where("id = ? &&  created_by =? ", $group_id,$created_user_id )->fetchAll();
        if (count($rs) != 0) {
            ;
        }else{ //沒有建立群組 無法修改名稱
            return array('saveResult' => false, 'message' => '沒有這個群組喔');
        }

        //修改群組
        if (!empty($group_id)) {
            $query_arr = array();
            $query_arr['name'] = $group_name;
            $query_arr['updated_at'] = date('Y-m-d H:i:s');
            //end get
            DI()->notorm->message_groups->where('id = ?', $group_id)->update($query_arr);
            //回傳資料
            $rs = DI()->notorm->message_groups->where('id = ?', $group_id)->fetchOne();
            return array('saveResult' => true, 'message' => '', datas => $rs);
        }

    }

    public function addGroupMember($users_id, $group_id)
    {
        $created_user_id = $_SESSION['f_backend']['user_id'];


        //判斷有沒有已經開了一個群組了
        $rs = DI()->notorm->message_groups->where("id = ? &&  created_by =? ", $group_id,$created_user_id )->fetchAll();
        if (count($rs) != 0) {
            ;
        }else{ //沒有建立群組 無法修改名稱
            return array('saveResult' => false, 'message' => '沒有這個群組喔');
        }

        //判斷這些名單有沒有已經開了一個群組了
        $rs = DI()->notorm->message_groups->where('id = ?', $group_id)->fetchAll();
        $groups_id = "";
        if (count($rs) != 0) {
                //判斷是不是已經進入群組
                $groupData = DI()->notorm->message_members
                ->where('user_id', $users_id)
                ->where('groups_id = ?', $group_id)->fetchAll();
                if (count($groupData) == count($users_id)) {
                    return array('saveResult' => false, 'message' => '已經加入群組');
                }
        }else{ //沒有建立群組 無法修改名稱
            return array('saveResult' => false, 'message' => '沒有這個群組喔');
        }

        //加入群組
        if (!empty($group_id)) {
            $query_arr = array();
            $query_arr['id'] = $this->gen_uuid();;
            $query_arr['user_id'] = $users_id;
            $query_arr['groups_id'] = $group_id;
            $query_arr['created_by'] = $created_user_id;
            $query_arr['updated_by'] = $created_user_id;
            $query_arr['created_at'] = date('Y-m-d H:i:s');
            $query_arr['updated_at'] = date('Y-m-d H:i:s');
            //end get
            $rs = DI()->notorm->message_members->where('groups_id = ?', $group_id)->insert($query_arr);
            //回傳資料
            $rs = DI()->notorm->message_members->where('id = ?', $query_arr['id'])->fetchOne();
            return array('saveResult' => true, 'message' => '', datas => $rs);
        }

    }

    public function RemoveGroupMember($users_id, $group_id)
    {
        $created_user_id = $_SESSION['f_backend']['user_id'];


        //判斷有沒有已經開了一個群組了
        $rs = DI()->notorm->message_groups->where("id = ? &&  created_by =? ", $group_id,$created_user_id )->fetchAll();
        if (count($rs) != 0) {
            ;
        }else{ //沒有建立群組 無法修改名稱
            return array('saveResult' => false, 'message' => '沒有這個群組喔');
        }

        //判斷這些名單有沒有已經開了一個群組了
        $rs = DI()->notorm->message_groups->where('id = ?', $group_id)->fetchAll();
        $groups_id = "";
        if (count($rs) != 0) {
                //判斷是不是已經進入群組
                $groupData = DI()->notorm->message_members
                ->where('user_id', $users_id)
                ->where('groups_id = ?', $group_id)->fetchAll();
                if (count($groupData) == 0) {
                    return array('saveResult' => false, 'message' => '已經沒在群組');
                }
        }else{ //沒有建立群組 無法修改名稱
            return array('saveResult' => false, 'message' => '沒有這個群組喔');
        }

        //加入群組
        if (!empty($group_id)) {
            $query_arr = array();
            $query_arr['id'] = $this->gen_uuid();;
            $query_arr['user_id'] = $users_id;
            $query_arr['groups_id'] = $group_id;
            $query_arr['created_by'] = $created_user_id;
            $query_arr['updated_by'] = $created_user_id;
            $query_arr['created_at'] = date('Y-m-d H:i:s');
            $query_arr['updated_at'] = date('Y-m-d H:i:s');
            //end get
            $rs = DI()->notorm->message_members->where('groups_id = ?', $group_id)->delete();
            //回傳資料
            //$rs = DI()->notorm->message_groups->where('id = ?', $groups_id)->fetchOne();
            return array('saveResult' => true, 'message' => '', datas => '刪除成功');
        }

    }
    //GetFriendList  = User.FriendsList

    public function getNameEmail($text)
    {
        $created_user_id = $_SESSION['f_backend']['user_id'];
        if($text == ''){
            return array('saveResult' => false, 'message' => '不要搜尋空值');
        }
        $rs = DI()->notorm->teach->where('nikc_name = ? or email =? ', "%$text%","%$text%" )->fetchAll();
        return array('saveResult' => true, 'message' => '', datas => $rs);

    }



    private function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }


}
