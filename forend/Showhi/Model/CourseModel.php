<?php
class Model_CourseModel extends Common_ModelDefaultList {

    //建構子
    public function __construct() {  
        parent::__construct(); 

    }
    
    public function __destruct(){

    }
    
    
    public function getTableName($id) {
        return "student";
    }
    
    public function ViewList(){
        $tmp_table='teach';
        //get todo login
            $todaytime = strtotime(date('Y-m-01',strtotime("now")));
            $maxdaytime = strtotime(date('Y-m-d H:i:s',$todaytime)." +3 month ");
            $this_day = $this->laout_check($this->laout_check(DI()->request->get('this_day')));
            if(empty($this_day))
            $this_daytime = strtotime("now");
            else
            $this_daytime = strtotime("$this_day");
        
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
       //end get
       //內部確認
        if($this_daytime < $todaytime ||  $this_daytime > $maxdaytime ){
            $rs['msg_text'] =T('SystemError');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
                
        }
        //end 內部確認
        
        $today = date('Y-m-d',strtotime("now"));
        if(!empty($this_day)){
            $thisday = date('Y-m-d',strtotime("$this_day"));
            $thisday_sec = strtotime("$this_day");
                
            $day_num = date('w',strtotime("$this_day")); // 星期x
            $lesson_day = date('Y-m-d',strtotime("$this_day"));
            if($day_num ==0)$day_num =null;
            $lesson_day =date('Y-m-d',strtotime("$this_day"));        
            $can_tutor_time = 'can_tutor_time'.($day_num);

                
        }else{
            $thisday = $today;
            $thisday_sec = strtotime($today);

            $lesson_day = $today;
            $lesson_day = $today;
            $day_num = date('w',strtotime("now")); // 
            if($day_num ==0)$day_num =null;
            $can_tutor_time = 'can_tutor_time'.($day_num);
        }
        
            $next_month_limit = date('Y-m-t',strtotime("now +1 month")); // 
            $next2_month_limit = date('Y-m-t',strtotime("now +2 month")); // 
            $today_num = date('j',strtotime("now")); // 
            $today = date('Y-m-d',strtotime("now")); // 
            $today_month_firstday = date('Y-m-01',strtotime("now")); // 
            $today_year = date('Y',strtotime("now")); // 
            $next_year = date('Y',strtotime("$today_month_firstday +1 month")); // 
            $next2_year = date('Y',strtotime("$today_month_firstday +2 month")); // 
            $today_month = date('n',strtotime("now")); // 
            
            
            $next_month = date('n',strtotime("$today_month_firstday +1 month")); // 
            $next2_month = date('n',strtotime("$today_month_firstday +2 month")); // 
            
            
            $month_limit_day = date('t',strtotime("now")); // 
            $next_month_limit_day = date('t',strtotime("now +1 month")); // 
            $next2_month_limit_day = date('d',strtotime("now +2 month")); // 

        $prev_day = date('Y-m-d',strtotime($thisday." -1 day"));
        $next_day = date('Y-m-d',strtotime($thisday." +1 day"));
        
        
        
        //所有老師數 + 課程資料   
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //抓取搜尋
        $tmp_where = $this->search();
        //end 抓取搜尋
        $teach_id = $this->laout_check(DI()->request->get('teach_id'));
        
        if($teach_id =='') //是列表業開啟只顯示學校
            $sing_where = " and a.type_id >=2";
        else
            $sing_where = "";
            
        $tmp_order = 'order by a.type_id asc, a.id asc';
        $tmp_limit = " ";

        
        $sql = "select a.*,b.id as lesson_id ,b.lesson_day from ".$table_first_name."teach as a
                    LEFT JOIN 
                    (select * from ".$table_first_name."lesson
                    where 
                    UNIX_TIMESTAMP(lesson_day)  >= UNIX_TIMESTAMP(CURDATE()) )  AS b ON b.teach_id =a.id
                    and 
                    UNIX_TIMESTAMP(lesson_day) = unix_timestamp('$lesson_day') or isnull(lesson_day)
                      where a.approval ='Y' 
                     and ( 
                     (
                        UNIX_TIMESTAMP(lesson_day) = unix_timestamp('$lesson_day') or isnull(lesson_day)
                     ) or
                     (
                         unix_timestamp('$lesson_day') >= unix_timestamp(a.start_date) 
                         AND 
                         unix_timestamp('$lesson_day') <= UNIX_TIMESTAMP(a.end_date)
                     )
                         )
                     $sing_where 
                        
            ".$tmp_where.$tmp_order.$tmp_limit;
            
        $tmp_arr['tmp_teach_arr'] = $tmp_obj->queryAll($sql);
        $replace_arr=array('image','bg_image');
        
        $tmp_arr['tmp_teach_arr'] =  $this->RePlaceFilename($tmp_arr['tmp_teach_arr'],$replace_arr);
        
        //end 所有老師數 + 課程資料   

        $tmp_order = 'order by a.id asc';
        $tmp_limit = " ";
        $sql = "SELECT a.*,c.count,b.id AS lesson_id,b.lesson_day
                    FROM ".$table_first_name."teach AS a
                    LEFT JOIN (
                    SELECT *
                    FROM ".$table_first_name."lesson) AS b ON b.teach_id =a.id
                    LEFT JOIN (
                    SELECT a.id, COUNT(*) AS COUNT
                    FROM ".$table_first_name."teach AS a
                    LEFT JOIN ".$table_first_name."lesson AS b ON a.id =b.teach_id
                    LEFT JOIN ".$table_first_name."lesson_data AS c ON b.id =c.lesson_id
                    WHERE  UNIX_TIMESTAMP(b.lesson_day) <= UNIX_TIMESTAMP() AND (student_cancel !='Y' AND teach_leave !='Y') AND ! ISNULL(c.teach_score)
                    GROUP BY b.teach_id
                    ORDER BY b.teach_id ASC) AS c ON a.id=c.id
                    WHERE a.approval ='Y' 
                    GROUP BY a.id        
            ".$tmp_where.$tmp_order.$tmp_limit;

        $tmp_arr['tmp_teach_lesson_count_arr'] = $tmp_obj->queryAll($sql);
        

    if($tmp_arr['tmp_teach_arr'])
    foreach($tmp_arr['tmp_teach_arr'] as $key => $value){
        
        
    //所有已評價課程數
    if($tmp_arr['tmp_teach_lesson_count_arr'])
    foreach($tmp_arr['tmp_teach_lesson_count_arr'] as $key_le_count => $value_le_count){
            if($value['id'] == $value_le_count['id']){
                if($value_le_count['count'] ==null || $value_le_count['count'] ==0)
                $value_le_count['count'] = 1;
                    $tmp_arr['tmp_teach_arr'][$key]['lesson_count'] = $value_le_count['count'];
            }    
    }    
    //end 所有已評價課程數
    
    //所有課程數
            if(!empty($value['lesson_id'])){
                if(empty($tmp_id)){
                    $tmp_id = $tmp_id."'".$value['lesson_id']."'";    
                }else{
                    $tmp_id = $tmp_id.','."'".$value['lesson_id']."'";        
                }
            }    
    //end 所有課程數

    }
    //每堂學生數
    //Todo 可以在老師資料把插入學生數欄位或是在老師資料拉入群組表紀錄該群組老師學生上課數
    $lesson_student_count = $this->setting['setting']['lesson_student_count']['value'];


    //把當日課程放到老師的列表
    if(!empty($tmp_id)){
    $sql="select count(*) as lesson_student_count , can_tutor_time,lesson_id,teach_leave from ".$table_first_name."lesson_data where lesson_id IN ($tmp_id)  and student_cancel !='Y'
            group by lesson_id,teach_leave,can_tutor_time  order by lesson_id ";
        //    exit;
/*
         $sql="select * from ".$table_first_name."lesson_data where lesson_id IN ($tmp_id)  and student_cancel !='Y'
            order by lesson_id";
            */


            
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
    }

    if(($tmp_arr['tmp_teach_arr']))
    foreach($tmp_arr['tmp_teach_arr'] as $key => $value){
            $tmp_id = null;
            $tmp_teach_leave_id = null;
        $tmp_arr['tmp_teach_arr'][$key]['loginpwd'] =null;
    if($tmp_arr['tmp_arr'])            
                foreach($tmp_arr['tmp_arr'] as $key1 => $value1){
                    if($value['lesson_id'] == $value1['lesson_id'] && $value1['teach_leave'] !='Y' ){

                                $tmp_arr['tmp_teach_arr'][$key]['lesson_student_count'][$value1['can_tutor_time']] 
                                = $value1['lesson_student_count'];

                        if($value1['lesson_student_count']  ==  $lesson_student_count){
                            if(empty($tmp_id)){
                                $tmp_id = $tmp_id.$value1['can_tutor_time'];    
                            }else{
                                $tmp_id = $tmp_id.','. $value1['can_tutor_time'];    
                            }
                        }
                    }elseif($value['lesson_id'] == $value1['lesson_id'] && $value1['teach_leave'] =='Y'){

                                $tmp_arr['tmp_teach_arr'][$key]['lesson_student_count'][$value1['can_tutor_time']] 
                                = $value1['lesson_student_count'];

                        if(empty($tmp_teach_leave_id)){
                             $tmp_teach_leave_id = $tmp_teach_leave_id.$value1['can_tutor_time'];    
                        }else{
                             $tmp_teach_leave_id = $tmp_teach_leave_id.','. $value1['can_tutor_time'];    
                        }
                    }

            }

            //看看有無行政人員身分
            /*
                    $tmp_arr['fan_manage'] = DI()->notorm->fan_manage->select('*')
                    ->where("user_id = ? && `type` ='staff' ", $value['id'] )->fetchOne();
                    if($tmp_arr['fan_manage'][0]['type'] == 'staff'){
                        $tmp_arr['tmp_teach_arr'][$key]['fan_ident'] ='staff';
                    }
            */


            $tmp_arr['can_tutor_time'] = $can_tutor_time;
            $tmp_arr['tmp_teach_arr'][$key]['tmp_booking_tutor_time'] = $tmp_id;
            $tmp_arr['tmp_teach_arr'][$key]['tmp_leave_tutor_time'] = $tmp_teach_leave_id;            
    }

    //end 把當日課程放到老師的列表
    
    //計算當天以過時間的可教學id
         $refund_time = $this->setting['setting']['student_refund_lesson_time']['value'];
        $tmp_can_tutor_id = null;
    foreach($tmp_arr['tmp_arr_can_tutor_time'] as $key2 => $value2){ 
        if(strtotime("now") < strtotime("$today $value2[name]") && $tmp_can_tutor_id ==null  ){
                    $tmp_can_tutor_id = $value2['id'];
        }        
    }
        if($tmp_can_tutor_id == null)
        $tmp_can_tutor_id =124;
    
    //end 計算當天以過時間的可教學id
        $tmp_arr['month_arr1'] = 
        $this -> get_month_arr($tmp_arr['month_arr'],$today_num,$month_limit_day,$today_year,$today_month,$tmp_arr['tmp_teach_arr'],$tmp_can_tutor_id);
        $start_day = $today_year.'-'.$today_month.'-'.$today_num;
        $tmp_arr['month_arr'] =null;
        
        $tmp_arr['month_arr2'] =
        $this -> get_month_arr($tmp_arr['month_arr'],1,$next_month_limit_day,$next_year,$next_month,$tmp_arr['tmp_teach_arr']);
        $tmp_arr['month_arr'] =null;
        
        $tmp_arr['month_arr3'] =
        $this -> get_month_arr($tmp_arr['month_arr'],1,$next2_month_limit_day,$next2_year,$next2_month,$tmp_arr['tmp_teach_arr']);
        $tmp_arr['month_arr'] =null;

        $this_year = date('Y',strtotime($thisday));    
        $this_month = date('n',strtotime($thisday));    
        $this_day_num = date('j',strtotime($thisday));    
        if($this_month == $today_month){            
        if(is_array($tmp_arr['month_arr1'][$this_year][$this_month][$this_day_num]))    
            //把當天老師放進去
            $tmp_arr['this_day_arr'][$thisday] =$tmp_arr['month_arr1'][$this_year][$this_month][$this_day_num];
            
        }else{ //下兩個月
        if(is_array($tmp_arr['month_arr2'][$this_year][$this_month][$this_day_num]))    
            //把當天老師放進去
            $tmp_arr['this_day_arr'][$thisday] =$tmp_arr['month_arr2'][$this_year][$this_month][$this_day_num];    
    
        if(is_array($tmp_arr['month_arr3'][$this_year][$this_month][$this_day_num]))    
            //把當天老師放進去
            $tmp_arr['this_day_arr'][$thisday] =$tmp_arr['month_arr3'][$this_year][$this_month][$this_day_num];
    
        }
        

        //type    
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 4)->fetchAll();
        $tmp_arr['tmp_arr_expertise'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 3)->fetchAll();
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 13)->fetchAll();
        $tmp_arr['tmp_arr_can_totur_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 4)->fetchAll();
        $tmp_arr['tmp_arr_nat'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 2)->fetchAll();
        $tmp_arr['tmp_arr_education'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 9)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();
        $tmp_table='teach_article';
        $tmp_arr['tmp_arr_article'] = DI()->notorm->$tmp_table->select('*')->fetchAll();        
        //end type
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);

        /*
        print_r($tmp_arr['tmp_arr_interest']);
        exit;    
        */
        return $tmp_arr;

    }
    
    public function TeacherList(){
        $tmp_table='teach';
        
        //get todo login
        $identity=$_SESSION['f_backend']['identity'];
        $user_id=$_SESSION['f_backend']['user_id'];     
       //end get

        //自己的上課畫面
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //抓取搜尋
        $tmp_where = $this->search();
        //end 抓取搜尋
        
           if($_REQUEST['orderby'] == null)
                $tmp_orderby = 'count';
            else
                $order = $this->laout_check(DI()->request->get('order'));

            if($order =='count')    
                $order = ' order by lesson_count desc ';
            elseif($order =='exp')
                $order = ' order by exp asc '; 
            else
                $order = ' order by a.type_id asc, a.id asc ';    
                    
                
        $tmp_order = $order;
        $tmp_limit = " ";
        $sql = "SELECT a.* , b.id as teach_id,b.count as lesson_count FROM ".$table_first_name."teach as a 
                left join (
                    select 
                    a.id ,count(*) as count 
                    from ".$table_first_name."teach 
                    as a left join ".$table_first_name."lesson as b on a.id =b.teach_id 
                    left join ".$table_first_name."lesson_data as c on b.id =c.lesson_id 
                    where unix_timestamp(b.lesson_day) <= unix_timestamp() 
                    and (student_cancel !='Y' and teach_leave !='Y') 
                    and !isnull(c.teach_score) group by b.teach_id order by b.teach_id asc 
                ) as b on a.id=b.id
                where approval ='Y' and hot_teacher ='Y'
                and a.type_id>=2
            ".$tmp_where.$tmp_order.$tmp_limit;

        $tmp_arr['tmp_list'] = $tmp_obj->queryAll($sql);
        //end 自己的上課畫面

        //type    
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_interest'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 14)->fetchAll();
        $tmp_arr['tmp_arr_major'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 15)->fetchAll();
        $tmp_arr['tmp_arr_can_tutor_time'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 12)->fetchAll();
        $tmp_arr['tmp_arr_teacher_category'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ?', 17)->fetchAll();
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr_article'] = DI()->notorm->$tmp_table->select('*')->fetchAll();        
        //end type
        $tmp_arr['tmp_list'] = $this->Takeout_Password($tmp_arr['tmp_list']);
        return $tmp_arr;
            
    }
    

    public function TeacherText(){
        $tmp_table='teach';
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
        //get todo login
        $teach_id = $this->laout_check(DI()->request->get('teach_id'));
        $code_name = $this->laout_check(DI()->request->get('code_name'));
        $user_id=$_SESSION['f_backend']['user_id'];    
       //end get
            $tmp_sql = DI()->notorm->$tmp_table->select('*');
            $tmp_arr['tmp_list'] =$tmp_sql->where(' id = ?', $teach_id)->fetchAll();     
            $tmp_arr['tmp_list'] =$this->Takeout_Password($tmp_arr['tmp_list']);
            $replace_arr=array('image','bg_image');
            $tmp_arr['tmp_list'] =  $this->RePlaceFilename($tmp_arr['tmp_list'],$replace_arr,'id');

            //看看有無行政人員身分
            $tmp_arr['fan_manage'] = DI()->notorm->fan_manage->select('*')
            ->where("user_id = ? && ( `type` ='staff' || `type` ='admin'  ) and approval ='Y' ", $teach_id)->fetchAll();

            if($tmp_arr['fan_manage'][0]['type'] == 'staff' || $tmp_arr['fan_manage'][0]['type'] == 'admin' ){
                $tmp_arr['tmp_list']['0']['fan_ident'] ='staff';
            }else{
                $tmp_arr['tmp_list']['0']['fan_ident'] =null;
            }

            $tmp_id =null ;
            foreach($tmp_arr['fan_manage'] as $key => $value){ 

                if(empty($tmp_id)){
                    $tmp_id = $tmp_id."'".$value['fan_id']."'";    
                }else{
                    $tmp_id = $tmp_id.','."'".$value['fan_id']."'";        
                }

            }



            //看看是不是相同的社群
            if(!empty($tmp_id))
            $tmp_arr['fan_manage'] = DI()->notorm->fan_manage->select('*')
            ->where("user_id = ?  and approval ='Y' and fan_id IN ($tmp_id)  ", $user_id)->fetchAll();




            if(count($tmp_arr['fan_manage']) >=1)
                $tmp_arr['tmp_list']['0']['fan_ident_match'] ='Y';
            else
                $tmp_arr['tmp_list']['0']['fan_ident_match'] =null;
            

        return $tmp_arr;
            
    }
    
    public function StudentBookingChoose(){
        $tmp_table='teach_article';
        
        //get todo login
         $type = $this->laout_check(DI()->request->get('type'));
         $orderby = $this->laout_check(DI()->request->get('orderby'));
         $tag = $this->laout_check(DI()->request->get('tag'));
        //end get
       
            if($orderby == ''){
               $orderby ='id desc';
           }elseif($orderby == 'hot'){
               $orderby ='choose_count desc';
           }elseif($orderby == 'time'){
               $orderby ='id desc ';
           }else{
               $orderby ='id desc '; 
           }
       
            $tmp_sql = DI()->notorm->$tmp_table->select('*');
            //if(!empty($code_name)){
            $tmp_arr['tmp_list'] =$tmp_sql->
            where(" rel_id = ? and CONCAT(',',tag,',') like ? and approval ='Y' ", $type,"%,$tag,%")
            ->order(" $orderby ")
            ->fetchAll();
           //}
        return $tmp_arr;
    }
    
    public function StudentLessonCancel(){

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        //get
        $identity = $_SESSION['f_backend']['identity'];
        $loginid = $_SESSION['f_backend']['loginid'];
        $id = $this->laout_check(DI()->request->get('lesson_dt_id'));
        //end get
        $sql="select CONCAT(DATE_FORMAT(b.lesson_day,'%Y-%m-%d'),' ',c.name) as booking_select_time,a.*,d.email as student_email,d.phone as student_phone,e.phone as teach_phone,e.email as teach_email,e.nick_name
            from ".$table_first_name."lesson_data as a 
            left join  ".$table_first_name."lesson as b on b.id = a.lesson_id
            left join  ".$table_first_name."realtion_datas as c on a.can_tutor_time = c.id
            left join  ".$table_first_name."teach as d on d.id = a.student_id
            left join  ".$table_first_name."teach as e on e.id = b.teach_id
        where d.loginid ='$loginid' and a.id ='$id' and (student_cancel !='Y' and teach_leave !='Y') ";
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
        
        $need_point = $tmp_arr['tmp_arr'][0]['use_point'];
        $student_id = $tmp_arr['tmp_arr'][0]['student_id'];
        $article_link = $tmp_arr['tmp_arr'][0]['article_link'];
        $teach_email = $tmp_arr['tmp_arr'][0]['teach_email'];
        $student_email = $tmp_arr['tmp_arr'][0]['student_email'];
        $student_phone = $tmp_arr['tmp_arr'][0]['student_phone'];
        $teach_phone = $tmp_arr['tmp_arr'][0]['teach_phone'];
        $nick_name = $tmp_arr['tmp_arr'][0]['nick_name'];
        $lesson_id = $tmp_arr['tmp_arr'][0]['id'];
        $booking_select_time = $tmp_arr['tmp_arr'][0]['booking_select_time'];
        
        //時差轉換網址
        $time_con = date('Ymd\Thi',strtotime("$booking_select_time"));
        $time_con_link = "www.timeanddate.com/worldclock/converted.html?iso=".$time_con."&p1=241&p2=136&p3=179&p4=159&p5=75&p6=137&p7=56&p8=250&p9=265&p10=55&p11=256&p12=47";
        //end 時差轉換網址
        
        //內部確認
        if (count($tmp_arr['tmp_arr']) !=1){
            $rs['msg_text'] =T('NotFindLesson');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
        }
        $tmp_arr['tmp_arr'] = null;
        
        if(strtotime("now +6 hours") > strtotime("$booking_select_time") ){ //24小時判斷
            $rs['msg_text'] =T('BefaultStudnetHour');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
        }    
        $tmp_arr['tmp_arr'] = null;
        
        //end 內部確認   
                   $table_name ="lesson_data"; 
                   $tmp_where = "where id = $id";
                $query_arr=null;
                $query_arr['update_date']=date('Y-m-d H:i:s');
                $query_arr['student_cancel_date']=date('Y-m-d H:i:s');
                $query_arr['student_id']=$student_id;
                $query_arr['student_cancel']='Y';
                $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
                if($rs !=0){
                    $rs =array();
                    if($this -> student_point($student_id,$need_point)){      

                    //artile -1
                        $data = array('choose_count'  => new NotORM_Literal("choose_count - 1"));
                        $tmp_arr['tmp_list'] = DI()->notorm->teach_article->where('id',$article_link)->update($data);
                        $tmp_arr['tmp_list'] =null;
                    //end article -1

//send mail send sms
            $host_path =HOST_PATH;
            $subject = T('CourseCancellationNoticeSubject');
            $body = T('StudentCourseCancellationNotice', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => $booking_select_time ,
                'host_path' => $host_path,
                'time_con_link' => $time_con_link 
                ));
                
            $send_mail = $this->Mail->send($teach_email, $subject, $body);

            $Smsbody = T('SmsStudentCourseCancellationNotice', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => $booking_select_time 
                ));
            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($teach_phone,$Smsbody);
            }

//end send mail send sms
            $rs['msg_text'] =T('CancelSuccess');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='Y';
            return $rs;         
                    }
                }
        $rs =array();
        $rs['msg_text'] =T('CancelError');
        $rs['update_time'] =date('Y-m-d H:i:s');
        $rs['msg_state'] ='Y';
        return $rs; 
        
    }

    public function TeacherLessonCancel(){

        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        //get

        $lesson_id = $this->laout_check(DI()->request->get('lesson_id'));
        $lesson_dt_id = $this->laout_check(DI()->request->get('lesson_dt_id'));
        $can_tutor_time = $this->laout_check(DI()->request->get('can_tutor_time'));
        $identity = $_SESSION['f_backend']['identity'];
        $user_id = $_SESSION['f_backend']['user_id'];
        //end get
        //todo 把退課改抓lesson_id+時間 跑回圈退課?

        $sql="select a.*,d.email as student_email,d.phone as student_phone,d.nick_name,CONCAT(DATE_FORMAT(b.lesson_day,'%Y-%m-%d'),' ',c.name) as booking_select_time
            from ".$table_first_name."lesson_data as a 
            left join  ".$table_first_name."lesson as b on b.id = a.lesson_id
            left join  ".$table_first_name."realtion_datas as c on a.can_tutor_time = c.id
            left join  ".$table_first_name."teach as d on d.id = a.student_id
          where  a.lesson_id ='$lesson_id' and   b.teach_id='$user_id' and a.id='$lesson_dt_id' and a.can_tutor_time ='$can_tutor_time' and  (student_cancel !='Y' and teach_leave !='Y')
            and 
            student_cancel !='Y'
            and 
            teach_leave !='Y'"
            ;
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);

        if(isset($tmp_arr['tmp_arr'])) foreach($tmp_arr['tmp_arr'] as $key => $value){ 
        

            $need_point = $value['use_point'];
            $article_link = $value['article_link'];
            $student_id = $value['student_id'];
            $student_email = $value['student_email'];
            $student_phone = $value['student_phone'];
            $nick_name = $value['nick_name'];
            $id = $value['id'];
            $booking_select_time = $value['booking_select_time'];

        //時差轉換網址
        $time_con = date('Ymd\Thi',strtotime("$booking_select_time"));
        $time_con_link = "www.timeanddate.com/worldclock/converted.html?iso=".$time_con."&p1=241&p2=136&p3=179&p4=159&p5=75&p6=137&p7=56&p8=250&p9=265&p10=55&p11=256&p12=47";
        //end 時差轉換網址

        //內部確認  
            if(strtotime("now +24 hours") > strtotime("$booking_select_time") ){ //24小時判斷
                $rs['msg_text'] =T('BefaultTeacherHour');
                $rs['update_time'] =date('Y-m-d H:i:s');
                $rs['msg_state'] ='N';
                return $rs; 
            }    
        //end 內部確認

                   $table_name ="lesson_data"; 
                $query_arr=null;
                $query_arr['update_date']=date('Y-m-d H:i:s');
                $query_arr['teach_leave_date']=date('Y-m-d H:i:s');
                $query_arr['teach_leave']='Y';
                $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
                
                if($rs !=0){
                    if($this -> student_point($student_id,$need_point)){      

                    //artile -1
                        $data = array('choose_count'  => new NotORM_Literal("choose_count - 1"));
                        $tmp_arr['tmp_update_arr'] = DI()->notorm->teach_article->where('id',$article_link)->update($data);
                        $tmp_arr['tmp_update_arr'] =null;
                    //end article -1


            $host_path =HOST_PATH;
            $subject = T('CourseCancellationNoticeSubject');
            $body = T('TeacherCourseCancellationNotice', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => $booking_select_time ,
                'host_path' => $host_path,
                'time_con_link' => $time_con_link 
                ));
            $send_mail = $this->Mail->send($student_email, $subject, $body);

            $Smsbody = T('SmsTeacherCourseCancellationNotice', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => $booking_select_time 
                ));
            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($student_phone,$Smsbody);
            }


                }// end point +1
            }//end update lesson cancel
        }//end foreach
            $rs = null;
            $rs['msg_text'] =T('CancelSuccess');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='Y';
            return $rs;

    }
    
    
    public function StudentBooking(){

       //get
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        
         $loginid = $this->laout_check($_SESSION['f_backend']['loginid']);



         $article_link = $this->laout_check(DI()->request->get('article_link'));
         $lesson_day = $this->laout_check(DI()->request->get('lesson_day'));
         $booking_select_time =$lesson_day." ";
         $teach_id = $this->laout_check(DI()->request->get('teach_id'));
         $can_tutor_time = $this->laout_check(DI()->request->get('can_tutor_time'));
         $cause = $this->laout_check(DI()->request->get('cause'));
         $sql="select * from ".$table_first_name."teach where loginid = '$loginid' ";
         
         $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
         $student_id = $tmp_arr['tmp_arr']['0']['id'];


         //email 用到
        if($article_link !=''){
            $sql="select a.*,b.en_name as article_category from ".$table_first_name."teach_article as a
            inner join ".$table_first_name."realtion_datas as b on a.rel_id = b.id
            where a.id =$article_link";
            $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
            
                $email_article_link = $tmp_arr['tmp_arr'][0]['link'];
                $email_article_title = $tmp_arr['tmp_arr'][0]['title'];
                $email_article_category = $tmp_arr['tmp_arr'][0]['article_category'];
        }
        //email 用到

         $refund_time=$this->setting['setting']['student_refund_lesson_time']['value'];
         $today = date('Y-m-d',strtotime("now"));
         
        //bookin時間抓取
        $tmp_table='realtion_datas';
        $tmp_arr['tmp_arr'] = DI()->notorm->$tmp_table->select('*')->where('rel_id = ? and id =? ', 12,$can_tutor_time)->fetchAll();
        $booking_select_time =$lesson_day." ".$tmp_arr['tmp_arr'][0]['name'];
        //go to fd time
        $booking_hour_time =$tmp_arr['tmp_arr'][0]['name'];
        $lesson_fend_day =date('Y年m月d日',strtotime($lesson_day));
        $tmp_wdate =date('w',strtotime($lesson_day));
                          if($tmp_wdate == 0)
                              $tmp_wdate='日';
                          if($tmp_wdate == 1)
                              $tmp_wdate='一';
                          if($tmp_wdate == 2)
                              $tmp_wdate='二';
                          if($tmp_wdate == 3)
                              $tmp_wdate='三';
                          if($tmp_wdate == 4)
                              $tmp_wdate='四';
                          if($tmp_wdate == 5)
                              $tmp_wdate='五';
                          if($tmp_wdate == 6)
                              $tmp_wdate='六';             
         $lesson_fend_day = $lesson_fend_day."($tmp_wdate)";

                              
        $booking_am_pm =date('A',strtotime($booking_hour_time));
        if($booking_am_pm =='AM'){
             $booking_hour_time = '上午 '.$booking_hour_time;
        
        }else{
            $booking_hour_time = '下午 '.$booking_hour_time;
        }
        //end go to fd time
        
        //時差轉換網址
        $time_con = date('Ymd\Thi',strtotime("$booking_select_time"));
        $time_con_link = "www.timeanddate.com/worldclock/converted.html?iso=".$time_con."&p1=241&p2=136&p3=179&p4=159&p5=75&p6=137&p7=56&p8=250&p9=265&p10=55&p11=256&p12=47";
        //end 時差轉換網址
        
        $tmp_arr['tmp_arr'] =null;
        //end bookin時間抓取
        $start_time = strtotime("$booking_select_time");
        $end_time = strtotime("$booking_select_time +30 minutes" );
       

        //抓週數
        $day_num = date('w',strtotime("$lesson_day")); // 星期x
        if($day_num == 0){
            $day_num =null;
        }
        
        $teach_tutor_time = 'can_tutor_time'.($day_num);

          //end get   
      
      
        //內部確認
        if(strtotime($lesson_day) < strtotime($today)){ //選到過去天，惡意操作           
            $rs['msg_text'] =T('SystemErrorAgain');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
                
        }
        
    
    
        if(strtotime("now") > strtotime("$booking_select_time") ){ //24小時判斷
            $rs['msg_text'] =T('BefaultBooking');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
        }    
        //還要加判斷學生這個時間有沒有空
        
        
         $sql="select a.id as lesson_id ,a.lesson_day from ".$table_first_name."lesson AS a 
                left join ".$table_first_name."lesson_data AS b on a.id =b.lesson_id 
                left join ".$table_first_name."teach as c on b.student_id =c.id
                where 
                a.lesson_day ='$lesson_day' 
                and b.can_tutor_time ='$can_tutor_time'
                and student_id ='$student_id'
                and student_cancel !='Y'
                ";    
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
        
           if(count($tmp_arr['tmp_arr']) >=1 ){ 
            $rs['msg_text'] =T('YouhaveLesson');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 

        }            
        $tmp_arr['tmp_arr'] = null;
        
        //先確定老師常態課表這天有時間
         $sql="select a.id,a.need_point,a.loginid,a.email,a.phone ,a.nick_name from ".$table_first_name."teach as a

                  where !ISNULL(a.$teach_tutor_time)
                  and a.approval ='Y' 
                and a.id ='$teach_id'
            and a.$teach_tutor_time like '%$can_tutor_time%'";        
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
        
           if(count($tmp_arr['tmp_arr']) ==0 ){ 
            $rs['msg_text'] =T('TeacherNoTime');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
                
        }
        $need_point = $tmp_arr['tmp_arr'][0]['need_point'];
        $teach_email = $tmp_arr['tmp_arr'][0]['email'];
        $teach_phone = $tmp_arr['tmp_arr'][0]['phone'];
        $nick_name = $tmp_arr['tmp_arr'][0]['nick_name'];        
        $room_id = $tmp_arr['tmp_arr'][0]['room_id'];        
        $tmp_arr['tmp_arr'] = null;

        //抓取學生email
        $sql="select * from ".$table_first_name."teach where  id ='$student_id'";            
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
        $student_nick_name = $tmp_arr['tmp_arr'][0]['nick_name'];    
        $student_email = $tmp_arr['tmp_arr'][0]['email'];    
        $student_phone = $tmp_arr['tmp_arr'][0]['phone'];    
        $tmp_arr['tmp_arr'] = null;
            
            
        //換判段當天這個時間這個老師有沒有booking
        $tmp_order = '';
        $tmp_limit = " ";
        $tmp_where = "where
                b.lesson_day ='$lesson_day' 
                and a.id ='$teach_id' 
                and student_cancel !='Y'
                and c.can_tutor_time ='$can_tutor_time'";
        $sql = "select a.id from ".$table_first_name."teach as a
                 left join ".$table_first_name."lesson AS b on b.teach_id =a.id
                 left join ".$table_first_name."lesson_data AS c on b.id =c.lesson_id 
            ".$tmp_where.$tmp_order.$tmp_limit;

        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
        
    //Todo 可以在老師資料把插入學生數欄位或是在老師資料拉入群組表紀錄該群組老師學生上課數
     $lesson_student_count = $this->setting['setting']['lesson_student_count']['value'];

           if(count($tmp_arr['tmp_arr']) >=$lesson_student_count ){ 
            $rs['msg_text'] =T('LessonFullNumber');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
                
        }
        $tmp_arr['tmp_arr'] = null;
      //end 內部確認  
      

//send mail send sms
            //teacher
            $host_path =HOST_PATH;
            $subject = T('EmailBookingSubject');
            $body = T('EmailBooking', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => date('l, F d,Y',strtotime("$booking_select_time")) ,
                'booking_select_hour' => date('H:i',strtotime("$booking_select_time")),
                'host_path' => $host_path,
                'article_title' => $article_title,
                'article_category' => $article_category,
                'time_con_link' => $time_con_link 
                ));
            $send_mail = $this->Mail->send($teach_email, $subject, $body);

            $Smsbody = T('SmsEmailBooking', array(
                'subject' => $subject ,
                'nick_name' => $nick_name ,
                'booking_select_time' => date('l, F d,Y',strtotime("$booking_select_time")) ,
                'booking_select_hour' => date('H:i',strtotime("$booking_select_time")),
                'article_title' => $article_title,
                'article_category' => $article_category,
                ));

            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($teach_phone,$Smsbody);
            }

            //student
            $host_path =HOST_PATH;
            $subject = T('EmailBookingSubject');
            $body = T('EmailToStudentBooking', array(
                'subject' => $subject ,
                'nick_name' => $student_nick_name ,
                'booking_select_time' => date('l, F d,Y',strtotime("$booking_select_time")) ,
                'booking_select_hour' => date('H:i',strtotime("$booking_select_time")),
                'host_path' => $host_path,
                'article_title' => $article_title,
                'article_category' => $article_category,
                'time_con_link' => $time_con_link 
                ));
            $send_mail = $this->Mail->send($student_email, $subject, $body);

            $Smsbody = T('SmsEmailToStudentBooking', array(
                'subject' => $subject ,
                'nick_name' => $student_nick_name ,
                'booking_select_time' => date('l, F d,Y',strtotime("$booking_select_time")) ,
                'booking_select_hour' => date('H:i',strtotime("$booking_select_time")),
                'article_title' => $article_title,
                'article_category' => $article_category,
                ));
            if(isset($this->Sms)){
                $sendCodeResult = $this->Sms->sendSMS($student_phone,$Smsbody);
            }


//end send mail send sms

            //artile +1
                $data = array('choose_count'  => new NotORM_Literal("choose_count + 1"));
                $tmp_arr['tmp_list'] = DI()->notorm->teach_article->where('id',$article_link)->update($data);
                $tmp_arr['tmp_list'] =null;
            //end article +1


            
        //寫入程式 1.確認課程日期有沒有新增 沒有就新增 
        //有直接新增課程詳細    
        $tmp_order = '';
        $tmp_limit = " ";
        $tmp_where = "where a.lesson_day ='$lesson_day' and teach_id ='$teach_id'";
        $sql = "select * from ".$table_first_name."lesson as a 
            ".$tmp_where.$tmp_order.$tmp_limit;
        $tmp_arr['tmp_arr'] = $tmp_obj->queryAll($sql);
    if(count($tmp_arr['tmp_arr']) ==0){
        //新增課程
        

        //get
        $id=$this->laout_check($_REQUEST['id']);
        $this_week_path=$this->laout_check($_REQUEST['this_week_path']);
        $query_arr=null;
        $query_arr['post_date']=date('Y-m-d H:i:s');
        $query_arr['update_date']=date('Y-m-d H:i:s');
        $query_arr['teach_id']=$teach_id;
        $query_arr['lesson_day']=$lesson_day;
        $query_arr['id']=$this->get_uuid();
        $lesson_id = $query_arr['id'];

       //end get          
        $table_name="lesson";  
        //$rs = DI()->notorm->$table_name->insert($query_arr);
        //$rs = DI()->notorm->$table_name->insert($query_arr);
        $sql = "INSERT INTO plf_lesson
        (`id`, `teach_id`, `lesson_day`, `post_date`, `update_date`, `last_sql`, `teach_state`) 
        VALUES ('".$query_arr['id']."', 
        '".$query_arr['teach_id']."', 
        '".$query_arr['lesson_day']."', 
        '".$query_arr['post_date']."', '".$query_arr['update_date']."', 
        NULL, '0');";

        $rs = $tmp_obj->queryAll($sql);
       
        
        //print_r($rs);
        //exit;

        $rs = DI()->notorm->$table_name->where('teach_id',$teach_id)->order("post_date desc")->limit("1")->fetchAll();
                //寫入詳細課程

                //zoomopen
                /*
    public function meeting_booking($topic,
    $start_time,$duration,$timezone,
    */
    
                $booking_UTC_select_time = gmdate("Y-m-d\TH:i:s\Z",strtotime($booking_select_time));
                $tmp_room =$this->ZoomApiTw->meeting_booking('Appointment',$booking_UTC_select_time,30);
                

                $tmp_room = json_decode(json_encode($tmp_room), True);
                $teach_url  = $tmp_room['result']['data']['join_url'];
                $student_url  = $tmp_room['result']['data']['join_url'];
                $room_id  = $tmp_room['result']['data']['id'];
                //zoomopen


                   $table_name="lesson_data";  
                $query_arr=null;    
                $query_arr['id_sha1']=sha1($teach_id.'_'.$lesson_id.$can_tutor_time);            
                $query_arr['article_link'] = $article_link;
                $query_arr['post_date']=date('Y-m-d H:i:s');
                $query_arr['update_date']=date('Y-m-d H:i:s');
                $query_arr['student_id']=$student_id;
                $query_arr['can_tutor_time']=$can_tutor_time;
                $query_arr['cause']=$cause;
                $query_arr['use_point']=$need_point;
                $query_arr['lesson_id']=$lesson_id;
                $query_arr['teach_room_url']=$teach_url;
                $query_arr['student_room_url']=$student_url;
                $query_arr['type']=$type;
                $query_arr['room_id']=$room_id;
                $query_arr['id']=$this->get_uuid();
                if($this -> student_point($student_id,-$need_point)){  
                    $rs= DI()->notorm->$table_name->insert($query_arr);
                      if($rs['post_date'] !=''){
                            $rs['msg_text'] =T('BookingSuccess');
                            $rs['update_time'] =date('Y-m-d H:i:s');
                            $rs['booking_select_time'] =$lesson_fend_day;
                            $rs['booking_hour_time'] =$booking_hour_time;
                            $rs['nick_name'] =$nick_name;
                            $rs['msg_state'] ='Y';
                        return $rs;     
                    }
                }else{ 
                        $rs['msg_text'] =T('NoPoint');
                        $rs['update_time'] =date('Y-m-d H:i:s');
                        $rs['booking_select_time'] =$lesson_fend_day;
                        $rs['booking_hour_time'] =$booking_hour_time;
                        $rs['nick_name'] =$nick_name;
                        $rs['msg_state'] ='N';
                    return $rs;         
                
                }
                
         
        
           
    }else{
        //寫入詳細課程

                //zoomopen

                $booking_UTC_select_time = gmdate("Y-m-d\TH:i:s\Z",strtotime($booking_select_time));
                $tmp_room =$this->ZoomApiTw->meeting_booking('Appointment',$booking_UTC_select_time,30);
                
                $tmp_room = json_decode(json_encode($tmp_room), True);
                $teach_url  = $tmp_room['result']['data']['join_url'];
                $student_url  = $tmp_room['result']['data']['join_url'];
                $room_id  = $tmp_room['result']['data']['id'];
                //zoomopen

                $lesson_id = $tmp_arr['tmp_arr'][0]['id'];
                   $table_name="lesson_data";  
                $query_arr=null;    
                $query_arr['id']=$this->get_uuid();
                $query_arr['id_sha1']=sha1($teach_id.'_'.$lesson_id.$can_tutor_time);            
                $query_arr['article_link'] = $article_link;
                $query_arr['post_date']=date('Y-m-d H:i:s');
                $query_arr['update_date']=date('Y-m-d H:i:s');
                $query_arr['student_id']=$student_id;
                $query_arr['can_tutor_time']=$can_tutor_time;
                $query_arr['cause']=$cause;
                $query_arr['use_point']=$need_point;
                $query_arr['lesson_id']=$lesson_id;
                $query_arr['teach_room_url']=$teach_url;
                $query_arr['student_room_url']=$student_url;
                $query_arr['type']=$type;
                $query_arr['room_id']=$room_id;
                if($this -> student_point($student_id,-$need_point)){    
                    $rs = DI()->notorm->$table_name->insert($query_arr);

                      if($rs['post_date'] !=''){
                            $rs['msg_text'] =T('BookingSuccess');
                            $rs['update_time'] =date('Y-m-d H:i:s');
                            $rs['booking_select_time'] =$lesson_fend_day;
                            $rs['booking_hour_time'] =$booking_hour_time;
                            $rs['nick_name'] =$nick_name;
                            $rs['msg_state'] ='Y';
                        return $rs; 
                        }    
                }else{
                        $rs['msg_text'] =T('NoPoint');
                        $rs['update_time'] =date('Y-m-d H:i:s');
                        $rs['booking_select_time'] =$lesson_fend_day;
                        $rs['booking_hour_time'] =$booking_hour_time;
                        $rs['nick_name'] =$nick_name;
                        $rs['msg_state'] ='N';
                    return $rs;         
                
                }                                
    }
        
        
}


    function get_month_arr($month_arr,$start_day=1,$end_date,$today_year,$today_month,$tmp_teach_arr,$tmp_can_tutor_id =null){
        //補月份開始的空格
        $tmp_date = $today_year.'-'.$today_month.'-'.'01';
        $month_num = -date('w',strtotime("$tmp_date")); 
        //$this->de_array($tmp_arr['tmp_teach_arr']);
        $tmp_date =null;
        for ($i = -1; $i >= $month_num; $i--) {
                    $tmp_arr['month_arr'][$today_year][$today_month][$i]=0;    
        }
        //end補月份開始的空格
    $tmp_arr['tmp_teach_arr'] = $tmp_teach_arr;
/*
print_r($tmp_arr['tmp_teach_arr']);
exit;
*/

    if($tmp_arr['tmp_teach_arr'])
        for ($i = 1; $i <= $end_date; $i++) {
            //日期秒數1
            $today_sec = strtotime($today_year.'-'.$today_month.'-'.$i);
            $today_date = $today_year.'-'.$today_month.'-'.$i;
             
            if($start_day>$i){
                $tmp_arr['month_arr'][$today_year][$today_month][$i]=0;    
            }

                if(isset($tmp_arr['tmp_teach_arr'])) foreach($tmp_arr['tmp_teach_arr'] as $key1 => $value1){ 
                
                    //老師到期日
                    if($today_sec < strtotime($value1['start_date'])){
                        continue;
                    }
                    if($today_sec > strtotime($value1['end_date'])){
                        continue;
                    }
                    //老師到期日
                
            //日期秒數2
                $lesson_day_sec = strtotime($value1['lesson_day']);
                $end_date_sec = strtotime($value1['end_date']);
                
                $day_num = date('w',strtotime("$today_date")); 
                if($day_num ==0)$day_num =null;
                $can_tutor_time = 'can_tutor_time'.($day_num);

                //差集
                $tmp_arr0 = explode(',',$value1[$can_tutor_time]);
                $tmp_arr1 = explode(',',$value1['tmp_booking_tutor_time']);
                $tmp_arr2 = explode(',',$value1['tmp_leave_tutor_time']);
                $tmp_arr_ready = array_diff($tmp_arr0,$tmp_arr1,$tmp_arr2);
                //$tmp_arr =null;
                
                //當天實際可預約時間
                
                if($i == $start_day && $tmp_can_tutor_id != null){
                    if(isset($tmp_arr0))
                    foreach($tmp_arr0 as $key2 => $value2){
                        if($value2>=$tmp_can_tutor_id){
                            $tmp_x_arr[] =$value2;
                        }
                    }
                        $tmp_arr0 = $tmp_x_arr;
                        if($tmp_arr0 ==null)
                        $tmp_arr0 = array();
                        $tmp_x_arr =null;
                    if(isset($tmp_arr_ready))
                    foreach($tmp_arr_ready as $key2 => $value2){
                        if($value2>=$tmp_can_tutor_id){
                            $tmp_x_arr[] =$value2;
                        }
                    }                        
                        $tmp_arr_ready = $tmp_x_arr;
                        if($tmp_arr_ready ==null)
                        $tmp_arr_ready = array();
                        $tmp_x_arr =null;
                        
                }
                
                  natsort($tmp_arr0);
                  natsort($tmp_arr_ready);

                //end 當天實際可預約時間
                 //可以booking 為0的 continue (包含當天被booking完以及本來就沒有開時間)
            //if($today_sec > $end_date_sec || $start_day>$i || ( count($tmp_arr_ready) == 0 && $today_sec == $lesson_day_sec  ) || count($tmp_arr0) ==0 || $tmp_arr0[0] == null ){
                //可以booking 為0的 continue (只包含本來就沒有開時間)
            if($today_sec > $end_date_sec || $start_day>$i  || count($tmp_arr0) ==0 || $tmp_arr0[0] == null ){
                continue;
                //到期日超過 or 可以booking的時間為0 的寫進來
            }
            elseif($value1['lesson_day'] ==null){    
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['id'] = $value1['id'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['nick_name'] = $value1['nick_name'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['sml_image'] = $value1['sml_image'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['image'] = $value1['image'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['birth_day'] = $value1['birth_day'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['exp'] = $value1['exp'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['gender'] = $value1['gender'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['nationality'] = $value1['nationality'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['major'] = $value1['major'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['can_totur_major'] = $value1['can_totur_major'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['expertise'] = $value1['expertise'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['occupation'] = $value1['occupation'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['interest'] = $value1['interest'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['education'] = $value1['education'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['need_point'] = $value1['need_point'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['end_date'] = $value1['end_date'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['teacher_category'] = $value1['teacher_category'];
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['can_tutor_time'] = $tmp_arr_ready;                                    
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_student_count'] = $value1['lesson_student_count'];    
                        

                        //$tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['old_can_tutor_time'] = $tmp_arr0;
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['text'] = $value1['text'];    
                        $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['school_name'] = $value1['school_name'];    

                                $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['tmp_booking_tutor_time'] = $value1['tmp_booking_tutor_time'];        
                                $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['tmp_leave_tutor_time'] = $value1['tmp_leave_tutor_time'];    
                                $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_day'] = $value1['lesson_day'];    
                                $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['rate_count'] = $value1['rate_count'];    
                                $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_count'] = $value1['lesson_count'];    
                    
                }
                else{

                //    if($tmp_id != $value1['id'] && $today_sec != $lesson_day_sec ){ //把不等於當天的先填一個空的老師狀態
                    if($tmp_id != $value1['id']){ //先填一個空的老師狀態
                        $tmp_id =$value1['id'];
                        $tmp_key = $key1;
                        if($tmp_key ==0){
                            $tmp_key ='0';
                        }
                        //exit;

                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['id'] = $value1['id'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['nick_name'] = $value1['nick_name'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['sml_image'] = $value1['sml_image'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['image'] = $value1['image'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['birth_day'] = $value1['birth_day'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['exp'] = $value1['exp'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['gender'] = $value1['gender'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['nationality'] = $value1['nationality'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['major'] = $value1['major'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['can_totur_major'] = $value1['can_totur_major'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['expertise'] = $value1['expertise'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['occupation'] = $value1['occupation'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['interest'] = $value1['interest'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['education'] = $value1['education'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['need_point'] = $value1['need_point'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['end_date'] = $value1['end_date'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['teacher_category'] = $value1['teacher_category'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['can_tutor_time'] = $tmp_arr0;    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_student_count'] = $value1['lesson_student_count'];    

                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['text'] = $value1['text'];    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['school_name'] = $value1['school_name'];            
                            
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['tmp_booking_tutor_time'] = $value1['tmp_booking_tutor_time'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['tmp_leave_tutor_time'] =$value1['tmp_leave_tutor_time'];
                            //$tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_day'] = $value1['lesson_day'];    //原本多天的狀況
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_day'] = null; ////showhi 只抓一天解決方案 ;    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['rate_count'] = $value1['rate_count'];    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$key1]['lesson_count'] = $value1['lesson_count'];
                            
                    }
                    if($tmp_id == $value1['id'] &&  $today_sec == $lesson_day_sec){
                        $tmp_id =$value1['id'];
                            if($tmp_key == null){
                                $tmp_key =$key1;
                                if($tmp_key ==0){
                                    $tmp_key ='0';
                                }                
                            }

                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['id'] = $value1['id'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['nick_name'] = $value1['nick_name'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['sml_image'] = $value1['sml_image'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['image'] = $value1['image'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['birth_day'] = $value1['birth_day'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['exp'] = $value1['exp'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['gender'] = $value1['gender'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['nationality'] = $value1['nationality'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['major'] = $value1['major'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['can_totur_major'] = $value1['can_totur_major'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['expertise'] = $value1['expertise'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['occupation'] = $value1['occupation'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['interest'] = $value1['interest'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['education'] = $value1['education'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['need_point'] = $value1['need_point'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['end_date'] = $value1['end_date'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['teacher_category'] = $value1['teacher_category'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['can_tutor_time'] = $tmp_arr_ready;
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['lesson_student_count'] = $value1['lesson_student_count'];                            
                            //$tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['can_tutor_time'] = $tmp_arr;        

                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['text'] = $value1['text'];    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['school_name'] = $value1['school_name'];
                            
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['old_can_tutor_time'] = $tmp_arr0;        
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['tmp_booking_tutor_time'] = $value1['tmp_booking_tutor_time'];        
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['tmp_leave_tutor_time'] = $value1['tmp_leave_tutor_time'];    
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['lesson_day'] = $value1['lesson_day'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['rate_count'] = $value1['rate_count'];
                            $tmp_arr['month_arr'][$today_year][$today_month][$i][$tmp_key]['lesson_count'] = $value1['lesson_count'];        

                    }        
                }
                /*
                echo $value1['id'].'<br>';
            echo 'Today:'.$today_date.':'.$today_sec.'<br>';
            echo 'less_day:'.$value1['lesson_day'].':'.$lesson_day_sec.'<br>';*/
            }//老師迴圈結束

            /*
            print_r($tmp_arr['month_arr'][$today_year][$today_month][$i]);
            echo $i;
            */
            /*    
            if(!isset($tmp_arr['month_arr'][$today_year][$today_month][$i])){
                $tmp_arr['month_arr'][$today_year][$today_month][$i]=0;
            }*/
            


            $tmp_key =null;
            $tmp_id =null;
        }//1234
            //exit;
            //將月曆表的老師陣列中把沒空的老師剔除（用新陣列）
            /*
                if(is_array($tmp_arr['month_arr'][$today_year][$today_month]))
                foreach($tmp_arr['month_arr'][$today_year][$today_month] as $key => $value){
                    if($value ==0)
                    $tmp_arr['tmp_month_arr'][$today_year][$today_month][$key]=0; //
                    if(is_array($value))
                        foreach($value as $key1 =>$value1 ){
                            if($value1['can_tutor_time'] == null){
                                //沒有空
                            }else{
                            $tmp_arr['tmp_month_arr'][$today_year][$today_month][$key][$key1]=$value1;
                            }
                        }
                }
                */
//exit;


        /*$tmp_arr['month_arr'][$today_year][$today_month] = 
        $this->takeout_room_max_time($tmp_arr['month_arr'][$today_year][$today_month]);*/
        return $tmp_arr['month_arr'];

    }

    
    

    
/** 未使用 **/    
    
    
    function student_point($student_id,$add_point=1){
        //get
        $tmp_arr['tmp_arr'] =null;
          //end get   
        //內部確認

            $student_id = $this->laout_check($student_id);
            $add_point = $this->laout_check($add_point);
        $tmp_table='teach';
        $tmp_arr['tmp_arr'] = DI()->notorm->$tmp_table->select('*')
        ->where('id =?',$student_id)->fetchAll();

        
           if(count($tmp_arr['tmp_arr']) !=1){
                $rs['msg_text'] =T('SystemError');
                $rs['update_time'] =date('Y-m-d H:i:s');
                $rs['msg_state'] ='N';
                return $rs; 

           }

           if($tmp_arr['tmp_arr'][0]['point']+ $add_point <0){

                $rs['msg_text'] =T('NoPoint');
                $rs['update_time'] =date('Y-m-d H:i:s');
                $rs['msg_state'] ='N';
                return $rs; 
           }
           $tmp_arr['tmp_arr'] =null;
        //end 內部確認  
        $data = array('point'  => new NotORM_Literal("point + $add_point"));
        $rs = DI()->notorm->teach->where('id',$student_id)->update($data);
            return $rs;
    }  
    
    

    

}
