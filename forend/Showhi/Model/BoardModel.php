<?php

class Model_BoardModel extends Common_ModelDefaultList {
    
    //ex 有用到
    public function getByUserId($userId) {
        return $this->getORM()
            ->select('*')
            ->where('id = ?', $userId)
            ->fetch();
    }


    public function getByUserIdWithCache($userId) {
        $key = 'userbaseinfo_' . $userId;
        //要自己寫 原本的沒有宣告 cache
        DI()->cache = new PhalApi_Cache_File(array('path' => API_ROOT . '/Runtime', 'prefix' => 'demo'));
        //
        $rs = DI()->cache->get($key);
        if ($rs === NULL) {
            $rs = $this->getByUserId($userId);
            DI()->cache->set($key, $rs, 600);
        }
        return $rs;
    }
    //end ex
    
    //建構子
    public function __construct() {   
        parent::__construct(); 
        $this->Counter = new Common_CounterLikeShare();
    }
    
    public function __destruct(){

    }
    
    
    public function getTableName($id) {
        return "student";
    }
    
    public function BoardList($query_arr){

        //get 
            $user_id = $this->laout_check(DI()->request->get('user_id'));
       //end get
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.user_id =:user_id and a.approval='Y'  ";

        $tmp_order = " order by a.post_date desc ";
        $tmp_limit = " ";
        $sql_count = "select a.*,a.attached_id attached_name,a.in_cloud,b.nick_name,b.image from ".$table_first_name."board  as a
                        left join ".$table_first_name."teach as b on a.post_id =b.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
                
        //抓資料sql
        $sql = $sql_count." limit :limit_start , :limit_num ";

        //分頁
        $params = array(':user_id' => $user_id);
        $tmp_count['board'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['board']);
           $this -> page_rec = 10;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁
        $params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $tmp_arr['board'] = $tmp_obj->queryAll($sql,$params);
        

        //呼叫livehousein 放入迴圈
        //抓出原始的fileid
	    if($tmp_arr['board']){
            $tmp_arr['file_arr'] =array();
            
            foreach($tmp_arr['board'] as $key => $value ){


                //if($value['live_video_id'] ==null &&  $value['end_live'] == 'Y' ){
                if($value['live_video_id'] ==null && $value['live_id'] != null  ){
                    $rs = $this->Livehousein->GetLiveVideoId($value['live_id']);  
                    if($rs['result']['videos']["0"]->id !=null){  
                        $_REQUEST['jsondata'] =array (
                            "status" =>"ended",
                         );

                        $_REQUEST['live_id'] = $value['live_id'];                         
                         DI()->set('jsondata',$_REQUEST['jsondata']);
                         DI()->set('live_id',$_REQUEST['live_id']);
                        $this->Livehousein->UpdateLive();
                        $query_arr =null;
                        $query_arr['end_live'] ='Y';
                        $query_arr['live_video_id'] =$rs['result']['videos']["0"]->id;
                        $table_name="board";                          
                        DI()->notorm->$table_name->where('id = ?',$value['id'])->update($query_arr);  
                        $tmp_arr['board'][$key]['live_video_id'] = $rs['result']['videos']["0"]->id;            
                    }
                    elseif($rs['result']["started_at"] == null && strtotime($rs['result']["start_time"]."+5 minutes") < strtotime("now")  ){
                        $query_arr =null;
                        $query_arr['approval'] ='N';
                        $table_name="board";                          
                        DI()->notorm->$table_name->where('id = ?',$value['id'])->update($query_arr);  
                        $tmp_arr['board'][$key]['in_cloud'] = 'N';
                    }elseif($rs['result']["ended_at"] != null && $value['user_id'] == $user_id ){

                        $query_arr =null;
                        $query_arr['end_live'] ='Y';
                        $table_name="board";                          
                        DI()->notorm->$table_name->where('id = ?',$value['id'])->update($query_arr);  
                        //$tmp_arr['board'][$key]['in_cloud'] = 'N';

                        $_REQUEST['jsondata'] =array (
                            "status" =>"ended",
                         );
                        $_REQUEST['live_id'] = $value['live_id'];                         
                         DI()->set('jsondata',$_REQUEST['jsondata']);
                         DI()->set('live_id',$_REQUEST['live_id']);
                        $this->Livehousein->UpdateLive();
                        $query_arr =null;
                        $query_arr['live_video_id'] =$rs['result']['videos']["0"]->id;
                        $table_name="board";                          
                        DI()->notorm->$table_name->where('id = ?',$value['id'])->update($query_arr);  
                        $tmp_arr['board'][$key]['live_video_id'] = $rs['result']['videos']["0"]->id;            
                    }
                }
                //end livehouse in 
                
                //抓board arr
                $tmp_arr['board_arr'][] = $value['id'];
                //抓fileid to array
                $tmp_file_arr = explode(',',$tmp_arr['board'][$key]['attached_name']);
                foreach($tmp_file_arr as $key_file => $value_file ){
                    if($value_file !=0)
                    $tmp_arr['file_arr'][] = $value_file;
                }
            
            }
        }
        
        //end 抓出原始的file id
        //end 呼叫livehousein 放入迴圈
        
        $replace_arr=array('image');
        $tmp_arr['board'] =  $this->RePlaceFilename($tmp_arr['board'],$replace_arr,'post_id');
        
        $replace_arr=array('attached_name');
        $tmp_arr['board'] =  $this->RePlaceMutileFilename($tmp_arr['board'],$replace_arr,'user_id');
        

        $tmp_in_id_arr ='';
	    if($tmp_arr['board']){
            foreach($tmp_arr['board'] as $key => $value ){
                $tmp_in_id_arr[] ="'".$value['id']."'";
            }
            $tmp_in_id =implode(",",$tmp_in_id_arr);
            $tmp_where_in = "and b.board_id in( $tmp_in_id )";
        }
        $tmp_where = " where a.user_id =:user_id $tmp_where_in  and a.approval='Y' and b.approval='Y' ";
        $tmp_where_in =null;
        $tmp_order = " order by b.post_date desc ";
        $tmp_limit = " ";
        $sql_count = "select b.*,b.attached_id attached_name,c.nick_name,c.image from ".$table_first_name."board  as a
                        inner join ".$table_first_name."board_data as b on a.id =b.board_id
                        inner join ".$table_first_name."teach as c on b.re_user_id =c.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;

        //分頁
        $params = array(':user_id' => $user_id);
        $tmp_count['board_data'] = $tmp_obj->queryAll($sql_count,$params);

        /*
        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['board_data']);
           $this -> page_rec = 10;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        */
        
        //end 分頁
        //$params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $params = array(':user_id' => $user_id);
        $tmp_arr['board_data'] = $tmp_obj->queryAll($sql,$params);

        
        $replace_arr=array('image');
        $tmp_arr['board_data'] =  $this->RePlaceFilename($tmp_arr['board_data'],$replace_arr,'re_user_id');

        $replace_arr=array('attached_name');
        $tmp_arr['board_data'] =  $this->RePlaceMutileFilename($tmp_arr['board_data'],$replace_arr,'re_user_id');
        

        //轉
        foreach( $tmp_arr['board'] as $key => $value ) {
            $match = $this->Linkinfo->checkurl($value['text']);
            if($match !=null){
                $rs = $this->Linkinfo->getLinkinfo($match[0]);    
                $tmp_arr['board'][$key]['web_title'] = $rs['title'];
                $tmp_arr['board'][$key]['web_desc']  = $rs['description'];
                $tmp_arr['board'][$key]['web_image'] = $rs['image'];
                $tmp_arr['board'][$key]['web_url'] = $rs['web_url'];

                $tmp="<a target='_blank' href=".$rs['web_url']."> <div class='append_image'>".
                    "<div><img src='".$rs['image']."' /></div>".
                    "<div class='msgUsbTitle'>".$rs['title']."</div>".
                    "<div class='msgUsbCon'>".$rs['description']."</div>".   
                "</div></a>";
                $tmp_arr['board'][$key]['tmp'] = $tmp;
                //$tmp_arr['board'][$key]['text'] =$tmp_arr['board'][$key]['text'].$tmp;
                
            }            
        }
        return $tmp_arr;
        /*
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
         */       
    }
    
    
    public function BoardListMedia($query_arr){

        //get 
            $user_id = $this->laout_check(DI()->request->get('user_id'));
       //end get
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where a.user_id =:user_id and !isnull(attached_id) ";

        $tmp_order = " group by attached_id,read_permi
					  order by a.post_date desc ";
        $tmp_limit = " ";
        $sql_count = "select a.*,a.attached_id attached_name,b.nick_name,b.image from ".$table_first_name."board  as a
                        left join ".$table_first_name."teach as b on a.user_id =b.id
                        left join ".$table_first_name."fan as c on a.user_id =c.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
                
        //抓資料sql
        $sql = $sql_count." limit :limit_start , :limit_num ";

        //分頁
        $params = array(':user_id' => $user_id);
        $tmp_count['board'] = $tmp_obj->queryAll($sql_count,$params);

        $this -> page_num = $_REQUEST['page_num'];
           $this -> page_total_row = count($tmp_count['board']);
           $this -> page_rec = 9;    
           $this -> page = $this -> page_tool();

          $limit_start = $this -> page_start;
          $limit_num = $this -> page_rec;
        $tmp_arr['page'] = $this->page;
        //end 分頁
        $params = array(':user_id' => $user_id,':limit_start' => $limit_start ,':limit_num' => $limit_num);
        $tmp_arr['board_media'] = $tmp_obj->queryAll($sql,$params);
        $replace_arr=array('attached_name','image');
        $tmp_arr['board_media'] =  $this->RePlaceFilename($tmp_arr['board_media'],$replace_arr,'user_id');
        
        return $tmp_arr;
        /*
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
         */       
    }
    
    public function BoardPost($query_arr){
        //get 
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get

       
        //確認權限
        if($fan_id =='' || $fan_id =='null'){
            $fan_id =$user_id;
            $savePath ='/teach/';
        }else{
            $savePath ='/fan/';
        }
        //end 
        
       //upload

            if($query_arr['type'] =='1'){
                array_splice($_FILES,0,1);
            }
            

            foreach($_FILES as $key => $value){
                if($value['size'] !=0){
                        $field_name = 'BoardPost:user_id:'.$user_id;
                        $file_id =$file_id.$this->FileUpload->fileUpload($value,$field_name,$savePath,$fan_id).',';
                }
            }
            //去除最後一個逗號
            $file_id = substr($file_id,0,-1);
            //如果為影片在board寫上 in_cloud ='Y'
            if($query_arr['type'] =='2'){
                //單影片判斷 ，多影片直接註解
                
                $tmp_arr = explode(',',$file_id);
                
                $rs = DI()->notorm->files->select('*')
                    ->where(' id = ? ', $tmp_arr[0])->fetchAll();
                if($rs['0']['cloud_id'] != null){
                    $tmp_cloud_id = $rs['0']['cloud_id'];
                    $tmp_cloud_down_time = $rs['0']['cloud_down_time'];
                    $tmp_cloud_down_sec = $rs['0']['cloud_down_sec'];
                    $query_arr['in_cloud'] = 'Y';  
                }
            }
       //end upload
       
       
       
        //set query

            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['user_id'] = $fan_id;
            $query_arr['post_id'] = $user_id;
            $query_arr['loginid'] = $loginid;
            $query_arr['attached_id'] = $file_id;
        //end query 



        $table_name="board";  
        $rs = DI()->notorm->$table_name->insert($query_arr);
        $query_arr =null;
        $tmp_rs = DI()->notorm->$table_name->select('*')->where(' user_id = ? ', $fan_id)
        ->order('post_date desc ')->limit(1)->fetchOne();
        
        if(count($rs) !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['video_id'] =$tmp_cloud_id;
            $rs['id'] =$tmp_rs[0]['id'];
            $rs['cloud_down_time'] =$tmp_cloud_down_time;
            $rs['cloud_down_sec'] =$tmp_cloud_down_sec;
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }
    

    
    public function BoardChangeRead($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
       //end get

       //upload
            $field_name = 'BoardChangeRead:user_id:'.$user_id;
            if($_FILES["file"]["size"] !=0)
            $file_id = $this->FileUpload->fileUpload($_FILES["file"],$field_name);
       //end upload
       
        //set query
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        

        $table_name="board";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }
    
    
    public function BoardDelete($query_arr){
        
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
       //end get

       //upload
            $field_name = 'BoardChangeRead:user_id:'.$user_id;
            if($_FILES["file"]["size"] !=0)
            $file_id = $this->FileUpload->fileUpload($_FILES["file"],$field_name);
       //end upload
       
        //set query
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['approval'] = 'N';
        //end query 
        

        $table_name="board";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }

    public function ReBoardDelete($query_arr){
        
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
       //end get

       //upload
            $field_name = 'ReBoardDelete:user_id:'.$user_id;
            if($_FILES["file"]["size"] !=0)
            $file_id = $this->FileUpload->fileUpload($_FILES["file"],$field_name);
       //end upload
       
        //set query
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['approval'] = 'N';
        //end query 
        

        $table_name="board_data";  
        $rs = DI()->notorm->$table_name->where('id',$id)->update($query_arr);
        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('ReBoardDelete');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    }

    //ReBoardDelete
    public function BoardEditUP($query_arr){
        //get 
            $id = $this->laout_check(DI()->request->get('id'));
            $user_id = $_SESSION['f_backend']['user_id'];
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
            $delindex = $this->laout_check(DI()->request->get('delindex'));
            $attached_id = $this->laout_check(DI()->request->get('attached_id'));
       //end get

        if($delindex != null){
                //刪除檔案陣列化
                $del_arr =explode(',',$delindex);
                //原本檔案陣列化
                $att_arr =explode(',',$attached_id);

                //刪除檔案
                foreach($del_arr as $key => $value ){
                    $att_arr[$value] =null;

                }
                
                foreach($att_arr as $key => $value ){
                    if($value != null)
                    $new_att_arr[$key] =$value;
                }

           //轉回陣列,並設定寫入
           if(count($new_att_arr) >0){
               $attached_id =implode(',',$new_att_arr);           
           }else{
               $attached_id ='';           
           }

        }
        
        if($fan_id =='' || $fan_id =='null'){
            $fan_id =$user_id;
            $savePath ='/teach/';
        }else{
            $savePath ='/fan/';
        }
       //upload    
            array_splice($_FILES,0,1);
            foreach($_FILES as $key => $value){
                if($value['size'] !=0){
                        $field_name = 'BoardPost:user_id:'.$user_id;
                        $file_id =$file_id.$this->FileUpload->fileUpload($value,$field_name,$savePath,$fan_id).',';
                }
            }
            //去除最後一個逗號
            if($file_id !=null){
                $file_id = substr($file_id,0,-1);
                //就圖片id 加新圖片
                if($attached_id !=null)
                $attached_id = $attached_id.','.$file_id;
                else
                $attached_id = $file_id;


            }

       //end upload
       
        //set query
            $query_arr['attached_id'] =$attached_id;
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 


        $table_name="board";  
        $rs = DI()->notorm->$table_name->where('id = ? AND user_id = ?',$id,$fan_id)->update($query_arr);
        $tmp_arr['board'] = DI()->notorm->$table_name->select('* ,attached_id attached_name')
        ->where('id = ? AND user_id = ?',$id,$fan_id)->fetchAll();
        
        $replace_arr=array('attached_name');
        $tmp_arr['board'] =  $this->RePlaceMutileFilename($tmp_arr['board'],$replace_arr,'user_id');
        
        foreach( $tmp_arr['board'] as $key => $value ) {
            $match = $this->Linkinfo->checkurl($value['text']);
            if($match !=null){
                $rs = $this->Linkinfo->getLinkinfo($match[0]);    
                $tmp_arr['board'][$key]['web_title'] = $rs['title'];
                $tmp_arr['board'][$key]['web_desc']  = $rs['description'];
                $tmp_arr['board'][$key]['web_image'] = $rs['image'];
                $tmp_arr['board'][$key]['web_url'] = $rs['web_url'];

                $tmp="<a target='_blank' href=".$rs['web_url']."> <div class='append_image'>".
                    "<div><img src='".$rs['image']."' /></div>".
                    "<div class='msgUsbTitle'>".$rs['title']."</div>".
                    "<div class='msgUsbCon'>".$rs['description']."</div>".   
                "</div></a>";
                $tmp_arr['board'][$key]['tmp'] = $tmp;
                //$tmp_arr['board'][$key]['text'] =$tmp_arr['board'][$key]['text'].$tmp;
                
            }            
        }

        $tmp_arr['board']['sql_state'] ='Success';
        $tmp_arr['board']['msg_text'] =T('BoardEditUP');
        $tmp_arr['board']['msg_state'] ='Y';
        $tmp_arr['board']['update_time'] =date('Y-m-d H:i:s');
      
      return $tmp_arr['board']['0'];        
    }

    public function BoardEndLiveUP($query_arr){
        
        //get 
            $id = $this->laout_check(DI()->request->get('live_id'));
            $video_id = $this->laout_check(DI()->request->get('video_id'));
            $user_id = $_SESSION['f_backend']['user_id'];
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get

        //確認權限
        if($fan_id =='' || $fan_id =='null'){
            $fan_id =$user_id;
        }
        
       //upload
            $field_name = 'BoardEndLiveUP:user_id:'.$user_id;
            if($_FILES["file"]["size"] !=0)
            $file_id = $this->FileUpload->fileUpload($_FILES["file"],$field_name);
       //end upload
       
        //set query
            $query_arr['end_live'] = 'Y';
            $query_arr['live_video_id'] = $video_id;
            $query_arr['update_date'] = date('Y-m-d H:i:s');
        //end query 
        
        $table_name="board";  
        $rs = DI()->notorm->$table_name->where('live_id = ? AND user_id = ?',$id,$fan_id)->update($query_arr);

        
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardEndLiveUP');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        }
                
    } 


    public function ReBoardPost($query_arr){
        //get 
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get

       
        //確認權限
        if($fan_id =='' || $fan_id =='null'){
            $fan_id =$user_id;
            $savePath ='/teach/';
        }else{
            $savePath ='/fan/';
        }
        //end 
        
       //upload
       /*
            if($query_arr['type'] =='1'){
                array_splice($_FILES,0,1);
            }
            */


       //upload
            foreach($_FILES as $key => $value){
                if($value['size'] !=0){
                        $field_name = 'ReBoardPost:user_id:'.$user_id;
                        $file_id =$file_id.$this->FileUpload->fileUpload($value,$field_name,$savePath,$fan_id).',';
                }
            }

       //end upload
            //去除最後一個逗號
            if($file_id !=null){
                $file_id = substr($file_id,0,-1);
                //就圖片id 加新圖片
                if($attached_id !=null)
                $attached_id = $attached_id.','.$file_id;
                else
                $attached_id = $file_id;

                $query_arr['type'] =1;
            }

       //end upload
       
        //set query
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['re_user_id'] = $user_id;
            $query_arr['attached_id'] = $file_id;
        //end query 
        


        $table_name="board_data";  
        $rs = DI()->notorm->$table_name->insert($query_arr);
        $rs = DI()->notorm->$table_name->select('*')->order('post_date desc')->fetchOne();
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where b.id =:id ";
        $tmp_order = " order by b.post_date desc ";
        $tmp_limit = " ";
        $sql_count = "select b.*,c.nick_name,c.image,b.attached_id,b.attached_id attached_name from ".$table_first_name."board  as a
                        inner join ".$table_first_name."board_data as b on a.id =b.board_id
                        inner join ".$table_first_name."teach as c on b.re_user_id =c.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;
        
        $params = array(':id' => $rs['id']);
        $rs = $tmp_obj->queryAll($sql_count,$params);
        $replace_arr=array('image');
        $rs =  $this->RePlaceFilename($rs,$replace_arr,'re_user_id');

        $replace_arr=array('attached_name');
        $rs =  $this->RePlaceMutileFilename($rs,$replace_arr,'re_user_id');

        return $rs;
        /*
        if($rs['id'] !=0){
            $rs['sql_state'] ='Success';
            $rs['msg_text'] =T('BoardPost');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;
        } 
        */
    }

    public function ReBoardEditUP($query_arr){
        //get 
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            $id = $this->laout_check(DI()->request->get('id'));
       //end get

        //確認權限
        if($fan_id =='' || $fan_id =='null'){
            $fan_id =$user_id;
            $savePath ='/teach/';
        }else{
            $savePath ='/fan/';
        }
        //end 
        
       //upload
       /*
            if($query_arr['type'] =='1'){
                array_splice($_FILES,0,1);
            }
            */


       //upload
            foreach($_FILES as $key => $value){
                if($value['size'] !=0){
                        $field_name = 'ReBoardPost:user_id:'.$user_id;
                        $file_id =$file_id.$this->FileUpload->fileUpload($value,$field_name,$savePath,$fan_id).',';
                }
            }

       //end upload
            //去除最後一個逗號
            if($file_id !=null){
                $file_id = substr($file_id,0,-1);
                //就圖片id 加新圖片
                if($attached_id !=null){
                $attached_id = $attached_id.','.$file_id;
                }
                else{
                $attached_id = $file_id;
                }
                //$query_arr['attached_id'] = $file_id;
            }
                $query_arr['type'] =1;
       //end upload

        //set query
           // $query_arr['update_date'] = date('Y-m-d H:i:s');
           // $query_arr['post_date'] = date('Y-m-d H:i:s');
            $query_arr['re_user_id'] = $user_id;

        //end query 



        $table_name="board_data";  
        //->where('id = ?',$value['id'])->update($query_arr);  
        $rs = DI()->notorm->$table_name->where('id = ?',$id)->update($query_arr);
        $rs = DI()->notorm->$table_name->select('*')->order('post_date desc')->fetchOne();
        
        $tmp_obj = $this->getORM();
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $tmp_where = " where b.id =:id ";
        $tmp_order = " order by b.post_date desc ";
        $tmp_limit = " ";
        $sql_count = "select b.*,c.nick_name,b.attached_id attached_name,c.image from ".$table_first_name."board  as a
                        inner join ".$table_first_name."board_data as b on a.id =b.board_id
                        inner join ".$table_first_name."teach as c on b.re_user_id =c.id
                ".$tmp_where.$tmp_order.$tmp_limit;   
        //抓資料sql
        //$sql = $sql_count." limit :limit_start , :limit_num ";
        $sql = $sql_count;
        
        $params = array(':id' => $rs['id']);
        $rs = $tmp_obj->queryAll($sql_count,$params);
        $replace_arr=array('image');
        $rs =  $this->RePlaceFilename($rs,$replace_arr,'re_user_id');
        
        $replace_arr=array('attached_name');
        $rs =  $this->RePlaceMutileFilename($rs,$replace_arr,'re_user_id');


        return $rs;

    } 
    
    //通知功能
    public function sendSubscription($query_arr){
        //get 
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            $fan_id = $this->laout_check(DI()->request->get('fan_id'));
       //end get

       
        //確認權限 
        if($fan_id =='' || $fan_id =='null'){
            //return; //關閉一般身分的人的關注通知
            $fan_id =$user_id;
        }
        //end 
        
        
                //訂閱者通知 for public
                $tmp_obj = $this->getORM();
                $table_first_name = DI()->TABLE_FIRST_NAME;
                
                        //取得ele_id 這裡為 board_id
                            $tmp_rs = DI()->notorm->board->select('*')
                            ->where('user_id = ?', $fan_id)->limit(1)->order('post_date desc')
                            ->fetch();
                            $board_id = $rs['id'];
                        //拉出訂閱者
                        
                    //訂閱者通知
                    $sql = "select a.*,b.email,c.image  from ".$table_first_name."user_friends as a 
                            inner join  ".$table_first_name."teach as b on b.id =a.user_id 
                            inner join  ".$table_first_name."fan as c on c.id =a.who_user_id
                            where who_user_id ='$fan_id' and type=2 and a.approval ='Y'";

                    $tmp_rs = $tmp_obj->queryAll($sql); 
                    $tmp_logo_image = $tmp_rs[0]['image'];
                    $receive_user_list = $tmp_rs;



                    //成員發通知
                    $sql = "select a.id , a.image,b.user_id,c.email  from ".$table_first_name."fan as a 
                            inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                            inner join  ".$table_first_name."teach as c on c.id =b.user_id
                            where a.id='$fan_id'";
                    $tmp_rs = $tmp_obj->queryAll($sql); 
                    //合併資料
                    $receive_user_list = array_merge($receive_user_list,$tmp_rs);
                        //寫入通知
                        //file_id 走留言板圖片  $tmp_logo_image 走粉絲團logo
                        $uuid = $this->get_uuid();
                        $this->Notice->sendSubscription($receive_user_list,$board_id,$fan_id
                        ,$query_arr['text'],$tmp_logo_image,'A','fan','teach','board',$user_id,$loginid,$uuid);
                //end 訂閱者通知
                
            if(count($tmp_rs) !=0){
                $tmp_rs['sql_state'] ='Success';
                $tmp_rs['msg_text'] =T('sendSubscription');
                $tmp_rs['msg_state'] ='Y';
                $tmp_rs['update_time'] =date('Y-m-d H:i:s');
                return $tmp_rs;
            }
        }

    public function sendFanMember($query_arr){
            //get 
                $user_id = $_SESSION['f_backend']['user_id'];
                $loginid = $_SESSION['f_backend']['loginid'];
                $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        //end get

        
            //確認權限 
            if($fan_id =='' || $fan_id =='null'){
                //return; //關閉一般身分的人的關注通知
                $fan_id =$user_id;
            }
            //end 
            
                    //成員通知 for private
                    $tmp_obj = $this->getORM();
                    $table_first_name = DI()->TABLE_FIRST_NAME;
                    
                            //取得ele_id 這裡為 board_id
                                $tmp_rs = DI()->notorm->board->select('*')
                                ->where('user_id = ?', $fan_id)->limit(1)->order('post_date desc')
                                ->fetch();
                                $board_id = $rs['id'];

                        //成員發通知
                        $sql = "select a.id , a.image,b.user_id,c.email  from ".$table_first_name."fan as a 
                                inner join  ".$table_first_name."fan_manage as b on b.fan_id =a.id 
                                inner join  ".$table_first_name."teach as c on c.id =b.user_id
                                where a.id='$fan_id'";
                        $receive_user_list = $tmp_obj->queryAll($sql); 

                            //寫入通知
                            //file_id 走留言板圖片  $tmp_logo_image 走粉絲團logo
                            $uuid = $this->get_uuid();
                            $this->Notice->sendSubscription($receive_user_list,$board_id,$fan_id
                            ,$query_arr['text'],$tmp_logo_image,'A','fan','teach','board',$user_id,$loginid,$uuid);
                    //end 成員通知
                    
                if(count($tmp_rs) !=0){
                    $tmp_rs['sql_state'] ='Success';
                    $tmp_rs['msg_text'] =T('sendFanMember');
                    $tmp_rs['msg_state'] ='Y';
                    $tmp_rs['update_time'] =date('Y-m-d H:i:s');
                    return $tmp_rs;
                }
        }

}
