<?php
class Domain_BoardDomain extends Common_DomainDefaultList  {

    public function __construct() { 
        parent::__construct(); 

    }
    
    public function __destruct(){

    }

    public function BoardList($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardList($query_arr);
            //exit;
            //寫入view count
            //board

            if(is_array($rs['board_arr'])){
                $model->Counter->add_view_count($rs['board_arr'],'board');
                $rs['board'] = $model->Counter->get_like_count($rs['board_arr'],$rs['board'],'board');
            }
            //file
            if(is_array($rs['board_arr'])){
                $model->Counter->add_view_count($rs['file_arr'],'files');
                $rs['board'] = $model->Counter->get_like_count($rs['file_arr'],$rs['board'],'files');
            }
            
            //print_r($rs['board']);
           //exit;
            
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function BoardListMedia($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardListMedia($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function BoardPost($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardPost($query_arr);


            if($rs['msg_state'] == 'Y'){ //post ok 
                if($query_arr['read_permi'] =='public'){
                    $tmp_rs = $model->sendSubscription($query_arr);
                }

                if($query_arr['read_permi'] =='private'){
                    $tmp_rs = $model->sendFanMember($query_arr);
                }  
                 
            }
        
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function CreateLive($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->Livehousein->CreateLive();
            $liveinfo = array();
            $liveinfo['title'] = $rs['result']['title'];
            $liveinfo['stream_key'] = $rs['result']['stream_key'];
            $liveinfo['embed_url'] = $rs['result']['embed_url'];
            $liveinfo['live_id']  = $rs['result']['id'];
            $liveinfo['msg_state']  = 'Y';
            $liveinfo['msg_text']  = 'Success';     
            return $liveinfo;
    }
    
    public function UpdateLive($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->Livehousein->UpdateLive();
            $liveinfo = array();
            $liveinfo['title'] = $rs['result']['title'];
            $liveinfo['stream_key'] = $rs['result']['stream_key'];
            $liveinfo['embed_url'] = $rs['result']['embed_url'];
            $liveinfo['live_id']  = $rs['result']['id'];
            $liveinfo['video_id']  = $rs['result']['videos']["0"]->id;
            $liveinfo['msg_state']  = 'Y';
            $liveinfo['msg_text']  = 'Success';     
            return $liveinfo;
    }
    
    public function CheckVideo($query_arr = null) {

            $model = new $this->model_class();
            $rs = $model->Livehousein->CheckVideo();
            $liveinfo = array();
            $liveinfo['video_id'] = $rs['result']['video_id'];
            $liveinfo['status'] = $rs['result']['status'];
            $liveinfo['check_time'] = date('Y-m-d H:i:s');
            $liveinfo['status'] = $rs['result']['status'];
            $liveinfo['msg_state']  = 'Y';
            $liveinfo['msg_text']  = 'Success';     
            return $liveinfo;
    }
    
    
    public function BoardEditUP($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardEditUP($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function ReBoardEditUP($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ReBoardEditUP($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }



    public function BoardEndLiveUP($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardEndLiveUP($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    
    public function BoardChangeRead($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardChangeRead($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            

            if($rs['msg_state'] == 'Y'){ //post ok 
                if($query_arr['read_permi'] =='public'){
                    $tmp_rs = $model->sendSubscription($query_arr);
                }
                if($query_arr['read_permi'] =='private'){
                    $tmp_rs = $model->sendFanMember($query_arr);
                }   
            }
            return $rs;
    }
    
    public function BoardDelete($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->BoardDelete($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function ReBoardDelete($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ReBoardDelete($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    
    
    public function ReBoardPost($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ReBoardPost($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function AddLike($query_arr = null) {
             $count_arr[] = $this->laout_check(DI()->request->get('id'));
             $approval = $this->laout_check(DI()->request->get('approval'));
             $own = $this->laout_check(DI()->request->get('own'));

            $model = new $this->model_class();
            $rs = $model->Counter->add_like($count_arr,$own,$approval);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function UnLike($query_arr = null) {

             $count_arr[] = $this->laout_check(DI()->request->get('id'));
             $approval = $this->laout_check(DI()->request->get('approval'));
             $own = $this->laout_check(DI()->request->get('own'));

            $model = new $this->model_class();
            $rs = $model->Counter->add_like($count_arr,$own,$approval);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function AddShare($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->AddShare($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    } 
    
  
}
