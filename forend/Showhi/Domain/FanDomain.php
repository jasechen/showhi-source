<?php
class Domain_FanDomain extends Common_DomainDefaultList  {

    public function __construct() { 
        parent::__construct(); 

    }
    
    public function __destruct(){

    }

    public function FanText($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->FanText($query_arr);
            
            if(!empty($rs)){ //執行已讀設定
                $tmp_rs = $model->read_notice();
                //$tmp_rs['notice_update'] =x  //update 筆數
            }

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function FanMange($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->FanMange($query_arr);
            

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function FanReport($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->FanReport($query_arr);
            

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    
    public function FanMangeMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->FanMangeMember($query_arr);
            


            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    
    public function MyFanList($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->MyFanList($query_arr);
            


            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function AgreeFanMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->AgreeFanMember($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function RefuseFanMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->RefuseFanMember($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function CreateFan($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->CreateFan($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function DeleteFanMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->DeleteFanMember($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function ChangeFanMemberType($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ChangeFanMemberType($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    public function ChangeFanApproval($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ChangeFanApproval($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    
    public function DeleteFan($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->DeleteFan($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function InviteFanMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->InviteFanMember($query_arr);
         
            return $rs;
    }
    
    public function InviteMultiFanMember($query_arr = null) {
        
        //excel csv
        if($_FILES['file_excel']['type'] !='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && 
        $_FILES['file_excel']['type'] !='application/vnd.ms-excel' && 
        $_FILES['file_excel']['type'] !='.csv' ){
            $rs['msg_text'] ='Please select Excel file';
            $rs['msg_state'] ='N';    
            return $rs;
        }
        
            $model = new $this->model_class();
            $rs = $model->InviteMultiFanMember($query_arr);
         
            return $rs;
    }
    
    

    public function AddFanMember($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->AddFanMember($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function AgreeInvFan($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->AgreeInvFan($query_arr);

            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    
    public function ViewList($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->ViewList($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function FanEditUP($query_arr = null) {
            $model = new $this->model_class();
            $rs = $model->FanEditUP($query_arr);
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function PhotoUpload($field_name,$photoFile) {
        $user_id = $_SESSION['f_backend']['user_id'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $savePath ="/fan/";
        $model = new $this->model_class();
        //確定權限
        $fan_id =$fan_id;
        $type ='admin';
        $Permi=$model->FanCheckPermi($user_id,$type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('PhotoUpload');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
        
        
        
        //取得檔案在db的id
        $file_id = $this->FileUpload->fileUpload($_FILES["photoFile"],$field_name,$savePath,$fan_id);
        //寫入資料庫 edit

            $query_arr["$field_name"] = $file_id;
            $query_arr['update_date'] = date('Y-m-d H:i:s');
            
        //寫入資料庫 edit
            //要確定他有改圖的權限

            $rs_sql = $model->Select_Table('fan');
            $rs_sql = $rs_sql->where('id = ?', $fan_id);
            $sql_state = $model->Edit_Update($query_arr,$rs_sql);
 
            //update session
            $model->SetSession();
            
            if($sql_state >=1){
                $rs['update_state'] =$sql_state;
                $rs['msg_text'] ='Success Upload image done.';
                $rs['msg_state'] ='Y';
            }else{
                $rs['update_state'] =$sql_state;
                $rs['msg_text'] ='Success Data no Change.';
                $rs['msg_state'] ='Y';
            }
                return $rs;
    }
    
    public function BgPhotoUpload($field_name,$photoFile,$bg_image_style) {
        $user_id = $_SESSION['f_backend']['user_id'];
        $fan_id = $this->laout_check(DI()->request->get('fan_id'));
        $bg_image_style = $this->laout_check($bg_image_style);
        $savePath = "/fan/";
        $model = new $this->model_class();
        
        //確定權限
        $fan_id =$fan_id;
        $type ='admin';
        $Permi=$model->FanCheckPermi($user_id,$type,$fan_id);
        if($Permi ==0){
            $rs['sql_state'] ='Error Permi';
            $rs['msg_text'] =T('BgPhotoUpload');
            $rs['msg_state'] ='Y';
            $rs['update_time'] =date('Y-m-d H:i:s');
            return $rs;      
        }
        //end 確定權限
        
        //取得檔案在db的id
        $file_id = $this->FileUpload->fileUpload($_FILES["photoFile"],$field_name,$savePath,$fan_id);
        

        //寫入資料庫 edit
            //要確定他有改圖的權限
        
        
            $model = new $this->model_class();
            $query_arr["$field_name"] = $file_id;
            $query_arr["bg_image_style"] = $bg_image_style;
            $query_arr['update_date'] = date('Y-m-d H:i:s');


            $rs_sql = $model->Select_Table('fan');
            $rs_sql = $rs_sql->where('id = ?', $fan_id);
            $sql_state = $model->Edit_Update($query_arr,$rs_sql);
 
            //update session
            $model->SetSession();
        
      
            if($sql_state >=1){
                $rs['update_state'] =$sql_state;
                $rs['msg_text'] ='Success Upload image done.';
                $rs['msg_state'] ='Y';
            }else{
                $rs['update_state'] =$sql_state;
                $rs['msg_text'] ='Success Data no Change.';
                $rs['msg_state'] ='Y';
            }
                return $rs;
    }
    
    
}
