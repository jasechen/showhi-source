<?php
/*
$data = array('choose_count'  => new NotORM_Literal("choose_count + 1"));
update now +1
$tmp_arr['tmp_list'] = DI()->notorm->teach->where('id',$teach_id)->update($data);

*/

class Domain_CourseDomain extends Common_DomainDefaultList  {

    public function __construct() {   
        parent::__construct(); 

    }
    
    public function __destruct(){

    }

    
    public function StudentBookingChoose() {

        $model = new $this->model_class();
        
        //booking選教材前的內部確認
        $rs = $this->BookingRules();
        if($rs['msg_state'] == 'N'){
            return $rs;
        }
        //end booking選教材前的內部確認
        
        
            $rs = $model->StudentBookingChoose();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function StudentBooking() {

        $model = new $this->model_class();
        
        //booking選教材前的內部確認
        $rs = $this->BookingRules();
        if($rs['msg_state'] == 'N'){
            return $rs;
        }
        //end booking選教材前的內部確認
        
        
            $rs = $model->StudentBooking();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }

    public function TeacherText() {

        $model = new $this->model_class();

        
            $rs = $model->TeacherText();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }        
            return $rs;
    }
    
    
    public function ViewList() {

        $model = new $this->model_class();

        
            $rs = $model->ViewList();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }        
            return $rs;
    }
    
    public function TeacherList() {

        $model = new $this->model_class();

        
            $rs = $model->TeacherList();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function StudentLessonCancel() {

        $model = new $this->model_class();

        
            $rs = $model->StudentLessonCancel();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    
    public function TeacherLessonCancel() {

        $model = new $this->model_class();

        
            $rs = $model->TeacherLessonCancel();
            if($rs['msg_text'] ==''){
                $rs['msg_text'] ='Success';
                $rs['msg_state'] ='Y';    
            }            
            return $rs;
    }
    

    public function BookingRules() {
       //內部確認
         $identity=$_SESSION['f_backend']['identity'];
         $user_id=$_SESSION['f_backend']['user_id'];
         $email_va=$_SESSION['f_backend']['email_va'];
         $phone_va=$_SESSION['f_backend']['phone_va'];
         $point=$_SESSION['f_backend']['point'];
         $name=$_SESSION['f_backend']['name'];
         $approval=$_SESSION['f_backend']['approval'];
         $point_date_limit=$_SESSION['f_backend']['point_date_limit'];
       //end get
       
       $tmp_table  = $identity;

        if($email_va !='Y'){ //信箱沒有驗證
            $rs['msg_text'] =T('NoCheckEmail');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
        }     
        /*
        if($phone_va !='Y'){ //電話還沒有驗證
            $rs['msg_text'] =T('NoCheckPhone');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
        } 
        */    

        
        if($point =='0'){ //您的點數使用完畢
            $rs['msg_text'] =T('NoPoint');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
        }
        
        if($name=='' ){//沒有輸入暱稱
            $rs['msg_text'] =T('NoNickName');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
        }   
        
        if($approval !='Y'){ //帳號還沒開放
            $rs['msg_text'] =T('NoApproval');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs;
        }     
       
        $tmp_arr['tmp_list'] = DI()->notorm->$tmp_table->select('*')
        ->where('id = ?', $user_id)->fetchAll();
       if(count($tmp_arr['tmp_list']) != 1){ //帳號比數不等於一筆
            $rs['msg_text'] =T('AccountError');
            $rs['update_time'] =date('Y-m-d H:i:s');
            $rs['msg_state'] ='N';
            return $rs; 
       }
       //end 內部確認
    }
    
    
    
    

}
