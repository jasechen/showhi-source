<?php

/*
update now +1
$tmp_arr['tmp_list'] = DI()->notorm->teach->where('id',$teach_id)->update($data);

//send mail send sms
            $send_mail = $this->Mail->send(
                $query_arr['loginid'], 
            T('AccountVA'), 
            T('AccountVA').$query_arr['email_code']);

            $sendCodeResult = $this->Sms->sendSMS($query_arr['phone'],T('AccountVA').$query_arr['phone_code']);


*/

class Domain_MessageDomain extends Common_DomainDefaultList
{

    public function __construct()
    {
        parent::__construct();

    }

    public function __destruct()
    {

    }

    public function readMessageList()
    {
        $model = new $this->model_class();
        $rs['message_list'] = $model->readMessageList();
        if(!empty($rs['message_list']))
        foreach($rs['message_list'] as $key => $value){
            if($value['room_Noread_number'] >0){
                $rs['Noreadtotal'] =$rs['Noreadtotal'] + $value['room_Noread_number'];
            }
        }

        return $rs;
    }

    public function readMessageByGroupId($group_id)
    {
        $model = new $this->model_class();
        return $model->readMessageByGroupId($group_id);
    }

    public function sendMessage($message_text, $message_file, $groups_id)
    {
        if (empty($message_text) && empty($message_file)) {
            return array();
        }
        $file_id = -1;
        if (!empty($message_file)) {
            $file_id = $this->FileUpload->fileUpload($message_file, 0, '/message/');
        }

        $model = new $this->model_class();
        $rs['message'] = $model->insertMessage($message_text, $file_id, $groups_id);

        if (!empty($message_file) && $rs['message']['id'] != 0) {
            $this->FileUpload->updateFileIndex($file_id, $rs['message']['id']);
        }

        return $rs;
    }

    public function uploadFile($message_file)
    {
        $file_id = 0;
        if (!empty($message_file)) {
            $file_id = $this->FileUpload->fileUpload($message_file, 0, '/message/');
        }
        return $file_id;
    }

    public function createGroup($users_id, $group_name, $group_description)
    {
        $model = new $this->model_class();
        return $model->createGroup($users_id, $group_name, $group_description);
    }

    public function editGroup($group_id, $group_name)
    {
        $model = new $this->model_class();
        return $model->editGroup($group_id, $group_name);
    }

    public function addGroupMember($users_id, $group_id)
    {
        $model = new $this->model_class();
        return $model->addGroupMember($users_id, $group_id);
    }

    public function RemoveGroupMember($users_id, $group_id)
    {
        $model = new $this->model_class();
        return $model->RemoveGroupMember($users_id, $group_id);
    }

    public function getNameEmail($users_id, $group_id)
    {
        $model = new $this->model_class();
        return $model->getNameEmail($users_id, $group_id);
    }



}
