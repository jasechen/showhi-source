<?php

class Common_SignFilter implements PhalApi_Filter {


    public function __construct() {   
    }
    
    public function __destruct(){

    }


    //權限確認 api key等等
    public function check() {

        if (DI()->request->get('__debug__') == '1') {
            return;
        }
        
        //sign 原始版
        /*
        if (DI()->request->get('sign') != 'phalapi') {
            throw new PhalApi_Exception_BadRequest(T('wrong sign'));
        }
        */
        
        //header key

        $tmp_arr = getallheaders();
        if(!isset($tmp_arr['api_key']))
        $tmp_arr['api_key'] =null;


        if($tmp_arr['Host'] !='showhi.club' 
        && $tmp_arr['Host'] !='www.showhi.club'
        && $tmp_arr['Host'] !='showhi.co'
        && $tmp_arr['Host'] !='www.showhi.co'
        && $tmp_arr['Host'] !='showhi.edu'
        && $tmp_arr['Host'] !='www.showhi.edu'
        && $tmp_arr['Host'] !='127.0.0.1'
        && $tmp_arr['Host'] !='test.show.co'
        && $tmp_arr['Host'] !='test.showhi.co'
        ){
            if($tmp_arr['api_key'] != 'TEIJFIDM3569'){
                throw new PhalApi_Exception_BadRequest(T('wrong sign'));
            }
            else{
                if($_REQUEST['user_id'] != null){
                    $user_id=$_REQUEST['user_id'];
                    $tmp_arr = $this->check_user_id($user_id);
                    if($tmp_arr['data']['msg_state'] !='N'){ //判斷權限
                        return;
                    }else{
                        throw new PhalApi_Exception_BadRequest($tmp_arr['data']['msg_text']);
                    }
                }
                
            }
        }elseif($tmp_arr['api_key'] == 'TEIJFIDM3569'){ // 三個網址下的東西
                if($_REQUEST['user_id'] != null){
                    $user_id=$_REQUEST['user_id'];
                    $tmp_arr = $this->check_user_id($user_id);
                    if($tmp_arr['data']['msg_state'] !='N'){ //判斷權限
                        return;
                    }else{
                        throw new PhalApi_Exception_BadRequest($tmp_arr['data']['msg_text']);
                    }
                }
        }

        //end header key 
        //$this->obj_debug(DI());

            //確認是否有方法權限
        if(strtolower(DI()->API_ACTION_NAME) !='login' && strtolower(DI()->API_ACTION_NAME) !='logout' ){
            $this->check_pemi();
        }
            //end 確認是否有方法權限
        if($_SESSION['f_backend']['login'] ==1){
            DI()->IDENTITY =$_SESSION['f_backend']['identity'];
            DI()->USER_ID =$_SESSION['f_backend']['user_id'];
        }


            
        //確定是否登入
        /*
        if(strtolower(DI()->API_ACTION_NAME) !='login' && strtolower(DI()->API_ACTION_NAME) !='logout' ){

            
            if($_SESSION['f_backend']['login'] !=1){
                $_SESSION['f_backend']=null;
                throw new PhalApi_Exception_BadRequest(T('login error sign filter'));
            }
            else{
                DI()->IDENTITY =$_SESSION['f_backend']['identity'];
                DI()->USER_ID =$_SESSION['f_backend']['user_id'];
            }

        }
        else{

        }*/
        
        //end 確定是否登入


    }
    

    public function check_pemi(){
        $identity = $_SESSION['f_backend']['identity'];
        $tmp_action =""
        .""
        .""
        .",MaterialsUP,ChangeTeacher,ChangeStudent";
        $student_tmp_arr = explode(",",$tmp_action);
        
        $tmp_action ="StudentLessonCancel,fileUpload,EditGroup,AddGroupMember,RemoveGroupMember,GetFriendList,GetNameEmail,createGroup,sendMessage,readMessageByGroupId,readMessageList,StudentPointRecord,StudentRateUP,StudentRate,BgPhotoUpload,PhotoUpload,TeacherEdit,TeacherBookingRecord"
        .",TeacherAptRecord,UserEditUP,CanTutorTimeUP,EditPwUP,CanTutorTime,BoardPost,ReBoardPost,BoardChangeRead,BoardDelete,ReBoardEditUP,ReBoardDelete,BoardEditUP"
        .",TeacherLessonCancel,StudentBooking,WaitFriendsList"
        .",ZoomOpen"
        .",ReadSysInviteNotice"
        .",QRLinkAccountDelete,QRLinkUserNoteEditUP,QRLinkList,QRLinkCreate,QRLinkEditUP,QRLinkApproval,QRLinkBlock,QRLinkDeleteUP"
        .",AgreeUP,StudentAptRecord,ReadFriendsNote,TeacherPrivateMsg"
        .",AddSubscription,AgreeFriends,CancelFriends,CancelSubscription,CheckFriendsSubscription"
        .",UpdateLive,BoardEndLiveUP,CreateLive,EventBookingRecord"
        .",AddLike,UnLike,AddShare,WakeupAccountPhone,SendVaEmail"
        .",ChangeFanApproval,SendPhoneCode,AgreeInvFan,FanReport,InviteFanMember,InviteMultiFanMember,CreateFan,FanEditUP,AddFanMember,ChangeFanMemberType,AgreeFanMember,DeleteFanMember,RefuseFanMember,AgreeFanMember,MyFanList,FanMange,FanMangeMember,Agree,tandsinterface,TeacherInRoom,TeacherOpneLessonRoom,ChangeStudent,ChangeTeacher,AddFriends,SetBgHeight,StudentBookingRecord";
        $teacher_tmp_arr = explode(",",$tmp_action); 
        
        $tmp_action ="AddwebSubtion,RateClass,RateClassUP,RateList,Text,EditEmailPwUP,RegisterChoose,Register,RegisterStudentUP,RegisterTeacher,RegisterTeacherUP,logout,forget,preveiw,search"
        .",TeacherList,WakeupAccount,ForgetEmail,ResetPw,FriendsList,SubscriptionList"
        .",login,CheckVideo"
        .",QRLinkCheck"
        .",FanText,faq,RequestHelp,CourseContent,TeacherPwEdit,TeacherText,ViewList,GetSession,BoardList,BoardListMedia,UserList,ForgetPw";
        $nomal_tmp_arr = explode(",",$tmp_action);
        
        if($identity == 'student'){
            if(!in_array(DI()->API_ACTION_NAME,$student_tmp_arr) && !in_array(DI()->API_ACTION_NAME,$nomal_tmp_arr)  ){
                $_SESSION['f_backend']=null;
                throw new PhalApi_Exception_BadRequest('No Permissions1');
                return ;
            }
        }
        elseif($identity == 'teach'){
            if(!in_array(DI()->API_ACTION_NAME,$teacher_tmp_arr) && !in_array(DI()->API_ACTION_NAME,$nomal_tmp_arr)){
                $_SESSION['f_backend']=null;
                throw new PhalApi_Exception_BadRequest('No Permissions2');
                return ;
            }
        }
        else{
            //只是不讓老師學生執行這方法
            //if((in_array(DI()->API_ACTION_NAME,$teacher_tmp_arr) || in_array(DI()->API_ACTION_NAME,$student_tmp_arr)) ){
            //一定有設定這方法才會過
            if((in_array(DI()->API_ACTION_NAME,$teacher_tmp_arr) || in_array(DI()->API_ACTION_NAME,$student_tmp_arr)) || !in_array(DI()->API_ACTION_NAME,$nomal_tmp_arr)  ){
                $_SESSION['f_backend']=null;
                throw new PhalApi_Exception_BadRequest('No Permissions3');
                return ;
            }
            
        }        
      
    }
    
    
    
    
    
    
    function check_user_id($user_id = ''){
            $identity ='student';
                $tmp_arr = DI()->notorm->$identity->select('*')->where('id =?', $user_id)->fetchAll();
            if(count($tmp_arr) !=1){
                $identity ='teach';
                $tmp_arr = DI()->notorm->teach->select('*')->where('id =?', $user_id)->fetchAll();
            }
        //寫入seesion
                if(count($tmp_arr) ==1){
                      $_SESSION['f_backend']['identity']=$identity;
                      $_SESSION['f_backend']['login']=1;
                      $_SESSION['f_backend']['level']='1';   
                      $_SESSION['f_backend']['loginid']=$tmp_arr[0]['loginid'];                
                      $_SESSION['f_backend']['user_id']=$tmp_arr[0]['id']; 
                      $_SESSION['f_backend']['name']=$tmp_arr[0]['nick_name'];  
                      $_SESSION['f_backend']['email_va']=$tmp_arr[0]['email_va'];     
                      $_SESSION['f_backend']['approval']=$tmp_arr[0]['approval'];     
                      if($identity == 'student')
                      $_SESSION['f_backend']['def_page']='user.php?action/student_edit';
                      elseif($identity == 'teach'){
                        $_SESSION['f_backend']['def_page']='user.php?action/'.$identity.'er_edit';
                        $_SESSION['f_backend']['agree']=$tmp_arr[0]['agree'];
                      }
                      /*
                      DI()->IDENTITY =$identity;
                      DI()->USER_ID =$tmp_arr[0]['id'];
                      DI()->LOGINID =$tmp_arr[0]['loginid'];
                      DI()->NAME =$tmp_arr[0]['name'];
                      DI()->EMAIL_VA =$tmp_arr[0]['email_va'];
                      DI()->APPROVAL =$tmp_arr[0]['approval'];
                      if($identity == 'student')
                        DI()->DEF_PAGE='user.php?action/student_edit';
                      elseif($identity == 'teach'){
                        DI()->DEF_PAGE='user.php?action/'.$identity.'er_edit';
                        DI()->AGREE=$tmp_arr[0]['agree'];
                      }
                      */
                      
                      
                    $rs['data']['msg_text'] ='Success';
                    $rs['data']['msg_state'] ='Y';            
                    return $rs;
                }
                else{
                    $rs['data']['msg_text'] ='No User Id';
                    $rs['data']['msg_state'] ='N';            
                    return $rs;
                    
                }
 
        //end 寫入seesion
    } 

    function obj_debug($obj){
        echo "<pre>";
        print_r($obj);
        echo "<pre>";
        exit;
    }
    
 
}
