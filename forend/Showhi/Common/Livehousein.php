<?php

class Common_Livehousein
{

    public function __construct()
    {
        //撰寫create token
        $this->client_id = "32a8c71f02b2d646";
        $this->client_secret ="wt3Yt-sTl4c67stZ";
        $this->api_base_url ="https://app.straas.net/api/v1/";
        $this->api_name ="app/";
        $this->Authorization ='';
        $this->file_sec ='';
        $this->api_url =$this->api_base_url.$this->api_name;

        //$this->CreateLive();
        $this->CommFunc = new Common_CommFunc(); 
    }

    public function __destruct()
    {

    }
    //新建一個影片上傳需求
    public function  CreateToCloud($fileinfo =null){
        $this->CreateToke();
        if($fileinfo == null){
            $fileinfo['type'] = 'video/mp4';
            $fileinfo['name'] ='php test'.date('Y-m-d H:i:s');         
        }
        
		$param = array(
    	"content_type"=>$fileinfo['type'],
    	"video"=>array(
    	"title"=>$fileinfo['name'],
    	"projection"=>'flat'
            )
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost($this->api_url.'videos/ingestions',$param,$api_key);
        $rs = $this->UploadVideo($sendResult['result'],$fileinfo);
        //print_r($rs);
      
        

        //轉換影片
        sleep(2);
        $rs = $this->TranscodeVideoIngestion($sendResult['result']);
        //print_r($rs);
        //把影片設定為開放
		$param = array(
            "available"=>true,
            "listed"=>true  
		  ); 
          
        /* //tag 版
		$param = array(
            "available"=>true,
            "listed"=>true,
            "tags"=>array(
                "0" => array(
                    "name"=>"tag1"
                ),
                "1" => array(
                    "name"=>"tag2"
                )
            )     
		  );
          */
        $rs = $this->UpdateVideoDetails($sendResult['result'],$param);
        $return_info['video_id'] = $sendResult['result']['video_id'];
        $tmp_sec  = round($this->file_sec);
        $return_info['cloud_down_time'] = date('Y-m-d H:i:s',strtotime("now +$tmp_sec seconds"));
        $return_info['cloud_down_sec'] = $tmp_sec ;
        return $return_info;
    }
    
    //轉檔
    public function  TranscodeVideoIngestion($video_info){
        $this->CreateToke();
        /*
		$param = array(
            "title"=>"test john"
		  );
        */
          
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        
        $sendResult = self::_doPut($this->api_url.'videos/'.$video_info['video_id'].'/ingestion/transcode',$param,$api_key);
        return $sendResult['result'];
    }
    
    //修改影片資訊
    public function  UpdateVideoDetails($video_info,$param){
        $this->CreateToke();
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPut($this->api_url.'videos/'.$video_info['video_id'],$param,$api_key);
        return $sendResult['result'];
    }
    
    //把影片上上去
    public function  UploadVideo($video_info,$fileinfo=null){
        $this->CreateToke();
        $content_type =$video_info['content_type'];
        $url = $video_info['upload_url'];
        
        //接fileupload 
        $video = fopen($fileinfo['path'], "rb");
        $video_size = filesize($fileinfo['path']);
        $filename = $fileinfo['path'];
        if (!class_exists('getID3')) {
            require( '../../Library/getID3/getid3/getid3.php');
            $getID3 = new getID3;    
        }   
        $file = $getID3->analyze($filename);
        $this->file_sec =  $file['playtime_seconds'];
        //end fileupload
        

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        $headers = array();
        $headers[] = "Content-Type: $content_type";
        $headers[] = "cache-control: no-cache";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_PUT, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_INFILE, $video);
        curl_setopt($curl, CURLOPT_INFILESIZE, $video_size);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);  
        $result = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl); 
        fclose($video);
        return $result;
    }
    
    
    
    public function  CreateToke(){
        $headers = array(
            "content-type: application/json",
            "Host: app.straas.net",
            "Cookie: "
        );
        $data = array(
        "client_id"=>$this->client_id,
        "client_secret"=>$this->client_secret
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->api_url.'token',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>  json_encode($data),
        CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

            if ($err) {
            //echo "cURL Error #:" . $err;
            //exit;
                $err='1';
            } else {
            $response;
            }
        if($err !=1){    
            $response_arr = (array) json_decode($response);
            $this->Authorization = $response_arr['token'];  
        }else{
            $this->Authorization = 'error'; 
        }

    }
    
    
    //視訊
    public function  CreateLive(){
        $this->CreateToke();
        //撰寫create video
        //get 
             $title = $this->CommFunc->laout_check(DI()->request->get('title'));
             $fan_id = $this->CommFunc->laout_check(DI()->request->get('fan_id'));
             $highest = $this->CommFunc->laout_check(DI()->request->get('highest'));    
 



  
       //end get
       if(($title =='' || $title =='說說你要直播什麼...' || $title =='说说你要直播什么...' )){
           $title ='Hi';
       }
       
       if($highest ==''){
           $highest ='_720p';
       }
       /*
       echo $title;
       echo $fan_name;
       echo $highest;
       exit;
       */
		$param = array(
    	"title"=>$title,
    	"highest_resolution"=>$highest,
        "available"=>true,
        "vod_available"=>true,
        "vod_listed"=>true,
        "listed"=>true  
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost($this->api_url.'lives',$param,$api_key);
        $sendResult1  = $this->CreateChatRoom($sendResult['result']['id']);
        return $sendResult;
    }

    //更新直播
    public function  UpdateLive(){
        $this->CreateToke();
        //撰寫create video
        //get 
            //傳json 陣列進來用get 自動變陣列
            //傳json 所以要傳給別人用json 的話 要在 json_encode 一次
            //把他當傳遞的api 橋 這樣get 只要抓id 和json 就好
            
             $jsondata_arr = DI()->request->get('jsondata');
             $live_id = $this->CommFunc->laout_check(DI()->request->get('live_id'));
             if($jsondata_arr == null ){
                 $jsondata_arr = DI()->get('jsondata');
                 $live_id = $this->CommFunc->laout_check(DI()->get('live_id'));
             }
             //$jsondata_arr = $_REQUEST['jsondata'];
             //$live_id = $_REQUEST['jsondata'];
             //return $jsondata_arr;
             //ajax 本身已經有 json_encode 了
             //$json_data = json_decode($jsondata_arr,true);
       //end get
       if($live_id ==''){
           return;
       }
		$param = $jsondata_arr;
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPut($this->api_url.'lives/'.$live_id,$param,$api_key);
        return $sendResult;
    }

    //迴圈寫入livevieoid
    public function  GetLiveVideoId($live_id){
        $this->CreateToke();
        //撰寫create video
        //get 
        
       //end get
       if($live_id ==''){
           return;
       }
		$param = $jsondata_arr;
        $api_key=$this->Authorization;  
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doGet($this->api_url.'lives/'.$live_id.'?includes=videos',$param,$api_key);
        return $sendResult;
    }


    //建立room
    public function  CreateChatRoom($chatname){
        $this->CreateToke();
        //撰寫create room
        //get 
            //$title = $this->CommFunc->laout_check(DI()->request->get('title'));
       //end get

       /*
       echo $title;
       echo $fan_name;
       echo $highest;
       exit;
       */
		$param = array(
    	"chatroom_name"=>$chatname
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost('https://mgp.straas.net/public-chatroom',$param,$api_key);
        return $sendResult;
    }
    
    //建立room user
    public function  CreateChatMembers($chatname){
        $this->CreateToke();

        //撰寫create video
        //get 
             $id = $_SESSION['f_backend']['user_id'];
             $name = $_SESSION['f_backend']['name'];    
       
		$param = array(
    	"id"=>$id,
    	"name"=>$name
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost($this->api_url.'members',$param,$api_key);
        $sendResult1  = $this->CreateChatRoom($sendResult['result']['id']);
        return $sendResult;
    }
    
    //暱稱改變後 update
    public function  UpdateChatMembers($name){
        $this->CreateToke();

        //撰寫create video
        //get 
            $id = $_SESSION['f_backend']['user_id'];           
		$param = array(
    	"name"=>$name
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPut($this->api_url.'members/'.$id,$param,$api_key);
        return $sendResult;
    }
    
    //抓chat user 資料
    public function  GetChatMembersData(){
        $this->CreateToke();

        //撰寫create video
        //get 
            $id = $_SESSION['f_backend']['user_id'];           
		$param = array(
    	"name"=>$name
		  );
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doGet($this->api_url.'members/'.$id,$param,$api_key);

        return $sendResult;
    }
    
    //抓token
    public function  GetChatMembersToken(){
        $this->CreateToke();

        //撰寫create video
        //get 
        $id = $_SESSION['f_backend']['user_id'];
		$param = null;
        
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost($this->api_url.'members/'.$id.'/token',$param,$api_key);
        return $sendResult;
    }
    
    //重製token
    public function  ReChatMembersToken($chatname){
        $this->CreateToke();

        //撰寫create video
        //get 
             $refresh_token = $this->CommFunc->laout_check(DI()->request->get('refresh_token'));
                    
		$param = array(
    	"refresh_token"=>$refresh_token
		  );
        
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doPost($this->api_url.'members/token',$param,$api_key);

        return $sendResult;
    }   



    public function  CheckVideo($video_id =null){
        $this->CreateToke();
        //撰寫create video
        //get 
        if($video_id ==null)
            $video_id = $this->CommFunc->laout_check(DI()->request->get('video_id'));
       //end get

		$param = null;
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doGet($this->api_url.'videos/'.$video_id.'/ingestion',$param,$api_key);
        return $sendResult;
    }
    
    
    public function  CheckVideoUrl($video_id =null){
        $this->CreateToke();
        //撰寫create video
        //get 
        if($video_id ==null)
            $video_id = $this->CommFunc->laout_check(DI()->request->get('video_id'));
       //end get

		$param = null;
        $api_key=$this->Authorization; 
        $api_key ="Bearer ".$api_key;
        $sendResult = self::_doGet($this->api_url.'videos/'.$video_id,$param,$api_key);
        return $sendResult;
    }

    
    
    static private function _doGet($url,$data,$api_key) 
    {
        $headers = array(
            "content-type: application/json",
            "Authorization: $api_key",
            "Cookie: "
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        $response_arr = (array) json_decode($response);
        return array('result' => $response_arr , 'code' => $err);
    }

    static private function _doPost($url, $data,$api_key) 
    {
        $headers = array(
            "content-type: application/json",
            "Authorization: $api_key",
            "Cookie: "
        );
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => $headers
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response_arr = (array) json_decode($response);
        return array('result' => $response_arr , 'code' => $err);
    }
    
    static private function _doPut($url, $data,$api_key) 
    {

    
        $headers = array(
            "content-type: application/json",
            "Authorization: $api_key",
            "Host: app.straas.net",
            "Cookie: "
        );
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response_arr = (array) json_decode($response);
        return array('result' => $response_arr , 'code' => $err);
      

    }
    
    
    static private function _doDelete($url, $data,$api_key) 
    {
        $headers = array(
            "content-type: application/json",
            "Authorization: $api_key",
            "Cookie: "
        );
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "DELETE",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        $response_arr = (array) json_decode($response);
        return array('result' => $response_arr , 'code' => $err);

    }
 
}






