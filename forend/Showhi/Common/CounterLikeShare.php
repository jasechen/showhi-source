<?php

class Common_CounterLikeShare
{

    public function __construct()
    {
        $this->CommFunc = new Common_CommFunc(); 
    }

    public function __destruct()
    {

    }

    public function get_like_count($count_arr,$own_arr,$own){

            $count_arr = array_unique($count_arr); //去除重複
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];

            $ip = $this->CommFunc->getIP();
            $post_day = date('Y-m-d');
            //exit;
            //return print_r($count_arr);
            
            //非view 要確認是否已經like
            //view 的話 用新增一筆後直接Update count 數
            //利用起始時間去計算 日 周 月 年 每一個圖片 記錄每一日的count 之後只要撈出對應的時間就可以完成日 月 年 count
            
                //update insert 確認
                 if(!empty($count_arr))
                 foreach($count_arr as $key => $value){
                     //echo $value.'<br>';

                    //update
                    //tmp_own = own + files_id  = files_233_arr
                    //board board_media tmp_own = own 
                    if($own =='files'){
                        $tmp_own =$own.'_'.$value;
                    }else{
                        $tmp_own =$own; 
                    }


                    $rs_count = DI()->notorm->like_count
                    ->where("own=? && own_id =? && post_by =? && approval ='Y' ",$own,$value,$user_id)->limit(1)->fetchAll();
                    if(count($rs_count) !=0){
                        
                        if($own =='files'){
                            foreach($own_arr as $key_own => $value_own){ //給files 放到正確board boarddatas arr 用的
                                //if($value_own)
                                if(is_array($value_own['attached_id']))
                                if(in_array($value,$value_own['attached_id'])){
                                    $own_arr[$key_own][$tmp_own.'_like_arr'][]=$rs_count[0];   
                                }
                            }
                        }
                        else{
                        $own_arr[$key][$tmp_own.'_like_arr'][]=$rs_count[0];
                        }
                        
                        //$own_arr[$key][$tmp_own.'_like_arr'][]=$rs_count[0];
                    }

                    $rs_count = DI()->notorm->like_count
                    ->where("own=? && own_id =? && post_by !=? && approval ='Y' ",$own,$value,$user_id)->limit(19)->fetchAll();
                    //確定一天加一次
                    if(count($rs_count) !=0){
                        foreach($rs_count as $key1 => $value1){
                            //if files own_arr跑回圈
                        
                        if($own =='files'){
                            foreach($own_arr as $key_own => $value_own){ //給files 放到正確board boarddatas arr 用的
                                if(is_array($value_own['attached_id']))
                                if(in_array($value,$value_own['attached_id'])){
                                    $own_arr[$key_own][$tmp_own.'_like_arr'][]=$value1;
                                }
                            }
                        }else{
                            $own_arr[$key][$tmp_own.'_like_arr'][]=$value1;
                            }
                            
                            $own_arr[$key][$tmp_own.'_like_arr'][]=$value1;
                        }
                    }

                    $rs_count = DI()->notorm->like_count
                    ->where("own=? && own_id =?  ",$own,$value)->fetchAll();
                    //確定一天加一次
                    if(count($rs_count) >20){
                        $own_arr[$key][$tmp_own.'_like_arr_count']=count($rs_count)-20;
                    }else{
                        $own_arr[$key][$tmp_own.'_like_arr_count']=0;
                    }
                 }

                 return $own_arr;

    }

    public function add_view_count($count_arr,$own){
            //$user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            //print_r($count_arr);

            $user_id = DI()->request->get('user_id');


            $ip = $this->CommFunc->getIP();
            $post_day = date('Y-m-d');
            //exit;
            //return print_r($count_arr);
            
            //非view 要確認是否已經like
            //view 的話 用新增一筆後直接Update count 數
            //利用起始時間去計算 日 周 月 年 每一個圖片 記錄每一日的count 之後只要撈出對應的時間就可以完成日 月 年 count
            
                //update insert 確認
                 if(!empty($count_arr))
                 foreach($count_arr as $key => $value){
                    //update

                    $rs_count = DI()->notorm->view_count
                    ->where("own_id =? && UNIX_TIMESTAMP(post_day) =UNIX_TIMESTAMP(?) && ip = ? ",$value,$post_day,$ip)->fetchAll();
                    $query_arr = null;
                    //確定一天加一次
                    //if(count($rs_count) ==0){
                        
                        //rs 大部分都會為0 因為上面就是抓新增的 id+ip+日期 只能寫一次
                        //$query_arr['total_count'] = new NotORM_Literal("total_count + 1");
                        //$rs = DI()->notorm->view_count
                        //->where("own_id =? && UNIX_TIMESTAMP(post_day) =UNIX_TIMESTAMP(?) && ip = ? ",$value,$post_day,$ip)->update($query_arr);
                        //$query_arr = null;
                        //


                        
                        //insert count log
                        $query_arr['id'] = $this->CommFunc->get_uuid();
                        $query_arr['ip'] = $this->CommFunc->getIP();
                        $query_arr['own_id'] = $value;
                        $query_arr['total_count'] = 1;
                        $query_arr['type'] = 'view';
                        $query_arr['own'] = $own;
                        $query_arr['post_by'] = $user_id;
                        $query_arr['update_by'] = $user_id;
                        $query_arr['post_by_loginid'] = $loginid;
                        $query_arr['update_by_loginid'] = $loginid;
                        $query_arr['post_date'] = date('Y-m-d H:i:s');
                        $query_arr['post_day'] = date('Y-m-d');
                        $query_arr['update_date'] = date('Y-m-d H:i:s');
                        $rs = DI()->notorm->view_count->insert($query_arr);   
                        $query_arr = null; 
                        
                        //update count to board or files
                        $query_arr['view_count'] = new NotORM_Literal("view_count + 1");
                        $rs1 = DI()->notorm->$own->where("id =? ",$value)->update($query_arr);
                        $query_arr = null; 
                        
                    //}
                 }
    }

    public function add_share_count($count_arr,$own){
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            //print_r($count_arr);
            $ip = $this->CommFunc->getIP();
            //exit;
            //return print_r($count_arr);
            
            //非view 要確認是否已經like
            //view 的話 用新增一筆後直接Update count 數
            //利用起始時間去計算 日 周 月 年 每一個圖片 記錄每一日的count 之後只要撈出對應的時間就可以完成日 月 年 count
            
                //update insert 確認
                 if(!empty($count_arr))
                 foreach($count_arr as $key => $value){
                    //update

                    $query_arr['total_count'] = new NotORM_Literal("total_count + 1");
                    $post_day = date('Y-m-d');
                    $rs = DI()->notorm->view_count
                    ->where("own_id =? && UNIX_TIMESTAMP(post_day) =UNIX_TIMESTAMP(?) && ip = ? ",$value,$post_day,$ip)->update($query_arr);
                    $query_arr = null;
                    $query_arr['share_count'] = new NotORM_Literal("share_count + 1");
                    $rs1 = DI()->notorm->$own->where("id =? ",$value)->update($query_arr);
                    $query_arr = null; 
                    if($rs ==0){
                        //insert
                        $query_arr['id'] = $this->CommFunc->get_uuid();
                        $query_arr['ip'] = $this->CommFunc->getIP();
                        $query_arr['own_id'] = $value;
                        $query_arr['total_count'] = 1;
                        $query_arr['type'] = 'share';
                        $query_arr['own'] = $own;
                        $query_arr['post_by'] = $user_id;
                        $query_arr['update_by'] = $user_id;
                        $query_arr['post_by_loginid'] = $loginid;
                        $query_arr['update_by_loginid'] = $loginid;
                        $query_arr['post_date'] = date('Y-m-d H:i:s');
                        $query_arr['post_day'] = date('Y-m-d');
                        $query_arr['update_date'] = date('Y-m-d H:i:s');
                        $rs = DI()->notorm->view_count->insert($query_arr);   
                        $query_arr = null;   
                    }
                 }
    }

    
    public function add_like($count_arr,$own,$approval){
            $user_id = $_SESSION['f_backend']['user_id'];
            $loginid = $_SESSION['f_backend']['loginid'];
            //print_r($count_arr);
            $ip = $this->CommFunc->getIP();
            //$user_id = DI()->request->get('user_id');


            //exit;
            //return print_r($count_arr);
            
            //非view 要確認是否已經like
            //view 的話 用新增一筆後直接Update count 數
            //利用起始時間去計算 日 周 月 年 每一個圖片 記錄每一日的count 之後只要撈出對應的時間就可以完成日 月 年 count
            
                //update insert 確認
                 if(!empty($count_arr))
                 foreach($count_arr as $key => $value){
                    //update
                    /*

                    */
                    
                    $rs_count = DI()->notorm->like_count
                    ->where("own_id =? && post_by =?  ",$value,$user_id)->fetchAll();
                    
                    //要確定有改動或是直接抓原資料的count
                    if(count($rs_count) !=0){
                        //like +1
                        if($approval =='Y' && $rs_count[0]['approval'] =='N'){
                            $query_arr['like_count'] = new NotORM_Literal("like_count + 1");  
                            $rs1 = DI()->notorm->$own->where("id =? ",$value)->update($query_arr);
                            
                            //寫log
                            $query_arr = null;
                            $query_arr['approval'] = $approval;
                            $rs = DI()->notorm->like_count
                            ->where("own_id =? && post_by =? && approval !=? ",$value,$user_id,$approval)->update($query_arr);
                            $query_arr = null;
                        }                   
                        elseif($approval =='N' && $rs_count[0]['approval'] =='Y'){
                            $query_arr['like_count'] = new NotORM_Literal("like_count - 1");                              
                            $rs1 = DI()->notorm->$own->where("id =? ",$value)->update($query_arr);
                            
                            //寫log
                            $query_arr = null;
                            $query_arr['approval'] = $approval;
                            $rs = DI()->notorm->like_count
                            ->where("own_id =? && post_by =? && approval !=? ",$value,$user_id,$approval)->update($query_arr);
                            $query_arr = null;
                        }

                                                                       
                    }
                    
                    if(count($rs_count) ==0){
                        //insert
                        $query_arr['id'] = $this->CommFunc->get_uuid();
                        $query_arr['ip'] = $this->CommFunc->getIP();
                        $query_arr['own_id'] = $value;
                        $query_arr['type'] = 'like';
                        $query_arr['own'] = $own;
                        $query_arr['approval'] = 'Y';
                        $query_arr['post_by'] = $user_id;
                        $query_arr['update_by'] = $user_id;
                        $query_arr['post_by_loginid'] = $loginid;
                        $query_arr['update_by_loginid'] = $loginid;
                        $query_arr['post_date'] = date('Y-m-d H:i:s');
                        $query_arr['update_date'] = date('Y-m-d H:i:s');
                        $rs = DI()->notorm->like_count->insert($query_arr);   
                        $query_arr = null;   

                        $query_arr['like_count'] = new NotORM_Literal("like_count + 1");  
                        $rs1 = DI()->notorm->$own->where("id =? ",$value)->update($query_arr);
                        $query_arr = null;
                    }
                    //取得哪一筆值
                    $rs = DI()->notorm->like_count
                    ->where("own_id =? && post_by =?",$value,$user_id)->fetchAll();
                    $query_arr = null;
                    return $rs;
                 }
                 
    }
    
    //沒用到
    public function unlike($count_arr,$own,$approval){

    }
    
    
    

}


