<?php
class Common_DomainDefaultList  {
    public function __construct() {
        $this->Sms = new SMSTW_APISDK("22B57213DA8CB603319658C0FC7EFC60");  
        $this->Mail = new PHPMailer_Lite(true);   
        $this->FileUpload = new Common_FileUploadUtils(); 
        $this->Notice = new Common_NoticeUtils();
          $this->model_class =MODEL_CLASS_NAME;
    }
    
    public function __destruct(){

    }
    
    
function upload_file_to_fd($first_path,$file_type=null,$old_file_name=null,$w=null,$h=null){

if(empty($_FILES)){return "";}
  foreach ($_FILES["file"]["name"] as $key =>&$value) {
    $check_pass=null; //mine是否通過
    $value1=null; 
    $check_type=null; //確認mine副檔名
    $limit_name=null; //副檔名
    $tmp_time=null; //亂數不重複時間亂碼
    $name=null; //命名變數
    if($file_type !=null){
      $check_type=explode(',',$file_type);
      foreach ($check_type as &$value1) {
        if($value1 == $_FILES["file"]["type"][$key] ){
          $check_pass=1;
        }
      }
    }
    else{
      $check_pass=1;
    }
    /*
          echo "Upload: " . $_FILES["file"]["tmp_name"][$key] . "<br />";
          echo "Type: " . $_FILES["file"]["type"][$key] . "<br />";
          echo "Size: " . ($_FILES["file"]["size"][$key] / 1024) . " Kb<br />";
          echo "Temp file: " . $_FILES["file"]["tmp_name"][$key] . "<br />";
           exit;
    */
    if(!empty($_FILES["file"]["tmp_name"][$key]) && $check_pass =='1'){ //確認檔案存在，並且有
          if($w && $h)resizeTo($_FILES["file"]["tmp_name"][$key],$_FILES["file"]["tmp_name"][$key],$w,$h);
    
          if ($_FILES["file"]["error"][$key] > 0){
            echo "Return Code: " . $_FILES["file"]["error"][$key] . "<br />";
          }
          else
          {

          
            //抓取副檔名
            if(substr($_FILES["file"]["name"][$key],-4,4) =='html'){
              $limit_name=substr($_FILES["file"]["name"][0],-5,5);
            }
            else{
              $limit_name=substr($_FILES["file"]["name"][0],-4,4);
            }
            //end 抓取副檔名
            
            //避開exe 檔
            if($limit_name =='exe'){
              echo "error,don't upload .exe";
            }
            //end 避開exe 檔
            $tmp_time=uniqid(DI()->API_NAME.'_'); 
            $name=$tmp_time.$limit_name;
            if(file_exists($first_path)){

              //確認檔案是否有重複
              while(file_exists($first_path.$name)){
                $tmp_time=uniqid(DI()->API_NAME.'_'); 
                $name=$tmp_time.$limit_name;
              }
              //end  確認檔案是否有重複

              if(@move_uploaded_file($_FILES["file"]["tmp_name"][$key],"$first_path".$name)){

                //刪除update之前的圖片
                if($old_file_name[$key] !=null){
                    @unlink($first_path."$old_file_name[$key]");
                  }
                //end  刪除update之前的圖片
                $x[$key]=$name;
              } 
            }
            else{
              //新增資料夾
              @mkdir($first_path, 0700);
              //end  新增資料夾
            }
                 
           // echo "Stored in: " . "upload/$first_name/" . $name; 
          }
    }
    else{    
      return "";
    }
  } 
  return $x;
}

    function RePlaceFilename($tmpfunc_arr,$replace_arr,$where_filed='id'){

        foreach($tmpfunc_arr as $fun_arr_key =>$fun_arr_value ){
            $tmp_arr['tmp_user_image'] = DI()->notorm->files->select('*')
            ->where('created_by = ?', $fun_arr_value[$where_filed])->fetchAll();
            foreach($tmp_arr['tmp_user_image'] as $key =>$value ){

                foreach($replace_arr as $key1 =>$value1 ){
                    if($fun_arr_value[$value1] == $value['id']){
                        $tmpfunc_arr[$fun_arr_key][$value1] =  $value['name'];
                    }
                }

             
            }
            if($tmpfunc_arr[$fun_arr_key]['image'] ==''){
                $tmpfunc_arr[$fun_arr_key]['image'] ='preset_pic.png';
            }
        
            if($tmpfunc_arr[$fun_arr_key]['bg_image'] ==''){
                $tmpfunc_arr[$fun_arr_key]['bg_image'] ='bammer.jpg';
                $tmpfunc_arr[$fun_arr_key]['bg_image_style'] ='0';
            }
            
        }

        return $tmpfunc_arr; 
    }
    
    
    function RePlaceMutileFilename($tmpfunc_arr,$replace_arr,$where_filed='id'){

        foreach($tmpfunc_arr as $fun_arr_key =>$fun_arr_value ){
            $tmp_arr['tmp_user_image'] = DI()->notorm->files->select('*')
            ->where('created_by = ?', $fun_arr_value[$where_filed])->fetchAll();
            
            foreach($tmp_arr['tmp_user_image'] as $key =>$value ){
                foreach($replace_arr as $key1 =>$value1 ){
                    $tmp_arr = explode(',',$fun_arr_value[$value1]);
                        foreach( $tmp_arr as $tmp_key => $tmp_value){
                            if($tmp_value == $value['id']){
                                //$file_name_arr[] = $value['name'];
                                 $tmp_img_arr[]=  $value['name'];
                            }                            
                        }
                $tmpfunc_arr[$fun_arr_key][$value1] =$tmp_img_arr;

                }

            }
            $tmp_img_arr =null;
        }
        return $tmpfunc_arr; 
    }
    
    
    
    
    function obj_debug($obj){
        echo "<pre>";
        print_r($obj);
        echo "<pre>";
        exit;
    }
    
    function laout_check($str_arr=null){
      $linki = $GLOBALS['linki'];
      if(is_array($str_arr)){
        foreach ($str_arr as $key=>$value){
          $str_arr[$key]=htmlspecialchars($value);
          $str_arr[$key]=stripslashes($value);
        }
      }
      elseif(!is_null($str_arr) ){ 
          $str_arr=htmlspecialchars($str_arr);
          $str_arr=stripslashes($str_arr);  
      }
      else{
        $str_arr=null;
      }
    return $str_arr;
    }
    
}