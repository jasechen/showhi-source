<?php

class Common_NoticeUtils
{

    public function __construct()
    {

    }

    public function __destruct()
    {

    }
    
    public function sendSubscription($receive_user_list,$element_id,$send_id,$text,
                $attached_id,$receive,$send_table,$receive_table,$element,
                 $send_post_id,$send_loginid,$uuid){
                     
                $query_arr['element_id'] = $element_id;
                $query_arr['send_id'] = $send_id;
                $query_arr['text'] = $text;
                $query_arr['attached_id'] = $attached_id;
                $query_arr['receive'] = $receive;
                $query_arr['send_table'] = $send_table;
                $query_arr['receive_table'] = $receive_table;
                $query_arr['element'] = $element;
                $query_arr['send_post_id'] = $send_post_id;
                $query_arr['send_loginid'] = $send_loginid;
                $query_arr['id'] = $uuid;
        
                 if(!empty($receive_user_list))
                 foreach($receive_user_list as $key => $value){
                    $query_arr['receive_user_id'] = $value['user_id'];
                    $query_arr['post_date'] = date('Y-m-d H:i:s');
                    $query_arr['update_date'] = date('Y-m-d H:i:s');
                    $rs = DI()->notorm->notice->select('*')->insert($query_arr);
                    
                 }
    }

    public function SignesendSubscription($receive_user_list,$element_id,$send_id,$text,
                $attached_id,$receive,$send_table,$receive_table,$element,
                 $send_post_id,$send_loginid,$uuid){
                     
                $query_arr['element_id'] = $element_id;
                $query_arr['send_id'] = $send_id;
                $query_arr['text'] = $text;
                $query_arr['attached_id'] = $attached_id;
                $query_arr['receive'] = $receive;
                $query_arr['send_table'] = $send_table;
                $query_arr['receive_table'] = $receive_table;
                $query_arr['element'] = $element;
                $query_arr['send_post_id'] = $send_post_id;
                $query_arr['send_loginid'] = $send_loginid;
                $query_arr['id'] = $uuid;
        
                 if(!empty($receive_user_list))
                 foreach($receive_user_list as $key => $value){
                    $query_arr['receive_user_id'] = $value['user_id'];
                    $query_arr['post_date'] = date('Y-m-d H:i:s');
                    $query_arr['update_date'] = date('Y-m-d H:i:s');
                    $rs = DI()->notorm->notice->select('*')->insert($query_arr);
                    
                 }
    }
    
    //抓取社群通知
    public function notice($receive_user_id){
        /*
        $tmp_arr = DI()->notorm->notice->select('*, count(*) as count')->where(" receive_user_id =? and receive='A' ", 
        $receive_user_id)->group('send_id')->fetchAll();
        return $tmp_arr;
        */
        $tmp_obj = DI()->notorm->notice;
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "select count(*) as count,b.*,a.nick_name from plf_fan as a
                left join  plf_notice AS b on a.id =b.send_id
                WHERE 
                b.receive_user_id =:receive_user_id 
                AND b.`receive`=:receive 
                group by send_id
                ";
        $params = array(':receive_user_id' => $receive_user_id ,':receive' => 'A');
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
        return $tmp_arr;
        
    }
    //抓取社群邀請通知
    public function fan_invite_notice($receive_user_id){
        
        $tmp_obj = DI()->notorm->fan;
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "SELECT a.*,b.id as mang_id
                FROM ".$table_first_name."fan AS a
                LEFT JOIN ".$table_first_name."fan_manage AS b ON a.id =b.fan_id
                WHERE 
                b.user_id =:user_id 
                AND b.`approval`=:approval 
                ";
                
        $params = array(':user_id' => $receive_user_id ,':approval' => 'I');
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
        return $tmp_arr;
    }


    //抓取系統通知
    public function sys_invite_notice($receive_user_id){

        $tmp_obj = DI()->notorm->notice;
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "select b.*,a.nick_name from plf_teach as a
                left join  plf_notice AS b on a.id =b.send_id
                WHERE 
                b.receive_user_id =:receive_user_id 
                AND
                b.send_post_id ='System'    
                AND b.`receive`=:receive 
                ";

        $params = array(':receive_user_id' => $receive_user_id ,':receive' => 'A');
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
        return $tmp_arr;
    }

    public function read_sys_invite_notice($id,$user_id){

        $query_arr['receive'] = 'Y';
        $query_arr['update_date'] = date('Y-m-d H:i:s');
        $tmp_arr['notice_update'] = DI()->notorm->notice->select('*')->where(" id=? and receive_user_id =?  ", 
        $id,$user_id)->update($query_arr);
        return $tmp_arr;
    }


    //抓取被加好友的通知
    public function user_friends_notice($receive_user_id){
        $tmp_obj = DI()->notorm->fan;
        $table_first_name = DI()->TABLE_FIRST_NAME;
        $sql = "SELECT a.*,b.nick_name,b.image
                FROM ".$table_first_name."user_friends AS a
                left join ".$table_first_name."teach as b on a.who_user_id =b.id
                
                WHERE 
                a.approval ='Y' 
                and a.read_note !='Y'
                and a.`type` ='1'
                and a.user_id =:user_id
                ";
        $params = array(':user_id' => $receive_user_id);
        $tmp_arr = $tmp_obj->queryAll($sql,$params);
        return $tmp_arr;
    }
    
    
    //好友確認已讀後刪除
    public function read_friends_note($id,$user_id){

        $query_arr['read_note'] = 'Y';
        $query_arr['update_date'] = date('Y-m-d H:i:s');
        $tmp_arr['notice_update'] = DI()->notorm->user_friends->select('*')->where(" id=? and user_id =?  ", 
        $id,$user_id)->update($query_arr);
        return $tmp_arr;
    }
        
        


    //已讀後刪除通知
    public function read_notice($receive_user_id,$send_id,$receive,$element){
        $query_arr['receive'] = $receive;
        $query_arr['update_date'] = date('Y-m-d H:i:s');
        $tmp_arr['notice_update'] = DI()->notorm->notice->select('*')->where(" receive_user_id =? and receive='A' and send_id =? ", 
        $receive_user_id,$send_id)->update($query_arr);
        return $tmp_arr;
        
        
    }



}


