<?php

class Common_FileUploadUtils
{

    public function __construct()
    {
        $this->Livehousein = new Common_Livehousein();
    }

    public function __destruct()
    {

    }

    public function fileUpload($file, $key_id, $savePath = "/teach/",$fan_id=null)
    {

        $user_id = $_SESSION['f_backend']['user_id'];
        $loginid = $_SESSION['f_backend']['loginid'];
        
        //確認權限
        if($fan_id =='' || $fan_id =='null'){
            $created_by =$user_id;
        }else{
            $created_by =$fan_id;
        }
        //end 
        
        $url_path = $savePath;
        $savePath = UPLOAD . $savePath;


        //判斷資料夾在不在
        if(!is_dir($savePath)) {
            mkdir($savePath,0777,true);
        }

        //拿取使用者上傳的檔案

        //抓取檔案的原始檔案名稱
        $fileOriginalName = $file['name'];
        //將檔案計算MD5，編碼成檔案名稱

        try {
            $fileSaveName = md5_file($file['tmp_name']) . '.' . pathinfo($file['name'])['extension'];
        } catch (Exception $e) {
            $fileSaveName = $fileOriginalName;
        }
        //將檔案儲存在伺服器
        try {
            $saveResult = move_uploaded_file($file['tmp_name'], $savePath . $fileSaveName);
        } catch (\Exception $e) {
            $fileSaveName = $fileOriginalName;
            $saveResult = move_uploaded_file($file['tmp_name'], $savePath . $fileSaveName);
        }

        if ($saveResult != 1) {
            $rs['msg_text'] = $saveResult;
            $rs['msg_state'] = 'N';
            return $rs;
        }
        $mime_type = mime_content_type($savePath . $fileSaveName);
        $fileTypeArr = explode('/', $mime_type);
        $fileType = '3';//other
        $fileTypeName = 'other';//other
        if (count($fileTypeArr) >= 2 && strtolower($fileTypeArr[0]) == 'video') {
            $fileType = '2';
            $fileTypeName = 'video';
        } else if (count($fileTypeArr) >= 2 && strtolower($fileTypeArr[0]) == 'image') {
            $fileType = '1';
            $fileTypeName = 'image';
        }


        //檔案原始名稱 $fileOriginalName
        //檔案在伺服器路徑 $savePath . $fileSaveName
        //檔案類型 $fileType
        //寫入資料庫 edit

        //$model = new $this->model_class();

        $query_arr['updated_at'] = date('Y-m-d H:i:s');
        $query_arr['updated_by'] = $created_by;
        $query_arr['name'] = $fileSaveName;
        $query_arr['key_id'] = $key_id;
        $query_arr['path'] = $url_path . $fileSaveName;
        $query_arr['created_at'] = date('Y-m-d H:i:s');
        $query_arr['type'] = $fileType;
        $query_arr['type_name'] = $fileTypeName;
        $query_arr['created_by'] = $created_by;
        $query_arr['original_name'] = $fileOriginalName;
        $query_arr['user_id'] = $user_id;
        $query_arr['loginid'] = $loginid;

        $rs = DI()->notorm->files->select('*')
            ->where('  original_name = ? && name = ? && type =? && created_by = ? ', $fileOriginalName,$fileSaveName, $fileType, $created_by)->fetchAll();


        if (count($rs) == 0) { //確定這個檔案user沒有上過
            //判斷是影片 --> 傳雲端 -->把雲端id 存入資料表
           if($fileType ==2){
                $fileinfo = array(
                	"type"=>$mime_type,
                	"name"=>$fileOriginalName,
                	"path"=>$savePath . $fileSaveName
                );
                $return_info = $this->Livehousein->CreateToCloud($fileinfo);    
                $query_arr['cloud_id'] = $return_info['video_id'];
                $query_arr['cloud_down_time'] =  $return_info['cloud_down_time'];
                $query_arr['cloud_down_sec'] =  $return_info['cloud_down_sec'];
            }
            
            //$query_arr['cloud_id'] = $video_id; 
            
            $rs = DI()->notorm->files->select('*')
                ->where(' type =? && created_by = ? ', $fileType, $created_by)->insert($query_arr);

            
            if ($rs['id'] != 0) {

                return $rs['id'];
            }
        } else {

            //不用傳雲端因為檔案有重複
            return $rs[0]['id'];
        }

        return -1;
    }

    public function updateFileIndex($file_id,$key_id){

        $query_arr['key_id'] = $key_id;
        $rs = DI()->notorm->files->select('*')
            ->where(' id =? ', $file_id)->update($query_arr);
        if ($rs['id'] != 0) {
            return $rs['id'];
        }
        return -1;

    }

}

if (!function_exists('mime_content_type')) {

    function mime_content_type($filename)
    {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'mp4' => 'video/mp4',
            'qt' => 'video/quicktime',
            'avi' => 'video/avi',
            'mov' => 'video/mov',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        /*
        echo $filename;
        exit;
        */
        $ext = explode('.', $filename);
        $ext = strtolower(array_pop($ext));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        } else {
            return 'application/octet-stream';
        }
    }
}
