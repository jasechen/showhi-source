<?php
class Common_ApiDefaultList extends PhalApi_Api  {

    public function __construct() {
        $seesion_id  = $_REQUEST['sid'];
        if(!empty($seesion_id))
        session_id($seesion_id);

        session_start();
        //取得api相關資訊，判斷權限
        $this->get_api_info();
           //end 取得api相關資訊，判斷權限
    }
    
    public function __destruct(){

    }
    
    
    //取得api相關資訊，判斷權限
    protected function  get_api_info(){
        
        //先抓到 service 然後字串以'.'分開
        DI()->API_NAME = explode('.',DI()->request->get('service'))[0];
        DI()->API_ACTION_NAME = explode('.',DI()->request->get('service'))[1];    
        DI()->TABLE_FIRST_NAME = 'plf_';
        define ("DOMAIN_CLASS_NAME",'Domain_'.DI()->API_NAME.'Domain');
        define ("MODEL_CLASS_NAME",'Model_'.DI()->API_NAME.'Model');
        //end
        
        
        
        /*
        //获取DI
        $di = DI();
        //演示的key
        $key = 'demoKey';
        */
        /** ------------------ 设置 ------------------ **/
        //可赋值的类型：直接赋值、类名赋值、匿名函数
        /*
        $di->set($key, 'Hello DI!');
        $di->set($key, 'Simple');
        $di->set($key, function(){
            return new Simple();
        });
        */
        //设置途径：除了上面的set()，你还可以这样赋值
        /*
        $di->setDemoKey('Hello DI!');
        $di->demoKey = 'Hello DI!';
        $di['demoKey'] = 'Hello DI!';
        */
        /** ------------------ 获取 ------------------ **/
        //你可以这样取值
        /*
        echo $di->get('demoKey'), "\n";
        echo $di->getDemoKey(), "\n";
        echo $di->demoKey, "\n";
        echo $di['demoKey']. "\n";
        */
            
            return ;
    }   
    
    function laout_check($str_arr=null){
      $linki = $GLOBALS['linki'];
      if(is_array($str_arr)){
        foreach ($str_arr as $key=>$value){
          $str_arr[$key]=htmlspecialchars($value);
          $str_arr[$key]=stripslashes($value);
        }
      }
      elseif(!is_null($str_arr) ){ 
          $str_arr=htmlspecialchars($str_arr);
          $str_arr=stripslashes($str_arr);  
      }
      else{
        $str_arr=null;
      }
    return $str_arr;
    }   
        
}