<?php

class Common_Linkinfo
{

    public function __construct()
    {
        $this->CommFunc = new Common_CommFunc();   
        if (!class_exists('simple_html_dom')) { 
            include('../../Library/simplehtmldom/simple_html_dom.php');
        }
    }

    public function __destruct()
    {
        $this->HtmlDom =null;
    }
    //新建一個影片上傳需求
    public function  getLinkinfo($url =null){

                $this->HtmlDom = new simple_html_dom(); 

            //base url
            //$url = 'https://www.youtube.com/watch?v=wU2m-v3Hu3g';
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_REFERER, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);
            // Create a DOM object
            //$html_base = new simple_html_dom();
            // Load HTML from a string
            
            $regex = '@http://|https://|//@';
            /*
            var_dump( preg_match($regex, 'https://hockentest.com') );  //true
            */
            if(!preg_match($regex, $url,$matches)){
                $url ='//'.$url;
            }

            
            $rs['web_url'] =  $url;
            
            
            $this->HtmlDom->load($str);
            foreach($this->HtmlDom->find('title') as $element) 
                $rs['title'] = $element->plaintext;
            foreach($this->HtmlDom->find('meta[name=description]') as $element)  //$descr = $html->find('meta[description]');
                $rs['description'] = $element->content;

            if($rs['title'] =='')
            foreach($this->HtmlDom->find('meta[property=og:title]') as $element)  //$descr = $html->find('meta[description]');
                $rs['title'] = $element->content;
            foreach($this->HtmlDom->find('meta[property=og:image]') as $element)  //$descr = $html->find('meta[description]');
                $rs['image'] =  $element->content;
                
            if($rs['image'] == null)
            foreach($this->HtmlDom->find('img') as $element)  //$descr = $html->find('meta[description]');
                $rs['image'] =  $element->src;
                
            $this->HtmlDom->clear(); 
            unset($this->HtmlDom);

            return $rs; 
        }

    public function  checkurl($text =null){
        
        //注意，这里把上面的正则表达式中的单引号用反斜杠转义了，不然没法放在字符串里
            $regex = '@(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))@';
            /*
            var_dump( preg_match($regex, 'https://hockentest.com') );  //true
            */
            preg_match($regex, $text,$matches);
            return $matches;
        }

}






