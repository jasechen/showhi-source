<?php
    include('../simple_html_dom.php');
    //base url
    $base = 'https://www.youtube.com/watch?v=wU2m-v3Hu3g';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $base);
    curl_setopt($curl, CURLOPT_REFERER, $base);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $str = curl_exec($curl);
    curl_close($curl);

    // Create a DOM object
    $html_base = new simple_html_dom();
    // Load HTML from a string
    $html_base->load($str);

    foreach($html_base->find('title') as $element) 
        $rs['title'] = $element->plaintext;
    foreach($html_base->find('meta[name=description]') as $element)  //$descr = $html->find('meta[description]');
        $rs['description'] = $element->content;

    if($rs['title'] =='')
    foreach($html_base->find('meta[property=og:title]') as $element)  //$descr = $html->find('meta[description]');
        $rs['title'] = $element->content;

    foreach($html_base->find('meta[property=og:image]') as $element)  //$descr = $html->find('meta[description]');
        $rs['image'] =  $element->content;

    $html_base->clear(); 
    unset($html_base);
        return $rs;
        