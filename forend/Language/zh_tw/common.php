<?php
/**
 * 翻译说明：
 *
 * 1、带大括号的保留原来写法，如：{name}，会被系统动态替换
 * 2、没在以下出现的，可以自行追加
 *
 * @author dogstar <chanzonghuang@gmail.com> 2015-02-09
 */

return array(
    /*
    'Hi {name}, welcome to use PhalApi!' => '{name}您好，欢迎使用PhalApi！',
    'wrong sign' => '登入錯誤',
    'user not exists' => '用户不存在',
    
    //msg
    'NoCheckEmail' => '您的信箱還沒有驗證，請前去收信進行驗證',
    'NoCheckPhone' => '您的電話還沒有驗證，請前去收簡訊進行驗證',
    'NoPoint' => '您的點數使用完畢',
    'NoNickName' => '妳還沒有填寫英文暱稱喔',
    'NoApproval' => '您的帳號還沒有開放',
    'AccountError' => '你的帳號有錯誤',
    'SystemError' => '系統錯誤',
    'SystemErrorAgain' => '系統錯誤，請重新再來一次',
    'BefaultBooking' => '請在 30 小時前booking',
    'YouhaveLesson' => '你這個時間已經有課程了',
    'TeacherNoTime' => '老師這個時間沒有打開',
    'LessonFullNumber' => '上課人數已滿',
    'BookingSuccess' => '選課完成了',
    'NotFindLesson' => '找不到這個課程',
    'BefaultStudnetHour' => '請在6小時前退課',
    'CancelSuccess' => '退課完成，請點擊返回',
    'BefaultTeacherHour' => '請在24小時前退課',
    //end msg

//sms email
    'WakeupAccount' => '驗證帳號完成',
    'CodeError' => '驗證碼錯誤',
    'AccountVA' => '帳號驗證碼:',
    'AccountVANote' => '帳號建立完成，記得驗證信箱以及手機完成帳號喚醒',
    'EmailPhoneError' => '信箱或手機有錯',

    'CourseCancellationNoticeSubject' => 'Showhi 課程取消通知',
    'StudentCourseCancellationNotice' => '<div class="m-l1 nameBox">您好，{nick_name}</div>
             您在Showhi 預計教授的課程時間：<a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time)</strong>
             因故無法如期開課，不便之處敬請見諒。
             <br><br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br><br>
             謝謝！
             <br><br>',
    'SmsStudentCourseCancellationNotice' => '您好，{nick_name}\n
             您在Showhi 預計教授的課程時間：{booking_select_time}\n
             因故無法如期開課，不便之處敬請見諒。\n
            Showhi 謝謝！',

    'TeacherCourseCancellationNotice' => '<div class="m-l1 nameBox">您好，{nick_name}</div>
             您在Showhi 預約的課程時間：<a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time)</strong>
             因故無法如期開課，不便之處敬請見諒。 
             <br><br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br><br>
             謝謝！
             <br><br>',
    'SmsTeacherCourseCancellationNotice' => '您好，{nick_name}\n
             您在Showhi 預約的課程時間：{booking_select_time}\n
             因故無法如期開課，不便之處敬請見諒。\n
            Showhi 謝謝！',

            
    'EmailBookingSubject' => 'Showhi 課程安排通知',
    'EmailBooking' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             Your Showhi class has been arranged for <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time). </strong>  
             <br>
             <br>
            The Topic Area is {article_category}.<br>
            You’ll be teaching {article_title}.<br>
            The article is <a target="_blank" href="{article_link}">here</a>.<br>
             <br>

             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsEmailBooking' => '{subject}\n
        Hi {nick_name},\n
             Your Showhi class has been arranged for {booking_select_time}
             \n
             \n
            The Topic Area is {article_category}.\n
            You’ll be teaching {article_title}{article_level}.\n
            \n
             Sincerely,
             \n
             Showhi Team',
*/


    'Hello {name}, Welcome to use PhalApi!' => 'Hello {name}, Welcome to use PhalApi!',
    'wrong sign' => 'wrong sign',
    'user not exists' => 'user not exists',

    // msg
    'NoCheckEmail' => 'Your mailbox have not verified, please go to the reception for verification',
    'NoCheckPhone' => 'Your phone has not yet verified, please go to receive a message to verify',
    'NoPoint' => 'Your points after use',
    'NoNickName' => 'you has not filled out in English  nickname',
    'NoApproval' => 'Your account has not been open',
    'AccountError' => 'Your account has Error',
    'NoAccountError' => 'No Account Error',    
    'SystemError' => 'System Error',
    'SystemErrorAgain' => 'System error, please re-do it again',
    'BefaultBooking' => '請在 30 小時前booking',
    'YouhaveLesson' => 'This time you have a course',
    'TeacherNoTime' => 'The teacher did not open this time',
    'LessonFullNumber' => 'The number of classes is full',
    'BookingSuccess' => 'Courses completed',
    'NotFindLesson' => 'I can not find this course',
    'BefaultStudnetHour' => 'Please return 6 hours before lesson',
    'CancelSuccess' => 'Course withdrawal is complete, click Back',
    'BefaultTeacherHour' => 'Please return 24 hours before lesson',
    'Notify_invitee' => '已接受邀请并且注册成功，成为ShowHi的会员。',
    'Wait_approval' => '等待邀请者开通您的帐号。',
    'QR_approval' => '您的帐号已开通。',
    'Notify_modified_data' => "您的資料還未齊全，請至 <a href='User.TeacherEdit'>修改資料</a> 完善您的資料。",

    //end  msg

//sms email
    'WakeupAccount' => 'Wakeup Account',
    'CodeError' => 'Verification code error',
    'AccountVA' => 'Account Verification Link',
    'AccountVANote' => 'Account is completed, remember to verify the mailbox and the phone to complete the account wake up<br>
        Account:{loginid}<br>
        
        Activate account link:
        <a href="{host_path}User.WakeupAccount/{user_id}/{email_code}">
        {host_path}User.WakeupAccount/{user_id}/{email_code}</a><br>

             Showhi Team',
    'EmailPhoneError' => 'Mailbox or phone is wrong',
    
    'ResetPasswordVA' => 'Reset Password Verification:',



    'CourseCancellationNoticeSubject' => 'Showhi Course Cancellation Notice',

    'StudentCourseCancellationNotice' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             Your course on  <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time) </strong> <br>
             will be cancelled, Sorry for the inconvenience caused.
             <br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br>
             Sincerely,
             <br><br>
             Showhi Team<br>',

    'SmsStudentCourseCancellationNotice' => '{subject}
             Hi {nick_name},
             Your course on  {booking_select_time} (Taiwan Time) 
             will be cancelled, Sorry for the inconvenience caused.
             Sincerely,Showhi Team',

    'TeacherCourseCancellationNotice' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             Your course on  <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time) </strong> <br>
             will be cancelled, Sorry for the inconvenience caused.
             <br>
            <a target="_blank" href="{host_path}/User">Showhi</a>
             <br>
             Sincerely,
             <br><br>
             Showhi Team<br>',

    'SmsTeacherCourseCancellationNotice' => '{subject}
             Hi {nick_name},
             Your course on  {booking_select_time}
             will be cancelled, Sorry for the inconvenience caused.
             Sincerely,Showhi Team',

    'EmailBookingSubject' => 'Showhi Appointment Notification',
    'EmailBooking' => '
        <div class="m-l1 nameBox">Dear {nick_name},</div><br>
        This is the reminder for your Showhi appointment<br>
        <b>Date:</b>{booking_select_time}<br>
        <b>Time:</b>{booking_select_hour} (Taiwan Time)<br>

        Go to <a target="_blank" href="{time_con_link}">The World Clock</a>.<br>

        If you have any problem keeping this appointment,please contact the<br>
        requester or invitee immidiately!<br><br>

        Thank you!<br><br>

        This Notification is sent by automatic system,please do not reply.<br><br>

        --Explore the world with Showhi
             ',
    'SmsEmailBooking' => '{subject}
        Hi {nick_name},
             Your Showhi class has been arranged for {booking_select_time} (Taiwan Time) 
             Sincerely, Showhi Team',

    'EmailToStudentBooking' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
             You have an appointment in courses <a target="_blank" href="{time_con_link}">{booking_select_time}</a> <strong style="color:red;">(Taiwan Time) . </strong><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsEmailToStudentBooking' => '{subject}
        Hi {nick_name},
             You have an appointment in courses {booking_select_time} (Taiwan Time) 
             Sincerely,Showhi Team',
             
    'CronNoticeMinuteSubject' => 'Showhi notice: Your course will begin in five minutes.',
    'CronNoticeMinute' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
                Your course will begin in five minutes.
             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsCronNoticeMinute' => '{subject}
        Hi {nick_name},
        Your course will begin in five minutes.
        Sincerely,Showhi Team',



    'CronNoticeMinuteSubject' => 'Showhi notice: Your course will begin in five minutes.',
    'CronNoticeMinute' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
                Your course will begin in five minutes.
             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsCronNoticeMinute' => '{subject}
        Hi {nick_name},
        Your course will begin in five minutes.
        Sincerely,Showhi Team',

    'CronNoticeHourSubject' => 'Showhi notice: Your course will begin in {hour} hour(s). ',
    'CronNoticeHour' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
                Your course will begin in {hour} hour(s). 
             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsCronNoticeHour' => '{subject}
        Hi {nick_name},
        Your course will begin in {hour} hour(s). 
        Sincerely,Showhi Team',
             
             
    'CronNoticeRateSubject' => 'Showhi notice: Remember to evaluate the course{lesson_dt_id} ',
    'CronNoticeRate' => '<div class="shText-1">{subject}</div>
        <div class="m-l1 nameBox">Hi {nick_name},</div><br>
                <br>
                <a href="{host_path}/Course.Rate/{code}">{host_path}/Course.Rate/{code}</a>
             <br><br>
             Sincerely,
             <br><br>
             Showhi Team<br>
             ',
    'SmsCronNoticeRate' => '{subject}
        Hi {nick_name},
        Your course will begin in {hour} hour(s). 
        {host_path}/Course.Rate/{code}
        Sincerely,Showhi Team',
//end sms email
);
