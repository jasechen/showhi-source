var mysql = require('mysql');
var uuidUtils = require('../utils/uuidUtils');
var jsonUtils = require('../utils/jsonUtils');
var stringUtils = require('../utils/stringUtils');

function init() {
    var connection = mysql.createConnection({
        host : '127.0.0.1',
        port : '3307',
        user : 'root',
        password : '',
        database : 'plf_api'
    });
    return connection;
}

function queryData(returnFunc, sql, parameter) {
    var connection = init();
    parameter = typeof parameter !== 'undefined' ? parameter : null;
    if(parameter !== null){
        for(var key in parameter){
            sql = stringUtils.replaceAll(sql,":"+key,connection.escape(parameter[key]));
        }
    }
    connection.connect();
    connection.query(sql, function(error, rows, fields) {
        console.log('[' + new Date() + '][Query SQL] : ' + sql);
        returnFunc(error, rows, fields);
    });
    connection.end();
}

function insertData(tableName, data, returnFunc) {
    data['id'] = uuidUtils.genGuid();
    var connection = init();
    connection.connect();
    connection.query('INSERT INTO ' + tableName + ' SET ?', data, function(err, result) {
        if(err || result.affectedRows !== 1){
            returnFunc(err,data,result);
        }else{
            queryData(returnFunc, "select * from "+tableName+" where id = :id", {"id":data['id']});
        }
    });
    connection.end();
}

function updateData(tableName, data, returnFunc) {
    var uid = data['uid'];
    delete data['uid'];
    delete data['create_time'];
    data['update_time'] = new Date();
    var connection = init();
    connection.connect();
    connection.query('UPDATE ' + tableName + ' SET ? WHERE uid = \'' + uid + '\'', data, function(err, result) {
        if(err || result.affectedRows !== 1){
            returnFunc(err,data,result);
        }else{
            queryData(returnFunc, "select * from "+tableName+" where uid = :uid", {":uid":uid});
        }
    });
    connection.end();
}

function deleteData(tableName, uid, returnFunc) {
    var connection = init();
    connection.connect();
    connection.query('DELETE from ' + tableName + ' WHERE uid = \'' + uid + '\'', function(err, result) {
        returnFunc(err,result);
    });
    connection.end();
}

module.exports.queryData = queryData;
module.exports.insertData = insertData;
module.exports.updateData = updateData;
module.exports.deleteData = deleteData;