var Parse = require('node-parse-api').Parse;
var APP_ID = "eFMvZzmMmTrD1zuQfVasji2r0ctxS9ZLX8sSH7lQ";
var MASTER_KEY = "gLea8JIDcrceOpRAOJsuv9HMQwpc0mE7Rw8jtjzg";

var parseApp = new Parse(APP_ID, MASTER_KEY);

function sendNotify(receiveUserId,msg){
    var notification = {
        where : {
            "user_id" : receiveUserId
        },
        data : {
            alert : msg
        }
    };

    parseApp.sendPush(notification, function(err, resp) {
        if (err) {
            console.log('[' + new Date() + '][Query SQL] : ' + "send notify to user : " + receiveUserId + ", error message is : " + err);
        } else {
            console.log('[' + new Date() + '][Query SQL] : ' + "send notify to user : " + receiveUserId + ", message is : " + msg);
        }
    });
}

module.exports.sendNotify = sendNotify;