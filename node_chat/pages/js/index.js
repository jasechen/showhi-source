var nickname = null;

$(function() {

    var socket = io.connect('http://localhost:8081/');

    var pathName = document.location.pathname;

    nickname = prompt("請輸入帳號", "");
    while (!nickname) {
        nickname = prompt("請輸入帳號", "");
    }

    var password = prompt("請輸入密碼", "");
    while (!password) {
        password = prompt("請輸入密碼", "");
    }

    var to = null;

    $.ajax({
        url : apiUrl + '/member/login',
        data : {
            'phone_number' : nickname,
            'password' : password
        },
        type : 'POST',
        dataType : 'json',
        error : function(xhr) {
            var alertMsg = alert('登入錯誤，請重新整理頁面重新登入。');
            console.log(xhr);
        },
        success : function(response) {
            console.log(response);
            if (response.success == 'true') {
                nickname = response.data.uid;
                socket.emit('new user', nickname);
                $('#user_name').val(nickname);

                to = prompt("請輸入收件人uid", "");
                while (!to) {
                    to = prompt("請輸入收件人uid", "");
                }

                $('#to').val(to);

                $("#send").click(function(e) {

                    msg = $('#m').val(),

                    socket.emit('private message', nickname, to, msg);

                    var sendObject = new Object();
                    sendObject.sender_uid = nickname;
                    sendObject.receiver_uid = to;
                    sendObject.content_type = 'TEXT';
                    sendObject.content = msg;
                    sendObject.create_time = new Date();

                    // appendMessage(sendObject);
                    $('#m').val('');
                });

                socket.on('sendSid', function(data) {
                    console.log(data);
                    appendMessage(data);
                });

                $.ajax({
                    url : apiUrl + '/log/DialogList/' + nickname + '/' + to,
                    type : 'GET',
                    dataType : 'json',
                    error : function(xhr) {
                        var alertMsg = alert('讀取對話紀錄錯誤，請重新整理頁面重新登入。');
                        console.log(xhr);
                    },
                    success : function(response) {
                        if (response.success == 'true') {
                            for (var msgIndex = 0; msgIndex < response.data.length; msgIndex++) {
                                appendMessage(response.data[msgIndex]);
                            }
                        } else {
                            alert('讀取對話紀錄失敗.');
                        }
                    }
                });

                $('#chatDiv').show();
            } else {
                var alertMsg = alert('帳號或密碼錯誤，請重新整理頁面重新登入。');
            }
        }
    });

});

/**
 * content_type : TEXT , content : TEXT content_type : IMAGE , content : base64
 * data content_type : FILE , content : base64 data
 */
function appendMessage(msgData) {
    var msgFrom = ' left';
    if ((typeof msgData.sender_uid == 'undefined') && (typeof msgData.receiver_uid == 'undefined')) {
        msgFrom = '-system-msg';
    } else if (nickname == msgData.sender_uid) {
        msgFrom = ' right';
    }
    if (msgData.content_type == 'TEXT') {
        $('#messages').append($('<li class="triangle-border' + msgFrom + '">').append(msgData.content));
    } else if (msgData.content_type == 'IMAGE') {
        $('#messages').append(
                $('<li class="triangle-border' + msgFrom + '">').append(
                        '<a href="' + msgData.content + '" target="_blank"><img src="' + msgData.content
                                + '" style="max-width:20%;max-height:20%"/></a>'));
    } else {
        $('#messages')
                .append(
                        $('<li class="triangle-border' + msgFrom + '">')
                                .append(
                                        '<a href="'
                                                + msgData.content
                                                + '" target="_blank"><img src="http://pejuta.com.my/wp-content/uploads/2015/01/Download_File-512.png" style="max-width:5%;max-height:5%"/></a>'));
    }

    $('#messages').append($('<div style="width:100%;text-align: ' + msgFrom + ';"><span>').append(datetimeToString(msgData.create_time)));
    var message = document.getElementById("message_block");
    message.scrollTop = message.scrollHeight;
}

/**
 * file to base64 data
 */
function readImage(input) {
    if (input.files && input.files[0]) {
        var FR = new FileReader();
        FR.onload = function(e) {
            socket.emit('chat file', e.target.result);
            return false;
        };
        FR.readAsDataURL(input.files[0]);
    }
}

/**
 * trans date to string
 */
function datetimeToString(datetime) {
    var options = {
        year : "numeric",
        month : "short",
        day : "numeric",
        hour : "2-digit",
        minute : "2-digit"
    };
    return new Date(datetime).toLocaleTimeString(navigator.language, options);
}