var fs = require('fs'), http = require('https'), socketio = require('socket.io');
var appDB = require('./db/appDB');
var moment = require('moment');
var notifyUtils = require('./utils/notifyUtils');

var server = http.createServer(
	{key  : fs.readFileSync('/etc/ssl/showhico.key').toString(),
	cert : fs.readFileSync('/etc/ssl/8cd9aa7785e26071.crt').toString(),
	ca : fs.readFileSync('/etc/ssl/gd_bundle-g2-g1.crt').toString()}
    ,function (req, res) {
    res.writeHead(200, {
        'Content-type': 'text/html'
    });
    res.end(fs.readFileSync(__dirname + '/pages/index.html'));
}).listen(3001, function () {
    console.log('Listening at: https://localhost:3001');
});

var users = new Array();
var roomusers = new Array();

var io = socketio.listen(server,
	{key  : fs.readFileSync('/etc/ssl/showhico.key').toString(),
	cert : fs.readFileSync('/etc/ssl/8cd9aa7785e26071.crt').toString(),
	ca : fs.readFileSync('/etc/ssl/gd_bundle-g2-g1.crt').toString()}
);

/**
 * 開啟通訊管道
 */
io.sockets.on('connection', function (socket) {

    var socketid = socket.id;
    // console.log("a user connected, socketid:" + socketid);
    // var joinData = new Object();
    // joinData.content_type = 'TEXT';
    // joinData.content = "a user connected, socketid:" + socketid;
    // joinData.create_time = new Date();
    // io.emit('sendSid', joinData);
    socket.on('message', function (msg) {
        if (io.sockets.connected[socketid]) {
            // //指定的 id 廣播消息
        }
    });

    /**
     * 發送消息 param from 從哪個uid param to 發送到哪個uid param msg 發送訊息內容
     */
    socket.on('private message', function (msg, file_id) {
        var tosocketid = [];
        if (typeof socket == 'undefined' || typeof socket.user == 'undefined' || typeof socket.user.id == 'undefined' || socket.user == null || socket.user.id == null) {
            var reSendData = new Object();
            reSendData.msg = msg;
            reSendData.files_id = file_id;
            io.sockets.connected[socketid].emit('relink', reSendData);
            return;
        }
        //傳送個人訊息
        //io.sockets.connected[socketid].emit('myuserdata', users[i]);

        // 傳送訊息給某socketid
        // if(tosocketid == null){
        //users 全部的 skoektlist 名單
        //socket.user 自己的id

        // 判斷此userUid有沒有在已連線清單
        //console.log(users);



        for (var i = 0; i < roomusers.length; i++) {
            for (var j = 0; j < users.length; j++) {
                if (typeof users[j]['room_Noread_number'] == 'undefined')
                    users[j]['room_Noread_number'] = 0;
                if (roomusers[i]['user_id'] == users[j]['id']) // users 數組裡面的對像已經包含了用戶id（data）
                { //確認是否在roomlist 有無在 sooketlist
                    tmp_falg = 0;

                    //在還在房間內的user 不用傳
                    if (roomusers[i]['groups_id'] == users[j]['room']) {
                        console.log('========1');

                        var data = new Object();
                        data.groups_id = roomusers[i]['groups_id'];
                        data.user_id = socket.user.id;
                        data.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');
                        //更新進入時間
                        appDB.queryData(function (error, rows, fields) {
                        }, 'UPDATE  plf_message_members SET updated_at =:updated_at where groups_id = :groups_id and user_id = :user_id', data);


                    } else {
                        console.log('========2');
                    }
                    if (socket.user.id != roomusers[i]['user_id']) {
                        users[j]['room_Noread_number'] = parseInt(users[j]['room_Noread_number']) + 1;
                        users[j]['last_message_time'] = moment().format('YYYY-MM-DD HH:mm:ss');
                        users[j]['Noread_room'] = roomusers[i]['groups_id'];
                        console.log(users[j]['room_Noread_number']);
                        //io.sockets.connected[socketid].emit('myuserdata', users[j]);

                        //io.sockets.connected[users[j]['scId']].emit('myuserdata',users[j]);
                        io.sockets.in(users[j]['scId']).emit('myuserdata', users[j]);
                        //io.emit('myuserdata', users[j]);
                    }

                } else {
                    tmp_falg = 1;
                }

            }//end j
            if (tmp_falg)
                //抓出不在線ＩＤ
                roomusers[i]['online'] = 'off';

        }//end i

        //console.log(roomusers);
        //console.log(socket.room);
        //socket.emit('RoomUserList',socket.room);
        //for (var i = 0; i < users.length; i++) {

        //}
        /*
        for (var i = 0; i < users.length; i++) {
            if (users[i]['user_id'] == data['user_id']) // users 數組裡面的對像已經包含了用戶id（data）
            {
                flag = 1;
                users[i]['user_id'].scId = socketid;
                io.sockets.connected[socketid].emit('myuserdata', users[i]);
                socket.leave(socket.room);
                socket.join(users[i].groups_id);
                socket.room = users[i].groups_id;
                break;
            }
        }
        */


        // notifyUtils.sendNotify(to,'You had a new message.');
        // }

        var addData = new Object();
        addData.created_by = socket.user.id;
        addData.updated_by = socket.user.id;
        addData.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');
        addData.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
        addData.message = msg;
        addData.files_id = file_id;
        addData.groups_id = socket.room;
        // 寫入DB
        appDB.insertData('plf_messages', addData,
            function (err, result, data) {
                appDB.queryData(function (error, rows, fields) {
                    if (typeof rows === 'undefined' || rows.length <= 0) {
                        io.sockets.in(socket.room).emit('sendSid', addData);
                    } else {
                        rows[0].updated_at = moment(rows[0].updated_at).format('YYYY-MM-DD HH:mm:ss');
                        rows[0].created_at = moment(rows[0].created_at).format('YYYY-MM-DD HH:mm:ss');
                        io.sockets.in(socket.room).emit('sendSid', rows[0]);
                    }
                }, 'SELECT plf_messages.*,plf_teach.nick_name,plf_files.original_name,plf_files.path,plf_files.type FROM plf_messages left join plf_teach on plf_messages.created_by = plf_teach.id left join plf_files on plf_messages.files_id = plf_files.id where plf_messages.id = :id', result[0]);

            }



        );

    });

    // 監聽新訊息事件
    socket.on('public message', function (msg) {

        var addData = socket.user;
        // sender_uid:socket.username;
        addData.content_type = 'TEXT';
        addData.content = msg;
        console.log(msg);
        // 寫入DB
        // appDB.insertData('dialog_logs',addData,
        // function(err,result,data){
        io.emit('sendSid', addData);
        // }
        // );

    });

    /**
     * 進入Socket清單 param data 使用者uid
     */
    socket.on('new user', function (data) {
        var flag = 0;

        // // 判斷此userUid有沒有在已連線清單
        for (var i = 0; i < users.length; i++) {
            if (users[i]['user_id'] == data['user_id']) // users 數組裡面的對像已經包含了用戶id（data）
            {
                flag = 1;
                users[i]['user_id'].scId = socketid;
                io.sockets.connected[socketid].emit('myuserdata', users[i]);
                socket.leave(socket.room);
                socket.join(users[i].groups_id);
                socket.room = users[i].groups_id;
                break;
            }
        }
        //          
        // // 如果userUid有在連線清單內，flag = 1，沒有則為0，並且寫入users
        if (flag == 0) {
            if (typeof data === 'undefined' || data === null) {
                io.sockets.connected[socketid].emit('myuserdata', null);
                return;
            }

            appDB.queryData(function (error, rows, fields) {
                if (typeof rows === 'undefined' || rows.length <= 0) {
                    console.log("user " + data.user_id + " not found.");
                    io.sockets.connected[socketid].emit('myuserdata', null);
                    return;
                } else {
                    var nodeUser = rows[0];
                    nodeUser.scId = socketid; // 使用者socketID

                    // }

                    socket.user = nodeUser;
                    io.sockets.connected[socketid].emit('myuserdata', nodeUser);
                    //socket.leave(socket.room);
                    //socket.join(nodeUser.groups_id);
                    //socket.room = nodeUser.groups_id;
                    users.push(nodeUser);
                }
            }, 'SELECT * FROM plf_teach where id = :user_id', data);
        }
    });

    /*
     * 
     * 斷開連接
     * 
     * 需要把當前連接對應的用戶信息從數組裡面刪除。
     * 
     * 因為下次同一用戶再連接，userID不變，可socketID已經變動，原來的數據則失效
     * 
     */

    socket.on('disconnect', function () {

        var scId = socketid;
        var flag = 0;

        for (var i = 0, n = 0; i < users.length; i++) {

            if (users[i]['scId'] == scId) {
                this[n++] = this[i]; // 存在scId對應的用戶信息，則設置flag標示進行數組長度減少1
                flag = 1;
                console.log(' userid: ' + users[i]['user_id'] + ', socketid: ' + socketid + ' disconnected at ' + new Date());
                break;
            }

        }

        if (flag != 0) {
            users.length -= 1;
        }

    });

    socket.on('switchRoom', function (roomID) {


        var data = new Object();
        data.groups_id = roomID;
        data.user_id = socket.user.id;
        data.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');

        appDB.queryData(function (error, rows, fields) {
            if (typeof rows === 'undefined' || rows.length <= 0) {
                console.log("group " + data.groups_id + " not found.");
                io.sockets.connected[socketid].emit('myuserdata', null);
                return;
            } else {
                //socket.user = nodeUser;
                //io.sockets.connected[socketid].emit('myuserdata', nodeUser);
                socket.leave(socket.room);
                socket.join(data.groups_id);
                socket.room = data.groups_id;

                //更新進入時間
                appDB.queryData(function (error, rows, fields) {
                }, 'UPDATE  plf_message_members SET updated_at =:updated_at where groups_id = :groups_id and user_id = :user_id', data);

                //把room 放進來 清空未讀
                for (var i = 0; i < users.length; i++) {
                    if (users[i]['scId'] == socketid) // users 數組裡面的對像已經包含了用戶id（data）
                    {
                        users[i]['room_Noread_number'] = 0;
                        //users[i]['user_id'] == data['user_id']
                        users[i]['room'] = data.groups_id;
                    }
                }


                //users.push(nodeUser);
            }
        }, 'SELECT * FROM plf_message_members where groups_id = :groups_id and user_id = :user_id', data);

    });

    /**
     * 
     * 抓取當前房間的userlist
     * 
     */
    socket.on('RoomUserList', function (roomID) {
        console.log('RoomUserList:------------');
        console.log(socket.room);

        var data = new Object();
        data.groups_id = roomID;

        appDB.queryData(function (error, rows, fields) {
            if (typeof rows === 'undefined' || rows.length <= 0) {
                console.log("group " + data.groups_id + " not found.");
                io.sockets.connected[socketid].emit('myuserdata', null);
                return;
            } else {
                roomusers = new Array();
                for (var i = 0; i < rows.length; i++) {
                    roomusers.push(rows[i]);
                }
            }
        }, 'SELECT * FROM plf_message_members where groups_id = :groups_id ', data);

    });

    /**
     * 
     * 確認影片上傳是否完成
     * 
     */

    /*
    socket.on('CheckVideoDown', function (videoID, board_id) {
        if (typeof socket == 'undefined' || typeof socket.user == 'undefined' || typeof socket.user.id == 'undefined' || socket.user == null || socket.user.id == null) {
            var reSendData = new Object();
            io.sockets.connected[socketid].emit('baserelink', reSendData);
            return;
        }
        console.log('CheckVideoDown');
        console.log(videoID);
        console.log(board_id);
        // 訪問livehousein api
        // 1 訪問自己ＰＨＰ去訪問對方api

        function CheckVideoStatus(videoID) {
            var tmp_status = null;
            var http = require("http");
            console.log('videoid:' + videoID);
            var options = {
                "method": "POST",
                "hostname": "127.0.0.1",
                "port": 80,
                "path": "/showhi/forend/Public/showhi/?service=Board.CheckVideo",
                "headers": {
                    "api_key": "TEIJFIDM3569",
                    "enctype": "application/json",
                    "cache-control": "no-cache"
                }
            };

            var req = http.request(options, function (res) {
                var chunks = [];
                res.on("data", function (chunk) {
                    chunks.push(chunk);
                });

                res.on("end", function () {
                    var body = Buffer.concat(chunks);
                    console.log(body.toString());
                   // rs = JSON.parse(body.toString());
                    if (rs.data[0]['status'] == 'completed') {
                        var data = new Object();
                        data.approval = 'Y';
                        data.board_id = board_id;
                        data.update_date = moment().format('YYYY-MM-DD HH:mm:ss');
                        //開放留言板
                        appDB.queryData(function (error, rows, fields) {
                            var rsData = new Object();
                            rsData.update_date = moment().format('YYYY-MM-DD HH:mm:ss');
                            rsData.board_id = board_id;
                            rsData.video_id = videoID;
                            rsData.msg_state = 'completed';
                            io.sockets.connected[socketid].emit('getCheckVideo', rsData);
                            console.log(board_id);
                            console.log(videoID);
                            console.log('open');
                        }, 'UPDATE  plf_board SET update_date =:update_date ,approval=:approval where id = :board_id ', data);

                    } else if (rs.data[0]['status'] == null) {
                            io.sockets.connected[socketid].emit('getCheckVideo', 'null');
                    } else {
                        var interval = 10000; // 10秒
                        (function schedule() {
                            setTimeout(function do_it() {
                                CheckVideoStatus(videoID);
                                console.log(1);
                                //重複執行
                                //schedule();
                            }, interval);
                        }());
                    }
                });
            });
            req.write(JSON.stringify({ video_id: videoID }));
        }
        CheckVideoStatus(videoID);




        //


        return false;
        // end 訪問livehousein api

    });
    */

});
